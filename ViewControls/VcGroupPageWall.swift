//
//  VcGroupPageWall.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/22/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import FirebaseAnalytics

class VcGroupPageWall: UIViewController {

    
    @IBOutlet weak var groupImg: UIImageView!
    
    @IBOutlet weak var groupName: UILabel!
    
    @IBOutlet weak var groupPost: UILabel!

    @IBOutlet weak var groupFollower: UILabel!
    
    @IBOutlet weak var ContainerView: UIView!
    
    
    var groupId: Int!
    var groupNameStr : String!
    var groupFollowers : Int!
    var privacy :Bool!
    var profileImage :String!
    var groupParticipants = [ModFollowersobj]()
    var participantsId:[Int] = []
    var PagesCount = 1
    var isAdmin:Bool = false
    var editOrCreate :Bool = false
    var createGroupImg : UIImage!
    var create :Bool = false
    var deleted:Bool = false
    var left :Bool = false
    
    var GroupTimeline:VcMasterNewsFeed!
    var groupData = ModGroupsobj ()
    var notifiedGroupId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        groupImg.layer.cornerRadius = groupImg.frame.size.width / 2
        groupImg.clipsToBounds = true
        
        self.navigationItem.title = "List"
        groupFollowers = 0
        if notifiedGroupId == 0 {
            getPaticipants(PagesCount)
        }else {
          
            groupId = notifiedGroupId
            getGroupData(notifiedGroupId)
        }
        // Do any additional setup after loading the view.
        
        GroupTimeline = storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        GroupTimeline.SetupForGroups(String(groupId), Type: "1",parent_: self)
        GroupTimeline.ParentGroup = self
        
        GroupTimeline.willMoveToParentViewController(self)
        view.addSubview(GroupTimeline.view)
        addChildViewController(GroupTimeline)
        GroupTimeline.didMoveToParentViewController(self)
        
        FIRAnalytics.logEventWithName("MYBroadCasts", parameters: nil)
     
//
//        if notifiedGroupId > 0 {
//            
//           
//        }
    }
    
    
    
    override func viewWillDisappear(animated: Bool) {
         super.viewWillDisappear(animated)
        
//        if self.navigationController?.topViewController != self {
//            
//            
//        }
        
      //  if notifiedGroupId == 0 {
        if deleted == false && left == false {
        if (self.isMovingFromParentViewController()){
        
            if create == true {
                
                VcGetGroups.groupsStatic.getGroups(1, FromPagerOrRefresher: true, dontAppend: false)
                
                let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                
                var foundview = false
                for getGroups : UIViewController in allViewController
                {
                    if getGroups .isKindOfClass(VcGetGroups)
                    {
                        
                        let cast = getGroups as! VcGetGroups
                        cast.groups = []
                        cast.PagesCount = 1
                        foundview  = true
                        
                        self.navigationController?.popToViewController(getGroups, animated: true)
                    }
                }
                if (foundview == false)
                {
                    
                    let getGroups = self.storyboard?.instantiateViewControllerWithIdentifier("VcGetGroups") as! VcGetGroups
                    
                    self.navigationController?.pushViewController(getGroups, animated: true)
                }
                
                
                
            }

            }
        }
      //  }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        GroupTimeline.view.frame = ContainerView.frame;
        
    }

    
    
    override func viewWillAppear(animated: Bool) {
        
        
        if notifiedGroupId == 0 {
        if editOrCreate == false {
            
            if  profileImage == "" {
                
                groupImg.contentMode = .Center
            }else{
                
                 groupImg.contentMode = .ScaleAspectFill
            }
            
            groupImg.kf_setImageWithURL(NSURL(string: profileImage ), placeholderImage: UIImage(named:"Default-Group"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
        }else{
            
            if createGroupImg == nil {
                
                groupImg.contentMode = .Center
                
            }else {
                
                groupImg.contentMode = .ScaleAspectFill
                
                groupImg.image = createGroupImg

                
            }
        }
        
            groupName.text = groupNameStr
            groupFollower.text = String(groupFollowers)
            groupPost.text = "0"
            
        }
    }
    
    
    
  
    
    func back(sender: UIBarButtonItem) {
        
        
        let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
        
        var foundview = false
        for groups : UIViewController in allViewController
        {
            if groups .isKindOfClass(VcGetGroups)
            {
                
                //let cast = groups as! VcGetGroups
                
                foundview  = true
                
                self.navigationController?.popToViewController(groups, animated: true)
            }
        }
        if (foundview == false)
        {
            
            let groups = self.storyboard?.instantiateViewControllerWithIdentifier("VcGetGroups") as! VcGetGroups
            self.navigationController?.pushViewController(groups, animated: true)
        }
        
        
        
    }
    
    func switchValueDidChange(sender:UISwitch!){
        
       // print("Switch Value : \(sender.on))")
        
        privateGroup(sender.on) { (success) in
            self.privacy = sender.on
        }
    }
    
    // MARK: - option menu
    @IBAction func optionMenu(sender: AnyObject) {
        
        
        
        
        // 1
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        // 2
        
        
        
        let margin:CGFloat = 10.0
        let rect = CGRect(x: margin, y: margin, width: optionMenu.view.bounds.size.width - margin * 4.0, height: 40)
        let customView = UIView(frame: rect)
        
        
        let switchControl = UISwitch(frame:CGRectMake(optionMenu.view.bounds.size.width * 0.6 , 10, 0, 0));
        switchControl.on = true
        switchControl.setOn(privacy, animated: true);
        switchControl.addTarget(self, action: #selector(VcGroupPageWall.switchValueDidChange(_:)), forControlEvents: .ValueChanged);
        let name = UILabel(frame: CGRect(x: optionMenu.view.bounds.size.width * 0.3 , y: 13, width: 100, height: 24))
        name.text = "Privacy"
        name.font = UIFont(name: "Helvetica Neue", size: 20)
        name.textColor = UIColor(hex: "#b379e7")
        switchControl.onTintColor = UIColor(hex: "#b379e7")
        customView.addSubview(name)
        customView.addSubview(switchControl)
        
        let editGroup = UIAlertAction(title: "Edit List", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let editScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupEdit") as! VcGroupEdit
            
            for (var i = 0 ; i < self.groupParticipants.count ; i += 1) {
                if self.groupParticipants[i].Id == (User?.Id)! {
                    self.participantsId = self.participantsId.filter { $0 != (User?.Id)!}
                    self.groupParticipants.removeAtIndex(i)
                    i -= 1
                    break
                }
            }
            editScreen.getFollowees = self.groupParticipants
            editScreen.getSelectedFollowees = self.participantsId
            editScreen.gName = self.groupNameStr
            editScreen.navTitle = "Edit List"
            editScreen.privateGroup = self.privacy
            editScreen.edit = true
            editScreen.groupId = self.groupId
            
            if self.profileImage != nil {
                editScreen.gImage = self.profileImage
            }
            
            if self.createGroupImg != nil {
                
                editScreen.createGroupImg = self.createGroupImg
            }
            
            self.navigationController?.pushViewController(editScreen, animated: true)
            
        })
        
        
        
        let exitGroup = UIAlertAction(title: "Leave List", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            let alertController = UIAlertController(title: "ARE YOU SURE?", message: "", preferredStyle: .Alert)
            
            
            let leaveAction = UIAlertAction(title: "Leave List", style: .Destructive , handler: {
                (alert: UIAlertAction!) -> Void in
                
                self.left = true
                self.leaveGroup()
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { _ in }
            
            alertController.addAction(leaveAction)
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
           
            
        })
        
        
        let reportGroup = UIAlertAction(title: "Report List", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            
            var foundview = false
            for reportG : UIViewController in allViewController
            {
                if reportG .isKindOfClass(VcReport)
                {
                    
                    let cast = reportG as! VcReport
                    
                    foundview  = true
                    
                    cast.come = .Group
                    
                    self.navigationController?.popToViewController(reportG, animated: true)
                }
            }
            if (foundview == false)
            {
                
                let report = self.storyboard?.instantiateViewControllerWithIdentifier("VcReport") as! VcReport
                
                report.come = .Group
                report.groupId = self.groupId
                self.navigationController?.pushViewController(report, animated: true)
            }
            
            
        })
        
        
        let deleteGroup = UIAlertAction(title: "Delete List", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let alertController = UIAlertController(title: "ARE YOU SURE?", message: "", preferredStyle: .Alert)
            
            
            let deleteAction = UIAlertAction(title: "Delete List", style: .Destructive , handler: {
                (alert: UIAlertAction!) -> Void in
                
                self.deleted = true
                self.deleteGroup()
                
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { _ in }
            
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            
        })
        
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        
            if (isAdmin == true){
                
                // optionMenu.addAction(privacyGroup)
                
               // optionMenu.view.addSubview(customView)
                    optionMenu.message = nil
                
                optionMenu.addAction(editGroup)
                
                optionMenu.addAction(deleteGroup)
                //optionMenu.message = "\n\n"
              
                
            }else {
                optionMenu.message = nil
                optionMenu.addAction(reportGroup)
                optionMenu.addAction(exitGroup)
            }
            
        
        
       
        
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getPaticipants(index:Int)
    {
        
        let RequestParameters : NSDictionary = [
            "PageIndex":index,
            "PageSize":100,
            "ProfileUserId": self.groupId,
            "UserId": (User?.Id)!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetParticipantsByGroupId, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                
                
                
                if(response.result.isFailure)
                {
                    
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            
                            
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                        }
                        else
                        {
                            
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        
                        let Response = ModListOfPeoples(json:JSON)
                        
                        if (Response.ResultResponse == "21"){
                            
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                        }
                        else
                        {
                            
                            
                            self.groupParticipants.appendContentsOf(Response.Followersobj!)
                            print("Success")
                            
                            if Response.ListCount == 50 {
                                
                                self.getPaticipants(self.PagesCount + 1)
                                
                            }
                            
                            self.groupFollowers = self.groupParticipants.count
                            self.groupFollower.text = String(self.groupParticipants.count)
                            
                            for i in 0  ..< self.groupParticipants.count  {
                                
                                self.participantsId.append(self.groupParticipants[i].Id as! Int)
                                
                            }
                            
                            
                            for admin in self.groupParticipants {
                                
                                if (admin.Id == User?.Id) && (admin.IsAdmin == true){
                                    
                                    self.isAdmin = true
                                    
                                    break
                                }
                            }
                            
                        }
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
    }
    
    func getGroupData(id:Int)
    {
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
        let RequestParameters : NSDictionary = [
            "GroupId": id,
            "UserId": (User?.Id)!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetGroupByGroupId, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                
                if(response.result.isFailure)
                {
                    
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            
                            
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                        }
                        else
                        {
                            
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        
                        let Response = ModGetGroupResponse(json:JSON)
                        
                        if (Response.ResultResponse == "21"){
                            
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                        }
                        else
                        {
                            
                            
                            self.groupData = Response.Group!
                            print("Success")
                         
                            self.groupImg.kf_setImageWithURL(NSURL(string:  self.groupData.ProfilePicture! ), placeholderImage: UIImage(named:"Default-Group"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
                            self.groupName.text = self.groupData.Name
                            self.isAdmin = self.groupData.IsAdmin
                            self.privacy = self.groupData.Privacy
                            
                            if  self.groupData.ProfilePicture! == "" {
                                
                                self.groupImg.contentMode = .Center
                            }else{
                                
                                self.groupImg.contentMode = .ScaleAspectFill
                            }

                        }
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
    }

    
    
    func deleteGroup()
    {
        
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Deleting..."
        
        
        
        let RequestParameters : NSDictionary = [
            
            "GroupId": groupId,
            "GroupIdTypeId": groupId,
            "UserId": (User?.Id)!
        ]
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.DeleteGroup, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    
                    if( resultResponse == "0"){
                        
                        if self.notifiedGroupId == 0 {

                        VcGetGroups.groupsStatic.getGroups(1, FromPagerOrRefresher: true, dontAppend: false)
                        
                        let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                        
                        var foundview = false
                        for getGroups : UIViewController in allViewController
                        {
                            if getGroups .isKindOfClass(VcGetGroups)
                            {
                                
                                let cast = getGroups as! VcGetGroups
                                cast.groups = []
                                cast.PagesCount = 1
                                foundview  = true
                                
                                self.navigationController?.popToViewController(getGroups, animated: true)
                            }
                        }
                        if (foundview == false)
                        {
                            
                            let getGroups = self.storyboard?.instantiateViewControllerWithIdentifier("VcGetGroups") as! VcGetGroups
                            
                            self.navigationController?.pushViewController(getGroups, animated: true)
                        }
                        
                        }else{
                            
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                        }
                        
                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
        })
        
    }

    
    func leaveGroup()
    {
        let partArray = [(User?.Id)!]
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
        
        
        let RequestParameters : NSDictionary = [
            
            
            "IsAdmin":isAdmin,
            "ParticipantId":partArray,
            "GroupId": groupId,
            "UserId": (User?.Id)!
        ]
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.RemoveParticipantsFromGroup, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    
                    if( resultResponse == "0"){
                        if self.notifiedGroupId == 0 {
                        
                        VcGetGroups.groupsStatic.getGroups(1, FromPagerOrRefresher: true, dontAppend: false)
                        
                        let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                        
                        var foundview = false
                        for getGroups : UIViewController in allViewController
                        {
                            if getGroups .isKindOfClass(VcGetGroups)
                            {
                                
                                let cast = getGroups as! VcGetGroups
                                cast.groups = []
                                cast.PagesCount = 1
                                foundview  = true
                                
                                self.navigationController?.popToViewController(getGroups, animated: true)
                            }
                        }
                        if (foundview == false)
                        {
                            
                            let getGroups = self.storyboard?.instantiateViewControllerWithIdentifier("VcGetGroups") as! VcGetGroups
                           
                            self.navigationController?.pushViewController(getGroups, animated: true)
                        }

                        }else{
                            
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
        })
        
    }

    func privateGroup(privGroup:Bool ,completion:((success:Bool)->())?)
    {
        
        
        let RequestParameters : NSDictionary = [
            
            "GroupId": groupId,
            "Info":"String content",
            "Name":"String content",
            "Privacy": privGroup,
            "UserId": (User?.Id)!
        ]
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.UpdateGroupPrivacy, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                var succ=false
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    
                    if( resultResponse == "0"){
                        
                        succ = true
                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
                
                if completion != nil
                {
                    completion!(success: succ)
                }

                
        })
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
