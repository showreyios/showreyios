//
//  TvFashionPost.swift
//  ShowRey
//
//  Created by User on 11/1/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout
import Kingfisher
import AVKit
import AVFoundation
import SSImageBrowser
import MBProgressHUD
import SwiftyJSON
import FirebaseAnalytics

class TvFashionPost: UITableViewController, UICollectionViewDelegate , UICollectionViewDataSource {
    
    @IBOutlet weak var profileName: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var lblDateTime: UILabel!
    
    @IBOutlet weak var lblWeight: UILabel!
    
    @IBOutlet weak var lblHeight: UILabel!
    
    @IBOutlet weak var lblBodyType: UILabel!
    
    @IBOutlet weak var ImgBodyShape: UIImageView!
    
    @IBOutlet weak var lblSize: UILabel!
    
    @IBOutlet weak var lblPostText: UILabel!
    
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    
    @IBOutlet weak var btnComment: UIButton!
    
    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var btnDislike: UIButton! 
    
    @IBOutlet weak var btnShare: UIButton!
    
     // MARK: - items cells outlets
    
    @IBOutlet weak var cellDress: UITableViewCell!
    
    @IBOutlet weak var cellTop: UITableViewCell!
    
    @IBOutlet weak var cellBottom: UITableViewCell!
    
    @IBOutlet weak var cellAccess: UITableViewCell!
    @IBOutlet weak var cellPurse: UITableViewCell!
    @IBOutlet weak var cellShoes: UITableViewCell!
    
    @IBOutlet weak var cellOther: UITableViewCell!
    
    @IBOutlet weak var cellComments: UITableViewCell!
    
     // MARK: - like and dislike items outlets
    
    @IBOutlet weak var likeDress: UIButton!
    
    @IBOutlet weak var dislikeDress: UIButton!
    
    @IBOutlet weak var likeTop: UIButton!
    
    @IBOutlet weak var dislikeTop: UIButton!
    
    @IBOutlet weak var likeBottom: UIButton!
    
    @IBOutlet weak var dislikeBottom: UIButton!
    
    @IBOutlet weak var likeAccess: UIButton!
    
    @IBOutlet weak var dislikeAccess: UIButton!
    
    @IBOutlet weak var likePurse: UIButton!
    
    @IBOutlet weak var dislikePurse: UIButton!
    
    @IBOutlet weak var likeShoes: UIButton!
    
    @IBOutlet weak var dislikeShoes: UIButton!
    
    @IBOutlet weak var likeOther: UIButton!
    
    @IBOutlet weak var dislikeOther: UIButton!
    
    @IBOutlet weak var lblOtherTxt: UILabel!
    
    
    // MARK: - like and dislike items string outlets
    
    @IBOutlet weak var likeDressStr: UIButton!
    
    @IBOutlet weak var dislikeDressStr: UIButton!
    
    @IBOutlet weak var likeTopStr: UIButton!
    
    @IBOutlet weak var dislikeTopStr: UIButton!
    
    @IBOutlet weak var likeBottomStr: UIButton!
    
    @IBOutlet weak var dislikeBottomStr: UIButton!
    
    @IBOutlet weak var likeAccessStr: UIButton!
    
    @IBOutlet weak var dislikeAccessStr: UIButton!
    
    @IBOutlet weak var likePurseStr: UIButton!
    
    @IBOutlet weak var dislikePurseStr: UIButton!
    
    @IBOutlet weak var likeShoesStr: UIButton!
    
    @IBOutlet weak var dislikeShoesStr: UIButton!
    
    @IBOutlet weak var likeOtherStr: UIButton!
    
    @IBOutlet weak var dislikeOtherStr: UIButton!
    

  
     var images:[UIImage] = []
     var imagesUrl:[NSURL] = []
     var mainFeed = ModFeedFeedobj()
     var Parent:UIViewController!
     var avPlayer: AVPlayerViewController = AVPlayerViewController()
     var willExit:( () -> () )?
     var items : [ModFeedCItems] = []
     // MARK: - booleans
    
     var dressLike : Bool = false
     var dressDislike :Bool = false
    
    var topLike : Bool = false
    var topDislike :Bool = false

    var bottomLike : Bool = false
    var bottomDislike :Bool = false

    var accessLike : Bool = false
    var accessDislike :Bool = false

    var purseLike : Bool = false
    var purseDislike :Bool = false

    var shoesLike : Bool = false
    var shoesDislike :Bool = false

    var otherLike : Bool = false
    var otherDislike :Bool = false
    
    
    var DressFlag : Bool = false;
    var TopFlag : Bool = false;
    var AccessoriesFlag : Bool = false;
    var PurseFlag : Bool = false;
    var ShoesFlag : Bool = false;
    var BottomFlag : Bool = false;
    var OtherFlag : Bool = false;
    var otherStr : String!
    
    
    
    var dressObj:ModFeedCItems!
    var topObj:ModFeedCItems!
    var bottomObj:ModFeedCItems!
    var accessObj:ModFeedCItems!
    var purseObj:ModFeedCItems!
    var shoesObj:ModFeedCItems!
    var otherObj:ModFeedCItems!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        
        
        
        mediaCollectionView.registerClass(CcImageGallery.self, forCellWithReuseIdentifier: "CcImageGallery")
        mediaCollectionView.registerNib(UINib(nibName: "CcImageGallery",bundle: nil), forCellWithReuseIdentifier: "CcImageGallery")
        
        mediaCollectionView.reloadData()
        
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
        self.profileImage.clipsToBounds = true;
        
        
        profileImage.userInteractionEnabled = true
        //now you need a tap gesture recognizer
        //note that target and action point to what happens when the action is recognized.
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(TvCreatePost.profileImage(_:)))
        //Add the recognizer to your view.
        profileImage.addGestureRecognizer(tapRecognizer)
        
        self.navigationItem.title = "Fashion Post"
        
        
        let layout = UPCarouselFlowLayout()
        layout.itemSize.width = 250
        layout.itemSize.height = 300
        
        
        // self.currentPage = 0
        
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        mediaCollectionView.collectionViewLayout = layout
        
        
        self.addChildViewController(avPlayer)
        self.view.addSubview(avPlayer.view)
        avPlayer.view.frame =  CGRect(x: 20 , y: 120 , width: self.view.frame.size.width - 40 , height: 54 * 5)
        avPlayer.view.hidden = true

        setData()
        FIRAnalytics.logEventWithName("ViewPost", parameters: nil)
        
        
        
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        
        if let willExit = willExit {
            
            willExit()
        }
    
    
    }
     var tablereloaded = false
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        lblPostText.text = (mainFeed.Text)!
        lblPostText.sizeToFit()
        lblPostText.adjustsFontSizeToFitWidth = true
        if !tablereloaded
        {
            tablereloaded = true
            tableView.reloadData()
        }
                    
    }
    
    // MARK: - Set Data from model

    func setData() {
        
        profileName.setTitle(mainFeed.UserProfile?.FullName, forState: .Normal)
        profileImage.kf_setImageWithURL(NSURL(string: (mainFeed.UserProfile?.ProfilePicture!)!), placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        
        lblDateTime.text = mainFeed.Time
        
        btnComment.setTitle("\(mainFeed.NoOfComments!) Comment", forState: .Normal)
        btnLike.setTitle("\(mainFeed.NoOfLikes!) Yes", forState: .Normal)
        btnDislike.setTitle("\(mainFeed.NoOfDislikes!) No", forState: .Normal)
        
        
        if (mainFeed.ProfilePros != nil){
        lblWeight.text = "Weight: \((mainFeed.ProfilePros?.Weight)!.stringValue) kg"
        lblHeight.text = "Height: \((mainFeed.ProfilePros?.Height)!.stringValue) cm"
        
        
        if (mainFeed.ProfilePros?.BodyShape)! == 1 {
            //lblBodyType.text = "Body Type: Triangle"
            ImgBodyShape.image = UIImage(named: "4")
            
        }else if (mainFeed.ProfilePros?.BodyShape)! == 2 {
           // lblBodyType.text = "Body Type: Inverted Triangle"
            ImgBodyShape.image = UIImage(named: "5")
        }else if (mainFeed.ProfilePros?.BodyShape)! == 3 {
            //lblBodyType.text = "Body Type: Rectangle"
            ImgBodyShape.image = UIImage(named: "6")
        }else if (mainFeed.ProfilePros?.BodyShape)! == 4 {
           // lblBodyType.text = "Body Type: Hourglass"
            ImgBodyShape.image = UIImage(named: "3")
        }else if (mainFeed.ProfilePros?.BodyShape)! == 5 {
            //lblBodyType.text = "Body Type: Diamond"
            ImgBodyShape.image = UIImage(named: "1")
        }else if (mainFeed.ProfilePros?.BodyShape)! == 6 {
           // lblBodyType.text = "Body Type: Rounded"
            ImgBodyShape.image = UIImage(named: "2")
        }
        
        lblSize.text = "Size: \((mainFeed.ProfilePros?.Size)!)"
        
        
        }
        
        if let items = mainFeed.CItems {
        
        for var i in items {
            
            print("helloooooo: \(i.It_Name)")
            if i.It_Name == "Dress" {
                
                dressObj = i
                
                DressFlag = true
                
                likeDressStr.setTitle(i.NoOfLikes?.stringValue, forState: .Normal)
                dislikeDressStr.setTitle(i.NoOfDislikes?.stringValue, forState: .Normal)
                
                if i.Disliked == true {
                    
                    dressDislike = true
                    dislikeDress.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    dislikeDress.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    dressDislike = false
                    dislikeDress.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                    dislikeDress.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                if i.Liked == true {
                    
                    dressLike = true
                    likeDress.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    likeDress.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    dressLike = false
                    likeDress.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                    likeDress.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                
            }else if i.It_Name == "Top" {
                
                topObj = i
                
                TopFlag = true
                likeTopStr.setTitle(i.NoOfLikes?.stringValue, forState: .Normal)
                dislikeTopStr.setTitle(i.NoOfDislikes?.stringValue, forState: .Normal)
                
                if i.Disliked == true {
                    
                    topDislike = true
                    dislikeTop.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    dislikeTop.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    topDislike = false
                    dislikeTop.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                    dislikeTop.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                if i.Liked == true {
                    
                    topLike = true
                    likeTop.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    likeTop.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    topLike = false
                    likeTop.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                    likeTop.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                
            }
            else if i.It_Name == "Bottom" {
                
                bottomObj = i
                BottomFlag = true
                likeBottomStr.setTitle(i.NoOfLikes?.stringValue, forState: .Normal)
                dislikeBottomStr.setTitle(i.NoOfDislikes?.stringValue, forState: .Normal)
                
                if i.Disliked == true {
                    
                    bottomDislike = true
                    dislikeBottom.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    dislikeBottom.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    bottomDislike = false
                    dislikeBottom.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                    dislikeBottom.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                if i.Liked == true {
                    
                    bottomLike = true
                    likeBottom.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    likeBottom.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    bottomLike = false
                    likeBottom.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                    likeBottom.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                
                
            }
            else if i.It_Name == "Accessories" {
                
                accessObj = i
                
                AccessoriesFlag = true
                likeAccessStr.setTitle(i.NoOfLikes?.stringValue, forState: .Normal)
                dislikeAccessStr.setTitle(i.NoOfDislikes?.stringValue, forState: .Normal)
                
                if i.Disliked == true {
                    
                    accessDislike = true
                    dislikeAccess.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    dislikeAccess.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    accessDislike = false
                    dislikeAccess.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                    dislikeAccess.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                if i.Liked == true {
                    
                    accessLike = true
                    likeAccess.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    likeAccess.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    accessLike = false
                    likeAccess.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                    likeAccess.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                
                
            }
            else if i.It_Name == "Purse" {
                
                purseObj = i
                
                PurseFlag = true
                likePurseStr.setTitle(i.NoOfLikes?.stringValue, forState: .Normal)
                dislikePurseStr.setTitle(i.NoOfDislikes?.stringValue, forState: .Normal)
                
                if i.Disliked == true {
                    
                    purseDislike = true
                    dislikePurse.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    dislikePurse.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    purseDislike = false
                    dislikePurse.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                    dislikePurse.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                if i.Liked == true {
                    
                    purseLike = true
                    likePurse.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    likePurse.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    purseLike = false
                    likePurse.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                    likePurse.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                
                
            }
            else if i.It_Name == "Shoes" {
                
                shoesObj = i
                
                ShoesFlag = true
                likeShoesStr.setTitle(i.NoOfLikes?.stringValue, forState: .Normal)
                dislikeShoesStr.setTitle(i.NoOfDislikes?.stringValue, forState: .Normal)
                
                if i.Disliked == true {
                    
                    shoesDislike = true
                    dislikeShoes.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    dislikeShoes.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    shoesDislike = false
                    dislikeShoes.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                    dislikeShoes.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                if i.Liked == true {
                    
                    shoesLike = true
                    likeShoes.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    likeShoes.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    shoesLike = false
                    likeShoes.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                    likeShoes.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                
            }
            else {
                
                otherObj = i
                
                OtherFlag = true
                otherStr = i.It_Name
                likeOtherStr.setTitle(i.NoOfLikes?.stringValue, forState: .Normal)
                dislikeOtherStr.setTitle(i.NoOfDislikes?.stringValue, forState: .Normal)
                
                if i.Disliked == true {
                    
                    otherDislike = true
                    dislikeOther.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    dislikeOther.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    otherDislike = false
                    dislikeOther.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                    dislikeOther.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                
                if i.Liked == true {
                    
                    otherLike = true
                    likeOther.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    likeOther.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    
                }else {
                    
                    otherLike = false
                    likeOther.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                    likeOther.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                }
                               
                
            }
        }// for loop
        }// end if let
        
        
        if(mainFeed.AttachedFiles != nil)
        {
            var attachedFiles = mainFeed.AttachedFiles!
            
            if mainFeed.PostCType == PostContentType.Text_Images.rawValue
            {
//                if(mainFeed.AttachedFiles != nil)
//                {
                
                    
                    for i in 0  ..< mainFeed.AttachedFiles!.count  {
                        
                        let file: ModFeedAttachedFiles = attachedFiles[i]
                        
                        let imageURL = NSURL(string:  AppDelegate.ResorceDomain + file.URL!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
                        imagesUrl.append(imageURL!)
                        
                        
                        //                    KingfisherManager.sharedManager.retrieveImageWithURL(imageURL!, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
                        //                        print(image)
                        //                        self.images.append(image!)
                        //                    })
                        
                        
                  //  }
                    
                }
            }else if mainFeed.PostCType == PostContentType.Text_Videos.rawValue
            {
                
//                if mainFeed.AttachedFiles != nil
//                {
                    avPlayer.view.hidden = false
                    let file: ModFeedAttachedFiles = attachedFiles[0]
                    let URL = NSURL(string: file.URL!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
                    
                    let player = AVPlayer(URL: URL! as  NSURL)
                    avPlayer.player = player
                    
              //  }
            }
        }else {
            imagesUrl = []
            avPlayer.player = nil
        }
  
        var All : VcComments = VcComments()
        let storyboard = AppDelegate.storyboard
        
       let CommentsOnlyContainer = storyboard.instantiateViewControllerWithIdentifier("VcCommentsOnlyContainer") as! VcCommentsOnlyContainer
        
        
        
        CommentsOnlyContainer.ObjId = (mainFeed.Id?.integerValue)!
        CommentsOnlyContainer.IsItSpecialOffer = mainFeed.CommentPrivacy
        CommentsOnlyContainer.isItPrivate = false
        
        
        
        CommentsOnlyContainer.PostType = (mainFeed.PostType?.integerValue)!
        CommentsOnlyContainer.ScreenMod = 0

        
        
//        
//        All = storyboard.instantiateViewControllerWithIdentifier("VcComments") as! VcComments
//        All.ObjId = (mainFeed.Id?.integerValue)!
//        All.IsItSpecialOffer = mainFeed.CommentPrivacy
//        All.isItPrivate = false
//        
//      
//     
//        All.PostType = (mainFeed.PostType?.integerValue)!
//        All.ScreenMod = 0
//       // All.parentView = self.view
    
        
        
        let commentsController = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .All
        commentsController.InitMod( 0, ObjId: (mainFeed.Id?.integerValue)!, PostType: (mainFeed.PostType?.integerValue)!,isItPrivate : mainFeed.CommentPrivacy)
        
        CommentsOnlyContainer.parentView = self
        
   //  commentsController.isCommentModOnly = true
   //    CommentsOnlyContainer.view.frame = CGSize(width: view.frame.width, height: view.frame.height)
    
//        
        let frameVC = CGRect(x: 0, y: 0, width: (view.frame.width + 500), height: view.frame.height)
        
        
        CommentsOnlyContainer.view.frame = frameVC
        
        
//        
//        All.view.frame = frameVC
//        
//        All.willMoveToParentViewController(self)
//        self.addChildViewController(All)
//        All.didMoveToParentViewController(self)
        cellComments.addSubview(CommentsOnlyContainer.view)
   
        
//        commentsScroll
//       commentsContainer.addSubview(All.view)
        
     
    }
    
    
    
    
    // MARK: - Profile Actions
    
    func profileImage(gestureRecognizer: UITapGestureRecognizer) {
        profileNavigation()
        
    }
    
    @IBAction func profileNameAction(sender: AnyObject) {
        profileNavigation()
        
    }

    func profileNavigation()  {
        // self.view.makeToast("Go to user profile :D :P", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
        
        let profile = self.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        profile.SetupForTimeLine((mainFeed.UserProfile?.Id)!.stringValue)
        navigationController?.pushViewController(profile, animated: true)
        
    }
    
      // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 11
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        if indexPath.row == 0 {
            if (mainFeed.Text)! == "" || (mainFeed.Text) == nil {
                return 0
            }else {
                
                return lblPostText.bounds.size.height + 5
            }
            
        }else if indexPath.row == 1 {
            if mainFeed.AttachedFiles == nil {
                return 0
            }else {
                return 320
            }
            
        }else if indexPath.row == 2 {
            return 28
        }else if indexPath.row == 3 {
            
            if DressFlag == false {
                return 0
            }else {
                return 60
            }
        }else if indexPath.row == 4 {
            if TopFlag == false {
                return 0
            }else {
                return 60
            }
        }else if indexPath.row == 5 {
            if BottomFlag == false {
                return 0
            }else {
                return 60
            }
        }else if indexPath.row == 6 {
            if AccessoriesFlag == false {
                return 0
            }else {
                return 60
            }
        }else if indexPath.row == 7 {
            if PurseFlag == false {
                return 0
            }else {
                return 60
            }
        }else if indexPath.row == 8 {
            if ShoesFlag == false {
                return 0
            }else {
                return 60
            }
        }else if indexPath.row == 9 {
            if OtherFlag == false {
                return 0
            }else {
                lblOtherTxt.text = otherStr
                return 60
            }
            
        }else
            if indexPath.row == 10 {

                return 300
                
               // return UITableViewAutomaticDimension
        
        }
    
    
       return 0
    }

    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
         return 100
    }
    //# MARK: - Gallery View
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesUrl.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: CcImageGallery = collectionView.dequeueReusableCellWithReuseIdentifier("CcImageGallery", forIndexPath: indexPath) as! CcImageGallery
        
        cell.img.kf_setImageWithURL( imagesUrl[indexPath.row], placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        
        
        cell.btnDeselect.hidden = true
        
        cell.btnCrop.hidden = true
        
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell: CcImageGallery = collectionView.cellForItemAtIndexPath(indexPath) as! CcImageGallery
        
        var photos = [SSPhoto]()
        var photo: SSPhoto!
        
        photo = SSPhoto(image: cell.img.image!)
        photo.aCaption = ""
        photos.append(photo)
        
        // Create and setup browser
        let browser = SSImageBrowser(aPhotos: photos, animatedFromView: collectionView.cellForItemAtIndexPath(indexPath)! as UIView) // using initWithPhotos:animatedFromView: method to use the zoom-in animation
       
        browser.displayActionButton = true
        browser.displayArrowButton = true
        browser.displayCounterLabel = true
        browser.usePopAnimation = true
        
        
        
        // Show
        self.presentViewController(browser, animated: true, completion: nil)
        
    }
    
    
    //# MARK: - Options Menu
    
    @IBAction func dropMenu(sender: AnyObject) {
        
        
        // 1
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        // 2
        
        let Edit = UIAlertAction(title: "Edit Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            let editscreen = self.Parent.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost
            editscreen.modFeedFeedobj = self.mainFeed;
            if let masterPatrent = self.Parent as! VcMasterNewsFeed?
            {
                if masterPatrent.Mode == .GroupPosts {
                    editscreen.isItPrivate = masterPatrent.ParentGroup.privacy // if group private then all posts private by default
                }
            }
            editscreen.FromScreen = .EditPost
            editscreen.Editblock = { ()in
                if let masterPatrent = self.Parent as! VcMasterNewsFeed?
                {
                    VcMasterNewsFeed.RefrehFeed(self, PostID: self.mainFeed.Id!.integerValue,feedmaster: masterPatrent , callback: { (feed) in
                    
                       self.mainFeed = feed!
                       self.setData()
                       self.mediaCollectionView.reloadData()
                       self.tableView.reloadData()
                    
                    })
                }
                
            }
            self.navigationController?.pushViewController(editscreen, animated: true)
            
        })
        
        let postStatistic = UIAlertAction(title: "Post Result", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let statistic = self.Parent.storyboard?.instantiateViewControllerWithIdentifier("VcPostStatistics") as! VcPostStatistics
            
            statistic.postId = (self.mainFeed.Id?.integerValue)!
            statistic.come = .post
            
            self.navigationController?.pushViewController(statistic, animated: true)
            
        })
        
        
        let copy = UIAlertAction(title: "Copy Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let pb = UIPasteboard.generalPasteboard()
            
            if self.mainFeed.SharedPost == nil {
                pb.string = self.mainFeed.Text!
            }
            else  {
                pb.string = self.mainFeed.SharedPost?.Text!
            }
            
            self.view.makeToast("Copied", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
            
        })
        
        
        
        
        let save = UIAlertAction(title: "Save Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            TcNewsFeed.SavePost(self.view, data: self.mainFeed, completion: nil)
            
        })
        
        
        
        let delete = UIAlertAction(title: "Delete Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let alert = UIAlertController(title: "Confirm", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            let ok = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Default , handler:
                {(action: UIAlertAction) -> Void in
                    TcNewsFeed.DeletePost(self, for: self.mainFeed, completion: { (success) in
                        if success
                        {
                            if (self.Parent is VcMasterNewsFeed)
                            {
                                (self.Parent as! VcMasterNewsFeed).HidePost(self.mainFeed)
                            }
                            
                        }
                    })
            })
            let cancel = UIAlertAction(title: "Cancel", style: .Default, handler: {(action: UIAlertAction) -> Void in
                alert.dismissViewControllerAnimated(true, completion: { _ in })
            })
            alert.addAction(ok)
            alert.addAction(cancel)
            self.presentViewController(alert, animated: true, completion: { _ in })
            
            
        })
        
        
        let report = UIAlertAction(title: "Report post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let report = self.storyboard?.instantiateViewControllerWithIdentifier("VcReport") as! VcReport
            report.come = .Post
            report.feed = self.mainFeed
            self.navigationController?.pushViewController(report, animated: true)
            
        })
        
        
        
        if(User!.AccountType == AccountType.Moderator.rawValue)
        {
            let hide = UIAlertAction(title: "Hide Post", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                // showModeratorPasswordAlert(for: self.mainFeed)
                TcNewsFeed.HidePost_showModeratorPasswordAlert(self, for: self.mainFeed, completion: { (success) in
                    if success
                    {
                        if (self.Parent is VcMasterNewsFeed)
                        {
                            (self.Parent as! VcMasterNewsFeed).HidePost(self.mainFeed)
                        }
                    }
                })
                
            })
            optionMenu.addAction(hide)
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        if(mainFeed.UserProfile?.Id == User?.Id)
        {
            optionMenu.addAction(Edit)
            
        }
        optionMenu.addAction(copy)
        optionMenu.addAction(save)
        
        if(mainFeed.UserProfile?.Id == User?.Id)
        {
            optionMenu.addAction(postStatistic)
            optionMenu.addAction(delete)
            
        }else{
            
            optionMenu.addAction(report)
        }
        
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
        
    }
    
    

     //# MARK: - Social Actions (comment, Like, Dislike, Share)
    
    @IBAction func btnCommentAction(sender: AnyObject) {
        
        let commentsController = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .All
        commentsController.InitMod( 0, ObjId: (mainFeed.Id?.integerValue)!, PostType: (mainFeed.PostType?.integerValue)!,isItPrivate : mainFeed.CommentPrivacy)
        
        navigationController?.pushViewController(commentsController, animated: true)
    }
    @IBAction func btnLikeAction(sender: AnyObject) {
        
        
        let commentsController = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .Likers
        commentsController.InitMod( 0, ObjId: (mainFeed.Id?.integerValue)!, PostType: (mainFeed.PostType?.integerValue)!,isItPrivate : mainFeed.CommentPrivacy)
        
        navigationController?.pushViewController(commentsController, animated: true)
//        
//        let like = self.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
//        
//        like.LikersMod(0, LikeType: true, ObjId: (mainFeed.Id?.integerValue)!)
//        
//        self.navigationController?.pushViewController(like, animated: true)

    }
    
    @IBAction func btnDislikeAction(sender: AnyObject) {
        
        
        let commentsController = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .DisLikers
        commentsController.InitMod( 0, ObjId: (mainFeed.Id?.integerValue)!, PostType: (mainFeed.PostType?.integerValue)!,isItPrivate : mainFeed.CommentPrivacy)
        
        navigationController?.pushViewController(commentsController, animated: true)
        
//        let like = self.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
//        
//        like.LikersMod(0, LikeType: false, ObjId: (mainFeed.Id?.integerValue)!)
//        
//        self.navigationController?.pushViewController(like, animated: true)

    }
   
    @IBAction func btnShareAction(sender: AnyObject) {
    }
    
    
     //# MARK: - Items Actions
    
    
    @IBAction func btnLikesActions(sender: AnyObject) {
        likeActions(sender as! UIButton)
    }
    
    @IBAction func btnDislikeActions(sender: AnyObject) {
        dislikeActions(sender as! UIButton)
    }
    
    
    
    
    
    func likeActions (sender:UIButton) {
        
        if sender.tag == 1 {
            
            
            if !dressDislike {
                
                
                if dressLike {
                    
                    likeDislikePostItems(true, likeValue: false, itemId: 1, completion: { (success) in
                        self.likeDress.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                        self.likeDress.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.dressLike = false
                        self.likeDressStr.setTitle(String(Int((self.likeDressStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                    
                    
                    
                }else{
                    
                    
                    likeDislikePostItems(true, likeValue: true, itemId: 1, completion: { (success) in
                        self.likeDress.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                        self.likeDress.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.dressLike = true
                         self.likeDressStr.setTitle(String(Int((self.likeDressStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                
                likeDislikePostItems(true, likeValue: true, itemId: 1, completion: { (success) in
                    self.likeDress.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    self.likeDress.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.dressLike = true
                     self.likeDressStr.setTitle(String(Int((self.likeDressStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                
                
                self.dislikeDress.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                self.dislikeDress.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                self.dressDislike = false
                self.dislikeDressStr.setTitle(String(Int((self.dislikeDressStr.titleLabel?.text)!)! - 1) , forState: .Normal)
            }
            
        }else if sender.tag == 3 {
            
            if !topDislike {
                
                
                if topLike {
                    
                    likeDislikePostItems(true, likeValue: false, itemId: 2, completion: { (success) in
                        
                        self.likeTop.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                        self.likeTop.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.topLike = false
                        self.likeTopStr.setTitle(String(Int((self.likeTopStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                    
                }else{
                    likeDislikePostItems(true, likeValue: true, itemId: 2, completion: { (success) in
                        self.likeTop.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                        self.likeTop.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.topLike = true
                        self.likeTopStr.setTitle(String(Int((self.likeTopStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(true, likeValue: true, itemId: 2, completion: { (success) in
                    self.likeTop.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    self.likeTop.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.topLike = true
                    self.likeTopStr.setTitle(String(Int((self.likeTopStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                dislikeTop.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                dislikeTop.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                topDislike = false
                self.dislikeTopStr.setTitle(String(Int((self.dislikeTopStr.titleLabel?.text)!)! - 1) , forState: .Normal)
            }
            
            
        }else if sender.tag == 5 {
            
            if !bottomDislike {
                
                
                if bottomLike {
                    
                    likeDislikePostItems(true, likeValue: false, itemId: 3, completion: { (success) in
                        self.likeBottom.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                        self.likeBottom.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.bottomLike = false
                        self.likeBottomStr.setTitle(String(Int((self.likeBottomStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(true, likeValue: true, itemId: 3, completion: { (success) in
                        self.likeBottom.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                        self.likeBottom.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.bottomLike = true
                        self.likeBottomStr.setTitle(String(Int((self.likeBottomStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(true, likeValue: true, itemId: 3, completion: { (success) in
                    self.likeBottom.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    self.likeBottom.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.bottomLike = true
                    self.likeBottomStr.setTitle(String(Int((self.likeBottomStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                dislikeBottom.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                dislikeBottom.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                bottomDislike = false
                self.dislikeBottomStr.setTitle(String(Int((self.dislikeBottomStr.titleLabel?.text)!)! - 1) , forState: .Normal)
            }
            
            
        }else if sender.tag == 7 {
            
            if !accessDislike {
                
                
                if accessLike {
                    likeDislikePostItems(true, likeValue: false, itemId: 4, completion: { (success) in
                        self.likeAccess.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                        self.likeAccess.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.accessLike = false
                        self.likeAccessStr.setTitle(String(Int((self.likeAccessStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(true, likeValue: true, itemId: 4, completion: { (success) in
                        self.likeAccess.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                        self.likeAccess.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.accessLike = true
                        self.likeAccessStr.setTitle(String(Int((self.likeAccessStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(true, likeValue: true, itemId: 4, completion: { (success) in
                    self.likeAccess.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    self.likeAccess.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.accessLike = true
                    self.likeAccessStr.setTitle(String(Int((self.likeAccessStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                dislikeAccess.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                dislikeAccess.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                accessDislike = false
                self.dislikeAccessStr.setTitle(String(Int((self.dislikeAccessStr.titleLabel?.text)!)! - 1) , forState: .Normal)
            }
            
            
        }else if sender.tag == 9 {
            
            if !purseDislike {
                
                
                if purseLike {
                    
                    likeDislikePostItems(true, likeValue: false, itemId: 5, completion: { (success) in
                        self.likePurse.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                        self.likePurse.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.purseLike = false
                        self.likePurseStr.setTitle(String(Int((self.likePurseStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                    
                }else{
                    likeDislikePostItems(true, likeValue: true, itemId: 5, completion: { (success) in
                        self.likePurse.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                        self.likePurse.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.purseLike = true
                        self.likePurseStr.setTitle(String(Int((self.likePurseStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(true, likeValue: true, itemId: 5, completion: { (success) in
                    self.likePurse.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    self.likePurse.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.purseLike = true
                    self.likePurseStr.setTitle(String(Int((self.likePurseStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                dislikePurse.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                dislikePurse.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                purseDislike = false
                self.dislikePurseStr.setTitle(String(Int((self.dislikePurseStr.titleLabel?.text)!)! - 1) , forState: .Normal)
            }
            
            
        }else if sender.tag == 11 {
            
            if !shoesDislike {
                
                
                if shoesLike {
                    likeDislikePostItems(true, likeValue: false, itemId: 6, completion: { (success) in
                        self.likeShoes.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                        self.likeShoes.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.shoesLike = false
                        self.likeShoesStr.setTitle(String(Int((self.likeShoesStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(true, likeValue: true, itemId: 6, completion: { (success) in
                        self.likeShoes.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                        self.likeShoes.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.shoesLike = true
                        self.likeShoesStr.setTitle(String(Int((self.likeShoesStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(true, likeValue: true, itemId: 6, completion: { (success) in
                    self.likeShoes.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    self.likeShoes.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.shoesLike = true
                    self.likeShoesStr.setTitle(String(Int((self.likeShoesStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                dislikeShoes.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                dislikeShoes.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                shoesDislike = false
                self.dislikeShoesStr.setTitle(String(Int((self.dislikeShoesStr.titleLabel?.text)!)! - 1) , forState: .Normal)
            }
            
            
        }else if sender.tag == 13 {
            
            if !otherDislike {
                
                
                if otherLike {
                    likeDislikePostItems(true, likeValue: false, itemId: 7, completion: { (success) in
                        self.likeOther.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                        self.likeOther.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.otherLike = false
                        self.likeOtherStr.setTitle(String(Int((self.likeOtherStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(true, likeValue: true, itemId: 7, completion: { (success) in
                        self.likeOther.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                        self.likeOther.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.otherLike = true
                        self.likeOtherStr.setTitle(String(Int((self.likeOtherStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(true, likeValue: true, itemId: 7, completion: { (success) in
                    self.likeOther.setImage(UIImage(named: "Offers-(Like)"), forState: .Normal)
                    self.likeOther.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.otherLike = true
                    self.likeOtherStr.setTitle(String(Int((self.likeOtherStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                dislikeOther.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                dislikeOther.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                otherDislike = false
                self.dislikeOtherStr.setTitle(String(Int((self.dislikeOtherStr.titleLabel?.text)!)! - 1) , forState: .Normal)
            }
            
            
        }
    }
    
    func dislikeActions (sender:UIButton) {
        
        if sender.tag == 2 {
            
            if !dressLike {
                
                if dressDislike {
                    likeDislikePostItems(false, likeValue: false, itemId: 1, completion: { (success) in
                        self.dislikeDress.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                        self.dislikeDress.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.dressDislike = false
                        self.dislikeDressStr.setTitle(String(Int((self.dislikeDressStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(false, likeValue: true, itemId: 1, completion: { (success) in
                        
                        self.dislikeDress.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                        self.dislikeDress.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.dressDislike = true
                        self.dislikeDressStr.setTitle(String(Int((self.dislikeDressStr.titleLabel?.text)!)! +
                            1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(false, likeValue: true, itemId: 1, completion: { (success) in
                    
                    self.dislikeDress.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    self.dislikeDress.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.dressDislike = true
                    self.dislikeDressStr.setTitle(String(Int((self.dislikeDressStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                likeDress.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                likeDress.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                dressLike = false
                self.likeDressStr.setTitle(String(Int((self.likeDressStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                
            }
        }else if sender.tag == 4 {
            
            if !topLike {
                
                if topDislike {
                    likeDislikePostItems(false, likeValue: false, itemId: 2, completion: { (success) in
                        self.dislikeTop.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                        self.dislikeTop.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.topDislike = false
                        self.dislikeTopStr.setTitle(String(Int((self.dislikeTopStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(false, likeValue: true, itemId: 2, completion: { (success) in
                        self.dislikeTop.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                        self.dislikeTop.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.topDislike = true
                        self.dislikeTopStr.setTitle(String(Int((self.dislikeTopStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(false, likeValue: true, itemId: 2, completion: { (success) in
                    self.dislikeTop.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    self.dislikeTop.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.topDislike = true
                    self.dislikeTopStr.setTitle(String(Int((self.dislikeTopStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                likeTop.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                likeTop.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                topLike = false
                
                self.likeTopStr.setTitle(String(Int((self.likeTopStr.titleLabel?.text)!)! - 1) , forState: .Normal)
               
            }
            
            
        }else if sender.tag == 6 {
            
            if !bottomLike {
                
                if bottomDislike {
                    likeDislikePostItems(false, likeValue: false, itemId: 3, completion: { (success) in
                        self.dislikeBottom.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                        self.dislikeBottom.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.bottomDislike = false
                         self.dislikeBottomStr.setTitle(String(Int((self.dislikeBottomStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(false, likeValue: true, itemId: 3, completion: { (success) in
                        self.dislikeBottom.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                        self.dislikeBottom.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.bottomDislike = true
                         self.dislikeBottomStr.setTitle(String(Int((self.dislikeBottomStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(false, likeValue: true, itemId: 3, completion: { (success) in
                    self.dislikeBottom.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    self.dislikeBottom.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.bottomDislike = true
                     self.dislikeBottomStr.setTitle(String(Int((self.dislikeBottomStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                likeBottom.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                likeBottom.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                bottomLike = false
                
                self.likeBottomStr.setTitle(String(Int((self.likeBottomStr.titleLabel?.text)!)! - 1) , forState: .Normal)
               
            }
            
            
        }else if sender.tag == 8 {
            
            if !accessLike {
                
                if accessDislike {
                    likeDislikePostItems(false, likeValue: false, itemId: 4, completion: { (success) in
                        self.dislikeAccess.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                        self.dislikeAccess.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.accessDislike = false
                        self.dislikeAccessStr.setTitle(String(Int((self.dislikeAccessStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(false, likeValue: true, itemId: 4, completion: { (success) in
                        self.dislikeAccess.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                        self.dislikeAccess.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.accessDislike = true
                        self.dislikeAccessStr.setTitle(String(Int((self.dislikeAccessStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(false, likeValue: true, itemId: 4, completion: { (success) in
                    self.dislikeAccess.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    self.dislikeAccess.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.accessDislike = true
                    self.dislikeAccessStr.setTitle(String(Int((self.dislikeAccessStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                likeAccess.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                likeAccess.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                accessLike = false
                
                self.likeAccessStr.setTitle(String(Int((self.likeAccessStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                
            }
            
            
        }else if sender.tag == 10 {
            
            if !purseLike {
                
                if purseDislike {
                    likeDislikePostItems(false, likeValue: false, itemId: 5, completion: { (success) in
                        self.dislikePurse.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                        self.dislikePurse.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.purseDislike = false
                        self.dislikePurseStr.setTitle(String(Int((self.dislikePurseStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(false, likeValue: true, itemId: 5, completion: { (success) in
                        self.dislikePurse.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                        self.dislikePurse.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.purseDislike = true
                        self.dislikePurseStr.setTitle(String(Int((self.dislikePurseStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(false, likeValue: true, itemId: 5, completion: { (success) in
                    self.dislikePurse.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    self.dislikePurse.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.purseDislike = true
                    self.dislikePurseStr.setTitle(String(Int((self.dislikePurseStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                likePurse.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                likePurse.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                purseLike = false
                
                self.likePurseStr.setTitle(String(Int((self.likePurseStr.titleLabel?.text)!)! - 1) , forState: .Normal)
               
            }
            
            
        }else if sender.tag == 12 {
            
            if !shoesLike {
                
                if shoesDislike {
                    likeDislikePostItems(false, likeValue: false, itemId: 6, completion: { (success) in
                        self.dislikeShoes.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                        self.dislikeShoes.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.shoesDislike = false
                        self.dislikeShoesStr.setTitle(String(Int((self.dislikeShoesStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(false, likeValue: true, itemId: 6, completion: { (success) in
                        self.dislikeShoes.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                        self.dislikeShoes.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.shoesDislike = true
                        self.dislikeShoesStr.setTitle(String(Int((self.dislikeShoesStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(false, likeValue: true, itemId: 6, completion: { (success) in
                    self.dislikeShoes.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    self.dislikeShoes.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.shoesDislike = true
                    self.dislikeShoesStr.setTitle(String(Int((self.dislikeShoesStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                likeShoes.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                likeShoes.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                shoesLike = false
                
                self.likeShoesStr.setTitle(String(Int((self.likeShoesStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                
            }
            
            
        }else if sender.tag == 14 {
            
            if !otherLike {
                
                if otherDislike {
                    likeDislikePostItems(false, likeValue: false, itemId: 7, completion: { (success) in
                        self.dislikeOther.setImage(UIImage(named: "Offers-(DisLike)-unselected"), forState: .Normal)
                        self.dislikeOther.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                        self.otherDislike = false
                        self.dislikeOtherStr.setTitle(String(Int((self.dislikeOtherStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                    })
                }else{
                    likeDislikePostItems(false, likeValue: true, itemId: 7, completion: { (success) in
                        self.dislikeOther.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                        self.dislikeOther.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                        self.otherDislike = true
                        self.dislikeOtherStr.setTitle(String(Int((self.dislikeOtherStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                    })
                }
                
            }else{
                likeDislikePostItems(false, likeValue: true, itemId: 7, completion: { (success) in
                    self.dislikeOther.setImage(UIImage(named: "Offers-(DisLike)"), forState: .Normal)
                    self.dislikeOther.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
                    self.otherDislike = true
                    self.dislikeOtherStr.setTitle(String(Int((self.dislikeOtherStr.titleLabel?.text)!)! + 1) , forState: .Normal)
                })
                
                likeOther.setImage(UIImage(named: "Offers-(Like)-unselected"), forState: .Normal)
                likeOther.setTitleColor(UIColor(hex: "#BFBEC4"), forState: .Normal)
                otherLike = false
                
                self.likeOtherStr.setTitle(String(Int((self.likeOtherStr.titleLabel?.text)!)! - 1) , forState: .Normal)
                
            }
            
            
        }
        
        
    }
    
    
    
    
    @IBAction func likeItems(sender: UIButton) {
        
         let like = self.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        
        if sender.tag == 15 {
            
           
            like.ItemsLikersMod(1, LikeType: true, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        } else if sender.tag == 17 {
            
            
            
            like.ItemsLikersMod(2, LikeType: true, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        } else if sender.tag == 19 {
            
            
            
            like.ItemsLikersMod(3, LikeType: true, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        } else if sender.tag == 21 {
            
            
            
            like.ItemsLikersMod(4, LikeType: true, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        }else if sender.tag == 23 {
            
            
            
            like.ItemsLikersMod(5, LikeType: true, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        }else if sender.tag == 25 {
            
            
            
            like.ItemsLikersMod(6, LikeType: true, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        }else if sender.tag == 27 {
            
            
            
            like.ItemsLikersMod(7, LikeType: true, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        }
        
        
    }
    
    
    @IBAction func dislikeItems(sender: UIButton) {
        
         let like = self.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        
        if sender.tag == 16 {
          
            like.ItemsLikersMod(1, LikeType: false, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)

            
        } else if sender.tag == 18 {
            
            like.ItemsLikersMod(2, LikeType: false, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        } else if sender.tag == 20 {
            
            like.ItemsLikersMod(3, LikeType: false, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        } else if sender.tag == 22 {
            
            like.ItemsLikersMod(4, LikeType: false, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        }else if sender.tag == 24 {
            
            like.ItemsLikersMod(5, LikeType: false, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        }else if sender.tag == 26 {
            
            like.ItemsLikersMod(6, LikeType: false, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        }else if sender.tag == 28 {
            
            like.ItemsLikersMod(7, LikeType: false, PostId: (mainFeed.Id?.integerValue)!)
            
            self.navigationController?.pushViewController(like, animated: true)
            
        }
        
    }
    
    
    
    func likeDislikePostItems(likeType:Bool, likeValue:Bool, itemId:Int,completion:((success:Bool)->())?)
    {
        
        let RequestParameters : NSDictionary = [
            
            "LevelId":itemId,
            "LikeType":likeType,
            "LikeValue":likeValue,
            "PostId":mainFeed.Id!,
            "UserId": (User?.Id)!
        ]
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.LikeDislikePostItems, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                var succ=false
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    
                    if( resultResponse == "0"){
                        succ = true
                        
                        
                        
                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
                if completion != nil
                {
                    completion!(success: succ)
                }
                
                
                for var i in self.items {
                    
                   
                    if i.It_Name == "Dress" {
                      
                        
                        i.Liked = self.dressLike
                        i.Disliked = self.dressDislike
                        i.NoOfLikes = NSNumber(integer: Int((self.likeDressStr.titleLabel?.text)!)!)
                        i.NoOfDislikes = NSNumber(integer: Int((self.dislikeDressStr.titleLabel?.text)!)!)
                        
                        continue
                        
                    }else if i.It_Name == "Top" {
                      
                        i.Liked = self.topLike
                        i.Disliked = self.topDislike
                        i.NoOfLikes = NSNumber(integer: Int((self.likeTopStr.titleLabel?.text)!)!)
                        i.NoOfDislikes = NSNumber(integer: Int((self.dislikeTopStr.titleLabel?.text)!)!)
                        
                        continue

                    
                    }else if i.It_Name == "Bottom" {
                       
                        
                        i.Liked = self.bottomLike
                        i.Disliked = self.bottomDislike
                        i.NoOfLikes = NSNumber(integer: Int((self.likeBottomStr.titleLabel?.text)!)!)
                        i.NoOfDislikes = NSNumber(integer: Int((self.dislikeBottomStr.titleLabel?.text)!)!)
                        
                        continue
                        
                    
                    }else if i.It_Name == "Accessories" {
                        
                        i.Liked = self.accessLike
                        i.Disliked = self.accessDislike
                        i.NoOfLikes = NSNumber(integer: Int((self.likeAccessStr.titleLabel?.text)!)!)
                        i.NoOfDislikes = NSNumber(integer: Int((self.dislikeAccessStr.titleLabel?.text)!)!)
                        
                        continue
                        
                    }else if i.It_Name == "Purse" {
                      
                        i.Liked = self.purseLike
                        i.Disliked = self.purseDislike
                        i.NoOfLikes = NSNumber(integer: Int((self.likePurseStr.titleLabel?.text)!)!)
                        i.NoOfDislikes = NSNumber(integer: Int((self.dislikePurseStr.titleLabel?.text)!)!)
                        
                        continue
                        
                    
                    }else if i.It_Name == "Shoes" {
                        
                        i.Liked = self.shoesLike
                        i.Disliked = self.shoesDislike
                        i.NoOfLikes = NSNumber(integer: Int((self.likeShoesStr.titleLabel?.text)!)!)
                        i.NoOfDislikes = NSNumber(integer: Int((self.dislikeShoesStr.titleLabel?.text)!)!)
                        
                        continue
                        
                    }else {
                        
                        i.Liked = self.otherLike
                        i.Disliked = self.otherDislike
                        i.NoOfLikes = NSNumber(integer: Int((self.likeOtherStr.titleLabel?.text)!)!)
                        i.NoOfDislikes = NSNumber(integer: Int((self.dislikeOtherStr.titleLabel?.text)!)!)
                        
                        continue
                    }
                }

                
                
        })
        
    }
    

    
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
