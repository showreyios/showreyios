//
//  VcFollorwes.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 11/19/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD

class VcFollorwes_Likers_Search: UIViewController {
    
    var vcListOfPeoples : VcListOfPeoples!
   
    var PagesCount = 1
    
    enum ScreenMod : Int{
        case Folloing = 0
        case Follorwes = 1
        case Likers = 2
    }
    var MyScreenMod : ScreenMod = .Folloing
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
     //   Follorwes()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
 
    func Load_Follorwes_Likers_Search(WithPrgoress : Bool = true , AddPagetoInfinit : Bool = false){
        
        if (WithPrgoress ) { MBProgressHUD.showHUDAddedTo(self.view, animated: true).label.text = "Loading..."}
        
        if (AddPagetoInfinit) { PagesCount += 1 } else { PagesCount = 1}
        
        var RequestParameters : NSDictionary = NSDictionary()
        
        
        
        
        var servise = WSMethods.GetFollowees
        
        
        switch MyScreenMod {
        case .Folloing  :
          servise = WSMethods.GetFollowees
        case .Follorwes  :
            servise = WSMethods.GetFollowers
        case .Likers  :
            servise = WSMethods.GetLikers
              }
        
        RequestParameters =
            [   "UserId":User!.Id!,
                "PageIndex":PagesCount,
                "PageSize":50,
                "ProfileUserId":User!.Id!
        ]
        
        RequestParameters =
            [   "LevelId":1,
                "LikeType":true,
                "ObjId":1,
                "PageIndex":PagesCount,
                "PageSize":50,
                "UserId":User!.Id!
        ]
        

        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        
        NetworkHelper.RequestHelper(nil, service:servise, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                if (WithPrgoress ) {MBProgressHUD.hideHUDForView(self.view, animated: true)}
                if(response.result.isFailure)
                {
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.view.makeToast("No Internet connection")
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        let modListOfPeoples = ModListOfPeoples(json:JSON)
                        
                        if (self.PagesCount > 1){
                            
                        self.vcListOfPeoples.ListOfPeoplesArray.appendContentsOf(modListOfPeoples.Followersobj!)
                            
                        }
                        else
                        {
                            self.vcListOfPeoples.ListOfPeoplesArray = modListOfPeoples.Followersobj!
                        }
                       
                        self.vcListOfPeoples.ReloadTable()
                    }
                }
                
            }
            , callbackDictionary: nil)
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let DV = segue.destinationViewController as? VcListOfPeoples
            where segue.identifier == "FollorwesSegue" {
            vcListOfPeoples = DV
            vcListOfPeoples.ReloadTablefromWebService = {
                (AddPagetoInfinit : Bool)in
                if (AddPagetoInfinit){self.Load_Follorwes_Likers_Search(false,AddPagetoInfinit: true)} // with Infinit
                else{
                self.Load_Follorwes_Likers_Search(false) // without  Infinit
                }
                
            }
        }
    }
    
    
}
