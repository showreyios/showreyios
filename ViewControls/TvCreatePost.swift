//
//  TvCreatePost.swift
//  ShowRey
//
//  Created by User on 10/24/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Kingfisher
import YangMingShan
import UPCarouselFlowLayout
import RMDateSelectionViewController
import AVKit
import AVFoundation
import MobileCoreServices
import TOCropViewController
import Alamofire
import MBProgressHUD
import SwiftyJSON
import MJAutoComplete



class TvCreatePost: UITableViewController, YMSPhotoPickerViewControllerDelegate , UICollectionViewDelegate , UICollectionViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate,TOCropViewControllerDelegate ,MJAutoCompleteManagerDataSource, MJAutoCompleteManagerDelegate,UITextViewDelegate,UITextFieldDelegate {
    
    enum ComeFrom : Int{
        case CreatePost = 0
        case CreateFashion = 1
        case CreateComment = 2
        case EditPost = 3
        case EditComment = 4
        case CreateSpecialOffers = 5
        case EditSpecialOffers = 6
        case CreateCommentOffers = 7
        case EditCommentOffers = 8
        
    }
    
    
    enum ContentType : Int{
        case Text = 1
        case Images = 2
        case Videos = 3
        case Link = 4
        case Shared = 6
    }
    var s : RMDateSelectionViewController!;
    
    var PrivatePostFlag : Bool = false;
    
    var targetview : UIView!
    
    var PostToType = 1
    
    var PostToId = (User?.Id)!
    
    var modFeedFeedobj : ModFeedFeedobj = ModFeedFeedobj()
    var ListOfTaggedPeoples = [ModFollowersobj]()
    
    var Editblock : (() -> ())! = nil
    var Createblock : (() -> ())! = nil
    
    var LevelId = 0
    var DressFlag : Bool = false;
    var TopFlag : Bool = false;
    var AccessoriesFlag : Bool = false;
    var PurseFlag : Bool = false;
    var ShoesFlag : Bool = false;
    var BottomFlag : Bool = false;
    var OtherFlag : Bool = false;
    
    var FromScreen : ComeFrom = ComeFrom.CreatePost
    var comeFromWelcome : Bool = false
    var PostContentType : ContentType = ContentType.Text
    
    var FireButton = 0
    
    
    var enterAutoComplete:Bool = true
    var postid = "0"  // id of post  or comments
    
    var isItPrivate : Bool = false  // for private post
    
    // In case Comments Only
    var ObjId = -1
    var TxtComment = ""
    var isWhisper = false
    var CommentID = -1
    
    
    // MARK:- AutoComplete init
    var autoCompleteMgr : MJAutoCompleteManager!
    var _TaggedUserId : NSMutableArray = NSMutableArray()
    var _MyTaggedUserIdComplete : NSMutableArray = NSMutableArray()
    var _myranges : [String] = []
    var autoCompleteContainer:UIView!
    
    
    var mydate : String = "" // for date VALIDATION in mask Thh:mm:ss
    
    
    var didStartDownload : Bool = false;
    
    
    // MARK:-
    
    
    @IBOutlet weak var CameraBtn: UIButton!
    
    
    // offer
    @IBOutlet weak var btnValidTo: UIButton!
    @IBOutlet weak var TxtOrignalPrice: UITextField!
    @IBOutlet weak var TxtPrice: UITextField!
    @IBOutlet weak var TxtTile: UITextField!
    
    
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var GalleryCollectionView: UICollectionView!
    @IBOutlet weak var PrivatePost: UIButton!
    
    @IBOutlet weak var PrivatePostLAbel: UIButton!
    @IBOutlet weak var TxtPost: UITextView!
    
    // @IBOutlet weak var autoCompleteContainer: UIView!
    
    @IBOutlet weak var SpecialOffer: UITableViewCell!
    @IBOutlet weak var cell1: UITableViewCell!
    @IBOutlet weak var head: UIView!
    @IBOutlet weak var cell4: UITableViewCell!
    @IBOutlet weak var cell2: UITableViewCell!
    @IBOutlet weak var cell3: UITableViewCell!
    @IBOutlet weak var cell5: UITableViewCell!
    @IBOutlet weak var cell6: UITableViewCell!
    @IBOutlet weak var cell7: UITableViewCell!
    
    @IBOutlet weak var ChkDress: UIButton!
    @IBOutlet weak var ChkTop: UIButton!
    @IBOutlet weak var ChkBottom: UIButton!
    @IBOutlet weak var ChkAccessories: UIButton!
    @IBOutlet weak var ChkPurse: UIButton!
    @IBOutlet weak var ChkShoes: UIButton!
    @IBOutlet weak var ChkOther: UIButton!
    
    
    
    
    
    
    @IBOutlet weak var MediaCell: UITableViewCell!
    
    @IBOutlet weak var TxtOther: UITextField!
    
    var images : [UiImageWithTag] = [] // array of images
    var deteteimages : [Int] = []
    var detetevideos : [Int] = []
    
    
    var PostType :Int = 0
    var videoURL: NSURL? = nil
    var videoData: NSData!
    var VideoThumb : UIImage!
    var avPlayer: AVPlayerViewController = AVPlayerViewController()
    var mediaPicker: UIImagePickerController!
    var imageOrientation : UIImageOrientation!
    
    func createShowMoreButton()
    {
        let myButton: UIButton = UIButton(frame: CGRect(x: outletofbtnhide.frame.origin.x, y: self.view.bounds.size.height - 66, width: outletofbtnhide.frame.size.width, height: outletofbtnhide.frame.size.height))
        let btn: UIButton = UIButton(frame: CGRect(x: 0, y: self.view.bounds.size.height - 66, width: self.view.frame.size.width, height: outletofbtnhide.frame.size.height))
        myButton.setTitle("Show More", forState: .Normal)
        myButton.center=btn.center
        myButton.setBackgroundImage(UIImage(named: "Show-More-BTN"), forState: .Normal)
       // myButton.addTarget(self, action: #selector(TvCreatePost.showHideActiont(_:)), forControlEvents:.TouchUpInside)
        self.navigationController?.view.addSubview(myButton)
        
        SingletoneClass.sharedInstance.showMoreButton = myButton
         SingletoneClass.sharedInstance.showMoreButton.addTarget(self, action: #selector(TvCreatePost.showHideActiont(_:)), forControlEvents:.TouchUpInside)
        SingletoneClass.sharedInstance.showMoreButton.hidden = false
    }
    override func viewWillAppear(animated: Bool) {
        if let showMoreBtn = SingletoneClass.sharedInstance.showMoreButton
        {
            SingletoneClass.sharedInstance.showMoreButton.removeFromSuperview()
        }
        //else
        // {
        if expand == false
        {
             createShowMoreButton()
        }
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //////////////////////////////
       
       // }
        
        //////////////////////////////
        TxtOther.enabled = false
        
        autoCompleteContainer = UIView(frame: CGRect(x: 0, y: 150, width: UIScreen.mainScreen().bounds.width , height: UIScreen.mainScreen().bounds.height - 215))
        autoCompleteContainer.hidden = true
        self.view.addSubview(autoCompleteContainer)
        
        // MARK:- Auto Complete DidLoad
        self.autoCompleteMgr = MJAutoCompleteManager()
        self.autoCompleteMgr.dataSource = self
        self.autoCompleteMgr.delegate = self
        let atTrigger :MJAutoCompleteTrigger = MJAutoCompleteTrigger(delimiter: "@")
        atTrigger.cell = "autoComCell";
        self.autoCompleteMgr.addAutoCompleteTrigger(atTrigger)
        
        self.view.bringSubviewToFront(self.autoCompleteContainer)
        
        self.autoCompleteMgr.container = self.autoCompleteContainer;
        
        self.TxtPost.delegate = self;
        
        
        
        if (FireButton == 1){
            
            textViewDidEndEditing(self.TxtPost)
            
            CameraBtn.sendActionsForControlEvents(.TouchUpInside)
        }
        
        
        
        cell1.hidden = true
        cell2.hidden = true
        cell3.hidden = true
        cell4.hidden = true
        cell5.hidden = true
        cell6.hidden = true
        cell7.hidden = true
        
        tableView.scrollEnabled = false
        
        //if ((FromScreen == .CreateFashion)|| (FromScreen == .EditPost && modFeedFeedobj.PostType?.integerValue == 2))
        if (FromScreen == .EditPost && modFeedFeedobj.PostType?.integerValue == 2) || (FromScreen == .CreateFashion)
        {
            
            cell1.hidden = false
            cell2.hidden = false
            cell3.hidden = false
            cell4.hidden = false
            cell5.hidden = false
            cell6.hidden = false
            cell7.hidden = false
            
            tableView.scrollEnabled = true
        }
        if (FromScreen == .CreateSpecialOffers || FromScreen == .EditSpecialOffers)  {
            tableView.scrollEnabled = true
            SpecialOffer.hidden = false
        }
        else
        {
            SpecialOffer.hidden = true
        }
        
        
        tableView.tableHeaderView = head
        
        navigationController?.interactivePopGestureRecognizer?.enabled = false
        
        
        
        self.addChildViewController(avPlayer)
        self.view.addSubview(avPlayer.view)
        self.view.bringSubviewToFront(autoCompleteContainer)
        
        
        //# MARK:- Init avPlayer
        if FromScreen == .CreateSpecialOffers || FromScreen == .EditSpecialOffers {
            avPlayer.view.frame =  CGRect(x: 20 , y: 290 , width: self.view.frame.size.width - 40 , height: 54 * 5)
        }else{
            avPlayer.view.frame =  CGRect(x: 20 , y: 180 , width: self.view.frame.size.width - 40 , height: 54 * 5)
        }
        avPlayer.view.hidden = true
        self.avPlayer.view.tag =  -1
        
        
        let removebutton = UIButton(frame: CGRect(x: avPlayer.view.frame.size.width - 34, y: 10, width: 24, height: 24))
        removebutton.setImage(UIImage(named: "remove"), forState: UIControlState.Normal)
        removebutton.addTarget(self, action: #selector(removeVideoAction), forControlEvents: .TouchUpInside)
        
        avPlayer.view.addSubview(removebutton)
        
        
        // MARK:-
        
        GalleryCollectionView.registerClass(CcImageGallery.self, forCellWithReuseIdentifier: "CcImageGallery")
        GalleryCollectionView.registerNib(UINib(nibName: "CcImageGallery",bundle: nil), forCellWithReuseIdentifier: "CcImageGallery")
        
        
        
        
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
        self.profileImage.clipsToBounds = true;
        
        
        profileImage.userInteractionEnabled = true
        //now you need a tap gesture recognizer
        //note that target and action point to what happens when the action is recognized.
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(TvCreatePost.profileImage(_:)))
        //Add the recognizer to your view.
        profileImage.addGestureRecognizer(tapRecognizer)
        
        
        if ((User) != nil)
        {
            let str : String? = (User?.ProfilePicture)!
            
            let url = NSURL(string:str!)
            
            
            profileImage.kf_setImageWithURL(url,placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
        }
        
        
        self.navigationItem.title = "Create Post"
        
        
        let layout = UPCarouselFlowLayout()
        layout.itemSize.width = 250
        layout.itemSize.height = 300
        
        
        // self.currentPage = 0 //
        
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        GalleryCollectionView.collectionViewLayout = layout
        
        
        // in Case of Edit for tagging
        
        if (FromScreen == .EditPost )
        {
            TxtPost.text = modFeedFeedobj.Text
            
            let tagedContexArray = modFeedFeedobj.TaggedUsers
            var ContextDictiony  = [String:String]()
            
            for tagedContex in tagedContexArray!  {
                
                ContextDictiony["UserName"] = tagedContex.UserName
                ContextDictiony["Id"] = tagedContex.Id!.stringValue
                ContextDictiony["FullName"] = tagedContex.FullName
                ContextDictiony["ProfilePicture"] = tagedContex.ProfilePicture
                ContextDictiony["FollowStatus"] = tagedContex.FollowStatus!.stringValue
                
                _TaggedUserId.addObject(ContextDictiony["Id"]!)
                _MyTaggedUserIdComplete.addObject(ContextDictiony)
                
            }
            
            
        }
        
        
        
        if (FromScreen == .EditSpecialOffers )
        {
            TxtPost.text = modFeedFeedobj.Text
            TxtTile.text = modFeedFeedobj.Title
            
            print("############pint title \(modFeedFeedobj.Title)")
            TxtOrignalPrice.text = modFeedFeedobj.OriginalPrice?.stringValue
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"//this your string date format
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            let date = dateFormatter.dateFromString(modFeedFeedobj.ValidTo!)
            
            let dateFormatter2 = NSDateFormatter()
            dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter2.timeZone = NSTimeZone(name: "UTC")
            self.mydate = dateFormatter2.stringFromDate(date!)
            
            //self.mydate = modFeedFeedobj.ValidTo!
            print("############pint title \(modFeedFeedobj.ValidTo) *****")
            //            let index = (modFeedFeedobj.ValidTo!).index((modFeedFeedobj.ValidTo!).startIndex, offsetBy: 10)
            //            str.substring(to: index)
            btnValidTo.setTitle((modFeedFeedobj.ValidTo! as NSString).substringWithRange(NSRange(location: 0, length: 10)), forState: .Normal)
            TxtPrice.text = modFeedFeedobj.Price?.stringValue
            
        }
        
        if (FromScreen == .EditPost && modFeedFeedobj.PostType?.integerValue == 2)
        {
            
            for item in modFeedFeedobj.CItems!
            {
                if(item.It_Id?.integerValue == 2)
                {
                    ChkDress.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
                    DressFlag = !DressFlag
                }
                if(item.It_Id?.integerValue == 1)
                {
                    ChkTop.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
                    TopFlag = !TopFlag
                }
                if(item.It_Id?.integerValue == 3)
                {
                    ChkBottom.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
                    BottomFlag = !BottomFlag
                }
                if(item.It_Id?.integerValue == 4)
                {
                    ChkAccessories.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
                    AccessoriesFlag = !AccessoriesFlag
                }
                if(item.It_Id?.integerValue == 5)
                {
                    ChkPurse.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
                    PurseFlag = !PurseFlag
                }
                if(item.It_Id?.integerValue == 6)
                {
                    ChkShoes.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
                    ShoesFlag = !ShoesFlag
                }
                
                if(item.It_Id?.integerValue == 7)
                {
                    ChkOther.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
                    OtherFlag = !OtherFlag
                    TxtOther.enabled = true
                    TxtOther.text = item.It_Name
                }
            }
            
            
            
            
            
        }
        
        
        if (FromScreen == .EditPost || FromScreen == .EditSpecialOffers )
        {
            
            if (modFeedFeedobj.PostCType == 2) // image
                
            {
                // fetch images from attached files
                let ui:UIImageView = UIImageView()
                if modFeedFeedobj.AttachedFiles != nil {
                    for img in modFeedFeedobj.AttachedFiles!
                    {
                        let url = NSURL(string: img.URL!)
                        ui.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler:{ (image, error, cacheType, imageURL) in
                            let  imagedata : NSData = UIImagePNGRepresentation(image!)!
                            
                            
                            var  uitageimage : UiImageWithTag = UiImageWithTag()
                            
                            uitageimage = UiImageWithTag(data: imagedata)!
                            uitageimage.tag = (img.Id?.integerValue)!
                            self.images.append(uitageimage)
                            self.GalleryCollectionView.reloadData()
                            self.GalleryCollectionView.hidden = false
                            self.tableView.reloadData()
                        })
                    }
                }
            }
            
            // fetch video from attached files
            
            if (modFeedFeedobj.PostCType == 3) // video
                
            {
                for vid in modFeedFeedobj.AttachedFiles!
                {
                    let url = NSURL(string: vid.URL!)
                    
                    let player = AVPlayer(URL: url! as  NSURL)
                    avPlayer.player = player
                    self.avPlayer.view.tag = (vid.Id?.integerValue)!
                    avPlayer.view.hidden = false
                    self.tableView.reloadData()
                }
            }
            
            
        }
        
        // in Case of Comments
        
        if (FromScreen == .CreateComment || FromScreen == .EditComment || FromScreen == .CreateCommentOffers || FromScreen == .EditCommentOffers)
        {
            TxtPost.text = self.TxtComment
            self.navigationItem.title = "Comment"
            PrivatePost.hidden = true
            PrivatePostLAbel.hidden = true
        }
        
        if ( FromScreen == .CreateSpecialOffers || FromScreen == .EditSpecialOffers)
        {
            PrivatePost.hidden = true
            PrivatePostLAbel.hidden = true
            
        }
        
        
        
        if ( FromScreen == .EditPost )
        {
            self.navigationItem.title = "Edit Post"
            
        }
        
        if ( FromScreen == .EditSpecialOffers)
        {
            self.navigationItem.title = "Edit Offer"
            
        }
        
        if ( FromScreen == .CreateSpecialOffers)
        {
            self.navigationItem.title = "Create Offer"
            
        }
        
        
        
        if (modFeedFeedobj.CommentPrivacy)
        {
            Postflag()
        }
        
        if (isItPrivate)
        {
            PostflagNotEdited()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(animated: Bool) {
        SingletoneClass.sharedInstance.showMoreButton.removeFromSuperview()//SingletoneClass.sharedInstance.showMoreButton.hidden = true
    }
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        
        
        
        
    }
    func profileImage(gestureRecognizer: UITapGestureRecognizer) {
        
        let profile = self.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        profile.SetupForTimeLine((User?.Id)!.stringValue)
        self.navigationController?.pushViewController(profile, animated: true)
        
    }
    
    //# MARK: - Table Design
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        if (images.count == 0) && (self.videoURL == nil) {
            
            if FromScreen == .CreateFashion || (FromScreen == .EditPost && modFeedFeedobj.PostType?.integerValue == 2) {
                if indexPath.row == 0 { // offer
                    
                    return 0
                }
                else if indexPath.row == 1 { // photos
                    return 0
                }
              /*  else if indexPath.row == 2
                {
                    if expand == true
                    {
                        return 0
                    }
                    else
                    {
                        return 60
                    }
                    
                }*/
                else
                {
                    return 60
                }
            }
        }else{
            if FromScreen == .CreateFashion  || (FromScreen == .EditPost && modFeedFeedobj.PostType?.integerValue == 2)  {
                
                if indexPath.row == 0 { // offer
                    
                    return 0
                }
                else if indexPath.row == 1 { // photos
                     self.GalleryCollectionView.reloadData()
                    return 320
                }
               /* else if indexPath.row == 2
                {
                    if expand == true
                    {
                        return 0
                    }
                    else
                    {
                        return 60
                    }
                    
                }*/
                else
                {
                    return 60
                }
            }
        }
        
        if FromScreen == .CreateSpecialOffers || FromScreen == .EditSpecialOffers {
            
            if indexPath.row == 0 { // offer
                
                return 114
            }
            else if indexPath.row == 1 { // photos
                if (images.count == 0) && (self.videoURL == nil) {
                    return 0}
                else {return 320}
            }
            else
            {
                return 0 // fashion
            }
            
        }else
        {
            if indexPath.row == 0 { // offer
                
                return 0
            }
            else if indexPath.row == 1 { // photos
                if (images.count == 0) && (self.videoURL == nil) {
                    return 0}
                else {return 320}
            }
            else
            {
                return 0 // fashion
            }
        }
        
        
        //  return 320
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if  expand == true
            
        {
            return 10
        }
        else
        {return 2
        }
        
    }
    
    //# MARK: - ActionSheet Media Picker
    
    @IBAction func btnPickMedia(sender: AnyObject) {
        
        
        avPlayer.player?.pause()
        
        
        // 1
        let optionMenu = UIAlertController(title: "Select Madia", message: nil, preferredStyle: .ActionSheet)
        
        // 2
        let PhotoGallery = UIAlertAction(title: "Photo Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            self.GalleryCollectionView.hidden = true
            
            if (self.videoURL != nil){
                self.videoURL = nil
                self.avPlayer.view.hidden = true
                if (self.avPlayer.view.tag != -1 ) //old photo not new one
                {
                    self.detetevideos.append(self.avPlayer.view.tag)
                }
                
            }
            
            // for multi
            if (self.FromScreen == .CreateFashion || self.FromScreen == .CreatePost || self.FromScreen == .CreateSpecialOffers  )
            {
                self.ImagePicker();
            }
            else{
                
                self.mediaPicker =  UIImagePickerController()
                self.mediaPicker.delegate = self
                self.mediaPicker.sourceType = .PhotoLibrary
                
                
                self.presentViewController(self.mediaPicker, animated: true, completion: nil)
            }
            
            
        })
        let PhotoCamera = UIAlertAction(title: "Photo Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            
            self.GalleryCollectionView.hidden = true
            if (self.videoURL != nil){
                self.videoURL = nil
                self.avPlayer.view.hidden = true
                
                if (self.avPlayer.view.tag != -1 ) //old photo not new one
                {
                    self.detetevideos.append(self.avPlayer.view.tag)
                }
                
            }
            self.mediaPicker =  UIImagePickerController()
            self.mediaPicker.delegate = self
            self.mediaPicker.sourceType = .Camera
            
            
            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
            
            
        })
        let VideoGallery = UIAlertAction(title: "Video Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            for im in self.images
            {
                if (im.tag != -1 ) //old photo not new one
                {
                    self.deteteimages.append(im.tag)
                }
            }
            
            self.images.removeAll()
            self.GalleryCollectionView.hidden = true
            if (self.videoURL != nil){
                self.videoURL = nil
                self.avPlayer.view.hidden = true
                
            }
            self.mediaPicker =  UIImagePickerController()
            self.mediaPicker.sourceType = .PhotoLibrary
            self.mediaPicker.delegate = self
            self.mediaPicker.mediaTypes = ["public.movie"] //"public.image",
            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
            
            
        })
        let VideoCamera = UIAlertAction(title: "Video Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            for im in self.images
            {
                if (im.tag != -1 ) //old photo not new one
                {
                    self.deteteimages.append(im.tag)
                }
            }
            self.images.removeAll()
            self.GalleryCollectionView.hidden = true
            if (self.videoURL != nil){
                self.videoURL = nil
                self.avPlayer.view.hidden = true
            }
            self.mediaPicker =  UIImagePickerController()
            self.mediaPicker.sourceType = .Camera
            self.mediaPicker.delegate = self
            self.mediaPicker.mediaTypes = [kUTTypeMovie as String]
            //self.mediaPicker.mediaTypes = ["public.movie"] //"public.image",
            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(PhotoGallery)
        optionMenu.addAction(PhotoCamera)
        if (FromScreen == .CreateFashion || FromScreen == .CreatePost || FromScreen == .CreateSpecialOffers)
        {
            optionMenu.addAction(VideoGallery)
            optionMenu.addAction(VideoCamera)
        }
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
        
    }
    
    
    //# MARK: - Normal Picker and View
    
    // in case of camera (I or V)  or video gallery
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        
        mediaPicker.dismissViewControllerAnimated(true, completion: nil)
        
        
        if (info[UIImagePickerControllerOriginalImage] as? UIImage) != nil  //photo
        {
            
            var pickedImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            
            if (pickedImage.imageOrientation == UIImageOrientation.Right)
            {
                
                UIGraphicsBeginImageContextWithOptions(pickedImage.size, false, pickedImage.scale)
                pickedImage.drawInRect(CGRect(x: 0, y: 0, width: pickedImage.size.width, height: pickedImage.size.height))
                let normalizedImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                pickedImage = normalizedImage!
                
                
                //pickedImage = UIImage(CGImage: pickedImage.CGImage!, scale: 1, orientation: .Right)
            }
            
            let imagedata : NSData = UIImagePNGRepresentation(pickedImage)!
            let imageFromData = UiImageWithTag(data: imagedata)!
            
            
            
            images.append(imageFromData)
            
            
            
            /*
             let cropViewController:TOCropViewController = TOCropViewController(image: (info[UIImagePickerControllerOriginalImage] as? UIImage)!)
             cropViewController.delegate = self;
             self.presentViewController(cropViewController, animated: true, completion: nil)
             */
            
            // images.append((info[UIImagePickerControllerOriginalImage] as? UiImageWithTag)!)
            self.GalleryCollectionView.reloadData()
            self.GalleryCollectionView.hidden = false
            
            self.tableView.reloadData()
            
            
            
        }
        else // video
        {
            
            //# MARK: -  add video
            self.avPlayer.view.hidden = false
            
            videoURL = NSURL()
            videoURL = info["UIImagePickerControllerReferenceURL"] as? NSURL  // gallery
            videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL // camera
            
            
            videoData = NSData(contentsOfURL: videoURL!)
            
            
            let player = AVPlayer(URL: videoURL! as  NSURL)
            
            
            // generating thumbnail
            do {
                let asset = AVURLAsset(URL: videoURL!, options: nil)
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImageAtTime(CMTimeMake(0, 1), actualTime: nil)
                VideoThumb = UIImage(CGImage: cgImage)
                // lay out this image view, or if it already exists, set its image property to uiImage
            } catch let error as NSError {
                print("Error generating thumbnail: \(error)")
            }
            
            avPlayer.player = player
            self.avPlayer.view.tag = -1
            
            
            self.tableView.reloadData()
            
            
        }
    }
    
    func removeVideoAction(sender: UIButton!) {
        //    avPlayer.player = nil
        self.videoURL = nil
        //avPlayer.view.removeFromSuperview()
        
        detetevideos.append(self.avPlayer.view.tag)
        self.avPlayer.view.hidden = true
        //    avPlayer = AVPlayerViewController()
        
        self.tableView.reloadData()
        
    }
    
    //# MARK: - Mutil Photos Picker
    
    
    func ImagePicker()
    {
        let pickerViewController = YMSPhotoPickerViewController.init()
        
        if (FromScreen == .CreateFashion || FromScreen == .CreatePost || FromScreen == .CreateSpecialOffers)
        {
            
            pickerViewController.numberOfPhotoToSelect = 10
        }
        
        
        
        pickerViewController.theme.titleLabelTextColor = UIColor(hex: "#ffffff")
        
        
        pickerViewController.theme.tintColor = UIColor(hex: "#b379e7")
        pickerViewController.theme.orderTintColor = UIColor(hex: "#b379e7")
        pickerViewController.theme.cameraVeilColor = UIColor(hex: "#b379e7")
        pickerViewController.theme.cameraIconColor = UIColor(hex: "#ffffff")
        //   pickerViewController.theme.statusBarStyle = UIStatusBarStyleLightContent
        
        
        
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow photo album access?", message: "Need your permission to access photo albumbs", preferredStyle: .Alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .Cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .Default) { (action) in
            UIApplication.sharedApplication().openURL(NSURL.init(string: UIApplicationOpenSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .Alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .Cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .Default) { (action) in
            UIApplication.sharedApplication().openURL(NSURL.init(string: UIApplicationOpenSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    
    @IBOutlet weak var outletofbtnhide: UIButton!
    
    func photoPickerViewController(picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        // Remember images you get here is PHAsset array, you need to implement PHImageManager to get UIImage data by yourself
        picker.dismissViewControllerAnimated(true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .HighQualityFormat
            options.resizeMode = .Exact
            options.synchronous = true
            
            let mutableImages: NSMutableArray! = []
            
            for asset: PHAsset in photoAssets
            {
                
                let targetSize = CGSizeMake(200,300)
                
                imageManager.requestImageForAsset(asset, targetSize: targetSize, contentMode: .AspectFill, options: options, resultHandler: { (image, info) in
                    mutableImages.addObject(image!)
                })
            }
            // Assign to Array with images
            
            let multiimages = mutableImages.copy() as! [UIImage]
            
            
            for ima in multiimages
            {
                let  imagedata : NSData = UIImagePNGRepresentation(ima)!
                self.images.append(UiImageWithTag(data: imagedata)!)
            }
            
            
            
            // self.images.appendContentsOf((mutableImages.copy() as! [UiImageWithTag]))
            //  self.images = mutableImages.copy() as! [UIImage]
            
            /*
             if ( self.images.count == 1)
             {
             let cropViewController:TOCropViewController = TOCropViewController(image: self.images[0])
             cropViewController.delegate = self;
             self.presentViewController(cropViewController, animated: true, completion: nil)
             }
             */
            
            self.GalleryCollectionView.reloadData()
            self.GalleryCollectionView.hidden = false
            
            self.tableView.reloadData()
            //   var s = self.images[0] as! UIImage;
            
        }
    }
    
    
    
    
    //# MARK: - Gallery View
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: CcImageGallery = collectionView.dequeueReusableCellWithReuseIdentifier("CcImageGallery", forIndexPath: indexPath) as! CcImageGallery
        
        cell.img.image =  images[indexPath.row]
        
        cell.btnDeselect.addTarget(self, action:  #selector(removeImage(_:)), forControlEvents: .TouchUpInside)
        cell.btnDeselect.tag = indexPath.row
        
        cell.btnCrop.addTarget(self, action:  #selector(cropImage(_:)), forControlEvents: .TouchUpInside)
        cell.btnCrop.tag = indexPath.row
        
        
        return cell
        
    }
    
    func removeImage(mybutton: UIButton)
    {
        
        if (images[mybutton.tag].tag != -1 ) //old photo not new one
        {
            deteteimages.append(images[mybutton.tag].tag)
        }
        
        images.removeAtIndex(mybutton.tag)
        GalleryCollectionView.reloadData()
        self.tableView.reloadData()
        
    }
    
    func cropImage(mybutton: UIButton)
    {
        
        
        let cropViewController:TOCropViewController = TOCropViewController(image: images[mybutton.tag])
        cropViewController.delegate = self;
        self.presentViewController(cropViewController, animated: true, completion: nil)
        images.removeAtIndex(mybutton.tag)
        
        
    }
    
    
    //# MARK: - Crop Image
    
    func cropViewController(cropViewController: TOCropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int)
    {
        
        let  imagedata : NSData = UIImagePNGRepresentation(image)!
        
        images.append(UiImageWithTag(data: imagedata)!)
        self.GalleryCollectionView.reloadData()
        self.GalleryCollectionView.hidden = false
        
        self.tableView.reloadData()
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    var expand = false
    
    @IBAction func btnPrivatePost(sender: AnyObject) {
        
        Postflag()
        
    }
    @IBAction func showHideActiont(sender: AnyObject) {
        
      
        expand = !expand
       // UIView.animateWithDuration(0.3, animations: <#T##() -> Void#>, completion: <#T##((Bool) -> Void)?##((Bool) -> Void)?##(Bool) -> Void#>)
        let range = NSMakeRange(0, self.tableView.numberOfSections)
        let sections = NSIndexSet(indexesInRange: range)
        //self.GalleryCollectionView.reloadData()
        self.GalleryCollectionView.hidden = false
        self.tableView.reloadSections(sections as NSIndexSet, withRowAnimation: .None)
        SingletoneClass.sharedInstance.showMoreButton.hidden = expand == true ? true : false
       
    }
    
    @IBAction func btnPrivatePostText(sender: AnyObject) {
        Postflag()
    }
    
    
    func Postflag()
    {
        if(!PrivatePostFlag)
        {
            PrivatePost.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
            modFeedFeedobj.CommentPrivacy = true
        }
        else
        {
            PrivatePost.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
            modFeedFeedobj.CommentPrivacy = false
        }
        PrivatePostFlag = !PrivatePostFlag;
    }
    
    
    
    func PostflagNotEdited()
    {
        
        PrivatePost.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
        modFeedFeedobj.CommentPrivacy = true
        
        PrivatePost.enabled = false
        PrivatePostLAbel.enabled = false
        PrivatePostFlag = true;
    }
    
    //# MARK:- Post
    
    @IBAction func PostAction(sender: AnyObject) {
        
        
        PostContentType = .Text
        
        
        if (images.count > 0)
        {
            PostContentType = .Images
        }
        
        if (videoURL != nil)
        {
            PostContentType = .Videos
        }
        
        
        
        //validation
        if (PostContentType == .Text && self.TxtPost.text == "")
        {
            self.view.makeToast("Please write post", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
            return
        }
        
        if (( FromScreen == .CreateSpecialOffers || FromScreen == .EditSpecialOffers) && images.count == 0  && videoURL == nil  )  //FromScreen == .CreateFashion ||
        {
            self.view.makeToast("Please Add photo or video", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
            return
        }
        
        if ( FromScreen == .CreateSpecialOffers || FromScreen == .EditSpecialOffers)
        {
            if (TxtTile.text == ""){
                self.view.makeToast("Please enter title", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                return
            }
            if (TxtPrice.text == ""){
                self.view.makeToast("Please enter price", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                return
            }
            if (TxtOrignalPrice.text == ""){
                self.view.makeToast("Please enter orignal price", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                return
            }
            if let tempPrice = Float(TxtPrice.text!)
            {
                
                if let tempOrigPrice = Float(TxtOrignalPrice.text!)
                {
                    
                    if ( tempOrigPrice <= tempPrice)
                    {
                        self.view.makeToast("Please enter price less than orignal price", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        return
                    }
                    
                }
                
                
            }
            
            if (btnValidTo.titleLabel?.text == "" || btnValidTo.titleLabel?.text == nil){
                self.view.makeToast("Please enter valid to", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                return
            }
            
            
            
            
        }
        
        //end validation
        
        
        if ( FromScreen == .EditPost)
        {
            self.EditPost(modFeedFeedobj)
        }
        
        if ( FromScreen == .EditSpecialOffers)
        {
            self.EditSpecialOffers(modFeedFeedobj)
        }
        
        if (FromScreen == .CreateComment || FromScreen == .CreateCommentOffers)
        {
            self.CreateComment()
        }
        
        if (FromScreen == .EditComment || FromScreen == .EditCommentOffers )
        {
            self.EditComment()
        }
        
        
        
        if (FromScreen == .CreatePost ||  FromScreen == .CreateSpecialOffers)
        {
            if(self.comeFromWelcome)
            {
                // VcPostChoice
                let PostChoice = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcPostChoice") as! VcPostChoice
                PostChoice.tvCreatePost = self
                self.navigationController?.pushViewController(PostChoice, animated: true)
            }
            else
            {
                self.CreatePost(self.view,FeedProfilePros: nil)
            }
        }
        
        
        
        
        if (FromScreen == .CreateFashion )
        {
            
            var myItmesCount = 0
            if (DressFlag)
            {
                myItmesCount  = +1
            }
            if (TopFlag)
            {  myItmesCount  = +1
            }
            
            if (BottomFlag)
            {
                myItmesCount  = +1            }
            if (AccessoriesFlag)
            {
                myItmesCount  = +1
            }
            if (PurseFlag)
            {
                myItmesCount  = +1            }
            if (ShoesFlag)
            {  myItmesCount  = +1
            }
            if (OtherFlag)
            {
                myItmesCount  = +1
            }
            
            
            
            
            
            
            
            if (myItmesCount > 0 )
                
            {
                // FashionPostOptionsScreen
                let FashionPostOptionsScreen = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcFashionPostOptions") as! VcFashionPostOptions
                
                if (comeFromWelcome) {
                    
                    FashionPostOptionsScreen.comeFromWelcome = comeFromWelcome
                }
                
                FashionPostOptionsScreen.tvCreatePost = self
                self.navigationController?.pushViewController(FashionPostOptionsScreen, animated: true)
            }
            else
                
            {
                if (comeFromWelcome)
                {
                    let vcPostChoice = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcPostChoice") as! VcPostChoice
                    vcPostChoice.tvCreatePost = self
                    vcPostChoice.feedProfilePros = nil
                    self.navigationController?.pushViewController(vcPostChoice, animated: true)
                }
                    
                else
                {
                    
                    
                    
                    self.CreatePost(self.view, FeedProfilePros: nil)
                }
                
            }
        }
        
        
        
    }
    
    //# MARK:- CreatePost
    
    func CreatePost(target:UIView,FeedProfilePros:ModFeedProfilePros! , comeFromWelcome : Bool = false , WPostToId:[Int] = [Int](), WPostToType: Int = -1 )
    {
        for user  in _MyTaggedUserIdComplete {
            let s   = user["UserName"] as! String ;
            if TxtPost.text.rangeOfString("@\(s)") == nil{
                let userId = user["Id"]!
                _TaggedUserId.removeObject(userId!)
                
            }
        }
        
        var PostToIdarray :[Int] = [Int] ()
        if (comeFromWelcome)
        {
            PostToIdarray = WPostToId
            
        }
        else
        {
            PostToIdarray.append(PostToId.integerValue)
            
            
        }
        
        var WPostToTypeFinal = PostToType
        
        
        if (comeFromWelcome)
        {
            WPostToTypeFinal = WPostToType
            
        }
        
        
        
        var RequestParameters : NSDictionary = NSDictionary();
        
        
        
        
        
        
        if (FromScreen == .CreatePost)
        {
            targetview = self.view
            let PostOb : NSDictionary = [
                "CommentPrivacy":PrivatePostFlag,
                "PostCType":PostContentType.rawValue, // text image vid
                "PostToId": PostToIdarray,
                "PostToType":WPostToTypeFinal,
                "PostType":1,  // 1 regular 2 fashion
                "Text":TxtPost.text,
                "UserId":(User?.Id)!
            ]
            
            RequestParameters = [
                "TaggedUserId" : _TaggedUserId,
                "PostOb" : PostOb,
                "UserId" : (User?.Id)!,
            ]
            
        } // end CreatePost Request
        
        
        
        if (FromScreen == .CreateSpecialOffers)
        {
            
            
            var txtPrice:Int = 0
            if let temp = Int(TxtPrice.text!)
            {
                txtPrice = temp
            }
            var txtOrignalPrice:Int = 0
            if let temp = Int(TxtOrignalPrice.text!)
            {
                txtOrignalPrice = temp
            }
            
            targetview = self.view
            let PostOb : NSDictionary = [
                "OriginalPrice":txtOrignalPrice,
                "PostCType":PostContentType.rawValue, // text image vid
                "Price":txtPrice,
                "Text":TxtPost.text,
                "Title":TxtTile.text!,
                "UserId":(User?.Id)!,
                "ValidTo":self.mydate //(btnValidTo.titleLabel?.text)!
            ]
            
            RequestParameters = [
                "IsAdmin":true,
                "PostOb" : PostOb,
                "UserId" : (User?.Id)!,
            ]
            
        } // end CreatePost Request
        
        
        if (FromScreen == .CreateFashion)
        {
            
            var CItems  :[NSDictionary] = [NSDictionary]()
            
            
            if (DressFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"1",
                    "Name":"Dress"
                ]
                CItems.append(CItem)
            }
            if (TopFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"2",
                    "Name":"Top"
                ]
                CItems.append(CItem)
            }
            
            if (BottomFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"3",
                    "Name":"Bottom"
                ]
                CItems.append(CItem)
            }
            if (AccessoriesFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"4",
                    "Name":"Accessories"
                ]
                CItems.append(CItem)
            }
            if (PurseFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"5",
                    "Name":"Purse"
                ]
                CItems.append(CItem)
            }
            if (ShoesFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"6",
                    "Name":"Shoes"
                ]
                CItems.append(CItem)
            }
            if (OtherFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"7",
                    "Name":TxtOther.text!
                ]
                CItems.append(CItem)
            }

          
            var PostOb : NSDictionary = [
                "CommentPrivacy":PrivatePostFlag,
                "PostCType":PostContentType.rawValue, // text image vid
                "PostToId":PostToIdarray,
                "PostToType":WPostToTypeFinal,
                "PostType":2,  // 1 regular 2 fashion
                "Text":TxtPost.text,
                "UserId":(User?.Id)!
            ]
            
            
            if ((FeedProfilePros) != nil)
            {
                let ProfilePros : NSDictionary = [
                    "AgeGroup":FeedProfilePros.AgeGroup!,
                    "BodyShape":FeedProfilePros.BodyShape!,
                    "Gender":FeedProfilePros.Gender!,
                    "Height":FeedProfilePros.Height!,
                    "Size":FeedProfilePros.Size!, //
                    "Weight":FeedProfilePros.Weight!
                ]
                
                RequestParameters = [
                    "TaggedUserId" : _TaggedUserId,
                    "UserId" : (User?.Id)!,
                    "ProfilePros" : ProfilePros,
                    "PostOb" : PostOb,
                    "CItems" : CItems
                ]
            }
            else
            {
//                let ProfilePros : NSDictionary = NSDictionary()
//                RequestParameters = [
//                    "TaggedUserId" : _TaggedUserId,
//                    "UserId" : (User?.Id)!,
//                    "ProfilePros" : ProfilePros,
//                    "PostOb" : PostOb,
//                    "CItems" : CItems
//                ]
                
                PostOb = [
                    "CommentPrivacy":PrivatePostFlag,
                    "PostCType":PostContentType.rawValue, // text image vid
                    "PostToId":PostToIdarray,
                    "PostToType":WPostToTypeFinal,
                    "PostType":1,  // 1 regular 2 fashion
                    "Text":TxtPost.text,
                    "UserId":(User?.Id)!
                ]
                
                RequestParameters = [
                    "TaggedUserId" : _TaggedUserId,
                    "PostOb" : PostOb,
                    "UserId" : (User?.Id)!,
                ]
                
                
                
            }
            
        }  // end CreateFashion Request
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        var services = WSMethods.PostShowrey
        
        if (FromScreen == .CreateSpecialOffers)
        {
            services = WSMethods.PostSpecialOffers
        }
        
        
        NetworkHelper.RequestHelper(nil, service: services, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil , callbackDictionary: jsonCallBack)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(target, animated: true)
        loadingNotification.label.text = "Posting..."
        
    }
    
    
    
    //# MARK:- CreateComment
    func CreateComment() {
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Posting..."
        
        var RequestParameters : NSDictionary = NSDictionary();
        
        var myPostType = 1
        
        if (images.count > 0)
        {
            myPostType = 2
        }
        else
        {
            if (TxtPost.text == "")
            {
                self.view.makeToast("No Comment for send", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                return
            }
        }
        
        let PostOb : NSDictionary = [
            "LevelId":LevelId,
            "PostId":ObjId,
            "PostType":myPostType,
            "Text":TxtPost.text,
            "UserId":(User?.Id)!,
            "isWhisper":isWhisper
        ]
        
        RequestParameters = [
            "TaggedUserId":_TaggedUserId,
            "CommentOb" : PostOb,
            "UserId" : (User?.Id)!,
        ]
        
        var myServiec = WSMethods.WriteAComment
        if (FromScreen == .CreateCommentOffers)
        {
            myServiec = WSMethods.WriteSpecialOffersComment
        }
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: myServiec, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil , callbackDictionary: jsonCallBack)
        
        
    }
    
    //# MARK:- EditComment
    func EditComment() {
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Posting..."
        
        var RequestParameters : NSDictionary = NSDictionary();
        
        var myPostType = 1
        
        if (images.count > 0)
        {
            myPostType = 2
        }
        
        
        postid =  "\(CommentID)"
        
        
        let PostOb : NSDictionary = [
            "Id":CommentID,
            "LevelId":LevelId,
            "PostId":ObjId,
            "PostType":myPostType,
            "Text":TxtPost.text,
            "UserId":(User?.Id)!,
            "isWhisper":isWhisper
        ]
        
        RequestParameters = [
            "TaggedUserId":_TaggedUserId,
            "CommentOb" : PostOb,
            "UserId" : (User?.Id)!,
        ]
        
        
        var myServiec = WSMethods.EditComment
        if (FromScreen == .EditCommentOffers)
        {
            myServiec = WSMethods.EditSpecialOffersComment
        }
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: myServiec, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil , callbackDictionary: jsonCallBack)
        
        
    }
    
    
    //# MARK:- EditPost
    
    func EditPost(Feedobj:ModFeedFeedobj!) {
        
        for user  in _MyTaggedUserIdComplete {
            let s   = user["UserName"] as! String ;
            if TxtPost.text.rangeOfString("@\(s)") == nil{
                let userId = user["Id"]!
                _TaggedUserId.removeObject(userId!)
                
            }
        }
        
        
        var RequestParameters : NSDictionary = NSDictionary();
        
        
        postid = "\(Feedobj.Id!.integerValue)"
        
        Feedobj.PostCType = PostContentType.rawValue;
        if (Feedobj.PostType == 1) //regular
        {
            targetview = self.view
            
            
            let PostOb : NSDictionary = [
                "Id":Feedobj.Id!,
                "CommentPrivacy":Feedobj.CommentPrivacy,
                "PostCType":Feedobj.PostCType!, // text image vid
                "PostToId":(Feedobj.PostToId?.integerValue)!,
                "PostToType":Feedobj.PostToType!,
                "PostType":Feedobj.PostType!,  // 1 regular 2 fashion
                "Text":TxtPost.text,
                "UserId":(Feedobj.UserId?.integerValue)!
            ]
            
            RequestParameters = [
                "TaggedUserId" : _TaggedUserId,
                "PostOb" : PostOb,
                "UserId" : (Feedobj.UserId?.integerValue)!,
            ]
            
        }
        
        
        if (Feedobj.PostType == 2) //Fashion
        {
            
            var CItems  :[NSDictionary] = [NSDictionary]()
            
            if (TopFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"1",
                    "Name":"Top"
                ]
                CItems.append(CItem)
            }
            if (DressFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"2",
                    "Name":"Dress"
                ]
                CItems.append(CItem)
            }
            if (BottomFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"3",
                    "Name":"Bottom"
                ]
                CItems.append(CItem)
            }
            if (AccessoriesFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"4",
                    "Name":"Accessories"
                ]
                CItems.append(CItem)
            }
            if (PurseFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"5",
                    "Name":"Purse"
                ]
                CItems.append(CItem)
            }
            if (ShoesFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"6",
                    "Name":"Shoes"
                ]
                CItems.append(CItem)
            }
            if (OtherFlag)
            {
                let CItem : NSDictionary = [
                    "Id":"7",
                    "Name":TxtOther.text!
                ]
                CItems.append(CItem)
            }
            
            
            let PostOb : NSDictionary = [
                "Id":Feedobj.Id!,
                "CommentPrivacy":Feedobj.CommentPrivacy,
                "PostCType":Feedobj.PostCType!, // text image vid
                "PostToId":(Feedobj.PostToId?.integerValue)!,
                "PostToType":Feedobj.PostToType!,
                "PostType":Feedobj.PostType!,  // 1 regular 2 fashion
                "Text":TxtPost.text,
                "UserId":(Feedobj.UserId?.integerValue)!
            ]
            
            let ProfilePros : NSDictionary = [
                
                "AgeGroup":Feedobj.ProfilePros!.AgeGroup!,
                "BodyShape":Feedobj.ProfilePros!.BodyShape!,
                "Gender":Feedobj.ProfilePros!.Gender!,
                "Height":Feedobj.ProfilePros!.Height!,
                "Size":Feedobj.ProfilePros!.Size!, //
                "Weight":Feedobj.ProfilePros!.Weight!
            ]
            
            RequestParameters = [
                "TaggedUserId" : _TaggedUserId,
                "UserId" : (Feedobj.UserId?.integerValue)!,
                "ProfilePros" : ProfilePros,
                "PostOb" : PostOb,
                "CItems" : CItems
            ]
            
        }
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.EditPost, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil , callbackDictionary: jsonCallBack)
        
        
    }
    
    //# MARK:- EditSpecialOffers
    func EditSpecialOffers(Feedobj:ModFeedFeedobj!) {
        
        for user  in _MyTaggedUserIdComplete {
            let s   = user["UserName"] as! String ;
            if TxtPost.text.rangeOfString("@\(s)") == nil{
                let userId = user["Id"]!
                _TaggedUserId.removeObject(userId!)
                
            }
        }
        
        
        var RequestParameters : NSDictionary = NSDictionary();
        
        
        
        var txtPrice:Int = 0
        if let temp = Int(TxtPrice.text!)
        {
            txtPrice = temp
        }
        var txtOrignalPrice:Int = 0
        if let temp = Int(TxtOrignalPrice.text!)
        {
            txtOrignalPrice = temp
        }
        
        postid =  "\(Feedobj.Id!.integerValue)"
        
        targetview = self.view
        let PostOb : NSDictionary = [
            "Id":Feedobj.Id!,
            "PostCType":Feedobj.PostCType!, // text image vid
            "Text":TxtPost.text,
            "UserId":(Feedobj.UserId?.integerValue)!,
            "OriginalPrice":txtOrignalPrice,
            "Price":txtPrice,
            "Title":TxtTile.text!,
            "ValidTo":self.mydate//(btnValidTo.titleLabel?.text)!
            
            
        ]
        
        RequestParameters = [
            "IsAdmin":true,
            "PostOb" : PostOb,
            "UserId" : (Feedobj.UserId?.integerValue)!,
        ]
        
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.EditSpecialOffersPost, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil , callbackDictionary: jsonCallBack)
        
        
    }
    
    
    
    
    // Callback for all
    
    
    
    func jsonCallBack(response: (JSON: JSON, NSError: NSError?))
    {
        if (targetview ==  nil)
        {
            targetview = self.view
        }
        
        self.view.endEditing(true)
        MBProgressHUD.hideHUDForView(targetview, animated: true)
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                }
                else
                {
                    self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                    print("error")
                }
                return
            }
        }
        else
        {
            if (response.JSON["ResultResponse"].stringValue == "0") //scuess
            {
                
                
                
                
                if (FromScreen == .EditPost || FromScreen == .EditSpecialOffers)
                {
                    if (Editblock != nil)
                    {
                        Editblock()
                    }
                    
                }
                
                
                if ( response.JSON["RequestId"].stringValue != "")
                {
                    postid = response.JSON["RequestId"].stringValue
                }
                
                
                
                //# MARK:- DeleteImages
                
                
                var imgnumber = 0
                for img in deteteimages
                {
                    
                    let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                    loadingNotification.label.text = "Deleteing..."
                    
                    imgnumber += 1
                    var RequestParameters : NSDictionary = NSDictionary();
                    
                    RequestParameters = [
                        "LevelId":self.LevelId,
                        "PostId":img,
                        "UserId":(User?.Id)!
                    ]
                    
                    
                    
                    
                    let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
                    
                    var serviceName = WSMethods.DeleteImage
                    
                    if ( FromScreen == .EditSpecialOffers) {serviceName = WSMethods.DeleteSpecialOffersImage}
                    
                    
                    NetworkHelper.RequestHelper(nil, service: serviceName, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
                        , responseType: ResponseType.DictionaryJson , callbackString: nil , callbackDictionary:{ (JSON, NSError) in
                            imgnumber -= 1
                            if (imgnumber == 0){
                                if (self.Editblock != nil)
                                {
                                    self.Editblock()
                                }
                                
                                MBProgressHUD.hideHUDForView(self.targetview, animated: true)
                            }
                            
                    })
                    
                    
                }// end delete photos
                
                
                //# MARK:- DeleteVideo
                
                for vid in detetevideos
                {
                    var RequestParameters : NSDictionary = NSDictionary();
                    
                    RequestParameters = [
                        "LevelId":self.LevelId,
                        "PostId":vid,
                        "UserId":(User?.Id)!
                    ]
                    
                    
                    let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                    loadingNotification.label.text = "Deleteing Video..."
                    
                    let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
                    
                    var serviceName = WSMethods.DeleteVideo
                    
                    if ( FromScreen == .EditSpecialOffers) {serviceName = WSMethods.DeleteSpecialOffersVideo}
                    
                    
                    NetworkHelper.RequestHelper(nil, service: serviceName, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
                        , responseType: ResponseType.DictionaryJson , callbackString: nil , callbackDictionary:{ (JSON, NSError) in
                            if (self.Editblock != nil)
                            {
                                self.Editblock()
                            }
                            MBProgressHUD.hideHUDForView(self.targetview, animated: true)
                            
                    })
                    
                    
                }// end delete Video
                
                //# MARK:- PostImages
                
                if (images.count > 0)
                {
                    let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                    
                    loadingNotification.label.text = "Uploading..."
                    
                    var imgesNumber = 0
                    for img in images
                    {
                        imgesNumber += 1
                        
                        if (img.tag != -1){continue}// spical case in edit if it is old photo then don't upload it
                        
                        
                        let imageData :NSData = UIImageJPEGRepresentation(img, 1)!//UIImagePNGRepresentation(img)!
                        var serviceName = WSMethods.PostImages
                        
                        
                        if ( FromScreen == .CreateSpecialOffers) {serviceName = WSMethods.PostSpecialOffersImages}
                        
                        NetworkHelper.RequestHelper(nil, service: serviceName + "?PostId=\(postid)&UserId=\((User?.Id)!)&LevelId=\(LevelId)", hTTPMethod: Method.post, parameters: nil, httpBody: nil ,httpBodyData: imageData
                            , responseType: ResponseType.DictionaryJson , callbackString: nil ,callbackDictionary: {
                                
                                (JSON, NSError) in
                                
                                
                                if((response.NSError) != nil)
                                {
                                    if let networkError = response.NSError {
                                        if (networkError.code == -1009) {
                                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                                        }
                                        else
                                        {
                                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                                            print("error")
                                        }
                                        return
                                    }
                                }
                                else
                                {
                                    if (response.JSON["ResultResponse"].stringValue == "0") //scuess
                                    {
                                        
                                        
                                        imgesNumber -= 1
                                        if (imgesNumber == 0 ){
                                            loadingNotification.hide(true)
                                            // MBProgressHUD.hideHUDForView(self.targetview, animated: true)
                                        }
                                        
                                        if(self.comeFromWelcome)
                                        {
                                            let myHome = AppDelegate.RoutToScreen("VcHome") as! VcHome;
                                            myHome.invitAlertMessage = "Invite Code: \((User?.InviteCode)!)\nI have just shared post which is:\(self.TxtPost.text)"
                                            myHome.invitAlertMessageId = self.postid
                                            myHome.invitAlertMessageType = self.FromScreen.rawValue
                                            
                                        }else
                                        {
                                            if (self.Createblock != nil)
                                            {
                                                self.Createblock()
                                            }
                                            
                                            if (self.FromScreen == .EditPost || self.FromScreen == .EditSpecialOffers)
                                            {
                                                if (self.Editblock != nil)
                                                {
                                                    self.Editblock()
                                                }
                                                
                                                
                                            }
                                            // self.navigationController?.popViewControllerAnimated(true)
                                            
                                        }
                                    }
                                }
                        })
                        
                    }
                    
                    if (self.FromScreen == .CreateFashion && self.navigationController?.viewControllers.count > 2) {
                       
                        var flage = false
                        
                        for x in (navigationController?.viewControllers)!
                        {
                            
                            if x.isKindOfClass(VcFashionPostOptions)
                            {
                                flage = true
                            }
                        }
                    
                        
                        if (flage){
                            self.navigationController?.popToViewController((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 3])!, animated: true)
                        }
                        else
                        {
                            self.navigationController?.popViewControllerAnimated(true)
                        }

                    }else{
                        
                        
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                    
                    
                    // self.navigationController?.popViewControllerAnimated(true)
                }// end upload photos    //# MARK:- PostVideo
                else if (self.videoURL != nil)
                {
                    
                    let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                    loadingNotification.label.text = "Uploading..."
                    
                    
                    if let videoData = NSData(contentsOfURL: self.videoURL!) {
                        
                        var serviceName = WSMethods.PostVideos
                        
                        if ( FromScreen == .CreateSpecialOffers) {serviceName = WSMethods.PostSpecialOffersVideos}
                        
                        NetworkHelper.RequestHelper(nil, service: serviceName + "?PostId=\(postid)&UserId=\((User?.Id)!)&LevelId=\(LevelId)", hTTPMethod: Method.post, parameters: nil, httpBody: nil , httpBodyData : videoData
                            , responseType: ResponseType.DictionaryJson , callbackString: nil ,callbackDictionary: { (JSON, NSError) in
                                // MBProgressHUD.hideHUDForView(self.view, animated: true)
                                if((response.NSError) != nil)
                                {
                                    if let networkError = response.NSError {
                                        if (networkError.code == -1009) {
                                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                                        }
                                        else
                                        {
                                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                                            print("error")
                                        }
                                        return
                                    }
                                }
                                else
                                {
                                    if (response.JSON["ResultResponse"].stringValue == "0") //scuess
                                    {
                                        //# MARK:- PostVideo thumbunail
                                        
                                        if (self.VideoThumb != nil)
                                        {
                                            
                                            //let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                                            loadingNotification.label.text = "Uploading..."
                                            
                                            let imageData :NSData = UIImageJPEGRepresentation(self.VideoThumb, 0.35)!//UIImagePNGRepresentation(self.VideoThumb)!
                                            
                                            var serviceName = WSMethods.PostVideosThumbnails
                                            
                                            if ( self.FromScreen == .CreateSpecialOffers) {serviceName = WSMethods.PostSpecialOffersVideosThumbnails}
                                            
                                            NetworkHelper.RequestHelper(nil, service: serviceName + "?PostId=\(self.postid)&UserId=\((User?.Id)!)&LevelId=\(self.LevelId)", hTTPMethod: Method.post, parameters: nil, httpBody: nil ,httpBodyData: imageData
                                                , responseType: ResponseType.DictionaryJson , callbackString: nil ,callbackDictionary: { (JSON, NSError) in
                                                    
                                                    if((response.NSError) != nil)
                                                    {
                                                        if let networkError = response.NSError {
                                                            if (networkError.code == -1009) {
                                                                self.view.makeToast("No Internet connection")
                                                            }
                                                            else
                                                            {
                                                                self.view.makeToast("An error has been occurred")
                                                                print("error")
                                                            }
                                                            return
                                                        }
                                                    }
                                                    MBProgressHUD.hideHUDForView(self.targetview, animated: true)
                                                    if(self.comeFromWelcome)
                                                    {
                                                        let myHome = AppDelegate.RoutToScreen("VcHome") as! VcHome;
                                                        myHome.invitAlertMessage = "Invite Code: \((User?.InviteCode)!)\nI have just shared post which is:\(self.TxtPost.text)"
                                                        myHome.invitAlertMessageId = self.postid
                                                        myHome.invitAlertMessageType = self.FromScreen.rawValue
                                                    }else
                                                    {
                                                        if (self.Createblock != nil)
                                                        {
                                                            self.Createblock()
                                                            
                                                        }
                                                        
                                                        
                                                        // self.navigationController?.popViewControllerAnimated(true)
                                                    }
                                                    
                                            })
                                        }
                                        // end video thumbunail
                                    }
                                }
                        })
                        
                    }
                    
                    if self.FromScreen == .CreateFashion {
                        var flage = false
                        
                        for x in (navigationController?.viewControllers)!
                        {
                            
                            if x.isKindOfClass(VcFashionPostOptions)
                            {
                                flage = true
                            }
                        }
                        
                        
                        if (flage){
                            self.navigationController?.popToViewController((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 3])!, animated: true)
                        }
                        else
                        {
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                    }else{
                        
                        
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                    
                    
                }// end upload video
                else
                { // Txt only
                    if(comeFromWelcome)
                    {
                        let myHome = AppDelegate.RoutToScreen("VcHome") as! VcHome;
                        
                        myHome.invitAlertMessage =  "Invite Code: \((User?.InviteCode)!)\nI have just shared post:\(self.TxtPost.text)"
                        myHome.invitAlertMessageId = postid
                        myHome.invitAlertMessageType = self.FromScreen.rawValue
                        
                    }else
                    {
                        if (self.Createblock != nil)
                        {
                            self.Createblock()
                        }
                        
                        if (FromScreen == .CreateFashion && self.navigationController?.viewControllers.count > 2) {
                            var flage = false
                            
                            for x in (navigationController?.viewControllers)!
                            {
                                
                                if x.isKindOfClass(VcFashionPostOptions)
                                {
                                    flage = true
                                }
                            }
                                
                                
                                
                                
                                
                            if (flage){
                            self.navigationController?.popToViewController((self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 3])!, animated: true)
                            }
                            else
                            {
                                self.navigationController?.popViewControllerAnimated(true)
                            }
                            
                            
                            
                            
                        }else{
                            
                            
                            self.navigationController?.popViewControllerAnimated(true)
                        }
                        
                        //  self.navigationController?.popViewControllerAnimated(true)
                    }
                }
                //  }
                
                
            }// end sucess post  ShowRey
            
        }
        
        
        
    }
    
    
    //# MARK:- SharePost
    
    static func SharePost(FeedObject:ModFeedFeedobj ,PostToType:Int = 1 , callBack:( IsSuccess : Bool ,  Description : String)->())
    {
        //         print("started share")
        var SharedPostId : Int!
        
        if ( FeedObject.SharedPostId?.intValue > 0) // to share main post
        {
            SharedPostId = FeedObject.SharedPost!.Id!.integerValue as Int
        }
        else
        {
            SharedPostId = FeedObject.Id!.integerValue as Int
        }
        
        var RequestParameters : NSDictionary = NSDictionary();
        //var PostOb : NSDictionary = NSDictionary();
        
        let PostOb = [
            
            "PostCType":ContentType.Shared.rawValue, // text image vid
            "PostToId":[(User?.Id)!],
            "PostToType":PostToType,
            "PostType": FeedObject.PostType!.integerValue as Int,// 1 regular 2 fashion   //???
            "Text":"String content",
            "SharedPostId":SharedPostId,
            "UserId":(User?.Id)!
        ]
        
        var CItems  :[NSDictionary] = [NSDictionary]()
        if let citems = FeedObject.CItems
        {
            for var citem in citems
            {
                let CItem : NSDictionary = [
                    "Id":citem.It_Id!,
                    "Name":citem.It_Name!
                ]
                CItems.append(CItem)
                
            }
        }
        
        // var citemsReqJSON = citemsReq.toJsonString()
        
        //        citemsReqJSON = String(citemsReqJSON.characters.dropLast())
        //        citemsReqJSON = String(citemsReqJSON.characters.dropFirst())
        
        var ProfileProsJSON = FeedObject.ProfilePros
        // print("prof \(ProfileProsJSON)")
        //        ProfileProsJSON = String(ProfileProsJSON.characters.dropLast())
        //        ProfileProsJSON = String(ProfileProsJSON.characters.dropFirst())
        
        var ProfilePros : NSDictionary!
        if ProfileProsJSON != nil
        {
            ProfilePros = [
                "AgeGroup":ProfileProsJSON!.AgeGroup!,
                "BodyShape":ProfileProsJSON!.BodyShape!,
                "Gender":ProfileProsJSON!.Gender!,
                "Height":ProfileProsJSON!.Height!,
                "Size":ProfileProsJSON!.Size!, //
                "Weight":ProfileProsJSON!.Weight!
            ]
        }
        
        
        
        
        RequestParameters =
            [
                "PostOb" : PostOb,
                "UserId" : (User?.Id)!,
                "CItems" : CItems,
                "ProfilePros" : ProfilePros ?? ""
        ]
        
        //  print("prof \(ProfileProsJSON)")
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.PostShowrey, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString : { response in
                if((response.result.error) != nil)
                {
                    if let networkError =  response.result.error
                    {
                        if (networkError.code == -1009) {
                            callBack(IsSuccess: false, Description: "No Internet connection")
                        }
                        else
                        {
                            callBack(IsSuccess: false, Description: "An error has been occurred")
                            
                        }
                        return
                    }
                }
                else
                {
                    if let JSON = response.result.value
                    {
                        
                        print("---result = "+JSON)
                        let res = ModResponse(json: JSON)
                        if (res.ResultResponse == "0") //success
                        {
                            callBack(IsSuccess: true, Description: "Post Shared")
                        }
                        else
                        {
                            callBack(IsSuccess: false, Description: "Faild to Share")
                        }
                    }
                }
            },callbackDictionary: nil)
        
        
        
        
    }
    @IBAction func BtnDress(sender: AnyObject) {
        
        if(!DressFlag)
        {
            ChkDress.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
        }
        else
        {
            ChkDress.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
        }
        DressFlag = !DressFlag;
        
    }
    
    @IBAction func BtnTop(sender: AnyObject) {
        if(!TopFlag)
        {
            ChkTop.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
        }
        else
        {
            ChkTop.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
        }
        TopFlag = !TopFlag;
    }
    
    @IBAction func BtnBottom(sender: AnyObject) {
        if(!BottomFlag)
        {
            ChkBottom.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
        }
        else
        {
            ChkBottom.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
        }
        BottomFlag = !BottomFlag;
    }
    
    @IBAction func BtnAccessories(sender: AnyObject) {
        if(!AccessoriesFlag)
        {
            ChkAccessories.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
        }
        else
        {
            ChkAccessories.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
        }
        AccessoriesFlag = !AccessoriesFlag;
    }
    @IBAction func BtnPures(sender: AnyObject) {
        if(!PurseFlag)
        {
            ChkPurse.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
        }
        else
        {
            ChkPurse.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
        }
        PurseFlag = !PurseFlag;
    }
    @IBAction func BtnShoes(sender: AnyObject) {
        if(!ShoesFlag)
        {
            ChkShoes.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
        }
        else
        {
            ChkShoes.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
        }
        ShoesFlag = !ShoesFlag;
    }
    @IBAction func BtnOther(sender: AnyObject) {
        if(!OtherFlag)
        {
            ChkOther.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
            TxtOther.enabled = true
        }
        else
        {
            ChkOther.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
            TxtOther.enabled = false
        }
        OtherFlag = !OtherFlag;
    }
    
    
    
    @IBAction func ValidOfferAction(sender: AnyObject) {
        
        
        openDateSelectionViewController()
        
    }
    
    
    
    func openDateSelectionViewController() {
        let style = RMActionControllerStyle.White
        
        // style = RMActionControllerStyle.Black
        
        
        let selectAction = RMAction(title: "Select", style: RMActionStyle.Done) { controller in
            
            if let dateController = controller as? RMDateSelectionViewController {
                self.s = dateController;
                
                print("Successfully selected date: ", dateController.datePicker.date);
                
                
                
                if dateController.datePicker.date.earlierDate(NSDate()).isEqualToDate(dateController.datePicker.date)  {
                    dateController.datePicker.date = NSDate();
                }
                
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                self.mydate = dateFormatter.stringFromDate(dateController.datePicker.date)
                
                
                let dateFormatter2 = NSDateFormatter()
                dateFormatter2.dateFormat = "yyyy-MM-dd"
                // dateFormatter.timeZone = NSTimeZone(name: "UTC")
                let yourdate = dateFormatter2.stringFromDate(dateController.datePicker.date)
                
                self.btnValidTo.setTitle(yourdate, forState: .Normal)
                
                
            }
        }
        
        let cancelAction = RMAction(title: "Cancel", style: RMActionStyle.Cancel) { _ in
            print("Date selection was canceled")
        }
        
        let actionController = RMDateSelectionViewController(style: style, title: "", message: "", selectAction: selectAction, andCancelAction: cancelAction)!;
        
        let nowAction = RMAction(title: "Today", style: .Additional) { controller -> Void in
            if let dateController = controller as? RMDateSelectionViewController {
                dateController.datePicker.date = NSDate();
                print("Now button tapped");
            }
        }
        nowAction!.dismissesActionController = false;
        
        actionController.addAction(nowAction!);
        
        
        //You can access the actual UIDatePicker via the datePicker property
        actionController.datePicker.datePickerMode = .Date;
        actionController.datePicker.minuteInterval = 5;
        actionController.datePicker.date = NSDate(timeIntervalSinceReferenceDate: 0);
        
        //On the iPad we want to show the date selection view controller within a popover. Fortunately, we can use iOS 8 API for this! :)
        //(Of course only if we are running on iOS 8 or later)
        if actionController.respondsToSelector(Selector("popoverPresentationController:")) && UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            //First we set the modal presentation style to the popover style
            actionController.modalPresentationStyle = UIModalPresentationStyle.Popover
            
            //Then we tell the popover presentation controller, where the popover should appear
            if let popoverPresentationController = actionController.popoverPresentationController {
                popoverPresentationController.sourceView = self.view
                // popoverPresentationController.sourceRect = self.tableView.rectForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
            }
        }
        
        //Now just present the date selection controller using the standard iOS presentation method
        presentViewController(actionController, animated: true, completion: nil)
    }
    
    
    
    
    //# MARK:- AutoComplete textViewDidChange
    func textViewDidChange(textView: UITextView)
    {
        
        if (textView.text.isEqual("")) {
            _myranges =  []
            _TaggedUserId = NSMutableArray()
            _MyTaggedUserIdComplete =  NSMutableArray()
            autoCompleteContainer.hidden = true
            self.autoCompleteMgr.container = self.autoCompleteContainer;
        }else
        {
            
            autoCompleteMgr.processString(textView.text)
            
            if ( autoCompleteContainer.subviews.count < 1 )
            {
                autoCompleteContainer.hidden = true
            }
            if (textView.text.hasSuffix(" ") == true)
            {
                self.TxtPost.attributedText = formateTaggingString(self.TxtPost.text)
                autoCompleteContainer.hidden = true
            }
            
        }
        
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        self.TxtPost.attributedText = formateTaggingString(self.TxtPost.text)
    }
    
    // MARK:- AutoComplete Manager
    func autoCompleteManager(acManager: MJAutoCompleteManager!, itemListForTrigger trigger: MJAutoCompleteTrigger!, withString string: String!, callback: MJAutoCompleteListCallback!){
        
        /* Since we implemented this method, we are stuck and must handle ALL triggers */
        
        if (trigger.delimiter.isEqual("@")){
            if (!self.didStartDownload)
            {
                
                var itemList : [MJAutoCompleteItem] = []
                let myitems = TxtPost.text.componentsSeparatedByString("@")
                let mystring = myitems.last
                
                // self.didStartDownload = true;
                if (mystring == "") {return} // just work when there are char
                
                // network helper here
                
                let RequestParameters : NSDictionary = [
                    "Keyword" : mystring!,
                    "UserId": (User?.Id)!
                ]
                
                let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
                
                NetworkHelper.RequestHelper(nil, service: WSMethods.AutoCompleteTaggedUser, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
                    , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: { (JSON, NSError) in
                        
                        
                        if((NSError) != nil)
                        {
                            if let networkError = NSError {
                                if (networkError.code == -1009) {
                                    self.view.makeToast("No Internet connection")
                                }
                                else
                                {
                                    self.view.makeToast("An error has been occurred")
                                    print("error")
                                }
                                return
                            }
                        }
                        else
                        {
                            
                            
                            if ( JSON["ResultResponse"].stringValue == "0")
                            {
                                let Profileobjobj =   JSON["Followersobj"]
                                
                                let Response = ModListOfPeoples(json: JSON.rawString())
                                
                                self.ListOfTaggedPeoples.appendContentsOf(Response.Followersobj!)
                                
                                var ContextDictiony  = [String:String]()
                                
                                
                                for commentDictionary in Profileobjobj  {
                                    
                                    let item : MJAutoCompleteItem = MJAutoCompleteItem ()
                                    
                                    item.autoCompleteString = commentDictionary.1["UserName"].rawString()
                                    
                                    ContextDictiony["UserName"] = commentDictionary.1["UserName"].rawString()
                                    ContextDictiony["Id"] = commentDictionary.1["Id"].rawString()
                                    ContextDictiony["FullName"] = commentDictionary.1["FullName"].rawString()
                                    ContextDictiony["ProfilePicture"] = commentDictionary.1["ProfilePicture"].rawString()
                                    ContextDictiony["FollowStatus"] = commentDictionary.1["FollowStatus"].rawString()
                                    
                                    
                                    
                                    item.context = ContextDictiony
                                    
                                    itemList.append(item)
                                }
                                
                                self.enterAutoComplete = true
                                
                                trigger.autoCompleteItemList = itemList;
                                
                                callback(itemList);
                                
                            }
                        }
                        
                        
                })
                
            }
            else
            {
                
                callback(trigger.autoCompleteItemList);
            }
        }
        
    }
    
    
    
    
    func autoCompleteManager(acManager: MJAutoCompleteManager!, willPresentCell autoCompleteCell: AnyObject!, forTrigger trigger: MJAutoCompleteTrigger!)
        
    {
        
        self.autoCompleteContainer.hidden = false;
        if (trigger.delimiter.isEqual("@"))
        {
            
            
            let cell :autoComCell = autoCompleteCell as! autoComCell;
            cell.parent = self
            cell.CustomautoCompleteItem(cell.autoCompleteItem)
            
        }
    }
    
    
    func autoCompleteManager(acManager: MJAutoCompleteManager!, shouldUpdateToText newText: String!, selectedItem: MJAutoCompleteItem!)
    {
        self.TxtPost.text = newText;
        self.autoCompleteContainer.hidden = true;
        self.TxtPost.attributedText = formateTaggingString(self.TxtPost.text)
        //selectedItem.context.values(")
        
        _TaggedUserId.addObject(selectedItem.context["Id"]!)
        _MyTaggedUserIdComplete.addObject(selectedItem.context)
        
    }
    
    
    // MARK:- Attributed String
    func formateTaggingString(str:String) -> NSAttributedString
    {
        
        let attstring = NSMutableAttributedString(string: str);
        let mycolor : UIColor = UIColor(hex: "#0000EE")
        
        
        
        let splitSpace = str.componentsSeparatedByString(" ")
        
        for sub in splitSpace{
            
            if sub.hasPrefix("@") || sub.hasPrefix("#") {
                _myranges.append(sub)
            }
        }
        
        for object in _myranges {
            
            attstring.addAttribute(NSForegroundColorAttributeName, value: mycolor, range:(self.TxtPost.text as NSString).rangeOfString(object))
            attstring.addAttribute(NSUnderlineStyleAttributeName, value: Int(1), range: (self.TxtPost.text as NSString).rangeOfString(object))
            
        }
        
        
        return attstring
    }
    
    
    
}
