//
//  TcFeadsTip.swift
//  ShowRey
//
//  Created by M-Hashem on 11/23/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class TcFeadsTip: UITableViewCell
{

    @IBOutlet weak var TipTitle: UILabel!
    @IBOutlet weak var TipDetails: UILabel!
    @IBOutlet weak var shade: UIView!
    @IBOutlet weak var Content: UIView!
    var parent:VcMasterNewsFeed!
    var tipdata:ModTip!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Content.layer.cornerRadius = 4
        Content.clipsToBounds = true
        
        let swipeRight = UIPanGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
       // swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        addGestureRecognizer(swipeRight)
        
        addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(tipCliced)))
    }
    func tipCliced()
    {
        let mytips = VcMyTips(nibName: "VcMyTips", bundle: nil)
        parent.parent.navigationController?.pushViewController(mytips, animated: true)
        mytips.InitialTip = tipdata;
    }
    func respondToSwipeGesture(gesture: UIGestureRecognizer)
    {
        if let panGesture = gesture as? UIPanGestureRecognizer
        {
            //        get translation
            
            let translation = panGesture.translationInView(parent.view)
            panGesture.setTranslation(CGPointZero, inView: self)
          //  print(translation)

            center = CGPoint(x: center.x+translation.x, y: center.y)
            
            let distanceFCenter = frame.origin.x < 0 ? -frame.origin.x : frame.origin.x
            
            if panGesture.state == UIGestureRecognizerState.Ended
            {
                print(frame.origin.x)
                if distanceFCenter > frame.width / 3
                {
                    UIView.animateWithDuration(0.6, animations: {
                        if self.frame.origin.x > 0
                        {
                            self.frame.origin.x = self.frame.width
                        }
                        else
                        {
                            self.frame.origin.x = -self.frame.width
                        }
                        },completion:{finished in
                            self.alpha = 0.0
                            self.parent.HideTip(self.tipdata)
                            TcFeadsTip.lastDismisedTip = NSDate()
                        })
                   
                }
                else
                {
                    UIView.animateWithDuration(0.6, animations: {
                        
                            self.frame.origin.x = 0
                        
                        },completion:{finished in
                            
                        })
                }
            }
        }
    }

//    override func setSelected(_ selected: Bool, animated: Bool)
//    {
//        super.setSelected(selected, animated: animated)
//       
//
//        // Configure the view for the selected state
//    }
    
    func SetData(parent_:VcMasterNewsFeed,data:ModTip)
    {
        TipTitle.text = data.Title
        TipDetails.text = data.Text
         parent = parent_
        tipdata = data
    }
    override func layoutSubviews()
    {
        super.layoutSubviews()
         Styles.Shadow(self, target: Content, Radus: 1,shadowview: shade)
    }
    @IBAction func BtnClose(sender: AnyObject)
    {
        parent.HideTip(tipdata)
        
        TcFeadsTip.lastDismisedTip = NSDate()
    }
    
    internal static var lastDismisedTip : NSDate!
    {
        get
        {
            let val = NSUserDefaults.standardUserDefaults().objectForKey("lastDismisedTip")
            if val == nil {return nil}
            return val as! NSDate
        }
        set
        {
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setValue(newValue, forKey: "lastDismisedTip")
            userDefaults.synchronize();
        }
    }
    
}
