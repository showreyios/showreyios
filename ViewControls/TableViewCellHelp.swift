//
//  TableViewCellHelp.swift
//  ShowRey
//
//  Created by Appsinnovate on 1/16/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit

class TableViewCellHelp: AEAccordionTableViewCell {

    
    var parentTable : SampleTableViewController!
    var indexHelp : Int = 0 
    
    
    
    @IBOutlet weak var header_text: UILabel!
    @IBOutlet weak var headerArrow: UIImageView!
    
//    @IBOutlet weak var headerView: HeaderView!{
//        didSet
//        {
//            headerView.collapseArrow.tintColor = UIColor.whiteColor()
//        }
//    }
 
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    
    
  //  @IBOutlet weak var detailView: DetailView!

    
    @IBOutlet weak var details_Text: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setExpanded(expanded: Bool, animated: Bool) {
        super.setExpanded(expanded, animated: animated)

        if !animated {
            toggleCell()
        } else {
            let alwaysOptions: UIViewAnimationOptions = [.AllowUserInteraction, .BeginFromCurrentState, .TransitionCrossDissolve]
            let expandedOptions: UIViewAnimationOptions = [.TransitionFlipFromTop, .CurveEaseOut]
            let collapsedOptions: UIViewAnimationOptions = [.TransitionFlipFromBottom, .CurveEaseIn]
            let options: UIViewAnimationOptions = expanded ? alwaysOptions.union(expandedOptions) : alwaysOptions.union(collapsedOptions)
           
            UIView.transitionWithView(details_Text, duration: 0.3, options: options, animations: { () -> Void in
                self.toggleCell()
                }, completion: nil)
        }

    }
    
    private func toggleCell()
    {
        
        details_Text.hidden = !expanded
        headerArrow.transform = expanded ? CGAffineTransformMakeRotation(CGFloat(M_PI)) : CGAffineTransformIdentity
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
