//
//  TvSettings.swift
//  ShowRey
//
//  Created by User on 11/7/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import Firebase
import FirebaseCrash
import FirebaseInstanceID
import FirebaseMessaging


class TvSettings: UITableViewController {
    
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Settings"
        
        let button = UIButton()
        button.frame = CGRectMake(0, 0, 21, 31)
        button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
        button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
        let barButton = UIBarButtonItem()
        barButton.customView = button
        navigationItem.leftBarButtonItem = barButton
        
        
        notificationSwitch.addTarget(self, action: #selector(TvSettings.switchValueDidChange(_:)), forControlEvents: .ValueChanged);
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func switchValueDidChange(sender:UISwitch!){
        
        // print("Switch Value : \(sender.on))")
        
        
        if sender.on {
            
            UIApplication.sharedApplication().registerForRemoteNotificationTypes(([.Badge, .Sound, .Alert]))
            
            let settingsURL = NSURL(string: UIApplicationOpenSettingsURLString)!
            UIApplication.sharedApplication().openURL(settingsURL)
            
            
        }else {
            
            UIApplication.sharedApplication().unregisterForRemoteNotifications()
            let settingsURL = NSURL(string: UIApplicationOpenSettingsURLString)!
            UIApplication.sharedApplication().openURL(settingsURL)
        }
        
    }
    
    
    
    @IBAction func deactivateAccount(sender: AnyObject) {
        
        print("helloooooooo")
        
        
        let alertController = UIAlertController(title: "ARE YOU SURE?", message: "", preferredStyle: .Alert)
        
        
        let deactivateAction = UIAlertAction(title: "Deactivate Account", style: .Destructive , handler: {
            (alert: UIAlertAction!) -> Void in
            
            let passwordTextField = alertController.textFields![0] as UITextField
            self.deActivate(passwordTextField.text!)
            
            
        })
        
        deactivateAction.enabled = false
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { _ in }
        
        alertController.addTextFieldWithConfigurationHandler({textField in
            
            textField.placeholder = "Password"
            textField.secureTextEntry = true
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { notification in
                deactivateAction.enabled = textField.text != ""
            }
            
            
        })
        
        
        alertController.addAction(deactivateAction)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func logout(sender: AnyObject) {
        
        let TvAboutUs_=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("TvAboutUs")
        openedScreen = TvAboutUs_
        sideMenuViewController?.contentViewController = UINavigationController(rootViewController: TvAboutUs_)
        sideMenuViewController?.hideMenuViewController()
        
        
        /*
         let alertController = UIAlertController(title: "ARE YOU SURE?", message: "", preferredStyle: .Alert)
         
         
         let logoutAction = UIAlertAction(title: "Log Out", style: .Destructive , handler: {
         (alert: UIAlertAction!) -> Void in
         
         self.logout()
         })
         
         let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { _ in }
         
         alertController.addAction(logoutAction)
         alertController.addAction(cancelAction)
         self.presentViewController(alertController, animated: true, completion: nil)
         
         */
    }
    
    func deActivate(pass:String)
    {
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Deactivating..."
        
        
        let RequestParameters : NSDictionary = [
            
            "Password": pass,
            "UserId": (User?.Id)!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.DeactivateAccount, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    
                    if( resultResponse == "0"){
                        
                        //                        let appDomain = NSBundle.mainBundle().bundleIdentifier!
                        //                        NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
                        //                        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "launchedBefore")
                        //                        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                        //                        self.view.makeToast("Your account has been deactivated", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        //                        AppDelegate.RoutToScreen("VcLogin")
                        
                        let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                        
                        var foundview = false
                        for reportG : UIViewController in allViewController
                        {
                            if reportG .isKindOfClass(VcLogin)
                            {
                                
                                let cast = reportG as! VcLogin
                                
                                foundview  = true
                                
                                let appDomain = NSBundle.mainBundle().bundleIdentifier!
                                NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
                                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "launchedBefore")
                                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                                
                                self.navigationController?.popToViewController(reportG, animated: true)
                                cast.view.makeToast("Your account has been deactivated", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                                
                            }
                        }
                        if (foundview == false)
                        {
                            
                            let report = self.storyboard?.instantiateViewControllerWithIdentifier("VcLogin") as! VcLogin
                            
                            let appDomain = NSBundle.mainBundle().bundleIdentifier!
                            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
                            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "launchedBefore")
                            UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                            
                            self.navigationController?.pushViewController(report, animated: true)
                            report.view.makeToast("Your account has been deactivated", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        
                        
                    }else{
                        
                        self.view.makeToast(response.JSON["Description"].stringValue, duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
        })
        
    }
    
    func logout(){
        
        
        // self.tableView.makeToast("Logout", duration: 5, position: CGPoint(x: self.view.frame.size.width/2 , y: 450))
        let RequestParameters : NSDictionary = [
            "UserId":(User?.Id)!
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        NetworkHelper.RequestHelper(nil, service: WSMethods.Logout, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: jsonCallBack2)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
    }
    
    func jsonCallBack2(response: (JSON: JSON, NSError: NSError?))
    {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    
                    self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                }
                else
                {
                    self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                    
                    print("error")
                }
                return
            }
        }
        else
        {
            let resultResponse = response.JSON["ResultResponse"].stringValue
            if( resultResponse == "0"){
                
                var regId = FIRInstanceID.instanceID().token()
                if regId == nil {
                    
                    regId = NSUserDefaults.standardUserDefaults().stringForKey("regId")
                }
                
                let appDomain = NSBundle.mainBundle().bundleIdentifier!
                
                NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "launchedBefore")
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                NSUserDefaults.standardUserDefaults().setObject(regId, forKey: "regId")
                AppDelegate.RoutToScreen("VcLogin")
                
            }else{
                
                self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                
            }
            
        }
        
    }
    
    
    // MARK: - Table view data source
    /*
     override func numberOfSections(in tableView: UITableView) -> Int {
     // #warning Incomplete implementation, return the number of sections
     return 0
     }
     
     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     // #warning Incomplete implementation, return the number of rows
     return 0
     }
     */
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
