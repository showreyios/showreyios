//
//  CcTest3.swift
//  ShowRey
//
//  Created by User on 10/18/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class CcImageGallery: UICollectionViewCell {
    
    @IBOutlet weak var btnDeselect: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnCrop: UIButton!
    @IBOutlet weak var btnShape: UIButton!
    @IBOutlet weak var btnShapeText: UIButton!
    @IBOutlet weak var btnShapeRadio: UIButton!
    
}
