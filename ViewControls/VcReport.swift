//
//  VcReport.swift
//  ShowRey
//
//  Created by User on 11/5/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import FirebaseAnalytics

class VcReport: UIViewController {
    // targetiD = post id
    // typeid = option
    // reportid = type
    enum ComeFrom : Int{
        case Post = 1 //reportId
        case User = 2
        case Group = 3
        case Comment = 4
        case Offer = 5
        
    }
    
    var userId:String!
    var groupId:Int!
    var come = ComeFrom.Post
    var feed:ModFeedFeedobj!
    var typeId:Int!
    var CommentID:Int!
    var offerId:Int!
    
    
    @IBOutlet weak var btn1: UIButton!
    
    @IBOutlet weak var btn2: UIButton!
    
    @IBOutlet weak var btn3: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "Report"
        
        if come == ComeFrom.Post || come == .Offer {
            
            btn1.setTitle("  This is inappropriate for Showrey", forState: .Normal) //1 typeID
            btn2.setTitle("  It’s Spam", forState: .Normal) //2
            btn3.hidden = true
        }else if come == ComeFrom.User {
            
            btn1.setTitle("  Report This Profile", forState: .Normal) //3
            btn2.setTitle("  Report The image", forState: .Normal)//4
            btn3.setTitle("  This is fake account", forState: .Normal)//5
            btn3.hidden = false
            
        }else if come == ComeFrom.Group {
            
            btn1.setTitle("  Inappropriate Content", forState: .Normal)//6
            btn2.setTitle("  Incorrect information", forState: .Normal)//7
            
            btn3.hidden = true
        }else if come == ComeFrom.Comment {
            
            btn1.setTitle("  This is inappropriate", forState: .Normal) //8
            btn2.setTitle("  It’s Spam", forState: .Normal) //9
            
            btn3.hidden = true
        }
        FIRAnalytics.logEventWithName("Report", parameters: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn1Action(sender: AnyObject) {
        
        btn1.setImage(UIImage(named: "Radio-btn-selected"), forState: .Normal)
        btn2.setImage(UIImage(named: "Radio-btn"), forState: .Normal)
        btn3.setImage(UIImage(named: "Radio-btn"), forState: .Normal)
        
        if come == ComeFrom.Post || come == .Offer{
            
            typeId = 1
            
        }else if come == ComeFrom.User {
            
            typeId = 3
            
        }else if come == ComeFrom.Group {
            
            typeId = 6
            
        }else if come == ComeFrom.Comment {
            
            typeId = 8
            
        }
        
        
    }
    
    @IBAction func btn2Action(sender: AnyObject) {
        
        btn2.setImage(UIImage(named: "Radio-btn-selected"), forState: .Normal)
        btn1.setImage(UIImage(named: "Radio-btn"), forState: .Normal)
        btn3.setImage(UIImage(named: "Radio-btn"), forState: .Normal)
        
        if come == ComeFrom.Post || come == .Offer{
            
            typeId = 2
            
        }else if come == ComeFrom.User {
            
            typeId = 4
            
        }else if come == ComeFrom.Group {
            
            typeId = 7
            
        }else if come == ComeFrom.Comment {
            
            typeId = 9
            
        }
        
        
    }
    
    @IBAction func btn3Action(sender: AnyObject) {
        
        btn3.setImage(UIImage(named: "Radio-btn-selected"), forState: .Normal)
        btn2.setImage(UIImage(named: "Radio-btn"), forState: .Normal)
        btn1.setImage(UIImage(named: "Radio-btn"), forState: .Normal)
        
        typeId = 5
    }
    
    
    @IBAction func btnReport(sender: AnyObject) {
        
        
        if !(typeId == nil) {
            
            var target:String!
            
            if userId != nil {
                
                target = userId
                
            }else if groupId != nil {
                
                target = String(groupId)
            }else if CommentID != nil {
                
                target = String(CommentID)
            }else if (feed.Id?.stringValue)! != nil {
                
                target = (feed.Id?.stringValue)!
            }
//            else if offerId != nil {
//                
//                target = String(offerId)
//            }
            
            
            
            let RequestParameters : NSDictionary = [
                "ReportId":come.rawValue,
                "TargetId":target,
                "TypeId":typeId,
                "UserId":(User?.Id)!
            ]
            let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
            NetworkHelper.RequestHelper(nil, service: WSMethods.Report, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
                , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: jsonCallBack2)
            
            let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            loadingNotification.label.text = "Loading..."
            
        }else{
            
            self.view.makeToast("Please choose the reason that you want to report for !?")
        }
    }
    
    func jsonCallBack2(response: (JSON: JSON, NSError: NSError?))
    {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    self.view.makeToast("No Internet connection")
                }
                else
                {
                    self.view.makeToast("An error has been occurred")
                    print("error")
                }
                return
            }
        }
        else
        {
            let resultResponse = response.JSON["ResultResponse"].stringValue
            if( resultResponse == "0"){
                
                self.navigationController?.popViewControllerAnimated(true)
                
                //self.view.makeToast("Success")
            }else{
                self.view.makeToast("An error has been occurred")
            }
            
        }
        
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
