//
//  VcFollorwes.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 11/19/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD

class VcFollorwes_Likers_Search: UIViewController {
    
    
    @IBOutlet weak var containerview: UIView!
    var vcListOfPeoples : VcListOfPeoples!// =  VcListOfPeoples()
    var rootParent : UIViewController!
    
    var vcFollowContainer :VcFollowContainer!
    var vcCommentsContainer : VcCommentsContainer!
    
    var PagesCount = 1
    
    //Likers
    var LevelId = -1
    var LikeType = true
    var ObjId = -1
    
    //ItemsLikers
    var ItemId = -1
    var PostId = -1
    
    //PeopleSearch
    var Keyword = ""
    
    var PageTitle = "Follow"
    
    enum ScreenMod : Int{
        case Folloing = 0
        case Follorwes = 1
        case Likers = 2
        case ItemsLikers = 3
        case OffersLikers = 4
        case PeopleSearch = 5
    }
    var MyScreenMod : ScreenMod = .Folloing
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = PageTitle
        
        if MyScreenMod == .PeopleSearch
        {
            PeopleSearchMod(Keyword)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func FollorwesMod()
    {
        MyScreenMod =  .Follorwes
        Load_Follorwes_Likers_Search()
    }
    
    func FolloingMod()
    {
        MyScreenMod =  .Folloing
        Load_Follorwes_Likers_Search()
    }
    
    func LikersMod(LevelId:Int , LikeType:Bool , ObjId:Int)
    {
        self.LevelId = LevelId
        self.LikeType = LikeType
        self.ObjId = ObjId
        
        MyScreenMod =  .Likers
        Load_Follorwes_Likers_Search()
    }
    
    func ItemsLikersMod(ItemId:Int , LikeType:Bool , PostId:Int)
    {
        self.ItemId = ItemId
        self.LikeType = LikeType
        self.PostId = PostId
        
        MyScreenMod =  .ItemsLikers
        Load_Follorwes_Likers_Search()
    }
    
    func OffersLikersMod(LevelId:Int , LikeType:Bool , ObjId:Int)
    {
        self.LevelId = LevelId
        self.LikeType = LikeType
        self.ObjId = ObjId
        
        MyScreenMod =  .OffersLikers
        Load_Follorwes_Likers_Search()
    }
    
    
    func PeopleSearchMod(Keyword:String)
    {
       
      // vcListOfPeoples = VcListOfPeoples()
        
        self.Keyword = Keyword
        MyScreenMod =  .PeopleSearch
        Load_Follorwes_Likers_Search(false)
    }
    
    
    func Load_Follorwes_Likers_Search(WithPrgoress : Bool = true , AddPagetoInfinit : Bool = false){
    
        if (WithPrgoress ) {MBProgressHUD.showHUDAddedTo(self.view, animated: true).label.text = "Loading..."}
        
        if (AddPagetoInfinit) { PagesCount += 1 } else { PagesCount = 1}
        
        var RequestParameters : NSDictionary = NSDictionary()
        
        
        var servise = WSMethods.GetFollowees
        
        
        switch MyScreenMod {
        case .Folloing  :
            servise = WSMethods.GetFollowees
            
            RequestParameters =
                [   "UserId":User!.Id!,
                    "PageIndex":PagesCount,
                    "PageSize":50,
                    "ProfileUserId":User!.Id!
            ]
        case .Follorwes  :
            servise = WSMethods.GetFollowers
            RequestParameters =
                [   "UserId":User!.Id!,
                    "PageIndex":PagesCount,
                    "PageSize":50,
                    "ProfileUserId":User!.Id!
            ]
        case .Likers  :
            servise = WSMethods.GetLikers
            RequestParameters =
                [   "LevelId":self.LevelId,
                    "LikeType":self.LikeType,
                    "ObjId":self.ObjId,
                    "PageIndex":PagesCount,
                    "PageSize":50,
                    "UserId":User!.Id!
            ]
        case .ItemsLikers  :
            servise = WSMethods.GetPostItemsLikers
            RequestParameters =
                [   "ItemId":self.ItemId,
                    "LikeType":self.LikeType,
                    "PageIndex":PagesCount,
                    "PageSize":50,
                    "PostId":self.PostId,
                    "UserId":User!.Id!
            ]
        case .OffersLikers  :
            servise = WSMethods.GetSpecialOffersLikers
            RequestParameters =
                [   "LevelId":self.LevelId,
                    "LikeType":self.LikeType,
                    "ObjId":self.ObjId,
                    "PageIndex":PagesCount,
                    "PageSize":50,
                    "UserId":User!.Id!
            ]
        case .PeopleSearch  :
            servise = WSMethods.PeopleSearch
            RequestParameters =
                [   "Keyword":self.Keyword,
                    "PageIndex":PagesCount,
                    "PageSize":50,
                    "UserId":User!.Id!
            ]

        }
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        
        NetworkHelper.RequestHelper(nil, service:servise, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
               
               if (WithPrgoress )
               {
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                }
               // MBProgressHUD.hideHUDForView(self.view, animated: true)
                if(response.result.isFailure)
                {
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.view.makeToast("No Internet connection")
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        let modListOfPeoples = ModListOfPeoples(json:JSON)
                        
                        if (self.PagesCount > 1){
                            
                            self.vcListOfPeoples.ListOfPeoplesArray.appendContentsOf(modListOfPeoples.Followersobj!)
                            
                        }
                        else
                        {
                            let f = modListOfPeoples.Followersobj ?? []
                            self.vcListOfPeoples.ListOfPeoplesArray = f
                        }
                        
                        self.vcListOfPeoples.ReloadTable()
                    }
                }
                
            }
            , callbackDictionary: nil)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let DV = segue.destinationViewController as? VcListOfPeoples
            where segue.identifier == "Follorwes_Likers_Search_Segue" {
            DV.vcFollowContainer = vcFollowContainer
             DV.vcCommentsContainer = vcCommentsContainer
             DV.vcFollorwes_Likers_Search = self
                vcListOfPeoples = DV
                vcListOfPeoples.ReloadTablefromWebService = {
                (AddPagetoInfinit : Bool)in
                if (AddPagetoInfinit){self.Load_Follorwes_Likers_Search(false,AddPagetoInfinit: true)} // with Infinit
                else{
                    self.Load_Follorwes_Likers_Search(false) // without  Infinit
                }
                
            }
        }
    }
    
    
}
