//
//  TcCommentCellLevel1.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 12/2/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import KILabel
import SSImageBrowser
class TcCommentCellLevel2: UITableViewCell,SSImageBrowserDelegate {
    
    @IBOutlet weak var ViewLinkConstrians: NSLayoutConstraint!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var LblUserName: UILabel!
    @IBOutlet weak var LblDate: UILabel!
    
    @IBOutlet weak var CommentPhoto: UIImageView!
    
    @IBOutlet weak var TxtComment: KILabel!
    
    var parentView : VcCommentsContainer!
    var IsItSpecialOffer : Bool = false
    
    @IBOutlet weak var BodyView: UIView!
    @IBOutlet weak var LinkBodyView: UIView!
    
    @IBOutlet weak var NumLikes: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
    
    
    
    @IBOutlet weak var imgDislike: UIImageView!
    @IBOutlet weak var NumDislikes: UILabel!
    

   
    
     var rurl = ""
    func LinkPreviewClick()
    {
        attemptOpenURL(rurl)
    }
    
    
    var GmodCommentsFollowersobj: ModCommentsFollowersobj!
    
    func setCell(modCommentsFollowersobj : ModCommentsFollowersobj)
    {
        
      //  CommentPhoto = nil
       // CommentPhoto = UIImageView()
        
        CommentPhoto.image = nil
        LinkBodyView.hidden = true
        ViewLinkConstrians.constant = 15;
        
        for x in LinkBodyView.subviews
        {
            x.removeFromSuperview()
        }
        
        
        GmodCommentsFollowersobj = modCommentsFollowersobj
        
        
        LblUserName.text = modCommentsFollowersobj.FullName
        
        
        NumLikes.text = "\(modCommentsFollowersobj.NoOfLikes!) Yes"
        
        NumDislikes.text = "\(modCommentsFollowersobj.NoOfDilikes!) No"
        
        //# MARK:  Profile
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
        self.profileImage.clipsToBounds = true;
        let str : String? = (modCommentsFollowersobj.ProfilePicture)!
        let url = NSURL(string:str!)
        profileImage.kf_setImageWithURL(url,placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        
        if(modCommentsFollowersobj.AttachedFiles != nil){
            ViewLinkConstrians.constant = 220;
            let url2 = NSURL(string:modCommentsFollowersobj.AttachedFiles![0].URL!)
            CommentPhoto.kf_setImageWithURL(url2,placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: nil)}
        else
        {
            ViewLinkConstrians.constant = 230
            let rect = CGRect(x: 0 , y: 0 , width:LinkBodyView.frame.size.width , height: ViewLinkConstrians.constant - 150)
            let linkView = UiLinkPreview.linkPreview(nil, modCommentsFollowersobj: GmodCommentsFollowersobj, prameterNumber: 2, frame: rect)
            
            if linkView != nil
            {
                LinkBodyView.hidden = false
                linkView?.lblUrl.text
                //   ViewLinkConstrians.constant = (linkView?.frame.height)!
                LinkBodyView.addSubview(linkView!)
                rurl=linkView!.fullurl
                LinkBodyView.hidden = false
                linkView!.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LinkPreviewClick)))
                
            }
            else {ViewLinkConstrians.constant = 15
                LinkBodyView.hidden = true
            }
        }

        
        if (modCommentsFollowersobj.Liked){
            imgLike.image = UIImage(named: "Offers-(Like)")
            NumLikes.textColor =  UIColor(hex: "#b379e7")
        }
        else
        {
            imgLike.image = UIImage(named: "Offers-(Like)-unselected")
            NumLikes.textColor =  UIColor(hex: "#C1C0C5")
        }
        
        
        if (modCommentsFollowersobj.Disliked){
            imgDislike.image = UIImage(named: "Offers-(DisLike)")
            NumDislikes.textColor =  UIColor(hex: "#b379e7")
        }
        else
        {
            imgDislike.image = UIImage(named: "Offers-(DisLike)-unselected")
            NumDislikes.textColor =  UIColor(hex: "#C1C0C5")
        }
        
        
        
        profileImage.userInteractionEnabled = true
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(TvCreatePost.profileImage(_:)))
        profileImage.addGestureRecognizer(tapRecognizer)
        TxtComment.text = modCommentsFollowersobj.Comment
        LblDate.text = modCommentsFollowersobj.Time
        

        
    
      
        
        // KILABEL
        TxtComment.linkDetectionTypes = [KILinkTypeOption.Hashtag , .URL, .UserHandle]
        // PostText.setAttributes([atrib], forLinkType: .Hashtag)
        TxtComment.userHandleLinkTapHandler = {(label:KILabel,string:String,range:NSRange) in
            
            var selectedUser: ModTaggedUsers? = nil
            for user: ModTaggedUsers in self.GmodCommentsFollowersobj.TaggedUsers!
            {
                if string.containsString(user.UserName!)// && string.containsString(user.FullName!)
                {
                    selectedUser = user
                }
            }
            if selectedUser != nil
            {
                let profile = self.parentView.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
                profile.SetupForTimeLine("\(selectedUser!.Id!.integerValue)")
                self.parentView.navigationController?.pushViewController(profile, animated: true)
            }
        }
        
        TxtComment.hashtagLinkTapHandler = {(label: KILabel, string: String, range: NSRange) -> Void in
            
            let searchMasterViewController = self.parentView.storyboard!.instantiateViewControllerWithIdentifier("VcSearch") as! VcSearch
            searchMasterViewController.initWith(string, posts: true)
            self.parentView.navigationController!.pushViewController(searchMasterViewController, animated: true)
            
        }
        TxtComment.urlLinkTapHandler = {(label: KILabel, string: String, range: NSRange) -> Void in
            // Open URLs
            self.attemptOpenURL(string)
            
        }
        
    }
    
    
    func profileImage(gestureRecognizer: UITapGestureRecognizer) {
        let profile = parentView.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        profile.SetupForTimeLine("\(GmodCommentsFollowersobj.UserId!.integerValue)")
        parentView.navigationController?.pushViewController(profile, animated: true)
        
    }
    
    
    func clickonImage(){
    var photos = [SSPhoto]()
    var photo: SSPhoto!
    if ((CommentPhoto.image) != nil){
    photo = SSPhoto(image: CommentPhoto.image!)
    photo.aCaption = ""
    photos.append(photo)
    
    let browser = SSImageBrowser(aPhotos:  photos)
    
    browser.delegate = self
    browser.displayActionButton = true
    browser.displayArrowButton = true
    browser.displayCounterLabel = true
    browser.usePopAnimation = true
    
    parentView.presentViewController(browser, animated: true, completion: nil)
        
        }

    }
    
    
    func DisLikeAction () {
        
        var  CurrrentLike = false
        
        if (GmodCommentsFollowersobj.Disliked){
            CurrrentLike = false
        }
        else
        {
            CurrrentLike = true
        }
        
        var RequestParameters : NSDictionary = NSDictionary();
        
        
        RequestParameters = [
            "LevelId":1,
            "LikeType":false,
            "LikeValue":CurrrentLike,
            "PostId":(GmodCommentsFollowersobj.Id?.integerValue)!,
            "UserId" : (User?.Id)!,
        ]
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.LikeDislikeComment, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil , callbackDictionary: { (JSON, NSError) in
                
                
                if (self.GmodCommentsFollowersobj.Disliked){
                    
                    self.GmodCommentsFollowersobj.Disliked = false
                    self.GmodCommentsFollowersobj.NoOfDilikes! =  self.GmodCommentsFollowersobj.NoOfDilikes!.integerValue - 1
                    self.NumDislikes.text = "\(self.GmodCommentsFollowersobj.NoOfDilikes!) No"
                    
                    self.imgDislike.image = UIImage(named: "Offers-(DisLike)-unselected")
                    self.NumDislikes.textColor =  UIColor(hex: "#C1C0C5")
                    
                    
                    
                    
                    
                }
                else
                {
                    self.GmodCommentsFollowersobj.Disliked = true
                    self.GmodCommentsFollowersobj.NoOfDilikes! =  self.GmodCommentsFollowersobj.NoOfDilikes!.integerValue + 1
                    self.NumDislikes.text = "\(self.GmodCommentsFollowersobj.NoOfDilikes!) No"
                    self.imgDislike.image = UIImage(named: "Offers-(DisLike)")
                    self.NumDislikes.textColor =  UIColor(hex: "#b379e7")
                    
                    // to deslect the liked
                    if (self.GmodCommentsFollowersobj.Liked){
                        
                        self.GmodCommentsFollowersobj.Liked = false
                        self.GmodCommentsFollowersobj.NoOfLikes! =  self.GmodCommentsFollowersobj.NoOfLikes!.integerValue - 1
                        self.NumLikes.text = "\(self.GmodCommentsFollowersobj.NoOfLikes!) Yes"
                        self.imgLike.image = UIImage(named: "Offers-(Like)-unselected")
                        self.NumLikes.textColor =  UIColor(hex: "#C1C0C5")
                    }
                    
                }
                
                
                
                
                
                
                
                
        })
        
    }
    
    func ListofLikes(sender:UITapGestureRecognizer) {
        
        if let tag = sender.view?.tag {
            if ( tag == 1)//Likes
            {
                
           
                
                let vclikers = parentView.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
                vclikers.LikersMod(1, LikeType: true, ObjId: (GmodCommentsFollowersobj.Id?.integerValue)!)
                vclikers.title = "Likers"
                parentView.navigationController?.pushViewController(vclikers, animated: true)
             
            }
            else
            {
                let vclikers = parentView.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
                vclikers.LikersMod(1, LikeType: false, ObjId: (GmodCommentsFollowersobj.Id?.integerValue)!)
                vclikers.title = "No"
                parentView.navigationController?.pushViewController(vclikers, animated: true)
            }
        }
        
    }
    
    
    func LikeAction() {
        var  CurrrentLike = false
        
        if (GmodCommentsFollowersobj.Liked){
            CurrrentLike = false
            
        }
        else
        {
            CurrrentLike = true
        }
        
        var RequestParameters : NSDictionary = NSDictionary();
        
        
        RequestParameters = [
            "LevelId":1,
            "LikeType":true,
            "LikeValue":CurrrentLike,
            "PostId":(GmodCommentsFollowersobj.Id?.integerValue)!,
            "UserId" : (User?.Id)!,
        ]
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.LikeDislikeComment, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil , callbackDictionary: { (JSON, NSError) in
                
                if (self.GmodCommentsFollowersobj.Liked){
                    
                    self.GmodCommentsFollowersobj.Liked = false
                    self.GmodCommentsFollowersobj.NoOfLikes! =  self.GmodCommentsFollowersobj.NoOfLikes!.integerValue - 1
                    self.NumLikes.text = "\(self.GmodCommentsFollowersobj.NoOfLikes!) Yes"
                    self.imgLike.image = UIImage(named: "Offers-(Like)-unselected")
                    self.NumLikes.textColor =  UIColor(hex: "#C1C0C5")
                    
                    
                    
                }
                else
                {
                    self.GmodCommentsFollowersobj.Liked = true
                    self.GmodCommentsFollowersobj.NoOfLikes! =  self.GmodCommentsFollowersobj.NoOfLikes!.integerValue + 1
                    self.NumLikes.text = "\(self.GmodCommentsFollowersobj.NoOfLikes!) Yes"
                    self.imgLike.image = UIImage(named: "Offers-(Like)")
                    self.NumLikes.textColor =  UIColor(hex: "#b379e7")
                    
                    // to deslect the Disliked
                    if (self.GmodCommentsFollowersobj.Disliked){
                        
                        self.GmodCommentsFollowersobj.Disliked = false
                        self.GmodCommentsFollowersobj.NoOfDilikes! =  self.GmodCommentsFollowersobj.NoOfDilikes!.integerValue - 1
                        self.NumDislikes.text = "\(self.GmodCommentsFollowersobj.NoOfDilikes!) No"
                        
                        self.imgDislike.image = UIImage(named: "Offers-(DisLike)-unselected")
                        self.NumDislikes.textColor =  UIColor(hex: "#C1C0C5")
                    }
                }
                
                
                
                
        })
        
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgLike.userInteractionEnabled = true
         NumLikes.userInteractionEnabled = true
         imgDislike.userInteractionEnabled = true
         NumDislikes.userInteractionEnabled = true
         CommentPhoto.userInteractionEnabled = true
        BodyView.userInteractionEnabled = true
        
        imgLike.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(self.LikeAction)));
        
        NumLikes.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(self.ListofLikes)));
        
        
        imgDislike.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(self.DisLikeAction)))
        
        NumDislikes.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(self.ListofLikes)))
        
     
        
        CommentPhoto.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.clickonImage)))
        
        
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        // Configure the view for the selected state
    }
    
    
    func attemptOpenURL(urlstring: String) {
        let url = NSURL(string: urlstring.lowercaseString)!
        let safariCompatible = (url.scheme == "http") || (url.scheme == "https")
        if safariCompatible && UIApplication.sharedApplication().canOpenURL(url) {
            UIApplication.sharedApplication().openURL(url)
        }
        else {
            let strurl = "http://\(url)"
            UIApplication.sharedApplication().openURL(NSURL(string: strurl)!)
        }
    }

    
}
