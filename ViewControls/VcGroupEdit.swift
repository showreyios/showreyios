//
//  VcGroupEdit.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/20/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import FirebaseAnalytics

class VcGroupEdit: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource, UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    
    @IBOutlet weak var groupImg: UIImageView!
    
    @IBOutlet weak var groupName: UITextField!

    @IBOutlet weak var groupMembers: UICollectionView!
    
    @IBOutlet weak var btnPrivate: UIButton!
    
    
    var getSelectedFollowees:[Int] = []
    var getFollowees = [ModFollowersobj]()
    var parent:VcGroupSelectingList!
    var gName:String = ""
    var gImage:String = "" 
    var navTitle:String = "Create List"
    var groupId:Int!
    var privateGroup:Bool = false
    var edit:Bool = false
    var mediaPicker: UIImagePickerController!
    var createGroupImg : UIImage!
    var partArray = []
    var imageChanged:Bool = false
    var partCount:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        groupImg.layer.cornerRadius = groupImg.frame.size.width / 2
        groupImg.clipsToBounds = true
        
        if edit == true{
            
            if createGroupImg != nil {
                groupImg.image = createGroupImg
            }else {
             
                groupImg.kf_setImageWithURL(NSURL(string: gImage ), placeholderImage: UIImage(named:"Group-Image-Icon"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            }
            
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(VcGroupEdit.imageTapped(_:)))
        groupImg.userInteractionEnabled = true
        groupImg.addGestureRecognizer(tapGestureRecognizer)
        
        self.navigationItem.title = navTitle
        
        groupName.text = gName
        
//        if !privateGroup {
//            
//            btnPrivate.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
//        }else{
//            btnPrivate.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
//            
//        }

         partCount = getSelectedFollowees.count
        FIRAnalytics.logEventWithName("MYBroadCasts", parameters: nil)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.groupMembers.reloadData()
       
    }
    
    
    func imageTapped(img: AnyObject)
    {
        
        // 1
        let optionMenu = UIAlertController(title: "Select Madia", message: nil, preferredStyle: .ActionSheet)
        
        // 2
        let PhotoGallery = UIAlertAction(title: "Photo Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                self.mediaPicker = UIImagePickerController()
                self.mediaPicker.delegate = self
                self.mediaPicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                self.mediaPicker.allowsEditing = true
                self.presentViewController(self.mediaPicker, animated: true, completion: nil)

            }
            
            
        })
        let PhotoCamera = UIAlertAction(title: "Photo Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.mediaPicker =  UIImagePickerController()
            self.mediaPicker.delegate = self
            self.mediaPicker.sourceType = .Camera
            self.mediaPicker.allowsEditing = true
            
            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
            
            
//            var imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
//            imagePicker.allowsEditing = false
//            self.presentViewController(imagePicker, animated: true, completion: nil)
//            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(PhotoGallery)
        optionMenu.addAction(PhotoCamera)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
        
        
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        
            groupImg.image = image
        
            imageChanged = true
        self.dismissViewControllerAnimated(true, completion: nil);
    }


    
    
    @IBAction func privateAction(sender: AnyObject) {
        if !privateGroup {
            
            btnPrivate.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
            privateGroup = true
        }else{
            btnPrivate.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
            privateGroup = false
            
        }
        
        
    }
    
    
    
    
    // MARK: - collection view data source
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print(getFollowees.count + 1)
        
        return (getFollowees.count + 1)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell: CcEditGroup = collectionView.dequeueReusableCellWithReuseIdentifier("CcEditGroup", forIndexPath: indexPath) as! CcEditGroup
        
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.size.width / 2
        cell.profileImage.clipsToBounds = true
        
        
        if ( indexPath.row < getFollowees.count )
        {
            
                
                cell.profileImage.kf_setImageWithURL(NSURL(string: (getFollowees [indexPath.row].ProfilePicture)! ), placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
                
                cell.profileName.text = getFollowees[indexPath.row].FullName
            
        }else
        {
            
            cell.profileImage.image = UIImage(named: "Add-More")
            cell.profileName.text = ""
            
        }
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if ( indexPath.row == getFollowees.count ){
            
          
            let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            
            var foundview = false
            for selectingPeople : UIViewController in allViewController
            {
                if selectingPeople .isKindOfClass(VcGroupSelectingList)
                {
                    
                    let cast = selectingPeople as! VcGroupSelectingList
                    
                    foundview  = true
                    
                 
                    cast.selectedFollowees = getSelectedFollowees
                    cast.cameSelected = getFollowees
                    
                    self.navigationController?.popToViewController(selectingPeople, animated: true)
                }
            }
            if (foundview == false)
            {
                
            
                let followeesList = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupSelectingList") as! VcGroupSelectingList
                
                followeesList.selectedFollowees = getSelectedFollowees
                followeesList.cameSelected = getFollowees
                
                self.navigationController?.pushViewController(followeesList, animated: true)
            }
            
            
            
            
            
            
        }else {
            
            // 1
            let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
            
            // 2
            let remove = UIAlertAction(title: "Remove Participant", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                
                if self.edit == true {
              
                   self.removeParticipant((self.getFollowees[indexPath.row].Id?.integerValue)!, admin: self.getFollowees[indexPath.row].IsAdmin, index: indexPath.row)
                }else{
                
               // print(Int(self.getFollowees[indexPath.row].Id!))
                self.getSelectedFollowees = self.getSelectedFollowees.filter{ $0 != Int(self.getFollowees[indexPath.row].Id!) }
                self.getFollowees.removeAtIndex(indexPath.row)
                
                
                self.groupMembers.reloadData()
             
                }
            })
            
            
            let admin = UIAlertAction(title: "Make list admin", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                self.setParticipantAsAdmin((self.getFollowees[indexPath.row].Id?.integerValue)!)
                
                
            })
            
            
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
                (alert: UIAlertAction!) -> Void in
            })
            
            
            optionMenu.addAction(remove)
            
            if edit == true {
                // removed by ispha based on requirement
               // optionMenu.addAction(admin)
            }
            
            optionMenu.addAction(cancelAction)
            
            // 5
            self.presentViewController(optionMenu, animated: true, completion: nil)
            
            

        }
        
        
    }
    

    // MARK: - finish button action
    
    @IBAction func btnFinish(sender: AnyObject) {
        
        if groupName.text == "" {
            
            self.view.makeToast("Please type a name for the list!", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
            
        }else {
            
            if edit == true {
                
                if partCount < getSelectedFollowees.count {
                    addParticipant()
                }else{
                    self.editGroup()
                }
                
            }else{
                
                createGroup()
                
            }
        }
    }
    
    
    // MARK: - sent data to service
    
    func setParticipantAsAdmin(participantId:Int)
    {
        
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Setting Admin..."
        
        
        
        let RequestParameters : NSDictionary = [
            
            "GroupId":groupId,
            "IsAdmin":true,
            "ParticipantId":participantId,
            "UserId": (User?.Id)!
        ]
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.SetParticipantAsAdmin, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                if   response.JSON != nil
                {
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                     let json = response.JSON["ResultResponse"]
                   
                    let resultResponse = json.stringValue
                   
                     print("🙃respons = \(response) ,,row = \(json),,responseresul = \(resultResponse) ")
                    if( resultResponse == "0"){
                        
                        
                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
                }
                else
                {
                    self.view.makeToast("Got no reponse from backend!", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                }
        })
        
    }

    
    func removeParticipant(participantId:Int, admin:Bool, index:Int)
    {
        partArray = [participantId]
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Removing..."
        
        
        
        let RequestParameters : NSDictionary = [
            
            
            "GroupId":groupId,
            "IsAdmin":admin,
            "ParticipantId":partArray,
            "UserId": (User?.Id)!
        ]
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.RemoveParticipantsFromGroup, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    
                    if( resultResponse == "0"){
                        
                        self.getSelectedFollowees = self.getSelectedFollowees.filter{ $0 != Int(self.getFollowees[index].Id!) }
                        self.getFollowees.removeAtIndex(index)
                        
                        
                        self.groupMembers.reloadData()

                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
        })
        
    }

    
    
    func addParticipant()
    {
        
        
        let RequestParameters : NSDictionary = [
            
            
            "GroupId":groupId,
            "IsAdmin":false,
            "ParticipantId":getSelectedFollowees,
            "UserId": (User?.Id)!
        ]
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.AddParticipantToGroup, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    
                    if( resultResponse == "0"){
                        
                        self.editGroup()
                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
        })
        
    }
  
    
    func updateGroupPicture (gId:Int){
        
        if self.edit == true {
            
            let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            loadingNotification.label.text = "Editing List..."
            
        }else{
            let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            loadingNotification.label.text = "Creating List..."
        }
        
        
        let imageData :NSData = UIImageJPEGRepresentation(groupImg.image!, 0.35)!//UIImagePNGRepresentation(groupImg.image!)!
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.UpdateGroupPicture + "?UserId=\((User?.Id)!)&GroupId=\(gId)", hTTPMethod: Method.post, parameters: nil, httpBody: nil ,httpBodyData: imageData
            , responseType: ResponseType.DictionaryJson , callbackString: nil ,callbackDictionary: { (JSON, NSError) in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if((NSError) != nil)
                {
                    if let networkError = NSError {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                            
                        }
                        return
                    }
                }
                else
                {
                    if (JSON["ResultResponse"].stringValue == "0") //scuess
                    {
                        VcGetGroups.groupsStatic.getGroups(1, FromPagerOrRefresher: true, dontAppend: true)
                        
                        if self.edit == true {
                            
                            
                            let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                            
                            var foundview = false
                            for selectingPeople : UIViewController in allViewController
                            {
                                if selectingPeople .isKindOfClass(VcGroupPageWall)
                                {
                                    
                                    let cast = selectingPeople as! VcGroupPageWall
                                    
                                    foundview  = true
                                    
                                    
                                    cast.groupId = self.groupId
                                    cast.groupNameStr = self.groupName.text
                                    cast.groupFollowers = self.getSelectedFollowees.count
                                    cast.privacy = self.privateGroup
                                    cast.editOrCreate = true
                                    if self.imageChanged == true {
                                        cast.createGroupImg = self.groupImg.image
                                    }
                                    
                                    self.navigationController?.popToViewController(selectingPeople, animated: true)
                                }
                            }
                            if (foundview == false)
                            {
                                
                                let editGroup = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupPageWall") as! VcGroupPageWall
                                
                                editGroup.groupId = self.groupId
                                editGroup.groupNameStr = self.groupName.text
                                editGroup.groupFollowers = self.getSelectedFollowees.count
                                editGroup.privacy = self.privateGroup
                                editGroup.editOrCreate = true
                                
                                if self.imageChanged == true {
                                    editGroup.createGroupImg = self.groupImg.image
                                }
                                self.navigationController?.pushViewController(editGroup, animated: true)
                            }
                            
                            
                            
                            
                        }else{
                        
                        let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                        
                        var foundview = false
                        for selectingPeople : UIViewController in allViewController
                        {
                            if selectingPeople .isKindOfClass(VcGroupPageWall)
                            {
                                
                                let cast = selectingPeople as! VcGroupPageWall
                                
                                foundview  = true
                                
                                cast.groupId = gId
                                cast.groupNameStr = self.groupName.text
                                cast.groupFollowers = self.getSelectedFollowees.count
                                cast.privacy = self.privateGroup
                                cast.editOrCreate = true
                                cast.create = true
                                if self.imageChanged == true {
                                    cast.createGroupImg = self.groupImg.image
                                }
                                
                                self.navigationController?.popToViewController(selectingPeople, animated: true)
                            }
                        }
                        if (foundview == false)
                        {
                            
                            let createGroup = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupPageWall") as! VcGroupPageWall
                            
                            createGroup.groupId = gId
                            createGroup.groupNameStr = self.groupName.text
                            createGroup.groupFollowers = self.getSelectedFollowees.count
                            createGroup.privacy = self.privateGroup
                            createGroup.editOrCreate = true
                            createGroup.create = true
                            if self.imageChanged == true {
                                createGroup.createGroupImg = self.groupImg.image
                            }
                            self.navigationController?.pushViewController(createGroup, animated: true)
                        }
                        }
                    }else{
                        self.view.makeToast("An error has been occurred")
                    }
                }
        })
    }
    
    
    func createGroup()
    {
        
       if self.imageChanged == false {
            let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            loadingNotification.label.text = "Creating List..."
       
        }
        
        let RequestParameters : NSDictionary = [
            
            "Info":"String content",
            "Name": groupName.text!,
            "Participants": getSelectedFollowees,
            "Privacy":privateGroup,
            "UserId": (User?.Id)!
        ]
        
        
     
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.CreateGroup, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    let grpId = response.JSON["RequestId"].intValue
                    
                    if( resultResponse == "0"){
                        
                        if self.imageChanged == true {
                           self.updateGroupPicture(grpId)
                        }else{
                        
                            VcGetGroups.groupsStatic.getGroups(1, FromPagerOrRefresher: true, dontAppend: true)
                        
                        let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                        
                        var foundview = false
                        for selectingPeople : UIViewController in allViewController
                        {
                            if selectingPeople .isKindOfClass(VcGroupPageWall)
                            {
                                
                                let cast = selectingPeople as! VcGroupPageWall
                                
                                foundview  = true
                                
                                cast.groupId = grpId
                                cast.groupNameStr = self.groupName.text
                                cast.groupFollowers = self.getSelectedFollowees.count
                                cast.privacy = self.privateGroup
                                cast.editOrCreate = true
                                cast.create = true
                                if self.imageChanged == true {
                                    cast.createGroupImg = self.groupImg.image
                                }
                                
                                self.navigationController?.popToViewController(selectingPeople, animated: true)
                            }
                        }
                        if (foundview == false)
                        {
                            
                            let createGroup = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupPageWall") as! VcGroupPageWall
                            
                            createGroup.groupId = grpId
                            createGroup.groupNameStr = self.groupName.text
                            createGroup.groupFollowers = self.getSelectedFollowees.count
                            createGroup.privacy = self.privateGroup
                            createGroup.editOrCreate = true
                            createGroup.create = true
                            if self.imageChanged == true {
                                createGroup.createGroupImg = self.groupImg.image
                            }
                            self.navigationController?.pushViewController(createGroup, animated: true)
                        }
                        

                    }
                        
                        
                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
        })
        
    }
    
    func editGroup()
    {
        
        if self.imageChanged == false {
            let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            loadingNotification.label.text = "Editing List..."
            
        }
        
        let RequestParameters : NSDictionary = [
            
            "GroupId":groupId,
            "Info":"String content",
            "Name": groupName.text!,
            "Privacy":privateGroup,
            "UserId": (User?.Id)!
            
        ]
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.EditGroup, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    
                    if( resultResponse == "0"){
                        
                        if self.imageChanged == true {
                           self.updateGroupPicture(self.groupId)
                        }else{
                        
                        
                        VcGetGroups.groupsStatic.getGroups(1, FromPagerOrRefresher: true, dontAppend: true)
                        
                        let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                        
                        var foundview = false
                        for selectingPeople : UIViewController in allViewController
                        {
                            if selectingPeople .isKindOfClass(VcGroupPageWall)
                            {
                                
                                let cast = selectingPeople as! VcGroupPageWall
                                
                                foundview  = true
                                
                                
                                cast.groupId = self.groupId
                                cast.groupNameStr = self.groupName.text
                                cast.groupFollowers = self.getSelectedFollowees.count
                                cast.privacy = self.privateGroup
                                cast.editOrCreate = true
                                if self.imageChanged == true {
                                    cast.createGroupImg = self.groupImg.image
                                }
                                
                                self.navigationController?.popToViewController(selectingPeople, animated: true)
                            }
                        }
                        if (foundview == false)
                        {
                            
                            let editGroup = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupPageWall") as! VcGroupPageWall
                            
                            editGroup.groupId = self.groupId
                            editGroup.groupNameStr = self.groupName.text
                            editGroup.groupFollowers = self.getSelectedFollowees.count
                            editGroup.privacy = self.privateGroup
                            editGroup.editOrCreate = true
                            
                            if self.imageChanged == true {
                                editGroup.createGroupImg = self.groupImg.image
                            }
                            self.navigationController?.pushViewController(editGroup, animated: true)
                        }

                        
                        
                        }
                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
        })
        
    }

    
    
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
