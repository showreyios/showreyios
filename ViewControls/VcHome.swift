//
//  VcHome.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 9/9/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Hue
import MessageUI
import FirebaseMessaging
import UserNotifications

class VcHome:PagerController, PagerDataSource ,MFMessageComposeViewControllerDelegate,UNUserNotificationCenterDelegate {
    
    var titles: [String] = []
    let screenSize = ((UIScreen.mainScreen().bounds.width))/2
    var invitAlertMessage = ""
    var invitAlertMessageId = ""
    var invitAlertMessageType:Int = 0
    var feednews:VcMasterNewsFeed!

    var NotificationTab : VTapItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        HomeScreen = self;
        openedScreen = self
        
        self.dataSource = self
        
        let storyboard = AppDelegate.storyboard
        feednews = storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        feednews.parent = self;
        let Notifies = VcNotificationsLocal(nibName: "VcNotificationsLocal", bundle: nil) //storyboard.instantiateViewControllerWithIdentifier("VcNotificationsLocal") as! VcNotificationsLocal
        Notifies.parent = self
       //  Setting up the PagerController with Name of the Tabs and their respective ViewControllers
        NotificationTab = VTapItem.fromNib("VTapItem")
        self.setupPager(
            nil,
            tabImages: [("Image-2","Image-4",nil),("Image-3","Image-5",NotificationTab)],
            tabControllers: [feednews, Notifies])
      //  TapAction = tapAction;
        Notifies.NotificationTap = NotificationTab;
        VcNotificationsLocal.CalculateUnreadNotifications(NotificationTab)
        customizeTab()

        view.backgroundColor = UIColor.whiteColor()
        title = "Home"
        
//        let barColor: UIColor = UIColor(red: 103.0/255.0, green: 0.0/255.0, blue: 174.0/255.0, alpha: 1.0)
//        self.navigationController!.navigationBar.barTintColor = barColor
//        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
//        self.navigationController!.navigationBar.tintColor = UIColor.whiteColor()
//        self.navigationController!.setNavigationBarHidden(false, animated: false)
        
        
        let button = UIButton()
        button.frame = CGRectMake(0, 0, 21, 31)
        button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
        button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
        let barButton = UIBarButtonItem()
        barButton.customView = button
        navigationItem.leftBarButtonItem = barButton
        
        
        let button2 = UIButton()
        button2.frame = CGRectMake(0, 0, 31, 31)
        button2.setImage(UIImage(named: "Nav-Bar-(Search)"), forState: .Normal)
        button2.addTarget(self, action: #selector(Searchscreen), forControlEvents: .TouchUpInside)
        let barButton2 = UIBarButtonItem()
        barButton2.customView = button2
        navigationItem.rightBarButtonItem = barButton2
      
       // navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Left", style: .Plain, target: self, action: #selector(SSASideMenu.presentLeftMenuViewController))
        // Do any additional setup after loading the view.
        
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.Alert , .Badge, .Sound]
            UNUserNotificationCenter.currentNotificationCenter().requestAuthorizationWithOptions(authOptions, completionHandler: {_, _ in })
            
            
            // For iOS 10 display notification (sent via APNS)
      //      UNUserNotificationCenter.currentNotificationCenter().delegate = self
            // For iOS 10 data message (sent via FCM)
            // FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(forTypes: [.Alert , .Badge, .Sound], categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        }
        
        UIApplication.sharedApplication().registerForRemoteNotifications()
        if (invitAlertMessage != "")
        {
            self.invitAlert(invitAlertMessage, messageId: invitAlertMessageId, messageType: invitAlertMessageType)
            invitAlertMessage = ""
            invitAlertMessageId = ""
            invitAlertMessageType = 0
        }
        
    }
    func Searchscreen()
    {
        let search = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcSearch")
        navigationController?.pushViewController(search, animated: true)
    }
    override func overloadableTapAction(Index: Int) {
        if(Index == 0)
        {
            if feednews != nil
            {
                feednews.scrollToTop()
            }
        }
    }
    
    func customizeTab() {
        indicatorColor = UIColor(hex: "#b379e7")
        tabsViewBackgroundColor = UIColor.whiteColor()
        //(colorLiteralRed: 145 / 255, green: 78 / 255, blue: 233 / 255, alpha: 1)
        //(rgb: 0x00AA00)
        contentViewBackgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.32)
        
        //startFromSecondTab = false
        centerCurrentTab = true
        tabLocation = PagerTabLocation.Top
        tabHeight = 49
        tabOffset = 0
        tabWidth = screenSize
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.During
        selectedTabTextColor = UIColor(colorLiteralRed: 145 / 255, green: 78 / 255, blue: 233 / 255, alpha: 1)
        tabsTextFont = UIFont(name: "HelveticaNeue-Bold", size: 15)!
        // tabTopOffset = 10.0
        // tabsTextColor = .purpleColor()
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based applicaStringtion, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func sendTextMessage(Text:String,Num:String)
    {
        if (MFMessageComposeViewController.canSendText())
        {
            let controller = MFMessageComposeViewController()
            controller.body = Text
            controller.recipients = [Num]
            controller.messageComposeDelegate = self
            presentationController
             self.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.navigationBarHidden = false
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // # MARK:- invitAlert
    
    func invitAlert (message:String, messageId:String,  messageType:Int)
    {
        let alertController = UIAlertController(title: "Your ShowReY has been sent !", message: "Do you want to invite your friends to ShowReY App?", preferredStyle: .Alert)
        
        
        let inviteAction = UIAlertAction(title: "Invite", style: .Default , handler: {
            (alert: UIAlertAction!) -> Void in

            let TvInvite_=self.storyboard!.instantiateViewControllerWithIdentifier("VcTellAFriend") as! VcTellAFriend
             TvInvite_.postTxt = message
             TvInvite_.postId = messageId
             TvInvite_.postType = messageType
            self.navigationController?.pushViewController(TvInvite_, animated: true)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { _ in }
        
        alertController.addAction(inviteAction)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
        
        
     
    
    }
    
    
    
    static func callNumber(phoneNumber:String) {
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
}


