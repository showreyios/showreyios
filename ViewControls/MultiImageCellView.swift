//
//  MultiImageCellView.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/5/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Kingfisher

class MultiImageCellView: UIView {
    
    @IBOutlet weak var uiViewOne: UIView!
    @IBOutlet weak var img1V1: UIImageView!
    @IBOutlet weak var uiViewTwo: UIView!
    @IBOutlet weak var img1V2: UIImageView!
    @IBOutlet weak var img2V2: UIImageView!
    @IBOutlet weak var uiViewThree: UIView!
    @IBOutlet weak var img1V3: UIImageView!
    @IBOutlet weak var img2V3: UIImageView!
    @IBOutlet weak var img3V3: UIImageView!
    @IBOutlet weak var uiViewFour: UIView!
    @IBOutlet weak var img1V4: UIImageView!
    @IBOutlet weak var img2V4: UIImageView!
    @IBOutlet weak var img3V4: UIImageView!
    @IBOutlet weak var img4V4: UIImageView!
    @IBOutlet weak var btnAddMore: UIButton!
    
    @IBOutlet weak var imgLayer: UIImageView!
    
    static func imagesView(forAttachedFiles _attachedFiles: [ModFeedAttachedFiles], withFrame frame: CGRect) -> UIView
    {
        
        var attachedFiles = _attachedFiles
        let outletData = MultiImageCellView.fromNib("MultiImageCellView")
         outletData.frame = frame; //CGRect(x: 0,y: 0,width:outletData.frame.width ,height: frame.height);
        
     
        
        
        if attachedFiles.count == 1
        {
            
            outletData.uiViewOne.hidden = false
            outletData.uiViewTwo.hidden = true
            outletData.uiViewThree.hidden = true
            outletData.uiViewFour.hidden = true
            
            let file: ModFeedAttachedFiles = attachedFiles[0]
            let imageURL = NSURL(string:  AppDelegate.ResorceDomain + file.URL!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
            
            outletData.img1V1.kf_setImageWithURL( imageURL, placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
        }
        else if attachedFiles.count == 2
        {
            
            outletData.uiViewOne.hidden = true
            outletData.uiViewTwo.hidden = false
            outletData.uiViewThree.hidden = true
            outletData.uiViewFour.hidden = true
            for i in 0  ..< attachedFiles.count
            {
                
                let file: ModFeedAttachedFiles = attachedFiles[i]
                let imageURL = NSURL(string:  AppDelegate.ResorceDomain + file.URL!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
                if i == 0 {
                    outletData.img1V2.kf_setImageWithURL( imageURL, placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
                }else {
                    
                    outletData.img2V2.kf_setImageWithURL( imageURL, placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
                }
                
            }
            
        }
        else if attachedFiles.count == 3
        {
                        
            outletData.uiViewOne.hidden = true
            outletData.uiViewTwo.hidden = true
            outletData.uiViewThree.hidden = false
            outletData.uiViewFour.hidden = true
            for i in 0  ..< attachedFiles.count
            {
                
                let file: ModFeedAttachedFiles = attachedFiles[i]
                let imageURL = NSURL(string:  AppDelegate.ResorceDomain + file.URL!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
                if i == 0
                {
                    outletData.img1V3.clipsToBounds = true
                    
                    outletData.img1V3.contentMode = .ScaleAspectFill
                    outletData.backgroundColor = UIColor.redColor()
                    
                    outletData.img1V3.kf_setImageWithURL( imageURL, placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
                      outletData.img1V3.clipsToBounds = true
                      
                        outletData.img1V3.contentMode = .ScaleAspectFill
                        
                        
                        
                    })
                    
                  
                   
                }
                else if i == 1
                {
                    outletData.img2V3.kf_setImageWithURL( imageURL, placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
                }
                else
                {
                    outletData.img3V3.kf_setImageWithURL( imageURL, placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
      
                }
            }
        }
        else if attachedFiles.count >= 4{
            
            outletData.uiViewOne.hidden = true
            outletData.uiViewTwo.hidden = true
            outletData.uiViewThree.hidden = true
            outletData.uiViewFour.hidden = false
            for i in 0  ..< attachedFiles.count  {
                
                let file: ModFeedAttachedFiles = attachedFiles[i]
                let imageURL = NSURL(string:  AppDelegate.ResorceDomain + file.URL!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
                if i == 0 {
                    outletData.img1V4.kf_setImageWithURL( imageURL, placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
                }else if i == 1 {
                    
                    outletData.img2V4.kf_setImageWithURL( imageURL, placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
                }else if i == 2{
                    outletData.img3V4.kf_setImageWithURL( imageURL, placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
                }else if i == 3 {
                    outletData.img4V4.kf_setImageWithURL( imageURL, placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
                }else{
                    outletData.btnAddMore.hidden = false
                    outletData.imgLayer.hidden = false
                    let title : Int = attachedFiles.count - 4
                    outletData.btnAddMore.setTitle( "+\(title)", forState: .Normal)
                    
                    
                }
            }
            
        }
        
        
        return outletData
        
    }

}
