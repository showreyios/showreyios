//
//  VcPostChoice.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 1/11/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit

class VcPostChoice: UIViewController {

    @IBOutlet weak var DirectWall: UIButton!
    @IBOutlet weak var DirectFollowers: UIButton!
    @IBOutlet weak var DirectGroups: UIButton!
    
    var tvCreatePost : TvCreatePost!
    var feedProfilePros : ModFeedProfilePros!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DirectWall.titleLabel!.numberOfLines = 0; // Dynamic number of lines
        DirectWall.titleLabel!.lineBreakMode = .ByWordWrapping
        
        DirectFollowers.titleLabel!.numberOfLines = 0; // Dynamic number of lines
      //  DirectFollowers.titleLabel!.lineBreakMode = .ByWordWrapping
        
        DirectGroups.titleLabel!.numberOfLines = 0; // Dynamic number of lines
      //  DirectGroups.titleLabel!.lineBreakMode = .ByWordWrapping
        
        DirectFollowers.setTitle("        DIRECT\nTO FOLLOWERS", forState: .Normal)
        DirectGroups.setTitle(" DIRECT\nTO LISTS", forState: .Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BtnDirectWall(sender: AnyObject) {
        
      //tvCreatePost.CreatePost(self.view, FeedProfilePros: feedProfilePros ,comeFromWelcome: true)
        tvCreatePost.CreatePost(self.view, FeedProfilePros: feedProfilePros, comeFromWelcome: true, WPostToId: [(User?.Id)!.integerValue], WPostToType: 1)
    }
    @IBAction func BtnDirectFollowers(sender: AnyObject) {
       
        
        
        let GroupSelectingList = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcGroupSelectingList") as! VcGroupSelectingList
        GroupSelectingList.tvCreatePost = tvCreatePost
        GroupSelectingList.feedProfilePros = feedProfilePros
        GroupSelectingList.come = .createPost
        
        self.navigationController?.pushViewController(GroupSelectingList, animated: true)
        
        
    }
    @IBAction func BtnDirectGroups(sender: AnyObject) {
        
        let vcGetGroups = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcGetGroups") as! VcGetGroups
        vcGetGroups.tvCreatePost = tvCreatePost
        vcGetGroups.feedProfilePros = feedProfilePros
        vcGetGroups.come = .createPost

        self.navigationController?.pushViewController(vcGetGroups, animated: true)
        
        
        
       
    }
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
