//
//  VcIntroduction.swift
//  ShowRey
//
//  Created by User on 1/12/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit
import EAIntroView

class VcIntroduction: UIViewController, EAIntroDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let page1 = EAIntroPage()
        page1.bgImage = UIImage(named: "01 IPhone wall")
        
        let page2 = EAIntroPage()
        page2.bgImage = UIImage(named: "02 IPhone Offers")
        
        let page3 = EAIntroPage()
        page3.bgImage = UIImage(named: "03 IPhone Tips")
        
        let intro = EAIntroView()
        intro.frame = self.view.bounds
        intro.pages = [page1,page2,page3]
        intro.skipButton.setTitle("Skip", forState: .Normal)
        intro.delegate = self
        intro.tapToNext = true
        
//        let btn = UIButton(type: .RoundedRect)
//        btn.frame = CGRectMake(0, 0, 230, 40)
//        btn.setTitle("Skip", forState: .Normal)
//        btn.setTitleColor(UIColor.blackColor(), forState: .Normal)
//        btn.layer.borderWidth = 0.2
//        btn.layer.cornerRadius = 10
//        btn.layer.borderColor = UIColor.whiteColor().CGColor
//        intro.skipButton = btn;
//        intro.skipButtonY = self.view.bounds.size.height - 30
//        intro.skipButtonAlignment = EAViewAlignment.Center
      
        intro.showInView(self.view, animateDuration: 0.3)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func introDidFinish(introView: EAIntroView!, wasSkipped: Bool) {
//        if wasSkipped {
//            
//            
//        }else{
//            
//            
//        }
        
        let splash = self.storyboard?.instantiateViewControllerWithIdentifier("splash")
        VcLogin.autologin(splash!)
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
