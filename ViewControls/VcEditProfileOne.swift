//
//  VcEditProfileOne.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/28/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import EGFloatingTextField
import Kingfisher
import MBProgressHUD
import SwiftyJSON
import FirebaseAnalytics
import AVFoundation


class VcEditProfileOne: UIViewController ,UITextFieldDelegate ,UIImagePickerControllerDelegate , UINavigationControllerDelegate
{
    var profileMaster: VcMasterNewsFeed!
    
    @IBOutlet weak var imgProfilePicture: UIImageView!
    @IBOutlet weak var userNameText: EGFloatingTextField!
    @IBOutlet weak var mobileNumberText: EGFloatingTextField!
    @IBOutlet weak var lblUserNameError: UILabel!
    @IBOutlet weak var lblMobileNumberError: UILabel!
    var mediaPicker: UIImagePickerController!
    @IBOutlet weak var imgCoverPhoto: UIImageView!
    var targetview : UIView!
    var profileAndCoverPhoto : Bool = false
    var profileTaken: Bool = false
    var coverTaken: Bool = false
    
    override func viewDidAppear(animated: Bool)
    {
        userNameText.setPlaceHolder(" Full Name")
        mobileNumberText.setPlaceHolder(" Mobile Number")
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.title = "Edit Profile"
        
        imgProfilePicture.layer.cornerRadius = imgProfilePicture.frame.width/2
        
        userNameText.floatingLabel = true
        userNameText.validationType = .Empty
        userNameText.errorLabel = lblUserNameError
        userNameText.customerrorMessage = "Enter fullname"
        
        mobileNumberText.floatingLabel = true
        mobileNumberText.validationType = .Empty
        mobileNumberText.errorLabel = lblMobileNumberError
        mobileNumberText.customerrorMessage = "Enter your mobile number"
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(VcEditProfileOne.imageTapped(_:)))
        imgProfilePicture.userInteractionEnabled = true
        imgProfilePicture.addGestureRecognizer(tapGestureRecognizer)
        
        userNameText.text = User?.FullName
        mobileNumberText.text = User?.MobileNumber
        imgCoverPhoto.kf_setImageWithURL(NSURL(string: User!.CoverPage!), placeholderImage: UIImage(named: "Slide-Menu-BG"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        imgProfilePicture.kf_setImageWithURL(NSURL(string: User!.ProfilePicture!), placeholderImage: UIImage(named: "Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
    
        FIRAnalytics.logEventWithName("Profile", parameters: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editImgBtn(sender: AnyObject) {
        
        // 1
        let optionMenu = UIAlertController(title: "Select Madia", message: nil, preferredStyle: .ActionSheet)
        
        // 2
        let PhotoGallery = UIAlertAction(title: "Photo Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imagePicker.allowsEditing = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
                self.profileAndCoverPhoto = true
            }
            
        })
        let PhotoCamera = UIAlertAction(title: "Photo Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
//            self.mediaPicker =  UIImagePickerController()
//            self.mediaPicker.delegate = self
//            self.mediaPicker.sourceType = .Camera
//            
//            
//            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
            
            if AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) ==  AVAuthorizationStatus.Authorized
            {
                self.mediaPicker = UIImagePickerController()
                self.mediaPicker.delegate = self
                self.mediaPicker.sourceType = UIImagePickerControllerSourceType.Camera;
                self.mediaPicker.allowsEditing = false
                self.presentViewController(self.mediaPicker, animated: true, completion: nil)
                self.profileAndCoverPhoto = true
            }
            else
            {
                AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                    if granted == true
                    {
                        self.mediaPicker = UIImagePickerController()
                        self.mediaPicker.delegate = self
                        self.mediaPicker.sourceType = UIImagePickerControllerSourceType.Camera;
                        self.mediaPicker.allowsEditing = false
                        self.presentViewController(self.mediaPicker, animated: true, completion: nil)
                        self.profileAndCoverPhoto = true
                    }
                    else
                    {
                        // User Rejected
                        NSLog("No Camera.")
                        
                        let alertController = UIAlertController (title: "Title", message: "Go to Settings?", preferredStyle: .Alert)
                        
                        let settingsAction = UIAlertAction(title: "Settings", style: .Default) { (_) -> Void in
                            guard let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString) else {
                                return
                            }
                            
                            if UIApplication.sharedApplication().canOpenURL(settingsUrl) {
                                UIApplication.sharedApplication().openURL(settingsUrl)
                            }
                        }
                        alertController.addAction(settingsAction)
                        let cancelAction = UIAlertAction(title: "Cancel", style: .Default, handler: nil)
                        alertController.addAction(cancelAction)
                        
                        self.presentViewController(alertController, animated: true, completion: nil)

                    }
                });
            }
            
            
            
            
            
            
            
            
            
//            if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
//             self.mediaPicker = UIImagePickerController()
//            self.mediaPicker.delegate = self
//            self.mediaPicker.sourceType = UIImagePickerControllerSourceType.Camera;
//            self.mediaPicker.allowsEditing = false
//            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
//            self.profileAndCoverPhoto = true
//            }else{
//                
//                NSLog("No Camera.")
//                
//                let alertController = UIAlertController (title: "Title", message: "Go to Settings?", preferredStyle: .Alert)
//                
//                let settingsAction = UIAlertAction(title: "Settings", style: .Default) { (_) -> Void in
//                    guard let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString) else {
//                        return
//                    }
//                    
//                    if UIApplication.sharedApplication().canOpenURL(settingsUrl) {
//                        UIApplication.sharedApplication().openURL(settingsUrl)
//                    }
//                }
//                alertController.addAction(settingsAction)
//                let cancelAction = UIAlertAction(title: "Cancel", style: .Default, handler: nil)
//                alertController.addAction(cancelAction)
//                
//                self.presentViewController(alertController, animated: true, completion: nil)
//            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(PhotoGallery)
        optionMenu.addAction(PhotoCamera)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
    }
    
    func imageTapped(img: AnyObject)
    {
        
        // 1
        let optionMenu = UIAlertController(title: "Select Madia", message: nil, preferredStyle: .ActionSheet)
        
        // 2
        let PhotoGallery = UIAlertAction(title: "Photo Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imagePicker.allowsEditing = true
                self.presentViewController(imagePicker, animated: true, completion: nil)
                self.profileAndCoverPhoto = false
            }
        
            
        })
        let PhotoCamera = UIAlertAction(title: "Photo Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) ==  AVAuthorizationStatus.Authorized
            {
                self.mediaPicker = UIImagePickerController()
                self.mediaPicker.delegate = self
                self.mediaPicker.sourceType = UIImagePickerControllerSourceType.Camera;
                self.mediaPicker.allowsEditing = false
                self.presentViewController(self.mediaPicker, animated: true, completion: nil)
                self.profileAndCoverPhoto = false
            }
            else
            {
                AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                    if granted == true
                    {
                        self.mediaPicker = UIImagePickerController()
                        self.mediaPicker.delegate = self
                        self.mediaPicker.sourceType = UIImagePickerControllerSourceType.Camera;
                        self.mediaPicker.allowsEditing = false
                        self.presentViewController(self.mediaPicker, animated: true, completion: nil)
                        self.profileAndCoverPhoto = false
                    }
                    else
                    {
                        // User Rejected
                        NSLog("No Camera.")
                        
                        let alertController = UIAlertController (title: "Title", message: "Go to Settings?", preferredStyle: .Alert)
                        
                        let settingsAction = UIAlertAction(title: "Settings", style: .Default) { (_) -> Void in
                            guard let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString) else {
                                return
                            }
                            
                            if UIApplication.sharedApplication().canOpenURL(settingsUrl) {
                                UIApplication.sharedApplication().openURL(settingsUrl)
                            }
                        }
                        alertController.addAction(settingsAction)
                        let cancelAction = UIAlertAction(title: "Cancel", style: .Default, handler: nil)
                        alertController.addAction(cancelAction)
                        
                        self.presentViewController(alertController, animated: true, completion: nil)
                        
                    }
                });
            }

//         
//            self.mediaPicker =  UIImagePickerController()
//            self.mediaPicker.delegate = self
//            self.mediaPicker.sourceType = .Camera
//            
//            
//            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
//            
//            
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
//            imagePicker.allowsEditing = false
//            self.presentViewController(imagePicker, animated: true, completion: nil)
//            self.profileAndCoverPhoto = false
//            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(PhotoGallery)
        optionMenu.addAction(PhotoCamera)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
        


    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        
        if(profileAndCoverPhoto == false){
            
            imgProfilePicture.image = image
            profileTaken = true
        }else {
            coverTaken = true
            imgCoverPhoto.image = image
        }
        
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    @IBAction func btnNextProfileTwo(sender: AnyObject)
    {
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Uploading..."
        
        
        var PhotoUploadedSucc:Bool?
        var CoverUploadedSucc:Bool?
        var ProfileUpdatedSucc:Bool?
        
        if profileTaken == true {
        let imageData :NSData = UIImageJPEGRepresentation(imgProfilePicture.image!, 0.35)!// UIImagePNGRepresentation(imgProfilePicture.image!)!
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.updateProfileImage + "?UserId=\((User?.Id)!)", hTTPMethod: Method.post, parameters: nil, httpBody: nil ,httpBodyData: imageData
            , responseType: ResponseType.DictionaryJson , callbackString: nil ,callbackDictionary: { (JSON, NSError) in
                //((User?.Id)!)"
                //MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                PhotoUploadedSucc = false;
                if((NSError) != nil)
                {
                    if let networkError = NSError {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                            
                        }
                        return
                    }
                }
                else
                {
                    if (JSON["ResultResponse"].stringValue == "0") //scuess
                    {
                        self.view.makeToast("Success")
                        PhotoUploadedSucc = true
                        
                    }
                    else
                    {
                        self.view.makeToast("An error has been occurred")
                    }
                }
                self.doneUpload(PhotoUploadedSucc, Cover: CoverUploadedSucc, Profile: ProfileUpdatedSucc)
        })
        
        }
        
        if coverTaken == true {
         
        let imageDataCover :NSData = UIImagePNGRepresentation(imgCoverPhoto.image!)!

        NetworkHelper.RequestHelper(nil, service: WSMethods.updateCoverPhoto + "?UserId=\((User?.Id)!)", hTTPMethod: Method.post, parameters: nil, httpBody: nil ,httpBodyData: imageDataCover
            , responseType: ResponseType.DictionaryJson , callbackString: nil ,callbackDictionary: { (JSON, NSError) in
                
                //MBProgressHUD.hideHUDForView(self.view, animated: true)
                CoverUploadedSucc = false
                if((NSError) != nil)
                {
                    if let networkError = NSError {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                            
                        }
                        return
                    }
                }
                else
                {
                    if (JSON["ResultResponse"].stringValue == "0") //scuess
                    {
                        CoverUploadedSucc = true
                        self.view.makeToast("Success")
                    }
                    else
                    {
                        self.view.makeToast("An error has been occurred")
                    }
                }
                self.doneUpload(PhotoUploadedSucc, Cover: CoverUploadedSucc, Profile: ProfileUpdatedSucc)
        })
            
        }
        
        let RequestParameters : NSDictionary = [
            
            "FullName": userNameText.text!,
            "MobileNumber": mobileNumberText.text!,
            "UserId":(User?.Id)!
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.updateProfileOne, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
               // MBProgressHUD.hideHUDForView(self.view, animated: true)
                ProfileUpdatedSucc = false
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    //let editProfileId = response.JSON["RequestId"].intValue
                    
                    if( resultResponse == "0"){
                        ProfileUpdatedSucc = true
                       
                        
                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
                self.doneUpload(PhotoUploadedSucc, Cover: CoverUploadedSucc, Profile: ProfileUpdatedSucc)
        })

 
        
    }
    func doneUpload(photo:Bool?,Cover:Bool?,Profile:Bool?)
    {
        
        
        if (photo != nil||profileTaken == false)&&(Cover != nil || coverTaken == false)&&Profile != nil
        {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            let editProfileTwo = self.storyboard?.instantiateViewControllerWithIdentifier("VcFashionPostOptions") as! VcFashionPostOptions
            
            editProfileTwo.updateProfile = true;
            
            editProfileTwo.fashionMaster = self.profileMaster
            self.navigationController?.pushViewController(editProfileTwo, animated: true)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
