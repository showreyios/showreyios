//
//  VcGroupsCreate.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/19/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Toast_Swift
import SwiftyJSON
import FirebaseAnalytics

class VcGetGroups: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var groups = [ModGroupsobj]();
    var selectedGroups:[Int] = []
    var PagesCount = 1
    static var groupsStatic:VcGetGroups!
    var tvCreatePost : TvCreatePost!
    var feedProfilePros : ModFeedProfilePros!
    
    @IBOutlet weak var groupTable: UITableView!
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnAdd: UIBarButtonItem!
    @IBOutlet weak var LblDefaultMsg: UILabel!
    @IBOutlet weak var imgDeafultMsg: UIImageView!
    
    enum comeFrom : Int{
        
        case createGroup = 0
        case createPost = 1
        case welcome = 2
    }
    
    var come = comeFrom.createGroup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = "Lists"
        getGroups(PagesCount, FromPagerOrRefresher: false, dontAppend: false)
        groupTable.addInfiniteScrollWithHandler { (UITableView) in
            
            self.getGroups(self.PagesCount, FromPagerOrRefresher: true, dontAppend: false)
        }
        
        
        
        VcGetGroups.groupsStatic = self
        // Do any additional setup after loading the view.
        FIRAnalytics.logEventWithName("MYBroadCasts", parameters: nil)
        
        if come == .createGroup || come == .welcome {
            
            if come == .createGroup {
                let button = UIButton()
                button.frame = CGRectMake(0, 0, 21, 31)
                button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
                button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
                let barButton = UIBarButtonItem()
                barButton.customView = button
                navigationItem.leftBarButtonItem = barButton
            }
            
            imgDeafultMsg.hidden = false
            self.groupTable.bounds.size.height = (self.view.bounds.size.height - 64)
            self.groupTable.frame.origin.y = 64
        }
        
        if come == .createPost {
            
            LblDefaultMsg.text = "No lists to show"
            LblDefaultMsg.frame.origin.y = self.view.center.y
            imgDeafultMsg.hidden = true
            groupTable.allowsSelection = true
            btnSend.hidden = false
            btnAdd.enabled = false
            
            navigationItem.rightBarButtonItem?.tintColor = UIColor.clearColor();
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        groupTable.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if groups.count == 0 {
            tableView.hidden = true
            if come == .createPost {
                btnSend.hidden = true
            }
        }else {
            tableView.hidden = false
            if come == .createPost {
                btnSend.hidden = false
            }
        }
        
        return groups.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: TcGroups = tableView.dequeueReusableCellWithIdentifier("TcGroups", forIndexPath: indexPath) as! TcGroups
        
        // cell.imgGroup.image = UIImage(named: "back" )
        
        cell.imgGroup.layer.cornerRadius = cell.imgGroup.frame.size.width / 2
        cell.imgGroup.clipsToBounds = true
        if  (groups[indexPath.row].ProfilePicture!) == "" {
            cell.imgGroup.contentMode = .Center
            
        }else {
            cell.imgGroup.contentMode = .ScaleAspectFill
            
        }
        cell.imgGroup.kf_setImageWithURL(NSURL(string: (groups[indexPath.row].ProfilePicture!)),placeholderImage:UIImage(named:"Default-Group"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        cell.lblGroupName.text = groups[indexPath.row].Name
        
        // Mo7eb :P
        if indexPath.row % 2 == 0 {
            
            cell.backgroundColor = UIColor(hex: "#f7f7f7")
            
        }else {
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        if come == .createGroup || come == .welcome {
            
            cell.btnAdds.hidden = true
        }
        
        cell.imgGroup.tag = indexPath.row
        
        if come == .createPost {
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VcGetGroups.showProfileInfo(_:)))
            
            cell.imgGroup.userInteractionEnabled = true
            
            cell.imgGroup.addGestureRecognizer(tapGestureRecognizer)
            if selectedGroups.contains((groups[indexPath.row].Id?.integerValue)!) {
                cell.btnAdds.setImage(UIImage(named: "Check-Mark"), forState: .Normal)
                
            }else{
                cell.btnAdds.setImage(UIImage(named: "Add-Mark"), forState: .Normal)
            }
            
            cell.btnAdds.tag = groups[indexPath.row].Id as! Int
            cell.btnAdds.addTarget(self, action:  #selector(addGroup(_:)), forControlEvents: .TouchUpInside)
            
        }
        
        cell.selectionStyle = .None
        
        return cell
    }
    
    func showProfileInfo(sender:AnyObject){
        print("you tap image number : \(sender.view.tag)")
        let createGroup = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupPageWall") as! VcGroupPageWall
        
        createGroup.groupId = Int(groups[sender.view.tag].Id!)
        createGroup.groupNameStr = groups[sender.view.tag].Name
        createGroup.privacy = groups[sender.view.tag].Privacy
        createGroup.profileImage = groups[sender.view.tag].ProfilePicture
        
        self.navigationController?.pushViewController(createGroup, animated: true)
    }
    
    func addGroup(mybutton: UIButton)
    {
        
        let img = mybutton.imageForState(.Normal)
        
        
        if(img  == UIImage(named: "Check-Mark"))
        {
            mybutton.setImage(UIImage(named: "Add-Mark"), forState: .Normal)
            selectedGroups = selectedGroups.filter { $0 != mybutton.tag }
            
            
        }
        else{
            
            mybutton.setImage(UIImage(named: "Check-Mark"), forState: .Normal)
            selectedGroups.append(mybutton.tag)
            
            
        }
        
        
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        if come == .createPost {
            let cell = tableView.cellForRowAtIndexPath(indexPath) as!
            TcGroups
            let mybutton = cell.btnAdds
            let img = mybutton.imageForState(.Normal)
            
            
            if(img  == UIImage(named: "Check-Mark"))
            {
                mybutton.setImage(UIImage(named: "Add-Mark"), forState: .Normal)
                selectedGroups = selectedGroups.filter { $0 != mybutton.tag }
                
                
            }
            else{
                
                mybutton.setImage(UIImage(named: "Check-Mark"), forState: .Normal)
                selectedGroups.append(mybutton.tag)
                
                
            }
            
        } else {
            
            let createGroup = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupPageWall") as! VcGroupPageWall
            
            createGroup.groupId = Int(groups[indexPath.row].Id!)
            createGroup.groupNameStr = groups[indexPath.row].Name
            createGroup.privacy = groups[indexPath.row].Privacy
            createGroup.profileImage = groups[indexPath.row].ProfilePicture
            
            self.navigationController?.pushViewController(createGroup, animated: true)
            
        }
        
        // moved to be on image click only
        /*
        
         */
        
    }
    
    ///////////send group ID////////////////////////////
    @IBAction func actionBtn(sender: AnyObject) {
        if selectedGroups.count == 0 {
            
            self.view.makeToast("Please select group")
            return
        }
        
        tvCreatePost.CreatePost(self.view, FeedProfilePros: feedProfilePros ,comeFromWelcome: true, WPostToId: selectedGroups, WPostToType: 2)
        
    }
    @IBAction func addGroups(sender: AnyObject) {
        
        let createGroup = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupSelectingList") as! VcGroupSelectingList
        self.navigationController?.pushViewController(createGroup, animated: true)
        
        // self.navigationItem.leftBarButtonItem!.title! = "back"
        // self.navigationItem.backBarButtonItem! = UIBarButtonItem(barButtonSystemItem: "back", target: nil?, action: nil)
        
    }
    
    func getGroups(index:Int, FromPagerOrRefresher:Bool, dontAppend:Bool){
        
        if !FromPagerOrRefresher
        {
            let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            loadingNotification.label.text = "Loading..."
        }
        
        if dontAppend == true {
            
            self.groups = []
            self.PagesCount = 1
        }
        
        let RequestParameters : NSDictionary = [
            "PageIndex" : index,
            "PageSize" :100,
            "ProfileUserId" : User!.Id!,
            "UserId" : User!.Id!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetMyGroups, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                self.groupTable.finishInfiniteScroll()
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response = ModGetGroupsResponse(json:JSON)
                        
                        if (Response.ResultResponse == "21"){
                            self.view.makeToast("An error has been occurred")
                            
                            
                        }
                        else
                        {
                            //self.view.makeToast("got the groups :D")
                            
                            self.groups.appendContentsOf(Response.Groupsobj!);
                            self.PagesCount += 1
                            self.groupTable.reloadData()
                            
                            
                            
                        }
                        
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
