//
//  VcRegisteration.swift
//  ShowRey
//
//  Created by Appsinnovate on 9/20/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import PureLayout
import EGFloatingTextField
import Alamofire
import MBProgressHUD
import Toast_Swift
import SwiftyJSON
import NVActivityIndicatorView

class VcRegisteration: UIViewController ,UITextFieldDelegate{
    
    @IBOutlet weak var LblErrorRetype: UILabel!
    @IBOutlet weak var LblErrorPassword: UILabel!
    @IBOutlet weak var LblErrorFull: UILabel!
    @IBOutlet weak var LblErrorUser: UILabel!
    @IBOutlet weak var LblErrorEmail: UILabel!
    @IBOutlet weak var LblErrorMobileNumber: UILabel!
    @IBOutlet weak var LblErrorInviteCode: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    @IBOutlet weak var TxtUserName: EGFloatingTextField!
    @IBOutlet weak var TxtFullName: EGFloatingTextField!
    @IBOutlet weak var TxtPassword: EGFloatingTextField!
    @IBOutlet weak var TxtMobileNumber: EGFloatingTextField!
    @IBOutlet weak var TxtRetypePassword: EGFloatingTextField!
    @IBOutlet weak var TxtEmail: EGFloatingTextField!
    @IBOutlet weak var TxtInviteCode: EGFloatingTextField!
    
    @IBOutlet weak var IndicatorUserName: NVActivityIndicatorView!
    
    @IBOutlet weak var IndicatorEmail: NVActivityIndicatorView!
    var instagramProfile : SLInstagramUser!
    
    var FacebookName = "";
    var FacebookEmail = "";
    var FacebookID = "";
    var FacebookImageUrl = "";
    
    
    
    override func viewDidAppear(animated: Bool) {
        
        TxtUserName.setPlaceHolder(" User Name")
        TxtFullName.setPlaceHolder(" Full Name")
        TxtMobileNumber.setPlaceHolder(" Mobile Number")
        TxtInviteCode.setPlaceHolder(" Invite Code (optional)")
        TxtPassword.setPlaceHolder(" Password")
        TxtRetypePassword.setPlaceHolder(" Retype Password")
        TxtEmail.setPlaceHolder(" Email Address (optional)")
        
        if (self.TxtUserName.text != ""){
            self.UserNameEndEdit(self.TxtUserName);
        }
        
        /*
         if (self.TxtEmail.text != ""){
         self.EmailEndEdit(self.TxtEmail);
         }
         */
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = "Register"
        
        TxtUserName.floatingLabel = true
        // TxtUserName.text = "radwa5"
        TxtUserName.validationType = .Empty
        TxtUserName.errorLabel = LblErrorUser
        //  TxtUserName.customerrorMessage = "Enter username"
        
        
        
        
        TxtFullName.floatingLabel = true
        TxtFullName.validationType = .OnlyText
        //  TxtFullName.text = "ks"
        TxtFullName.errorLabel = LblErrorFull
        //  TxtFullName.customerrorMessage = "Full name should be less than 20 charachter "
        
        TxtPassword.floatingLabel = true
        
        //  TxtPassword.text = "ks"
        TxtPassword.validationType = .Password
        TxtPassword.errorLabel = LblErrorPassword
        // TxtPassword.customerrorMessage = "Password should be more than 5 characters"
        TxtPassword.tag = 2
        TxtRetypePassword.floatingLabel = true
        // TxtPassword.text = "ss"
        
        TxtRetypePassword.validationType = .ConfirmPassword
        TxtRetypePassword.validationARG1 = TxtPassword
        TxtRetypePassword.errorLabel = LblErrorRetype
        TxtRetypePassword.customerrorMessage = "Password dosen't match"
        TxtRetypePassword.tag = 2
        
        TxtEmail.floatingLabel = true
        //    TxtEmail.text = "Mina.fared@appsinnovate.com"
        TxtEmail.validationType = .Email
        //TxtEmail.errorLabel = LblErrorEmail
        // TxtEmail.customerrorMessage = "Enter a valid email address"
        
        
        TxtMobileNumber.floatingLabel = true
        TxtMobileNumber.validationType = .Phone
        TxtMobileNumber.errorLabel = LblErrorMobileNumber
        // TxtMobileNumber.customerrorMessage = "Phone number should be less than 15 charachter"
        
        TxtInviteCode.floatingLabel = true
        
        // Do any additional setup after loading the view.
        if(instagramProfile != nil)
        {
            TxtUserName.text = instagramProfile.username
            TxtFullName.text = instagramProfile.fullName
            TxtPassword.hidden = true
            TxtRetypePassword.hidden = true
        }
        else if (FacebookID != "")
        {
            TxtEmail.text = FacebookEmail
            TxtFullName.text = FacebookName
            TxtPassword.hidden = true
            TxtRetypePassword.hidden = true
            
        }
        else
        {
            TxtPassword.hidden = false
            TxtRetypePassword.hidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func BtnRegister(sender: AnyObject)
    {
        
        //  if let accessToken = AccessToken.current {
        
        for subview in self.view.subviews {
            
            if subview.tag == 2 && subview.hidden
            {
                
                continue
                
            }
            if let textField = subview as? EGFloatingTextField {
                
                textField.resignFirstResponder()
                
                if textField.hasError == true
                {
                    return
                }
            }
            
        }
        
        var RequestParameters: [String:String] = [:]  //= NSDictionary()
        var wsmethod = WSMethods.Registration
        
        if(instagramProfile == nil && FacebookID == "")
        {
            RequestParameters = [
                "City":"",
                "Country":"",
                "Email":"\(TxtEmail.text!)",
                "FullName":"\(TxtFullName.text ?? "")",
                "MobileNumber":"\(countryCodeLabel.text! + TxtMobileNumber.text!)",
                "Password":"\(TxtPassword.text ?? "")",
                "RegId":"",
                "Username":"\(TxtUserName.text ?? "")",
                "InviteCode":"\(TxtInviteCode.text ?? "")"
            ]
        }
        else
        {
            wsmethod = WSMethods.RegistrationSocial
            //RequestParameters = [];
            
            //AA : To remove the first zero if its zero
            if (TxtMobileNumber.text?.hasPrefix("0"))! {
                TxtMobileNumber.text?.removeAtIndex((TxtMobileNumber.text?.startIndex)!)
            }
            
            if (self.countryCodeLabel.text?.hasPrefix("+"))! {
                self.countryCodeLabel.text?.removeAtIndex((self.countryCodeLabel.text?.startIndex)!)
            }
            
            RequestParameters["RegType"] = (instagramProfile != nil ? "3" : "2")
            RequestParameters["SMUserId"] = (instagramProfile != nil ? instagramProfile!.ID!: FacebookID)
            RequestParameters["City"] = ""
            RequestParameters["Country"] = ""
            RequestParameters["Email"] = "\(TxtEmail.text ?? "")"
            RequestParameters["FullName"] = "\(TxtFullName.text ?? "")"
            RequestParameters["MobileNumber"] = "\(countryCodeLabel.text! + TxtMobileNumber.text!)"
            RequestParameters["ProfilePicture"] = (instagramProfile != nil ? (instagramProfile.profilePictureURL ?? "") : FacebookImageUrl)
            //                "Password":"\(TxtPassword.text)",
            RequestParameters["RegId"] = ""
            RequestParameters["Username"] = "\(TxtUserName.text ?? "")"
            RequestParameters["InviteCode"] = "\(TxtInviteCode.text ?? "")"
            
        }
        
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: wsmethod, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString , responseType: ResponseType.DictionaryJson
            , callbackString: nil, callbackDictionary: jsonCallBack)
        
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
    }
    
    
    func jsonCallBack(response: (JSON: JSON, NSError: NSError?))
    {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    self.view.makeToast("No Internet connection")
                }
                else
                {
                    self.view.makeToast("An error has been occurred")
                    print("error")
                }
                return
            }
        }
        else
        {
            
            //  let ResultResponse = response.JSON["ResultResponse"].stringValue;
            
            let UserId = response.JSON["UserId"].stringValue;
            
            
            // get profile
            
            
            AppDelegate.RoutToScreen("VcLogin")
            
            //            VcLogin.GetProfile(self, UserID: UserId, complition: { (success,pro) in
            //                if(success)
            //                {
            //
            //                    AppDelegate.RoutToScreen("VcPostingOptions")
            //                }
            //
            //            })
        }
        
    }
    
    @IBAction func UserNameEndEdit(sender: AnyObject) {
        
        if ( sender.text == ""){return}
        
        IndicatorUserName.startAnimating()
        
        
        let RequestParameters : NSDictionary = [
            "MobileNumberorUserName" : TxtUserName.text!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.NickNameValidator, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString:nil , callbackDictionary:
            {response in
                
                self.IndicatorUserName.stopAnimating()
                
                if((response.NSError) != nil)
                {
                    
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                        }
                        return
                    }
                }
                else{
                    if (response.JSON["ResultResponse"].stringValue != "0")
                    {
                        self.LblErrorUser.text = response.JSON["Description"].string
                        self.TxtUserName.hasError = true
                        self.LblErrorUser.hidden = false
                    }
                    else{
                        //       self.TxtUserName.hasError = false
                    }
                    
                    
                }
                
            }
        )
        
    }
    
    @IBAction func EmailEndEdit(sender: AnyObject) {
        
        self.TxtEmail.hasError = false
        
        /*
         if ( sender.text == ""){return}
         IndicatorEmail.startAnimating()
         
         
         let RequestParameters : NSDictionary = [
         "EmailorUserName" : TxtEmail.text!
         ]
         
         let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
         
         NetworkHelper.RequestHelper(nil, service: WSMethods.EmailValidator, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
         , responseType: ResponseType.DictionaryJson , callbackString:nil , callbackDictionary:
         {response in
         
         self.IndicatorEmail.stopAnimating()
         
         if((response.NSError) != nil)
         {
         
         if let networkError = response.NSError {
         if (networkError.code == -1009) {
         self.view.makeToast("No Internet connection")
         
         }
         else
         {
         self.view.makeToast("An error has been occurred")
         }
         return
         }
         }
         else{
         if (response.JSON["ResultResponse"].stringValue != "0")
         {
         self.LblErrorEmail.text = response.JSON["Description"].string
         self.TxtEmail.hasError = true
         self.LblErrorEmail.hidden = false
         }
         else{
         //  self.TxtEmail.hasError = false
         }
         
         
         }
         
         }
         )
         */
        
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if (textField.tag == 10 ) //userName
        {
            if (string == " " || string == "." || string == "@" || string == "#") {
                return false
            }
            return true
        }
        else
        {
            return true
        }
        
    }
    
    // MARK: - Actions
    
    @IBAction func termsAndCond(sender: AnyObject) {
        
        let view = self.storyboard?.instantiateViewControllerWithIdentifier("VcTerms") as! VcTerms
        self.navigationController?.pushViewController(view, animated: true)
        view.terms = 2
    }
    
    @IBAction func selectCountryCodeAction(sender: AnyObject) {
        
        let countryListVc = CountryListViewController(nibName: "CountryListViewController", delegate: self)
        self.presentViewController(countryListVc, animated: true, completion: nil)
        
    }
    
    // MARK: - CountryListViewDelegate Methods
    
    func didSelectCountry(country: [NSObject : AnyObject]!) {
        
        //AA : To update country code label
        countryCodeLabel.text = country["dial_code"] as? String ?? ""
        
    }
    
}



