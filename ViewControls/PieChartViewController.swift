//
//  PieChartViewController.swift
//  ShowRey
//
//  Created by User on 12/20/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import CorePlot
import Alamofire
import MBProgressHUD
import Toast_Swift
import SwiftyJSON
import FirebaseAnalytics

class PieChartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    var postStatistics = [ModPostStatistics]()
    var groupId: Int = 0
    var postId: Int = 0
    var h:CGFloat = 0
    var yesFracArr : [Float]!
    var noFracArr : [Float]!
    var noAnsFracArr : [Float]!
    
    enum comeFrom : Int{
        case profile = 0
        case group = 1
        case post = 2
    }

    var come = comeFrom.profile
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        tableView.registerClass(TcProfileStatistics.self, forCellReuseIdentifier: "TcProfileStatistics")
        tableView.registerNib(UINib(nibName: "TcProfileStatistics",bundle: nil), forCellReuseIdentifier: "TcProfileStatistics")
        
        tableView.registerClass(TcNewsFeed.self, forCellReuseIdentifier: "TcStatisticColor")
        tableView.registerNib(UINib(nibName: "TcStatisticColor",bundle: nil), forCellReuseIdentifier: "TcStatisticColor")
        

        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 232
        
        
        
       
        if come == comeFrom.post || come == comeFrom.group {
            
            getStatisticsByPostId()
            tableView.separatorStyle = .None
            tableView.scrollEnabled = false
            
        }else if come == comeFrom.profile{
            
            getStatisticsForProfile()
            
        }
        
      FIRAnalytics.logEventWithName("Profile", parameters: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //tableView.reloadData()
    }
    
    
      // MARK: - table data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  
        if(section == 0)
        {
            return 1
            
        }
        
        if postStatistics.count == 0 {
            
            tableView.hidden = true
        
        }else {
            
            tableView.hidden = false
            
        }

        return postStatistics.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        if(indexPath.section == 0)
        {
           
                let colorCell = tableView.dequeueReusableCellWithIdentifier("TcStatisticColor") as! TcStatisticColor
            
            
            
                return colorCell
            
        }else {
            
            
            let cell = tableView.dequeueReusableCellWithIdentifier("TcProfileStatistics") as! TcProfileStatistics
            
            
            cell.lblTitle.text = postStatistics[indexPath.row].Title
            cell.lblDate.text = postStatistics[indexPath.row].Date
            
            let yesCount = Int(self.postStatistics[indexPath.row].YES!.floatValue * 10.0)
            let noCount = Int(self.postStatistics[indexPath.row].NO!.floatValue * 10.0)
            let noAnsCount = Int(self.postStatistics[indexPath.row].NoAnswer!.floatValue * 10.0)
            cell.dataForChart = [0.0,0.0,0.0]
            
            print("\(yesCount), \(noCount), \(noAnsCount)" )
            
            
            
            if (yesCount == 0) && (noCount == 0) && (noAnsCount == 0){
                
                for i in 1...10 {
                    
                    cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#ffffff")
                }
                
                
                
            }else{
                
                cell.dataForChart = [(noAnsFracArr[indexPath.row]), (noFracArr[indexPath.row]),(yesFracArr[indexPath.row])]
                
                if yesCount != 0 {
                    
                    for i in 1...yesCount{
                        
                        cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#b379e7")
                    }
                }
                
                if noCount != 0 {
                    
                    if !((yesCount + noCount) < yesCount + 1) {
                        
                        for i in yesCount + 1...(yesCount + noCount) {
                            cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#E5833A")
                        }
                    }
                }
                
                if noAnsCount != 0 {
                    if !((yesCount + noCount + noAnsCount) < (yesCount + noCount + 1)) {
                        for i in (yesCount + noCount + 1)...(yesCount + noCount + noAnsCount) {
                            cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#9E9FA3")
                        }
                    }
                }
                
                
                
            }
            
            
            return cell
            
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            
            return 70
        }else {
            return UITableViewAutomaticDimension
        }
    }
    
    
    func getStatisticsForProfile(){
        
        
            let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            loadingNotification.label.text = "Loading..."
        
        
        let RequestParameters : NSDictionary = [
            "GroupId": 0,
            "PostId": 0,
            "UserId" : User!.Id!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetPostStatistics, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response = ModPostStatisticsResponse(json:JSON)
                        
                        if (Response.ResultResponse == "21"){
                            self.view.makeToast("An error has been occurred")
                        }
                        else
                        {
                            
                            self.postStatistics.appendContentsOf(Response.PostStatistics!)
                            
                            self.yesFracArr = [Float] ()
                            self.noFracArr = [Float]()
                            self.noAnsFracArr = [Float] ()
                           
                            for i in 0...9 {
                                
                               // print((((self.postStatistics[i].YES)?.floatValue)! * 10.0 ) % 1)
                               let yesFracPart = (((self.postStatistics[i].YES)?.floatValue)! * 10.0 ) % 1
                               let noFracPart = (((self.postStatistics[i].NO)?.floatValue)! * 10.0 ) % 1
                               let noAnsFracPart = (((self.postStatistics[i].NoAnswer)?.floatValue)! * 10.0 ) % 1
                                
                               self.yesFracArr.append(yesFracPart)
                               self.noFracArr.append(noFracPart)
                               self.noAnsFracArr.append(noAnsFracPart)
                             
                            }
                         
                            
                            self.tableView.reloadData()
                            
                            
                            
                            
                        }
                        
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
        
    }
    
    
    func getStatisticsByPostId(){
        
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
        
        let RequestParameters : NSDictionary = [
            "GroupId": groupId,
            "PostId": postId,
            "UserId" : User!.Id!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetPostStatisticbyPostId, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response = ModPostStatisticsResponse(json:JSON)
                        
                        if (Response.ResultResponse == "21"){
                            self.view.makeToast("An error has been occurred")
                        }
                        else
                        {
                            
                            self.postStatistics.append(Response.PostStatisticob!)
                            
                            self.yesFracArr = [Float] ()
                            self.noFracArr = [Float]()
                            self.noAnsFracArr = [Float] ()
                            
                            
                                
                                // print((((self.postStatistics[i].YES)?.floatValue)! * 10.0 ) % 1)
                                let yesFracPart = (((self.postStatistics[0].YES)?.floatValue)! * 10.0 ) % 1
                                let noFracPart = (((self.postStatistics[0].NO)?.floatValue)! * 10.0 ) % 1
                                let noAnsFracPart = (((self.postStatistics[0].NoAnswer)?.floatValue)! * 10.0 ) % 1
                                
                                self.yesFracArr.append(yesFracPart)
                                self.noFracArr.append(noFracPart)
                                self.noAnsFracArr.append(noAnsFracPart)
                                
                
                            
                            
                            self.tableView.reloadData()
                            
                            
                            
                            
                        }
                        
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
        
    }

    
    
    
//    
//    if (yesCount == 1) && (noCount == 0) && (noAnsCount == 0) {
//    
//    for i in 1...10 {
//    
//    cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#b379e7")
//    }
//    
//    }
//    
//    if (yesCount == 0) && (noCount == 1) && (noAnsCount == 0){
//    
//    for i in 1...10 {
//    
//    cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#E5833A")
//    }
//    
//    }
//    
//    if (yesCount == 0) && (noCount == 0) && (noAnsCount == 1) {
//    
//    for i in 1...10 {
//    
//    cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#9E9FA3")
//    }
//    
//    }
//    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
