//
//  CcOffers.swift
//  ShowRey
//
//  Created by Appsinnovate on 10/8/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class CcOffers: UICollectionViewCell {
    
    @IBOutlet weak var shade: UIView!
    @IBOutlet weak var Container: UIView!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var lblPriceOne: UILabel!
    @IBOutlet weak var lblPriceTwo: UILabel!
    var setData : ModFeedFeedobj!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Container.layer.cornerRadius = 3
        Container.clipsToBounds = true
        
    }
    override func layoutSubviews()
    {
        super.layoutSubviews()
        // shadow
        Styles.Shadow(self, target: Container, Radus: 2,shadowview: nil)
        displayOfferData()
    }
    
    func SetData(data:ModFeedFeedobj)
    {//
       // if (setData == nil)
       // {
        setData = data
      //  displayOfferData()
        layoutSubviews()
      //  }
    }
    
    func displayOfferData(){
        
        for subvivew in imgView.subviews
        {
            subvivew.removeFromSuperview()
        }
        if (setData.PostCType == PostContentType.Text_Videos.rawValue)
        {
            // imgView.kf_setImageWithURL(NSURL(string: data.AttachedFiles?[0].Thumb ?? ""), placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
            // let bf = CGRect(x: 0 , y: 0 , width:imgView.frame.size.width , height: imgView.frame.size.height)
           
            let videoContainerView = VedioThumbnail.videoViewWithAttachedFile(setData.AttachedFiles?[0], frame: imgView.frame) { }
            
            
            
            imgView.addSubview(videoContainerView)
            //          imgView.hidden = true
        }
        else
        {
            imgView.kf_setImageWithURL(NSURL(string: setData.AttachedFiles?[0].URL ?? ""), placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: setData.OriginalPrice!.stringValue + " SAR")
        attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
        
        lblCaption.text = setData.Title!
        lblPriceOne.attributedText = attributeString
        lblPriceTwo.text = setData.Price!.stringValue + " SAR"
        
        
    }
    

}


