//
//  VcPostStatistics.swift
//  ShowRey
//
//  Created by User on 1/23/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Toast_Swift
import SwiftyJSON
import FirebaseAnalytics

class VcPostStatistics: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    
    @IBOutlet weak var statisticsTable: UITableView!
    
    
    var postStatistics = [ModPostStatistics]()
    
    var postID = 0
    var groupId: Int = 0
    var postId: Int = 0
    var h:CGFloat = 0
    var yesFracArr : [Float]!
    var noFracArr : [Float]!
    var noAnsFracArr : [Float]!
    
    enum comeFrom : Int{
        case profile = 0
        case group = 1
        case post = 2
    }
    
    var come = comeFrom.profile
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        statisticsTable.registerClass(TcStackBar.self, forCellReuseIdentifier: "TcStackBar")
        statisticsTable.registerNib(UINib(nibName: "TcStackBar",bundle: nil), forCellReuseIdentifier: "TcStackBar")
        
        statisticsTable.rowHeight = UITableViewAutomaticDimension
        statisticsTable.estimatedRowHeight = 232
        
        
        
        
        if come == comeFrom.post || come == comeFrom.group {
            
            getStatisticsByPostId()
            statisticsTable.separatorStyle = .None
            statisticsTable.scrollEnabled = false
            
        }else if come == comeFrom.profile{
            
            getStatisticsForProfile()
            
        }
        
        FIRAnalytics.logEventWithName("Profile", parameters: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //tableView.reloadData()
    }
    
    
    // MARK: - table data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if postStatistics.count == 0 {
            
            tableView.hidden = true
            
        }else {
            
            tableView.hidden = false
            
        }
        
        return postStatistics.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("TcStackBar") as! TcStackBar
        
        
        cell.lblTitle.text = postStatistics[indexPath.row].Title
        cell.lblDate.text = postStatistics[indexPath.row].Date
        cell.usersCount.text = "\((postStatistics[indexPath.row].UserCount)!) USERS"
        
        let yesCount = Int(self.postStatistics[indexPath.row].YES!.floatValue * 100.0)
        let noCount = Int(self.postStatistics[indexPath.row].NO!.floatValue * 100.0)
        let noAnsCount = Int( self.postStatistics[indexPath.row].NoAnswer!.floatValue * 100.0)
        
        if yesCount == 0 && noCount == 0 && noAnsCount == 0 {
            cell.dataForChart = []
            cell.containerView.hidden = true
        }else{
            cell.containerView.hidden = false
            cell.dataForChart = [yesCount,noCount,noAnsCount]
        }
        
        postID = (postStatistics[indexPath.row].PostId?.integerValue)!
        
        cell.outeltofviewNo.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toDisLikers)))
        cell.outeltofviewYes.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toLikers)))
        cell.cotentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toAll)))
        
        
        cell.lblYes.text = "\(yesCount) %"
        cell.lblNo.text = "\(noCount) %"
        cell.lblNoAns.text = "\(noAnsCount) %"
        cell.lblUsersYes.text = "\((postStatistics[indexPath.row].CountYES)!) Users" // "100 Users"
        cell.lblUsersNo.text = "\((postStatistics[indexPath.row].CountNO)!) Users" //"30 Users"
        cell.lblUsersNoAns.text =  "\((postStatistics[indexPath.row].CountNoAnswer)!) Users"//"10 Users"
        
        print("\(yesCount), \(noCount), \(noAnsCount)" )
        
        
        //            if (yesCount == 0) && (noCount == 0) && (noAnsCount == 0){
        //
        //                for i in 1...10 {
        //
        //                    cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#ffffff")
        //                }
        //
        //
        //
        //            }else{
        //
        //  cell.dataForChart = [(yesFracArr[indexPath.row]),(noFracArr[indexPath.row]),(noAnsFracArr[indexPath.row])]
        //
        //                if yesCount != 0 {
        //
        //                    for i in 1...yesCount{
        //
        //                        cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#b379e7")
        //                    }
        //                }
        //
        //                if noCount != 0 {
        //
        //                    if !((yesCount + noCount) < yesCount + 1) {
        //
        //                        for i in yesCount + 1...(yesCount + noCount) {
        //                            cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#E5833A")
        //                        }
        //                    }
        //                }
        //
        //                if noAnsCount != 0 {
        //                    if !((yesCount + noCount + noAnsCount) < (yesCount + noCount + 1)) {
        //                        for i in (yesCount + noCount + 1)...(yesCount + noCount + noAnsCount) {
        //                            cell.viewWithTag(i)?.backgroundColor = UIColor(hex: "#9E9FA3")
        //                        }
        //                    }
        //                }
        //
        //
        //
        //            }
        
        
        return cell
        
        
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let commentsController = self.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .All
         commentsController.InitMod( 0, ObjId: (postStatistics[indexPath.row].PostId?.integerValue)!, PostType: 1,isItPrivate : false)
        
        self.navigationController?.pushViewController(commentsController, animated: true)
        
    }
    
    func toLikers()
    {
        //        let likers = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        //        likers.LikersMod(0, LikeType: true, ObjId: (FeedData.Id?.integerValue)!)
        //        likers.title = "Likers"
        //        Parent.parent.navigationController?.pushViewController(likers, animated: true)
        
        let commentsController = self.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .Likers
        // commentsController.InitMod( 0, ObjId: (FeedData.Id?.integerValue)!, PostType: (FeedData.PostType?.integerValue)!, isItPrivate : false)
        
          commentsController.InitMod( 0, ObjId: postID, PostType: 1,isItPrivate : false)
        
        self.navigationController?.pushViewController(commentsController, animated: true)
        
    }
    
    func toDisLikers()
    {
        //        let likers = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        //        likers.LikersMod(0, LikeType: false, ObjId: (FeedData.Id?.integerValue)!)
        //        likers.title = "Dislikers"
        //        Parent.parent.navigationController?.pushViewController(likers, animated: true)
        //
        let commentsController = self.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .DisLikers
        commentsController.InitMod( 0, ObjId: postID, PostType: 1,isItPrivate : false)
        
        self.navigationController?.pushViewController(commentsController, animated: true)
    }
    
    func toAll()
    {
        let commentsController = self.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .All
        commentsController.InitMod( 0, ObjId: postID, PostType: 1,isItPrivate : false)
        
        self.navigationController?.pushViewController(commentsController, animated: true)
        
    }
    
    
    func getStatisticsForProfile(){
        
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
        
        let RequestParameters : NSDictionary = [
            "GroupId": 0,
            "PostId": 0,
            "UserId" : User!.Id!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetPostStatistics, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                print("🤡 res =\(response ) ,,json = \(response.result) ,,,val = \(response.result.value )")
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response = ModPostStatisticsResponse(json:JSON)
                        
                        if (Response.ResultResponse == "21"){
                            self.view.makeToast("An error has been occurred")
                        }
                        else
                        {
                            
                            self.postStatistics.appendContentsOf(Response.PostStatistics!)
                            print(self.postStatistics)
                            //                            self.yesFracArr = [Float] ()
                            //                            self.noFracArr = [Float]()
                            //                            self.noAnsFracArr = [Float] ()
                            //
                            //                            for i in 0...9 {
                            //
                            //                                // print((((self.postStatistics[i].YES)?.floatValue)! * 10.0 ) % 1)
                            //                                let yesFracPart = (((self.postStatistics[i].YES)?.floatValue)! * 10.0 ) % 1
                            //                                let noFracPart = (((self.postStatistics[i].NO)?.floatValue)! * 10.0 ) % 1
                            //                                let noAnsFracPart = (((self.postStatistics[i].NoAnswer)?.floatValue)! * 10.0 ) % 1
                            //
                            //                                self.yesFracArr.append(yesFracPart)
                            //                                self.noFracArr.append(noFracPart)
                            //                                self.noAnsFracArr.append(noAnsFracPart)
                            //
                            //                            }
                            //
                            //
                            self.statisticsTable.reloadData()
                            
                            
                            
                            
                        }
                        
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
        
    }
    
    
    func getStatisticsByPostId(){
        
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
        
        let RequestParameters : NSDictionary = [
            "GroupId": groupId,
            "PostId": postId,
            "UserId" : User!.Id!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetPostStatisticbyPostId, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response = ModPostStatisticsResponse(json:JSON)
                        
                        if (Response.ResultResponse == "21"){
                            self.view.makeToast("An error has been occurred")
                        }
                        else
                        {
                            
                            self.postStatistics.append(Response.PostStatisticob!)
                            
                            //                            self.yesFracArr = [Float] ()
                            //                            self.noFracArr = [Float]()
                            //                            self.noAnsFracArr = [Float] ()
                            //
                            //
                            //
                            //                            // print((((self.postStatistics[i].YES)?.floatValue)! * 10.0 ) % 1)
                            //                            let yesFracPart = (((self.postStatistics[0].YES)?.floatValue)! * 10.0 ) % 1
                            //                            let noFracPart = (((self.postStatistics[0].NO)?.floatValue)! * 10.0 ) % 1
                            //                            let noAnsFracPart = (((self.postStatistics[0].NoAnswer)?.floatValue)! * 10.0 ) % 1
                            //                            
                            //                            self.yesFracArr.append(yesFracPart)
                            //                            self.noFracArr.append(noFracPart)
                            //                            self.noAnsFracArr.append(noAnsFracPart)
                            //                            
                            //                            
                            
                            
                            self.statisticsTable.reloadData()
                            
                            
                            
                            
                        }
                        
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
        
    }
}
