//
//  VcComments.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 11/27/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import MJAutoComplete
import DZNEmptyDataSet


class VcComments: UIViewController,UITableViewDelegate,UITableViewDataSource,MJAutoCompleteManagerDataSource, MJAutoCompleteManagerDelegate,UITextViewDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
    
    
    @IBOutlet weak var editingView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var WhisperBtn: UIButton!
    @IBOutlet weak var TableOfComments: UITableView!
    @IBOutlet weak var TxtComment: KMPlaceholderWrappers!
    
    @IBOutlet weak var privatePoP: UILabel!
    
    var modCommentsFollowersobj : [ModCommentsFollowersobj] = [ModCommentsFollowersobj]()
    
     var ParentComment: ModCommentsFollowersobj!
   
    
    // MARK:- AutoComplete init
    
    var firstTime:Bool = true
    var IsitReplay = false // for replay mod
    var ScreenMod = 0 //all 1 whisper
    var enterAutoComplete = true
    var isItPrivate: Bool = false
    
    var parentView: VcCommentsContainer!
    var parentViewforNotifcation: TvFashionPost!
    
    var autoCompleteMgr : MJAutoCompleteManager!
    var _TaggedUserId : NSMutableArray = NSMutableArray()
    var _MyTaggedUserIdComplete : NSMutableArray = NSMutableArray()
    var _myranges : [String] = []
    var didStartDownload : Bool = false;
    var autoCompleteContainer:UIView!
    
    var IsItSpecialOffer : Bool = false
    
    
    var ObjId = -1
    var PostType = -1
    var isWhisper = false
    
    var PagesCount = 1
    
    var refreshControl: UIRefreshControl!
    
    var mayAddCommentFromCreatePost = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (IsitReplay)
        {
         self.navigationItem.title = "Replies"
            
//            self.TableOfComments.center.y = self.TableOfComments.center.y + 64
//            self.TableOfComments.si.y = self.TableOfComments.center.y + 64
            self.TableOfComments.frame = CGRectMake(0 , 0,  self.TableOfComments.frame.width,  self.TableOfComments.frame.height - 60)
            self.editingView.frame = CGRectMake(0 , self.TableOfComments.frame.height ,  self.TableOfComments.frame.width,  73)

            
        }
        else
        {
             self.TableOfComments.frame = CGRectMake(0 , 0,  self.TableOfComments.frame.width,  self.TableOfComments.frame.height - 10)
        }
        TableOfComments!.emptyDataSetSource = self
        TableOfComments!.emptyDataSetDelegate = self
        
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(VcComments.handleRefresh), forControlEvents: UIControlEvents.ValueChanged)
        TableOfComments.addSubview(refreshControl)
        
        TableOfComments.addInfiniteScrollWithHandler {
            
            (UITableView) in
            
            self.handleRefresh(true)
        }
        
        
        
        autoCompleteContainer = UIView(frame: CGRect(x: 0, y: 210, width: UIScreen.mainScreen().bounds.width , height: UIScreen.mainScreen().bounds.height - 400))
        autoCompleteContainer.hidden = true
        self.scrollView.addSubview(autoCompleteContainer)
        
        // MARK:- Auto Complete DidLoad
        self.autoCompleteMgr = MJAutoCompleteManager()
        self.autoCompleteMgr.dataSource = self
        self.autoCompleteMgr.delegate = self
        let atTrigger :MJAutoCompleteTrigger = MJAutoCompleteTrigger(delimiter: "@")
        atTrigger.cell = "autoComCell";
        self.autoCompleteMgr.addAutoCompleteTrigger(atTrigger)
        
        self.view.bringSubviewToFront(self.autoCompleteContainer)
        
        self.autoCompleteMgr.container = self.autoCompleteContainer;
        self.TxtComment.delegate = self;
        
        
        
        
        TableOfComments.registerClass(TcCommentCellLevel1.self, forCellReuseIdentifier: "TcCommentCellLevel1")
        TableOfComments.registerNib(UINib(nibName: "TcCommentCellLevel1",bundle: nil), forCellReuseIdentifier: "TcCommentCellLevel1")
        
        
        TableOfComments.registerClass(TcCommentCellLevel2.self, forCellReuseIdentifier: "TcCommentCellLevel2")
        TableOfComments.registerNib(UINib(nibName: "TcCommentCellLevel2",bundle: nil), forCellReuseIdentifier: "TcCommentCellLevel2")
        
        
        TableOfComments.rowHeight = UITableViewAutomaticDimension
        TableOfComments.estimatedRowHeight = 100
        
        if (ScreenMod == 1 || isItPrivate == true) // whisper Mod or privete mod
        {
            if ScreenMod == 1 {
                WhisperBtn.enabled = false
                WhisperBtn.setImage(UIImage(named: "Comments-(Whisper-button)"), forState: .Normal)
            }
            isWhisper = !isWhisper
            privatePoP.hidden = false
            TableOfComments.frame = CGRectMake(0 , 0, TableOfComments.frame.width, TableOfComments.frame.height - 60)
            
        }
     
        
     
        
       privatePoP.layer.masksToBounds = true
        privatePoP.layer.cornerRadius = 5
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        scrollView.contentOffset = CGPointMake(0.0, 0.0)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if (parentViewforNotifcation != nil)
        {
        editingView.hidden = true
        }
        else
        {
        editingView.hidden = false
        }
        LoadComments()
        if (mayAddCommentFromCreatePost){
            if (self.parentView != nil)
            {
            if (self.parentView.Commentblock != nil)
            {
                self.parentView.Commentblock(self.ObjId)
            }
            }
        }
        scrollView.contentOffset = CGPointMake(0.0, 0.0)
    }
    
    func keyboardWasShown(notification: NSNotification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.scrollEnabled = true
        let info : NSDictionary = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        
        var plus :CGFloat!
        if firstTime {
            plus = 50.0
            
        }else {
             plus = 10.0
        }
        
        var slidePoint: CGFloat = 0.0
        let keyBoard_Y_Origin: CGFloat = self.view.bounds.size.height - (keyboardSize?.height)!
        let textFieldButtomPoint: CGFloat = TxtComment.superview!.frame.origin.y + (TxtComment.frame.origin.y + TxtComment.frame.size.height)
        if keyBoard_Y_Origin < textFieldButtomPoint - scrollView.contentOffset.y {
            slidePoint = textFieldButtomPoint - keyBoard_Y_Origin + plus
            let point = CGPointMake(0.0, slidePoint)
            scrollView.contentOffset = point
        }
        
    }
    
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "No Comments Found", attributes: [NSFontAttributeName : UIFont.boldSystemFontOfSize(18)
            ,NSForegroundColorAttributeName:UIColor.grayColor()])
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (IsitReplay){
            return modCommentsFollowersobj.count + 1
        }
        else
        {
            return modCommentsFollowersobj.count
        }
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if (!IsitReplay)// comment
        {
        let tcCommentCellLevel1 = tableView.dequeueReusableCellWithIdentifier("TcCommentCellLevel1") as! TcCommentCellLevel1
            
            tcCommentCellLevel1.IsItSpecialOffer = IsItSpecialOffer
        
        
        tcCommentCellLevel1.selectionStyle = UITableViewCellSelectionStyle.None
        tcCommentCellLevel1.parentView = parentView
            tcCommentCellLevel1.parentViewforNotifcation = parentViewforNotifcation
        tcCommentCellLevel1.setCell(modCommentsFollowersobj[indexPath.row])
        
        let CommentLongPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressDelete(_:)))  // for edit
        tcCommentCellLevel1.addGestureRecognizer(CommentLongPress);
        return tcCommentCellLevel1
        }
        else
        { // replay
            
            if(indexPath.row == 0)
            {
            let tcCommentCellLevel1 = tableView.dequeueReusableCellWithIdentifier("TcCommentCellLevel1") as! TcCommentCellLevel1
                
            tcCommentCellLevel1.IsItSpecialOffer = IsItSpecialOffer
            
            tcCommentCellLevel1.selectionStyle = UITableViewCellSelectionStyle.None
            tcCommentCellLevel1.parentView = parentView
                 tcCommentCellLevel1.parentViewforNotifcation = parentViewforNotifcation
            tcCommentCellLevel1.setCell(ParentComment)
            tcCommentCellLevel1.hidReplaies = true
            return tcCommentCellLevel1
            }
            else
            {
                let tcCommentCellLevel2 = tableView.dequeueReusableCellWithIdentifier("TcCommentCellLevel2") as! TcCommentCellLevel2
                
                tcCommentCellLevel2.IsItSpecialOffer = IsItSpecialOffer
                
                tcCommentCellLevel2.selectionStyle = UITableViewCellSelectionStyle.None
                tcCommentCellLevel2.parentView = parentView
                tcCommentCellLevel2.setCell(modCommentsFollowersobj[indexPath.row - 1])
                
                let CommentLongPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressDelete(_:)))  // for edit
                tcCommentCellLevel2.addGestureRecognizer(CommentLongPress);
                return tcCommentCellLevel2
            }
        }
        
    }
    
    
    
    func handleRefresh(AddPagetoInfinit: Bool = false)
    {
        
        LoadComments(false, AddPagetoInfinit: AddPagetoInfinit, GotolastRow: false)
        TableOfComments.finishInfiniteScroll()
        refreshControl.endRefreshing()
    }
    
    
    // MARK:- long Press action
    func longPressDelete(sender: UILongPressGestureRecognizer) {
        
        
       
        let pPoint :CGPoint =  sender.locationInView(self.TableOfComments)
       let indexPath = self.TableOfComments.indexPathForRowAtPoint(pPoint)!
        
        var rowIndex = indexPath.row
        if (IsitReplay) {rowIndex = rowIndex - 1}
        
        if (rowIndex == -1) {
            //   long press on table view but not on a row
        }
        else
        {
            
            // 1
            let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
            
            // 2
            
            // MARK:- Copy Comment
            let CopyAction = UIAlertAction(title: "Copy Text", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                UIPasteboard.generalPasteboard().string = self.modCommentsFollowersobj[rowIndex].Comment
            })
            
            // MARK:- Edit Comment
            let EditAction = UIAlertAction(title: "Edit", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                let tvCreatePost = self.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost
                if (self.IsItSpecialOffer){
                    tvCreatePost.FromScreen = .EditCommentOffers
                }
                else{
                    tvCreatePost.FromScreen = .EditComment
                }
                
                tvCreatePost.ObjId = self.ObjId
                let idnumber = self.modCommentsFollowersobj[rowIndex].Id!
                tvCreatePost.CommentID = idnumber.integerValue
                tvCreatePost.TxtComment = self.modCommentsFollowersobj[rowIndex].Comment!
                
                let E_TaggedUserId : NSMutableArray = NSMutableArray()
                let E_MyTaggedUserIdComplete : NSMutableArray = NSMutableArray()
                
                let tagedContexArray = self.modCommentsFollowersobj[rowIndex].TaggedUsers
                var ContextDictiony  = [String:String]()
                
                for tagedContex in tagedContexArray!  {
                    
                    ContextDictiony["UserName"] = tagedContex.UserName
                    ContextDictiony["Id"] = tagedContex.Id!.stringValue
                    ContextDictiony["FullName"] = tagedContex.FullName
                    ContextDictiony["ProfilePicture"] = tagedContex.ProfilePicture
                    ContextDictiony["FollowStatus"] = tagedContex.FollowStatus!.stringValue
                    
                    E_TaggedUserId.addObject(ContextDictiony["Id"]!)
                    E_MyTaggedUserIdComplete.addObject(ContextDictiony)
                    
                }
                
                tvCreatePost._TaggedUserId = E_TaggedUserId
                tvCreatePost.LevelId = 1
                tvCreatePost.isWhisper = self.modCommentsFollowersobj[rowIndex].isWhisper
                tvCreatePost._MyTaggedUserIdComplete = E_MyTaggedUserIdComplete
                tvCreatePost.FireButton = 0 // no photo to fire
                self.TxtComment.resignFirstResponder()
                if (self.parentView == nil)
                {
                self.parentView.navigationController?.pushViewController(tvCreatePost, animated: true)
                }
                else
                {
                    self.parentViewforNotifcation.navigationController?.pushViewController(tvCreatePost, animated: true)
                }
                
            })
            
            // MARK:- Delete Comment
            func Delete(myAction : UIAlertAction)
            {
                MBProgressHUD.showHUDAddedTo(self.view, animated: true).label.text = "Deleting..."
                
                let idnumber = self.modCommentsFollowersobj[rowIndex].Id!
                let RequestParameters : NSDictionary =
                    [
                        "LevelId":1,
                        "PostId":idnumber.integerValue,
                        "UserId":User!.Id!
                ]
                
                let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
                
                
                var myServiec = WSMethods.DeletePostandCommment
                if (IsItSpecialOffer){myServiec = WSMethods.DeleteSpecialOffersPostandCommment}
                
                NetworkHelper.RequestHelper(nil, service: myServiec, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
                    , responseType: ResponseType.StringJson , callbackString:
                    {response in
                        MBProgressHUD.hideHUDForView(self.view, animated: true)
                        
                        if(response.result.isFailure)
                        {
                            
                            if let networkError = response.result.error
                            {
                                if (networkError.code == -1009)
                                {
                                    self.view.makeToast("No Internet connection")
                                    
                                }
                                else
                                {
                                    self.view.makeToast("An error has been occurred")
                                    print("error")
                                }
                                return
                            }
                        }
                        
                        if(response.result.isSuccess)
                        {
                            if let JSON = response.result.value
                            {
                                // print("JSON: \(JSON)")
                                let FeedsResponse = ModResponse(json:JSON)
                                
                                if (FeedsResponse.ResultResponse != "0")
                                {
                                    // error
                                    self.view.makeToast("Couldn't delete post")
                                }
                                else
                                {
                                    
                                    self.view.makeToast("Post deleted")
                                    
                                    if (self.IsitReplay){
                                    self.ParentComment.NoOfComments = (self.ParentComment.NoOfComments?.integerValue)! - 1
                                    }
                                    self.modCommentsFollowersobj.removeAtIndex(rowIndex)
                                    
                                     self.TableOfComments.reloadData()
                                    
                                    if (self.parentView != nil)
                                    {
                                        if (self.parentView.Commentblock != nil)
                                        {
                                            self.parentView.Commentblock(self.ObjId)
                                        }
                                    }
                                  
                                    
                                
                                }
                            }
                        }
                    }
                    , callbackDictionary: nil)
                
                
            }
            
            let DeleteAction = UIAlertAction(title: "Delete", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete your comment?", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: Delete))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil))
                
                self.presentViewController(alert, animated: true, completion: nil)
                
            })
            
            
            // MARK:- Hide Comment
            let HideAction = UIAlertAction(title: "Hide", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                
                
                let alert = UIAlertController(title: "Password required", message: "Please enter your password", preferredStyle: UIAlertControllerStyle.Alert)
                let ok = UIAlertAction(title: "hide", style: UIAlertActionStyle.Default , handler: {(action: UIAlertAction) -> Void in
                    let passwordTextField = alert.textFields![0]
                    if passwordTextField.text!.characters.count == 0
                    {
                        let alertView = UIAlertView(title: "Password Required", message: "Please enter your password to hide post", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "")
                        alertView.show()
                    }
                    else
                    {
                        
                        
                        MBProgressHUD.showHUDAddedTo(self.view, animated: true).label.text = "Hiding..."
                        let idnumber = self.modCommentsFollowersobj[rowIndex].Id!
                        let RequestParameters : NSDictionary =
                            [
                                "LevelId":1,
                                "ModeratorPassword": passwordTextField.text!,
                                "PostId":idnumber.integerValue,
                                "UserId":User!.Id!
                        ]
                        
                        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
                        
                        NetworkHelper.RequestHelper(nil, service: WSMethods.HidePostorComment, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
                            , responseType: ResponseType.StringJson , callbackString:
                            {response in
                                MBProgressHUD.hideHUDForView(self.view, animated: true)
                                
                                if(response.result.isFailure)
                                {
                                    
                                    if let networkError = response.result.error
                                    {
                                        if (networkError.code == -1009)
                                        {
                                            self.view.makeToast("No Internet connection")
                                            
                                        }
                                        else
                                        {
                                            self.view.makeToast("An error has been occurred")
                                            print("error")
                                        }
                                        return
                                    }
                                }
                                
                                if(response.result.isSuccess)
                                {
                                    if let JSON = response.result.value
                                    {
                                        // print("JSON: \(JSON)")
                                        let FeedsResponse = ModResponse(json:JSON)
                                        
                                        if (FeedsResponse.ResultResponse != "0")
                                        {
                                            // error
                                            self.view.makeToast("Couldn't hide post")
                                        }
                                        else
                                        {
                                            
                                            self.LoadComments()
                                            if (self.parentView != nil)
                                            {
                                            if (self.parentView.Commentblock != nil)
                                            {
                                                self.parentView.Commentblock(self.ObjId)
                                            }
                                            }
                                            
                                            self.view.makeToast("Post Hidden")
                                            
                                        }
                                    }
                                }
                                
                                
                            }
                            , callbackDictionary: nil)
                        
                    }
                })
                let cancel = UIAlertAction(title: "Cancel", style: .Default, handler: {(action: UIAlertAction) -> Void in
                    alert.dismissViewControllerAnimated(true, completion: nil)
                })
                alert.addAction(ok)
                alert.addAction(cancel)
                alert.addTextFieldWithConfigurationHandler({(textField: UITextField) -> Void in
                    textField.placeholder = "Password"
                    textField.secureTextEntry = true
                })
                
                self.presentViewController(alert, animated: true, completion: nil)
                
            })
            // MARK:- Report Comment
            let ReportAction = UIAlertAction(title: "Report", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                
                let report = self.parentView.storyboard?.instantiateViewControllerWithIdentifier("VcReport") as! VcReport
                report.come = .Comment
                let idnumber = self.modCommentsFollowersobj[rowIndex].Id!
                report.CommentID = idnumber.integerValue
                self.parentView.navigationController?.pushViewController(report, animated: true)
                
            })
            
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
                (alert: UIAlertAction!) -> Void in
            })
            
            
            // 4
            optionMenu.addAction(CopyAction)
            if ((User?.Id)! == self.modCommentsFollowersobj[rowIndex].UserId!){
                optionMenu.addAction(EditAction)
                optionMenu.addAction(DeleteAction)
                }
            if ((User?.AccountType)! == 3 &&  !(IsItSpecialOffer) )
            {
            optionMenu.addAction(HideAction)
            }
            if (((User?.Id)! != self.modCommentsFollowersobj[rowIndex].UserId!) && ((User?.AccountType)! != 4))
            {
            optionMenu.addAction(ReportAction)
            }
            optionMenu.addAction(cancelAction)
            
            // 5
            self.presentViewController(optionMenu, animated: true, completion: nil)
            
            
        }
        
    }
    
    func LoadComments( WithPrgoress : Bool = true , AddPagetoInfinit : Bool = false , GotolastRow : Bool = false ) {
        
        
        if (AddPagetoInfinit) { PagesCount += 1 } else { PagesCount = 1}
        
        let RequestParameters : NSDictionary = [
            "LevelId":IsitReplay ? 2 : 1,
            "ObjId":ObjId,
            "PageIndex":PagesCount,
            "PageSize":50,
            "UserId":(User?.Id)!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        var serviseModd = ""
       
        if (ScreenMod == 0) // whisper Mod or privete mod
        {
            serviseModd = WSMethods.GetComments
            if (IsItSpecialOffer){serviseModd = WSMethods.GetSpecialOffersComments}
            
        }
        else
        {
            serviseModd = WSMethods.GetWhispers
             if (IsItSpecialOffer){serviseModd = WSMethods.GetSpecialOffersWhispers}
        }
        
        
        NetworkHelper.RequestHelper(nil, service: serviseModd, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString: { response in
                
                if (WithPrgoress ) {MBProgressHUD.hideHUDForView(self.view, animated: true)}
     
                if(response.result.isFailure)
                {
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {  //self.view.makeToast("No Internet Connection")
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response =  ModComments(json:JSON)
                        
                        if (Response.ResultResponse == "0"){
                          //  self.modCommentsFollowersobj = Response.Followersobj!
                            
                            if (self.PagesCount > 1){
                                
                                self.modCommentsFollowersobj.appendContentsOf(Response.Followersobj!)
                                
                            }
                            else
                            {
                                 self.modCommentsFollowersobj = Response.Followersobj!
                            }
                            
                            self.TableOfComments.reloadData()
                            
                            if (GotolastRow)
                            {
                                if self.TableOfComments.numberOfRowsInSection(0) != 0 {
                                    let lastRowIndex = self.TableOfComments!.numberOfRowsInSection(0) - 1
                                    let pathToLastRow = NSIndexPath(forRow: lastRowIndex, inSection: 0)
                                    self.TableOfComments?.scrollToRowAtIndexPath(pathToLastRow, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
                                    
                                    
                                }
                            }
                        }
                        else{
                            self.view.makeToast("An error has been occurred")
                            
                        }
                                                
                        
                    }
                }
            }, callbackDictionary: nil)
        
        
        if (WithPrgoress ) { MBProgressHUD.showHUDAddedTo(self.view, animated: true).label.text = "Loading..."}
    }
    
    
    @IBAction func WhisperAction(sender: AnyObject) {
        
        
        if(!isWhisper)
        {
            WhisperBtn.setImage(UIImage(named: "Comments-(Whisper-button)"), forState: .Normal)
        }
        else
        {
            WhisperBtn.setImage(UIImage(named: "Comments-(Whisper-button)Unselect"), forState: .Normal)
        }
        
        
        isWhisper = !isWhisper
    }
    
    
    
    @IBAction func AddPhotoAction(sender: AnyObject) {
        let tvCreatePost = self.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost
        //  TxtComment.text
        mayAddCommentFromCreatePost = true
        tvCreatePost.FromScreen = .CreateComment
        tvCreatePost.ObjId = ObjId
        tvCreatePost.TxtComment = TxtComment.text
        tvCreatePost._TaggedUserId = _TaggedUserId
        tvCreatePost.LevelId = IsitReplay ? 2 : 1
        tvCreatePost.isWhisper = isWhisper
        tvCreatePost._MyTaggedUserIdComplete = _MyTaggedUserIdComplete
        tvCreatePost.FireButton = 1
        tvCreatePost.Createblock = {() in
            self.LoadComments(true, AddPagetoInfinit: false, GotolastRow: true)
        }
        self.TxtComment.text = ""
        self.TxtComment.resignFirstResponder()
        self.parentView.navigationController?.pushViewController(tvCreatePost, animated: true)
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func BtnSendComment(sender: AnyObject) {
        
        
        if (TxtComment.text == "" ){
             self.view.makeToast("No Comment for send", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
            return
        }
        let sendButton =  sender as! (UIButton)
        sendButton.enabled = false
        TxtComment.resignFirstResponder()
        
        var RequestParameters : NSDictionary = NSDictionary();
        
        let PostOb : NSDictionary = [
            "LevelId":IsitReplay ? 2 : 1,
            "PostId":ObjId,
            "PostType":1,   //text
            "Text":TxtComment.text,
            "UserId":(User?.Id)!,
            "isWhisper":isWhisper
        ]
        
        RequestParameters = [
            "TaggedUserId":_TaggedUserId,
            "CommentOb" : PostOb,
            "UserId" : (User?.Id)!,
        ]
        
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        var myServiec = WSMethods.WriteAComment
         if (IsItSpecialOffer){myServiec = WSMethods.WriteSpecialOffersComment}
        
        NetworkHelper.RequestHelper(nil, service: myServiec, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil , callbackDictionary: { (JSON, NSError) in
                if((NSError) != nil)
                {
                    if let networkError = NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                        }
                        return
                    }
                }
                else
                {
                    if (JSON["ResultResponse"].stringValue == "0") //scuess
                    {
                        if (self.IsitReplay){
                        self.ParentComment.NoOfComments = (self.ParentComment.NoOfComments?.integerValue)! + 1
                        }
                        self.LoadComments(true, AddPagetoInfinit: false, GotolastRow: true)
                        
                    }
                    else
                    {
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                    }
                    
                    
                    if (self.parentView != nil)
                    {
                    if (self.parentView.Commentblock != nil)
                    {
                        self.parentView.Commentblock(self.ObjId)
                    }
                    }
                    
                    self.TxtComment.text = ""
                    sendButton.enabled = true
                }
        })
        
    }
    
    
    
    
    
    //# MARK:- AutoComplete textViewDidChange
    func textViewDidChange(textView: UITextView)
    {
    
        if (textView.text.isEqual("")) {
            _myranges =  []
            _TaggedUserId = NSMutableArray()
            _MyTaggedUserIdComplete =  NSMutableArray()
            autoCompleteContainer.hidden = true
            self.autoCompleteMgr.container = self.autoCompleteContainer;
        }else
        {
            
            autoCompleteMgr.processString(textView.text)
            
            if ( autoCompleteContainer.subviews.count < 1 )
            {
                autoCompleteContainer.hidden = true
            }
            if (textView.text.hasSuffix(" ") == true)
            {
                self.TxtComment.attributedText = formateTaggingString(self.TxtComment.text)
                autoCompleteContainer.hidden = true
            }
            
        }
        
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(VcComments.keyboardWasShown(_:)), name: UIKeyboardDidShowNotification, object: nil)

    }
    
    func textViewDidEndEditing(textView: UITextView) {
        self.TxtComment.attributedText = formateTaggingString(self.TxtComment.text)
        firstTime = false
       
    }
    
    // MARK:- AutoComplete Manager
    func autoCompleteManager(acManager: MJAutoCompleteManager!, itemListForTrigger trigger: MJAutoCompleteTrigger!, withString string: String!, callback: MJAutoCompleteListCallback!){
        
        /* Since we implemented this method, we are stuck and must handle ALL triggers */
        
        if (trigger.delimiter.isEqual("@")){
            if (!self.didStartDownload)
            {
                
                var itemList : [MJAutoCompleteItem] = []
                let myitems = TxtComment.text.componentsSeparatedByString("@")
                let mystring = myitems.last
                
                // self.didStartDownload = true;
                if (mystring == "") {return} // just work when there are char
                
                // network helper here
                
                let RequestParameters : NSDictionary = [
                    "Keyword" : mystring!,
                    "UserId": (User?.Id)!
                ]
                
                let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
                
                NetworkHelper.RequestHelper(nil, service: WSMethods.AutoCompleteTaggedUser, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
                    , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: { (JSON, NSError) in
                        
                        
                        if((NSError) != nil)
                        {
                            if let networkError = NSError {
                                if (networkError.code == -1009) {
                                    self.view.makeToast("No Internet connection")
                                }
                                else
                                {
                                    self.view.makeToast("An error has been occurred")
                                    print("error")
                                }
                                return
                            }
                        }
                        else
                        {
                            
                            
                            if ( JSON["ResultResponse"].stringValue == "0")
                            {
                                let Profileobjobj =   JSON["Followersobj"]
                                
                                var ContextDictiony  = [String:String]()
                                
                                
                                for commentDictionary in Profileobjobj  {
                                    
                                    let item : MJAutoCompleteItem = MJAutoCompleteItem ()
                                    
                                    item.autoCompleteString = commentDictionary.1["UserName"].rawString()
                                    
                                    ContextDictiony["UserName"] = commentDictionary.1["UserName"].rawString()
                                    ContextDictiony["Id"] = commentDictionary.1["Id"].rawString()
                                    ContextDictiony["FullName"] = commentDictionary.1["FullName"].rawString()
                                    ContextDictiony["ProfilePicture"] = commentDictionary.1["ProfilePicture"].rawString()
                                    ContextDictiony["FollowStatus"] = commentDictionary.1["FollowStatus"].rawString()
                                    
                                    
                                    
                                    item.context = ContextDictiony
                                    
                                    itemList.append(item)
                                }
                                self.enterAutoComplete = true
                                trigger.autoCompleteItemList = itemList;
                                
                                callback(itemList);
                                
                            }
                        }
                        
                        
                })
                
            }
            else
            {
                
                callback(trigger.autoCompleteItemList);
            }
        }
        
    }
    
    
    
    
    func autoCompleteManager(acManager: MJAutoCompleteManager!, willPresentCell autoCompleteCell: AnyObject!, forTrigger trigger: MJAutoCompleteTrigger!)
        
    {
        
        self.autoCompleteContainer.hidden = false;
        if (trigger.delimiter.isEqual("@"))
        {
            
            
            let cell :autoComCell = autoCompleteCell as! autoComCell;
            cell.parentComment = self
            cell.CustomautoCompleteItem(cell.autoCompleteItem)
            
        }
    }
    
    
    func autoCompleteManager(acManager: MJAutoCompleteManager!, shouldUpdateToText newText: String!, selectedItem: MJAutoCompleteItem!)
    {
        self.TxtComment.text = newText;
        self.autoCompleteContainer.hidden = true;
        self.TxtComment.attributedText = formateTaggingString(self.TxtComment.text)
        //selectedItem.context.values(")
        
        _TaggedUserId.addObject(selectedItem.context["Id"]!)
        _MyTaggedUserIdComplete.addObject(selectedItem.context)
        
    }
    
    
    // MARK:- Attributed String
    func formateTaggingString(str:String) -> NSAttributedString
    {
        
        let attstring = NSMutableAttributedString(string: str);
        let mycolor : UIColor = UIColor(hex: "#0000EE")
        
        
        
        let splitSpace = str.componentsSeparatedByString(" ")
        
        for sub in splitSpace{
            
            if sub.hasPrefix("@") || sub.hasPrefix("#") {
                _myranges.append(sub)
            }
        }
        
        for object in _myranges {
            
            attstring.addAttribute(NSForegroundColorAttributeName, value: mycolor, range:(self.TxtComment.text as NSString).rangeOfString(object))
            attstring.addAttribute(NSUnderlineStyleAttributeName, value: Int(1), range: (self.TxtComment.text as NSString).rangeOfString(object))
            
        }
        
        
        return attstring
    }
    
}
