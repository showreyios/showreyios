//
//  VcSearch.swift
//  ShowRey
//
//  Created by M-Hashem on 11/20/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Hue
import FirebaseAnalytics

class VcSearch: PagerController,PagerDataSource,UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate
{
    var searchController : UISearchController!
    let screenSize = ((UIScreen.mainScreen().bounds.width))/2
    var searchkeyword :String = ""
    var feednews:VcMasterNewsFeed!
    var Follorwes_Likers_Search: VcFollorwes_Likers_Search!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: "TapChangeIndex", object: nil)
        
        dataSource = self
        let storyboard = AppDelegate.storyboard
        feednews = storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        feednews.SetupForSearch("")
        feednews.parent = self;
        Follorwes_Likers_Search = storyboard.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        Follorwes_Likers_Search.MyScreenMod =  .PeopleSearch
        Follorwes_Likers_Search.rootParent = self
        // Follorwes_Likers_Search.PeopleSearchMod("")
      // Setting up the PagerController with Name of the Tabs and their respective ViewControllers
        
        self.setupPager(["Posts","People"],
            tabImages: nil,
            tabControllers: [feednews, Follorwes_Likers_Search])
        // TapAction = tapAction;
        
        customizeTab()
        
        self.searchController = UISearchController(searchResultsController:  nil)
        
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.tintColor = UIColor.darkGrayColor()
        
        self.navigationItem.titleView = searchController.searchBar
        
        self.definesPresentationContext = true
        
//        view.backgroundColor = UIColor.whiteColor()
//        title = "Search"
        
        if let InintialKeyWord = InintialKeyWord
        {
            searchController.searchBar.text = InintialKeyWord
             defaultSetup()
            if InintialTypeisPost
            {
                selectTabAtIndex(0)
                feednews.searchWord(InintialKeyWord)
            }
            else
            {
                selectTabAtIndex(1)
                Follorwes_Likers_Search.PeopleSearchMod(InintialKeyWord)
            }

        }
        FIRAnalytics.logEventWithName("Search", parameters: nil)
    }
    
    var InintialKeyWord:String? = nil
    var InintialTypeisPost:Bool = true
    
    func  initWith(KeyWord:String,posts:Bool)
    {
        
        InintialKeyWord = KeyWord
        InintialTypeisPost = posts
    }
    
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
    }
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        searchkeyword = searchBar.text!
        
        if currentIndex == 0
       {
            feednews.searchWord(searchBar.text!)
       }
       else
        {
            Follorwes_Likers_Search.Keyword = searchBar.text!
            Follorwes_Likers_Search.PeopleSearchMod(searchBar.text!)
       }
    
    
    
  

    }
    
    func customizeTab()
    {
        indicatorColor = UIColor(hex: "#b379e7")
        tabsViewBackgroundColor = UIColor.whiteColor()
        //(colorLiteralRed: 145 / 255, green: 78 / 255, blue: 233 / 255, alpha: 1)
        //(rgb: 0x00AA00)
        contentViewBackgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.32)
        
        //startFromSecondTab = false
        centerCurrentTab = true
        tabLocation = PagerTabLocation.Top
        tabHeight = 49
        tabOffset = 0
        tabWidth = screenSize
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.During
        selectedTabTextColor = UIColor(colorLiteralRed: 145 / 255, green: 78 / 255, blue: 233 / 255, alpha: 1)
        tabsTextFont = UIFont(name: "HelveticaNeue-Bold", size: 15)!
        // tabTopOffset = 10.0
        tabsTextColor = .darkGrayColor()
        
    }
    
    
    
    func showSpinningWheel(notification: NSNotification) {
        if let tapindex = notification.userInfo?["index"] as? Int {
            if tapindex == 1
            {
                Follorwes_Likers_Search.PeopleSearchMod(searchkeyword)
            }
        }
    }

}
