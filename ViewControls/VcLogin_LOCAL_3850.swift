//
//  VcLogin.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 9/17/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import PureLayout
import EGFloatingTextField


enum LoginTypes : Int{
    case Normal=0
    case Facebook=1
    case Instagram=2
}

class VcLogin: UIViewController,UITextFieldDelegate {

   // @IBOutlet weak var ChkRemember: CheckboxButton!
    @IBOutlet weak var TxtUserName: EGFloatingTextField!
    
    @IBOutlet weak var TxtPassword: EGFloatingTextField!
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        TxtPassword.floatingLabel = true
         TxtPassword.setPlaceHolder("Password")
        

        
        
        TxtUserName.floatingLabel = true
        TxtUserName.setPlaceHolder("UserName")

       
        
        /*
        let TxtUserNameSky = SkyFloatingLabelTextField(frame: CGRectMake(10, 10, 120, 45))
        TxtUserNameSky.placeholder = "Arrival"
        TxtUserNameSky.title = "Flying to"
        TxtUserNameSky.tintColor = UIColor.blueColor()
        TxtUserNameSky.selectedTitleColor = UIColor.redColor()
        TxtUserNameSky.selectedLineColor = UIColor.greenColor()
        
        self.view.addSubview(TxtUserNameSky)*/
        
        // Do any additional setup after loading the view.
    }

    @IBAction func InstgramLogin(sender: AnyObject) {
        
        let sen =  InstagramEngine.sharedEngine;
        
        sen.loginFromViewController(self) { (success, error) in
            if(success)
            {
                VcLogin.LastLoginType = .Instagram
                print("success instgram")
                VcLogin.InstgramFetchProfile(){ (success) in
                    if(success)
                    {
                        // go home
                    }
                }
            }
            else
            {
                print(error)
            }
        }
    }
    internal static func InstgramFetchProfile(completionHandler: ((success:Bool)->())?)
    {
        InstagramEngine.sharedEngine.selfWithSuccessClosure({ (profile, meta) in
            print(profile)
            
            let InstUser = ModUser()
            //id
            InstUser.Username = profile.username
            InstUser.FullName = profile.fullName
            
            
            // load the profile to a global variable
            if((completionHandler) != nil)
            {
                completionHandler!(success: meta.code == "200");
            }
            
            }, error: { (error) in
                if((completionHandler) != nil)
                { completionHandler!(success: false);}
                print(error)
        })
    }
    
    internal static var LastLoginType : LoginTypes!
    {
        get {
          let val = NSUserDefaults.standardUserDefaults().objectForKey("LastLoginType")
            if val == nil {return nil}
            return (LoginTypes(rawValue: (val?.integerValue)!));
        }
        set{
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setValue(newValue.rawValue, forKey: "LastLoginType")
            userDefaults.synchronize();
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func BtnForgetPassword(sender: AnyObject) {
        
        let secondview = self.storyboard?.instantiateViewControllerWithIdentifier("VcForgetPassword1") as! VcForgetPassword1
        self.navigationController?.pushViewController(secondview, animated: true)
    }

    @IBAction func BtnRegister(sender: AnyObject) {
        
        let secondview = self.storyboard?.instantiateViewControllerWithIdentifier("VcRegisteration") as! VcRegisteration
        self.navigationController?.pushViewController(secondview, animated: true)
        
    }
    
}
