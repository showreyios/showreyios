//
//  VcCreatePostViewController.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 10/16/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Kingfisher
import YangMingShan
import UPCarouselFlowLayout
import AVKit
import AVFoundation
import MobileCoreServices
import TOCropViewController


class VcCreatePost: UIViewController , YMSPhotoPickerViewControllerDelegate , UICollectionViewDelegate , UICollectionViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate,TOCropViewControllerDelegate{
    
    
    var PrivatePostFlag : Bool = false;
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var PrivatePost: UIButton!
    @IBOutlet weak var GalleryCollectionView: UICollectionView!
    
    
    var images : [UIImage] = [] // array of images
    var PostType :Int = 0
    var videoURL: NSURL? = NSURL()
    var av: AVPlayerViewController!
    var mediaPicker: UIImagePickerController!
    
    
    
    // var currentPage: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.interactivePopGestureRecognizer?.enabled = false
        
        av = AVPlayerViewController()
        
        GalleryCollectionView.registerClass(CcImageGallery.self, forCellWithReuseIdentifier: "CcImageGallery")
        GalleryCollectionView.registerNib(UINib(nibName: "CcImageGallery",bundle: nil), forCellWithReuseIdentifier: "CcImageGallery")
        
        
        
        
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
        self.profileImage.clipsToBounds = true;
        
        
        profileImage.userInteractionEnabled = true
        //now you need a tap gesture recognizer
        //note that target and action point to what happens when the action is recognized.
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(VcCreatePost.profileImage(_:)))
        //Add the recognizer to your view.
        profileImage.addGestureRecognizer(tapRecognizer)
        
        
        
        if ((User) != nil)
        {
            let str : String? = (User?.ProfilePicture)!
            
            let url = NSURL(string:str!)
            
            
            profileImage.kf_setImageWithURL(url,placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
            
        }
        
        
        self.navigationItem.title = "Create Post"
        
        
        let layout = UPCarouselFlowLayout()
        layout.itemSize.width = 250
        layout.itemSize.height = 300
        
        
        // self.currentPage = 0
        
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        GalleryCollectionView.collectionViewLayout = layout
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func profileImage(gestureRecognizer: UITapGestureRecognizer) {
        self.view.makeToast("Go to user profile :D :P")
        
    }
    
    
    //# MARK: - ActionSheet Media Picker
    
    @IBAction func btnPickMedia(sender: AnyObject) {
        
        
        av.player?.pause()
        
        
        // 1
        let optionMenu = UIAlertController(title: "Select Madia", message: nil , preferredStyle: .ActionSheet)
        
        // 2
        let PhotoGallery = UIAlertAction(title: "Photo Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.images.removeAll()
            self.GalleryCollectionView.hidden = true
            
            if (self.av.player) != nil {
                
                self.av.player = nil
                self.av.view.removeFromSuperview()
                
            }
            
            
            self.ImagePicker();
            
            
        })
        let PhotoCamera = UIAlertAction(title: "Photo Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.images.removeAll()
            self.GalleryCollectionView.hidden = true
            if (self.av.player) != nil {
                
                self.av.player = nil
                self.av.view.removeFromSuperview()
                
            }
            
            
            self.mediaPicker =  UIImagePickerController()
            self.mediaPicker.delegate = self
            self.mediaPicker.sourceType = .Camera
            
            
            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
            
            
        })
        let VideoGallery = UIAlertAction(title: "Video Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.images.removeAll()
            self.GalleryCollectionView.hidden = true
            if (self.av.player) != nil {
                
                self.av.player = nil
                self.av.view.removeFromSuperview()
                
            }
            
            
            self.mediaPicker =  UIImagePickerController()
            self.mediaPicker.sourceType = .PhotoLibrary
            self.mediaPicker.delegate = self
            self.mediaPicker.mediaTypes = ["public.movie"] //"public.image",
            
            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
            
            
        })
        let VideoCamera = UIAlertAction(title: "Video Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.images.removeAll()
            self.GalleryCollectionView.hidden = true
            if (self.av.player) != nil {
                
                self.av.player = nil
                self.av.view.removeFromSuperview()
                
            }
            
            
            self.mediaPicker =  UIImagePickerController()
            self.mediaPicker.sourceType = .Camera
            self.mediaPicker.delegate = self
            //self.mediaPicker.mediaTypes = [kUTTypeMovie as String]
            self.mediaPicker.mediaTypes = ["public.movie"] //"public.image",
            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(PhotoGallery)
        optionMenu.addAction(PhotoCamera)
        optionMenu.addAction(VideoGallery)
        optionMenu.addAction(VideoCamera)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
    }
    
    
    //# MARK: - Normal Picker and View
    
    // in case of camera (I or V)  or video gallery
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        
        mediaPicker.dismissViewControllerAnimated(true, completion: nil)
        
        
        if (info[UIImagePickerControllerOriginalImage] as? UIImage) != nil  //photo
        {
            
            
           /*
            let cropViewController:TOCropViewController = TOCropViewController(image: (info[UIImagePickerControllerOriginalImage] as? UIImage)!)
            cropViewController.delegate = self;
            self.presentViewController(cropViewController, animated: true, completion: nil)
            */
            
            images.append((info[UIImagePickerControllerOriginalImage] as? UIImage)!)
            self.GalleryCollectionView.reloadData()
            self.GalleryCollectionView.hidden = false
            
            
            
        }
        else // video
        {
            
            videoURL = info["UIImagePickerControllerReferenceURL"] as? NSURL  // gallery
            
            if (videoURL == nil)
            {
                videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL // camera
            }
            
            let player = AVPlayer(URL: videoURL! as  NSURL)
            // av = AVPlayerViewController()
            av.player = player
            av.view.frame =  CGRect(x: 20 , y: self.view.frame.size.height - 290  , width: self.view.frame.size.width - 40 , height: 54 * 5)
            
            self.addChildViewController(av)
            
            self.view.addSubview(av.view)
            av.didMoveToParentViewController(self)
            
            let removebutton = UIButton(frame: CGRect(x: av.view.frame.size.width - 34, y: 10, width: 24, height: 24))
            //   button.backgroundColor = .greenColor()
            
            removebutton.setImage(UIImage(named: "remove"), forState: UIControlState.Normal)
            // removebutton.setTitle("Test Button", forState: .Normal)
            removebutton.addTarget(self, action: #selector(removeVideoAction), forControlEvents: .TouchUpInside)
            
            av.view.addSubview(removebutton)
            
            
            
            
        }
    }
    
    func removeVideoAction(sender: UIButton!) {
        av.player = nil
        av.view.removeFromSuperview()
        av = AVPlayerViewController()
        
    }
    
    //# MARK: - Mutil Photos Picker
    
    
    func ImagePicker()
    {
        let pickerViewController = YMSPhotoPickerViewController.init()
        pickerViewController.numberOfPhotoToSelect = 10
        
        
        pickerViewController.theme.titleLabelTextColor = UIColor(hex: "#ffffff")
        
        
        pickerViewController.theme.tintColor = UIColor(hex: "#b379e7")
        pickerViewController.theme.orderTintColor = UIColor(hex: "#b379e7")
        pickerViewController.theme.cameraVeilColor = UIColor(hex: "#b379e7")
        pickerViewController.theme.cameraIconColor = UIColor(hex: "#ffffff")
        //   pickerViewController.theme.statusBarStyle = UIStatusBarStyleLightContent
        
        
        
        self.yms_presentCustomAlbumPhotoView(pickerViewController, delegate: self)
    }
    
    
    func photoPickerViewControllerDidReceivePhotoAlbumAccessDenied(picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow photo album access?", message: "Need your permission to access photo albumbs", preferredStyle: .Alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .Cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .Default) { (action) in
            UIApplication.sharedApplication().openURL(NSURL.init(string: UIApplicationOpenSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    func photoPickerViewControllerDidReceiveCameraAccessDenied(picker: YMSPhotoPickerViewController!) {
        let alertController = UIAlertController.init(title: "Allow camera album access?", message: "Need your permission to take a photo", preferredStyle: .Alert)
        let dismissAction = UIAlertAction.init(title: "Cancel", style: .Cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "Settings", style: .Default) { (action) in
            UIApplication.sharedApplication().openURL(NSURL.init(string: UIApplicationOpenSettingsURLString)!)
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        // The access denied of camera is always happened on picker, present alert on it to follow the view hierarchy
        picker.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    
    
    func photoPickerViewController(picker: YMSPhotoPickerViewController!, didFinishPickingImages photoAssets: [PHAsset]!) {
        // Remember images you get here is PHAsset array, you need to implement PHImageManager to get UIImage data by yourself
        picker.dismissViewControllerAnimated(true) {
            let imageManager = PHImageManager.init()
            let options = PHImageRequestOptions.init()
            options.deliveryMode = .HighQualityFormat
            options.resizeMode = .Exact
            options.synchronous = true
            
            let mutableImages: NSMutableArray! = []
            
            for asset: PHAsset in photoAssets
            {
                
                let targetSize = CGSizeMake(200,300)
                
                imageManager.requestImageForAsset(asset, targetSize: targetSize, contentMode: .AspectFill, options: options, resultHandler: { (image, info) in
                    mutableImages.addObject(image!)
                })
            }
            // Assign to Array with images
            
            
            
            self.images = mutableImages.copy() as! [UIImage]
            
            /*
            if ( self.images.count == 1)
            {
                let cropViewController:TOCropViewController = TOCropViewController(image: self.images[0])
                cropViewController.delegate = self;
                self.presentViewController(cropViewController, animated: true, completion: nil)
            }
            */
            
            self.GalleryCollectionView.reloadData()
            self.GalleryCollectionView.hidden = false
            //   var s = self.images[0] as! UIImage;
            
        }
    }
    
    
    
    
    //# MARK: - Gallery View
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: CcImageGallery = collectionView.dequeueReusableCellWithReuseIdentifier("CcImageGallery", forIndexPath: indexPath) as! CcImageGallery
        
        cell.img.image =  images[indexPath.row]
        
        cell.btnDeselect.addTarget(self, action:  #selector(removeImage(_:)), forControlEvents: .TouchUpInside)
        cell.btnDeselect.tag = indexPath.row
        
        cell.btnCrop.addTarget(self, action:  #selector(cropImage(_:)), forControlEvents: .TouchUpInside)
        cell.btnCrop.tag = indexPath.row
        
        
        return cell
        
    }
    
    func removeImage(mybutton: UIButton)
    {
        
        images.removeAtIndex(mybutton.tag)
        GalleryCollectionView.reloadData()
        
    }
    
    func cropImage(mybutton: UIButton)
    {
        
       
        let cropViewController:TOCropViewController = TOCropViewController(image: images[mybutton.tag])
        cropViewController.delegate = self;
        self.presentViewController(cropViewController, animated: true, completion: nil)
        images.removeAtIndex(mybutton.tag)
        
        
    }
    
    
    //# MARK: - Crop Image
    
    func cropViewController(cropViewController: TOCropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int)
    {
        
        images.append(image)
        self.GalleryCollectionView.reloadData()
        self.GalleryCollectionView.hidden = false
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    //# MARK: - Private Post Handle
    
    @IBAction func BtnPrivatePostText(sender: AnyObject) {
        Postflag()
    }
    @IBAction func BtnPRivatePost(sender: AnyObject) {
        Postflag()
    }
    
 
    
    
 func Postflag()
 {
        if(!PrivatePostFlag)
        {
            PrivatePost.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
        }
        else
        {
            PrivatePost.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
        }
        PrivatePostFlag = !PrivatePostFlag;
    }
    
    
    
    
}
