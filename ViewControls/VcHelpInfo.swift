//
//  VcHelpInfo.swift
//  ShowRey
//
//  Created by Appsinnovate on 1/3/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

//import UIKit
//
//struct Section {
//    var name: String!
//    var items: [String]!
//    var collapsed: Bool!
//    
//    init(name: String, items: [String], collapsed: Bool = false) {
//        self.name = name
//        self.items = items
//        self.collapsed = collapsed
//    }
//}
//
//class VcHelpInfo: UIViewController, UITableViewDelegate, UITableViewDataSource, CollapsibleTableViewHeaderDelegate, UISearchBarDelegate {
//
//    var sections = [Section]()
//    var search:Bool = false
//    var PagesCount = 1
//       var searchHelp = [HelpCenterObj]()
//    @IBOutlet weak var tableViewTest: UITableView!
//    
//   
//    @IBOutlet weak var txtSearch: UISearchBar!
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        self.navigationItem.title = "Help Us"
//        self.tableViewTest.rowHeight = UITableViewAutomaticDimension
//        self.tableViewTest.sectionHeaderHeight = UITableViewAutomaticDimension
//        self.tableViewTest.estimatedRowHeight = 50
//        self.tableViewTest.estimatedSectionHeaderHeight = 60
// 
//        navigationController!.navigationBar.barTintColor = UIColor(hex: "#b379e7")
//        
//        let nib = UINib(nibName: "TvCollapsedHeaderView", bundle: nil)
//        tableViewTest.registerNib(nib, forCellReuseIdentifier: "TvCollapsedHeaderView")
//        
//        let button = UIButton()
//        button.frame = CGRectMake(0, 0, 21, 31)
//        button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
//        button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
//        let barButton = UIBarButtonItem()
//        barButton.customView = button
//        navigationItem.leftBarButtonItem = barButton
//
//        getSearchableHelp("",index: self.PagesCount)
//       
//        // Do any additional setup after loading the view.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//
//    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        return sections.count
//    }
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return sections[section].items.count
//    }
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableViewTest.dequeueReusableCellWithIdentifier("Helpcell") as UITableViewCell? ?? UITableViewCell(style: .Default, reuseIdentifier: "Helpcell")
//
//        cell.textLabel?.autoresizingMask = UIViewAutoresizing.FlexibleLeftMargin
//        cell.textLabel?.autoresizingMask = UIViewAutoresizing.FlexibleRightMargin
//        cell.textLabel?.text = sections[indexPath.section].items[indexPath.row]
// 
//        return cell
//    }
//    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return sections[indexPath.section].collapsed! ? UITableViewAutomaticDimension  : 0
//      
//    }
//    
//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return UITableViewAutomaticDimension
//           }
//    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 1.0
//    }
//    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let header = tableViewTest.dequeueReusableHeaderFooterViewWithIdentifier("TvCollapsedHeaderView") as? TvCollapsedHeaderView ?? TvCollapsedHeaderView(reuseIdentifier: "TvCollapsedHeaderView")
//        header.setcell(sections[section].name)
//        
//        header.arrowLabel.text = ">"
//        header.setCollapsed(sections[section].collapsed)
//        header.section = section
//        header.delegate = self
//        
//        return header
//    }
//    
//    
//    
//    func toggleSection(header: TvCollapsedHeaderView, section: Int) {
//        let collapsed = !sections[section].collapsed
//        
//        // Toggle collapse
//        sections[section].collapsed = collapsed
//        header.setCollapsed(collapsed)
//        
//        // Adjust the height of the rows inside the section
//        tableViewTest.beginUpdates()
//        for i in 0 ..< sections[section].items.count {
//            
//            tableViewTest.reloadRowsAtIndexPaths([NSIndexPath(forRow: i, inSection: section)], withRowAnimation: .Automatic)
//        }
//        tableViewTest.endUpdates()
//    }
//    
//
//    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
//        
//            PagesCount = 1 
//            getSearchableHelp(searchText,index: self.PagesCount)
//
//    }
//    
//    func getSearchableHelp(searchStr : String,index:Int){
//        
//        let RequestParameters : NSDictionary = [
//            "Keyword" : searchStr,
//            "PageIndex":index,
//            "PageSize":10
//        ]
//        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
//        
//        NetworkHelper.RequestHelper(nil, service: WSMethods.GetHelpCenter, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
//            , responseType: ResponseType.StringJson , callbackString:
//            {response in
//                
//                
//                
//                if(response.result.isFailure)
//                {
//                    
//                    if let networkError = response.result.error {
//                        if (networkError.code == -1009) {
//                            
//                            self.view.makeToast("No Internet connection")
//                            
//                        }
//                        else
//                        {
//                            self.view.makeToast("An error has been occurred")
//                            print("error")
//                        }
//                        return
//                    }
//                }
//                
//                if(response.result.isSuccess)
//                {
//                    if let JSON = response.result.value {
//                        // print("JSON: \(JSON)")
//                        let Response = ModHelp(json:JSON)
//                        
//                        if (Response.ResultResponse == "21"){
//                            self.view.makeToast("An error has been occurred")
//                            
//                            
//                        }
//                        else
//                        {
//                            //self.view.makeToast("got the groups :D")
//                            
//                            self.searchHelp.appendContentsOf(Response.HelpCenterOb!);
//                            for index in 0..<self.searchHelp.count {
//                                self.sections.append(Section(name: self.searchHelp[index].Questions!, items: [self.searchHelp[index].Answer!]))
//                            }
//                            self.PagesCount += 1
//                            self.tableViewTest.reloadData()
//                            
//                
//                            
//                        }
//                        
//                        
//                    }
//                }
//                
//            }
//            , callbackDictionary: nil)
//        
//
//    }
//
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

//}
