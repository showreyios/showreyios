//
//  CvOffers.swift
//  ShowRey
//
//  Created by Appsinnovate on 10/8/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Toast_Swift
import SwiftyJSON
import DZNEmptyDataSet
import FirebaseAnalytics

class CvOffers: UICollectionViewController,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate
{
    var PagesCount = 1
    let refresher = UIRefreshControl()
    var DoneFirstFeedLoad = false
    var Offers = [ModFeedFeedobj]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.title = "Special Offers"
        
        collectionView!.emptyDataSetSource = self
        collectionView!.emptyDataSetDelegate = self
        
        let button = UIButton()
        button.frame = CGRectMake(0, 0, 21, 31)
        button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
        button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
        let barButton = UIBarButtonItem()
        barButton.customView = button
        navigationItem.leftBarButtonItem = barButton
        
        let accType = AccountType(rawValue: (User?.AccountType?.integerValue)!)//(User?.AccountType?.integerValue as AccountType?)
        
        if accType == AccountType.Verified || accType == AccountType.Moderator
        {
            let button2 = UIButton()
            button2.frame = CGRectMake(0, 0, 35, 35)
            button2.setImage(UIImage(named: "Add-Mark"), forState: .Normal)
            button2.addTarget(self, action: #selector(CreateOffer), forControlEvents: .TouchUpInside)
            let barButton2 = UIBarButtonItem()
            barButton2.customView = button2
            navigationItem.rightBarButtonItem = barButton2
            self.navigationItem.rightBarButtonItem!.tintColor = UIColor.whiteColor()
        }
        
        collectionView!.addInfiniteScrollWithHandler { (UITableView) in
            
            self.GetOffers(self.PagesCount,UserID:User!.Id!.stringValue, FromPagerOrRefresher: true)
        }

        GetOffers(PagesCount,UserID:User!.Id!.stringValue);
        
        refresher.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
        collectionView!.addSubview(refresher)
        FIRAnalytics.logEventWithName("Special offers", parameters: nil)
        
    }
    func CreateOffer() {
        
        let createPost = self.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost        
        createPost.FromScreen = .CreateSpecialOffers
        self.navigationController?.pushViewController(createPost, animated: true)
        
    }
//    override func viewDidLayoutSubviews()
//    {
//        super.viewDidLayoutSubviews()
//        collectionView!.collectionViewLayout.invalidateLayout()
//    }

    func refresh()
    {
        PagesCount = 1
        GetOffers(self.PagesCount,UserID:User!.Id!.stringValue, FromPagerOrRefresher:true)
    }
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of items
        return Offers.count
    }
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let wantedwidth = CGFloat((collectionView.bounds.size.width - 8)  * 0.467)
        return CGSizeMake(wantedwidth,wantedwidth*1.2)
    }
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "No Posts Found", attributes: [NSFontAttributeName : UIFont.boldSystemFontOfSize(18)
            ,NSForegroundColorAttributeName:UIColor.grayColor()])
    }
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        return Offers.count == 0 && DoneFirstFeedLoad
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell : CcOffers = collectionView.dequeueReusableCellWithReuseIdentifier("CcOffers", forIndexPath: indexPath) as! CcOffers
        cell.SetData(Offers[indexPath.row])
        
        return cell
    }
   
    func HidePost(toHide:ModFeedFeedobj) {
        
        let indexToRemove = Offers.indexOf(toHide)
        if indexToRemove != nil
        {
            Offers.removeAtIndex(indexToRemove!)

            collectionView?.deleteItemsAtIndexPaths([NSIndexPath(forItem: indexToRemove!, inSection: 1)])

        }

    }
    
    
    static func RefreshOffer(target: UIViewController, PostID:Int,completion:(succ:ModFeedFeedobj?)->())
    {
     
        MBProgressHUD.showHUDAddedTo(target.view, animated: true).label.text = "Loading..."
        let RequestParameters:NSDictionary =
            [
                "PostId" : PostID,
                "UserId" : User!.Id!
            ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetGetSpecialOfferByGetSpecialOfferId, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                var succ : ModFeedFeedobj?
                MBProgressHUD.hideHUDForView(target.view, animated: true)
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            target.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            target.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let FeedsResponse = ModNewsFeed_Single(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            
                        }
                        else
                        {
                            succ = FeedsResponse.Feedobj!
                        }
                    }
                }
                
                completion(succ: succ)
            }
            , callbackDictionary: nil)
    }
    func RefreshCellUI(feed:ModFeedFeedobj)
    {
        
        let index = self.Offers.indexOf({ (feed) -> Bool in
            return feed.Id == feed.Id
        })
        if index != nil
        {
            self.Offers[index!] = feed
            
            self.collectionView!.reloadItemsAtIndexPaths([NSIndexPath(forItem: index!, inSection: 0)])
//            self.collectionView!.reloadData()
        }

    }
    func GetOffers(Index:Int,UserID:String,FromPagerOrRefresher:Bool = false)
    {
        if !FromPagerOrRefresher
        {
            let loadingNotification = MBProgressHUD.showHUDAddedTo(view, animated: true)
            loadingNotification.label.text = "Loading..."
        }
        
        let RequestParameters : NSDictionary = [
            "PageIndex" : Index,
            "PageSize" :10,
            "ProfileUserId" : UserID,
            "UserId" : UserID
        ]
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetSpecialOffers, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                if(Index == 1)
                {
                    self.Offers.removeAll(keepCapacity: false)
                }
                
                self.DoneFirstFeedLoad = true
                if FromPagerOrRefresher
                {
                    
                    self.collectionView?.finishInfiniteScrollWithCompletion(nil)
                    self.refresher.endRefreshing()
                }
                else
                {
                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    //  self.refresher.endRefreshing()
                }
                
                if(response.result.isFailure)
                {
                  
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {

                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let Response = ModNewsFeed(json:JSON)
                        
                        if (Response.ResultResponse != "0")
                        {
//                            self.view.makeToast(Response.Description!)
                           
                        }
                        else
                        {
//                            if Index == 1
//                            {
//                                Response.Feedobj = [ModFeedFeedobj]()
//                                Response.ListCount = 2
//                                let t1 = ModFeedFeedobj()
//                                t1.Title = "t1"
//                                t1.Text = "offire 1"
//                                t1.OriginalPrice = 55
//                                t1.Price = 44
//                                Response.Feedobj?.append(t1)
//                                let t2 = ModFeedFeedobj()
//                                t2.Title = "t2"
//                                t2.Text = "offire 2"
//                                t2.OriginalPrice = 55
//                                t2.Price = 44
//                                Response.Feedobj?.append(t2)
//                                
//                            }
                            
                            if(Response.ListCount?.integerValue > 0)
                            {self.PagesCount += 1}
                            
                            self.Offers.appendContentsOf(Response.Feedobj!)
                        
                           // self.view.makeToast("got the offerssssss :D")
                            
                        }
                    }
                }
                self.collectionView!.reloadData()
            }
            , callbackDictionary: nil)
        
        
    }
    
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        //let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CcOffers
        let offersDetails = VcOffersDetails(nibName: "VcOffersDetails", bundle: nil)
        offersDetails.setData(self, data: Offers[indexPath.row])
        navigationController?.pushViewController(offersDetails, animated: true)
    }
    
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
