//
//  VcForgetPassword1.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 9/17/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import PureLayout
import EGFloatingTextField
import MBProgressHUD
import SwiftyJSON


class VcForgetPassword1: UIViewController {

    @IBOutlet weak var TxtEmailAddress: EGFloatingTextField!
    @IBOutlet weak var LblErrorEmail: UILabel!
    
    override func viewDidLoad() {
       
        
        self.navigationItem.title = "Forget password"
        
        super.viewDidLoad()
        TxtEmailAddress.floatingLabel = true
        TxtEmailAddress.setPlaceHolder(" Email Address")
        TxtEmailAddress.validationType = .Email
        TxtEmailAddress.errorLabel = LblErrorEmail
        TxtEmailAddress.customerrorMessage = "Invalid email"
      
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BtnConfirmPass1(sender: AnyObject) {
        
        
        let RequestParameters : NSDictionary = [
            "EmailorUserName" : TxtEmailAddress.text!
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        NetworkHelper.RequestHelper(nil, service: WSMethods.ForgetPasswordStp1, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: jsonCallBack2)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
    }
    
    func jsonCallBack2(response: (JSON: JSON, NSError: NSError?))
    {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    self.view.makeToast("No Internet connection")
                }
                else
                {
                    self.view.makeToast("An error has been occurred")
                    print("error")
                }
                return
            }
        }
        else
        {
            let resultResponse = response.JSON["ResultResponse"].stringValue
            if( resultResponse == "0"){
            
                
                let secondview = self.storyboard?.instantiateViewControllerWithIdentifier("VcForgetPassword2") as! VcForgetPassword2
                self.navigationController?.pushViewController(secondview, animated: true)
                
                secondview.email =  TxtEmailAddress.text!
                print("Success")
            }else{
                self.view.makeToast("Unregistered email")
            }
            
        }
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
