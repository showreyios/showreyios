//
//  ViewController.swift
//  Pager
//
//  Created by Lucas Oceano on 12/03/2015.
//  Copyright (c) 2015 Cheesecake. All rights reserved.
//

import UIKit

class VcViewPagerExample: PagerController, PagerDataSource {

    
    var titles: [String] = []
    let screenSize = ((UIScreen.mainScreen().bounds.width))/2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        // Instantiating Storyboard ViewControllers
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller1 = storyboard.instantiateViewControllerWithIdentifier("firstView")
        let controller2 = storyboard.instantiateViewControllerWithIdentifier("secondView")
        //		let controller3 = storyboard.instantiateViewController(withIdentifier: "thirdView")
        //		let controller4 = storyboard.instantiateViewController(withIdentifier: "tableView")
        //		let controller5 = storyboard.instantiateViewController(withIdentifier: "fifthView")
        //		let controller6 = storyboard.instantiateViewController(withIdentifier: "sixthView")
        
        // Setting up the PagerController with Name of the Tabs and their respective ViewControllers
        self.setupPager(
            nil,
            tabImages: [("Image-2","Image-4",nil),("Image-3","Image-5",nil)],
            tabControllers: [controller1, controller2])

        
        customizeTab()
        
        //		if let controller = controller1 as? GreyViewController {
        //			controller.didSelectRow = pushGreyDetailViewController
        //		}
    }
    
    // Customising the Tab's View
    func customizeTab() {
        indicatorColor = UIColor.whiteColor()
        tabsViewBackgroundColor = UIColor(colorLiteralRed: 145 / 255, green: 78 / 255, blue: 233 / 255, alpha: 1)
        //(colorLiteralRed: 145 / 255, green: 78 / 255, blue: 233 / 255, alpha: 1)
        //(rgb: 0x00AA00)
        contentViewBackgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.32)
        
        //startFromSecondTab = false
        centerCurrentTab = true
        tabLocation = PagerTabLocation.Top
        tabHeight = 49
        tabOffset = 0
        tabWidth = screenSize
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.During
        selectedTabTextColor = UIColor.whiteColor()
        tabsTextFont = UIFont(name: "HelveticaNeue-Bold", size: 15)!
        // tabTopOffset = 10.0
        // tabsTextColor = .purpleColor()
        
    }
    
    
    // Programatically selecting a tab. This function is getting called on AppDelegate
    //	func changeTab() {
    //		self.selectTabAtIndex(4)
    //	}
    
    
    //	func pushGreyDetailViewController(text: String) {
    //		let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //		if let detail = storyboard.instantiateViewController(withIdentifier: "greyTableDetail") as? GreyDetailViewController {
    //			detail.text = text
    //			self.navigationController?.pushViewController(detail, animated: true)
    //		}
}
