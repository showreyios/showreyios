//
//  NewsFeedHelper.swift
//  ShowRey
//
//  Created by M-Hashem on 11/7/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import SSImageBrowser

class NewsFeedHelper
{
    static func imageViewer(files:[ModFeedAttachedFiles],caption:String,presnter:UIViewController,animatefrom:UIView) {
        
        // Create an array to store IDMPhoto objects
        var photos = [SSPhoto]()
        
        for var file in files {
            
            let photo = SSPhoto(url: NSURL(string: file.URL!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!)
            photo.aCaption = caption
            photos.append(photo)
        }
        
        // Create and setup browser
        let browser = SSImageBrowser(aPhotos: photos, animatedFromView: animatefrom) // using initWithPhotos:animatedFromView: method to use the zoom-in animation
       // browser.delegate = (presnter as! SSImageBrowserDelegate)
        browser.displayActionButton = true
        browser.displayArrowButton = true
        browser.displayCounterLabel = true
        browser.usePopAnimation = false
        // browser.scaleImage = sender.currentImage
        
        browser.useWhiteBackgroundColor = true
        
        // Show
        presnter.presentViewController(browser, animated: true, completion: nil)
    }
}
