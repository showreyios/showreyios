//
//  TcNotification.swift
//  ShowRey
//
//  Created by M-Hashem on 12/18/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class TcNotification: UITableViewCell {

    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var PersonName: UILabel!
    @IBOutlet weak var Description: UILabel!
    @IBOutlet weak var NotiDate: UILabel!
    
    var IsRead_ = false
    var IsRead: Bool
    {
        get{return IsRead_}
        set
        {
            backgroundColor = newValue ? UIColor.whiteColor() : UIColor(hex: "e4d3f7")
            IsRead_ = newValue
        }
        
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        personImage.layer.cornerRadius = personImage.frame.width/2;
        personImage.layer.borderWidth = 0
        personImage.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func SetData(Data:ModNotification)
    {
        personImage.kf_setImageWithURL(NSURL(string: (Data.UserProfile?.ProfilePicture!)!), placeholderImage:  UIImage(named: "Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        PersonName.text = Data.UserProfile?.FullName!;
        Description.text = Data.Text!
        NotiDate.text = Data.Date!
        
        IsRead = Data.isread
    }
    
}











