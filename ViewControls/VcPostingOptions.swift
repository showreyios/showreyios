//
//  VcPostingOptions.swift
//  ShowRey
//
//  Created by Radwa Khaled on 10/4/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit


class VcPostingOptions: UIViewController{
    
    @IBOutlet weak var BtnFshoin: UIButton!
    
    @IBOutlet weak var btnWall: UIButton!
    @IBOutlet weak var btnRegular: UIButton!
    @IBOutlet weak var btnStatistics: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnLists: UIButton!
    @IBOutlet weak var createFashionPost: UIImageView!
    @IBOutlet weak var imgUser: UIImageView!
    
    
    let application:UIApplication! = nil
    
    static var openLink:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnWall.setBackgroundImage(UIImage(named: "Big-Btn-Darker"), forState: UIControlState.Highlighted)
        btnStatistics.setBackgroundImage(UIImage(named: "Big-Btn-Darker"), forState: UIControlState.Highlighted)
        btnProfile.setBackgroundImage(UIImage(named: "Big-Btn-Darker"), forState: UIControlState.Highlighted)
        btnLists.setBackgroundImage(UIImage(named: "Big-Btn-Darker"), forState: UIControlState.Highlighted)
        
        
        if ((User) != nil)
        {
            let str : String? = (User?.ProfilePicture)!
            let url = NSURL(string:str!)
            imgUser.kf_setImageWithURL(url, placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
        }
        
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(imageTapped(_:)))
        createFashionPost.userInteractionEnabled = true
        createFashionPost.addGestureRecognizer(tapGestureRecognizer)
        
        
        // self.dataSource = self
        // Do any additional setup after loading the view.
        
        //        let barColor: UIColor = UIColor(red: 103.0/255.0, green: 0.0/255.0, blue: 174.0/255.0, alpha: 1.0)
        //        self.navigationController!.navigationBar.barTintColor = barColor
        //        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        //        self.navigationController!.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController!.setNavigationBarHidden(false, animated: false)
        openedScreen = self
        
        if VcPostingOptions.openLink
        {
            if redirectionId != nil
            {
                if redirectionScreen == "inviteWithRegular"
                {
                    VcNotificationsLocal.ToRegularPost(Int(redirectionId)!, parent: self)
                }
                else if redirectionScreen == "inviteWithFashion"
                {
                    VcNotificationsLocal.ToFasionPost(Int(redirectionId)!, parent: self)
                }
                redirectionId = nil
            }
            if PushNotificationsRedirData != nil
            {
                
                // let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                // let aVariable = appDelegate.someVariable
                
                
                AppDelegate.HandlePushNotifications(appDelegate.comFromPushNotifcationFlag)
                
            }
            VcPostingOptions.openLink = false
        }
        // navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Left", style: .Plain, target: self, action: #selector(SSASideMenu.presentLeftMenuViewController))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func imageTapped(img: AnyObject)
    {
        // image action to go to fashion post
        
        let createPost = self.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost
        
        createPost.FromScreen = .CreateFashion
        createPost.comeFromWelcome = true
        self.navigationController?.pushViewController(createPost, animated: true)
        
    }
    
    @IBAction func btnGotoWall(sender: AnyObject) {
        
        AppDelegate.RoutToScreen("VcHome")
    }
    
    @IBAction func BtnCreateRegularPost(sender: AnyObject) {
        // AppDelegate.RoutToScreen("VcCreatePost")
        
        let createPost = self.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost
        
        createPost.FromScreen = .CreatePost
        createPost.comeFromWelcome = true
        self.navigationController?.pushViewController(createPost, animated: true)
        
    }
    
    @IBAction func BtnCreateFashionPost(sender: AnyObject) {
        
        let createPost = self.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost
        
        createPost.FromScreen = .CreateFashion
        createPost.comeFromWelcome = true
        self.navigationController?.pushViewController(createPost, animated: true)
        
    }
    
    
    @IBAction func BtnCreateStatistics(sender: AnyObject) {
        let postStat = self.storyboard?.instantiateViewControllerWithIdentifier("VcPostStatistics") as! VcPostStatistics
        
        postStat.come = .profile
        self.navigationController?.pushViewController(postStat, animated: true)
        
    }
    
    @IBAction func btnListsAction(sender: AnyObject) {
        
        let postStat = self.storyboard?.instantiateViewControllerWithIdentifier("VcGetGroups") as! VcGetGroups
        postStat.come = .welcome
        self.navigationController?.pushViewController(postStat, animated: true)
        
    }
    
    @IBAction func btnProfileAction(sender: AnyObject) {
        
        let Feedsmaster = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        Feedsmaster.SetupForTimeLine((User?.Id!.stringValue)!)
        self.navigationController?.pushViewController(Feedsmaster, animated: true)
        //        Feedsmaster.slideMenuCustomBtn()
        //        sideMenuViewController?.contentViewController = UINavigationController(rootViewController: Feedsmaster)
        //        sideMenuViewController?.hideMenuViewController()
        
    }
    
    
    @IBAction func btnHelp(sender: AnyObject) {
        
        //        let view = self.storyboard?.instantiateViewControllerWithIdentifier("VcTerms") as! VcTerms
        //        self.navigationController?.pushViewController(view, animated: true)
        //        view.terms = 1
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
