//
//  VcComments.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 11/27/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class VcCommentsContainer: PagerController, PagerDataSource {
    
    
    //Likers
    var LevelId = -1
    var ObjId = -1
    var PostType = -1
    
    var isItPrivate: Bool = false
    
    
    var IsItSpecialOffer : Bool = false

    
    var ScreenTitle = "All comments"
    
    
    
    
    
    var MyScreenMod : ScreenMod = .All
    
    
    enum ScreenMod : Int{
        case All = 0
        case Whisper = 1
        case Likers = 2
        case DisLikers = 3
       
    }

     var Commentblock : ((Int) -> ())! = nil
    
    var titles: [String] = []
    let screenSize = ((UIScreen.mainScreen().bounds.width))/4
    
    var feednews:VcMasterNewsFeed!
    
    var All : VcComments = VcComments()
    var Whispers : VcComments = VcComments()
    var Likes : VcFollorwes_Likers_Search = VcFollorwes_Likers_Search()
    var Dislikes : VcFollorwes_Likers_Search = VcFollorwes_Likers_Search()
    var tapAll : VTapComment!
    var tapWhispers : VTapComment!
    var tapLikes : VTapComment!
    var tapDislikes : VTapComment!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = ScreenTitle
        
 
        tapAll = VTapComment.fromNib("VTapComment")
        tapWhispers = VTapComment.fromNib("VTapComment")
        
        tapLikes = VTapComment.fromNib("VTapComment")
        tapDislikes = VTapComment.fromNib("VTapComment")
        
        tapAll.LblCommnet.text = "All"
        tapWhispers.LblCommnet.text = "Whisper"
        tapLikes.LblCommnet.text = "Yes"
        tapDislikes.LblCommnet.text = "No"
        
        self.dataSource = self
        
        
        
        let storyboard = AppDelegate.storyboard
        
        All = storyboard.instantiateViewControllerWithIdentifier("VcComments") as! VcComments
        All.ObjId = ObjId
        All.IsItSpecialOffer = IsItSpecialOffer
        All.isItPrivate = isItPrivate
        Whispers = storyboard.instantiateViewControllerWithIdentifier("VcComments") as! VcComments
        Whispers.ScreenMod = 1
        Whispers.ObjId = ObjId
        Whispers.isItPrivate = isItPrivate
        Whispers.IsItSpecialOffer = IsItSpecialOffer
        Likes = storyboard.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        
        Dislikes = storyboard.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        
        self.setupPager(nil, tabImages: [(activeImg: "Comments-(Chats)-selected" , inactiveImge: "Comments-(Chats)",Custom: tapAll),(activeImg: "Comments-(Whisper-Tab)-Selected" , inactiveImge: "Comments-(Whisper-Tab)",Custom: tapWhispers),(activeImg: "Offers-(Like)" , inactiveImge: "Offers-(Like)-unselected",Custom: tapLikes),(activeImg: "Offers-(DisLike)" , inactiveImge: "Offers-(DisLike)-unselected",Custom: tapDislikes)], tabControllers: [All, Whispers , Likes, Dislikes])
        
        customizeTab()
        
        defaultSetup()
        
        selectTabAtIndex(MyScreenMod.rawValue)
        
        
    }
    override func changeActiveTabIndex(newIndex: Int) {
       
        super.changeActiveTabIndex(newIndex)
        switch(newIndex)
        {
            case 0://All
                ScreenTitle = "All comments"
                All.ObjId = ObjId
                All.PostType = PostType
                All.ScreenMod = 0
                All.parentView = self
                All.IsItSpecialOffer = IsItSpecialOffer
                All.isItPrivate = isItPrivate
                self.navigationItem.title = ScreenTitle
            break
            case 1://Whisper
                ScreenTitle = "Whisper"
                Whispers.ObjId = ObjId
                Whispers.PostType = PostType
                Whispers.ScreenMod = 1
                Whispers.parentView = self
                Whispers.IsItSpecialOffer = IsItSpecialOffer
                Whispers.isItPrivate = isItPrivate
                self.navigationItem.title = ScreenTitle
            break
            case 2://Like
                ScreenTitle = "Likes"
                self.navigationItem.title = ScreenTitle
                Likes.vcCommentsContainer = self
            Likes.LikersMod(LevelId, LikeType: true, ObjId: ObjId)
            case 3://DisLike
                ScreenTitle = "DisLikes"
                self.navigationItem.title = ScreenTitle
                Dislikes.vcCommentsContainer = self
            Dislikes.LikersMod(LevelId, LikeType: false, ObjId: ObjId)
            default:
            break
        }
        
         if (newIndex == 0 )
         {
      //   following.FolloingMod()
         }
         else
         {
     //    follorwes.FollorwesMod()
         }
 
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customizeTab() {
        indicatorColor = UIColor(hex: "#b379e7")
        tabsViewBackgroundColor = UIColor.whiteColor()
        //(colorLiteralRed: 145 / 255, green: 78 / 255, blue: 233 / 255, alpha: 1)
        //(rgb: 0x00AA00)
        contentViewBackgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.32)
        
        //startFromSecondTab = false
        centerCurrentTab = true
        tabLocation = PagerTabLocation.Top
        tabHeight = 49
        tabOffset = 0
        tabWidth = screenSize
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.During
        selectedTabTextColor = UIColor(colorLiteralRed: 145 / 255, green: 78 / 255, blue: 233 / 255, alpha: 1)
        tabsTextColor = UIColor.lightGrayColor()
        tabsTextFont = UIFont(name: "HelveticaNeue-Bold", size: 15)!
        // tabTopOffset = 10.0
        // tabsTextColor = .purpleColor()
        
    }
        

    func InitMod(LevelId:Int , ObjId:Int , PostType:Int , IsItSpecialOffer : Bool = false , isItPrivate:Bool = false)
    {
        self.LevelId = LevelId
        self.ObjId = ObjId
        self.PostType = PostType
        self.IsItSpecialOffer = IsItSpecialOffer
        self.isItPrivate = isItPrivate
    }
    
    
}
