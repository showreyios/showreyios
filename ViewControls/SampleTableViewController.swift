//
//  SampleTableViewController.swift
//  AEAccordion
//
//  Created by Marko Tadic on 6/26/15.
//  Copyright © 2015 AE. All rights reserved.
//

import UIKit
import MBProgressHUD

class SampleTableViewController: AEAccordionTableViewController {
    
    // MARK: - Properties
    
    let cellIdentifier = "TableViewCellHelp"
    var PagesCount = 1
    var searchHelp = [HelpCenterObj]()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        title = NSBundle.mainBundle().infoDictionary!["CFBundleName"] as? String
        getSearchableHelp("",index: self.PagesCount)
        
       // expandFirstCell()
       
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//         self.tableView.reloadData()
    }

    // MARK: - Helpers
    
    func registerCell() {
        let cellNib = UINib(nibName: cellIdentifier, bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: cellIdentifier)
    }
    
    func expandFirstCell() {
        let firstCellIndexPath = NSIndexPath(forRow: 0, inSection: 0)
        expandedIndexPaths.append(firstCellIndexPath)
    }
    
    // MARK: - UITableViewDataSource
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchHelp.count + 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == searchHelp.count
        {return UITableViewCell()}
        
      let  cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! TableViewCellHelp
        
        cell.header_text.text = searchHelp[indexPath.row].Questions
        cell.details_Text.text = searchHelp[indexPath.row].Answer
        cell.parentTable = self
        cell.indexHelp = indexPath.row
        cell.header_text.sizeToFit()
       // cell.header_text.adjustsFontSizeToFitWidth = true
        cell.details_Text.sizeToFit()
        //cell.details_Text.adjustsFontSizeToFitWidth = true
        cell.headerHeight.constant = cell.header_text.bounds.size.height + 20
        searchHelp[indexPath.row].rowHieght = cell.headerHeight.constant + cell.details_Text.bounds.size.height + 15
        searchHelp[indexPath.row].headerHeight = cell.headerHeight.constant
        
       // cell.det.constant
        
        return cell
    }
    
     func reloadCellAtRow(row: Int) {
        let indexPath = NSIndexPath(forRow: row, inSection: 0)
        
        tableView.beginUpdates()
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        tableView.endUpdates()
    }
    
    // MARK: - UITableViewDelegate
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.row == searchHelp.count
        {return 60}
        return expandedIndexPaths.contains(indexPath) ? searchHelp[indexPath.row].rowHieght : searchHelp[indexPath.row].headerHeight
    }

  
   
    
   func getSearchableHelp(searchStr : String,index:Int){
    
    let loadingNotification = MBProgressHUD.showHUDAddedTo(view, animated: true)
    loadingNotification.label.text = "Loading..."

        let RequestParameters : NSDictionary = [
            "Keyword" : searchStr,
            "PageIndex":index,
            "PageSize":10
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetHelpCenter, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                  MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response = ModHelp(json:JSON)
                        
                        if (Response.ResultResponse == "21"){
                            self.view.makeToast("An error has been occurred")
                            
                            
                        }
                        else
                        {
                           
                            
                            self.searchHelp.appendContentsOf(Response.HelpCenterOb!);
                            // for index in 0..<self.searchHelp.count {
                            // self.sections.append(Section(name: self.searchHelp[index].Questions!, items: [self.searchHelp[index].Answer!]))
                            //}
                            self.PagesCount += 1
                            self.tableView.reloadData()
                            
                            
                            
                        }
                        
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
    }

}
