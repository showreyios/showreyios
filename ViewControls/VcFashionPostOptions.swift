//
//  VcFashionPostOptions.swift
//  ShowRey
//
//  Created by User on 10/29/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import RMDateSelectionViewController
import Kingfisher
import MBProgressHUD
import SwiftyJSON


class VcFashionPostOptions: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var comeFromWelcome : Bool = false
    var tvCreatePost : TvCreatePost!
    var fashionMaster: VcMasterNewsFeed!
    var shapeImg : NSArray! = nil
    var shapeTxt : NSArray! = nil
    var datePicker : NSArray! = nil
    var gender : Int = 0
    var shapeSelected: Bool = false
    
    var AgeGroup = 1
    
    var size = "Size"
    var dateFormatter:NSDateFormatter!
    var checkRowIndex:Int = 0
    var updateProfile : Bool = false
   
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var txtWeight: UITextField!
    @IBOutlet weak var txtHeight: UITextField!
    @IBOutlet weak var btnSize: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
   
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shapeImg = ["Body-Shape-5" ,"Body-Shape-2" ,"Body-Shape-1","Body-Shape-3","Body-Shape-6","Body-Shape-4"]
        shapeTxt = ["Triangle","Inverted Triangle" ,"Rectangle","Hourglass", "Diamond" ,"Rounded" ]

        datePicker = ["0 - 14" ,"15 - 19","20 - 24", "25 - 29", "30 - 34", "35 - 39", "40 - 44","45 - 49"
        ,"50 - 54", "55 - 59", "60 - 64", "65 Above"]
   //     let todaysDate:NSDate = NSDate()
         dateFormatter = NSDateFormatter()
         dateFormatter.dateFormat = "MM-dd-yyyy"
        
        if(updateProfile == true){
            self.navigationItem.title = "Update Profile"
           // self.btnSave.title = "Save"
        }
        
        if User!.Height?.integerValue <= 0 {
            txtHeight.text! = ""
        }else {
            txtHeight.text! = (User!.Height?.stringValue)!
        }
        
        if User!.Height?.integerValue <= 0 {
            txtWeight.text! = ""
            
        }else{
            
            txtWeight.text! = (User!.Weight?.stringValue)!
        }
        
        
        if((User!.Size) == "") {
             btnSize.setTitle("Size", forState: .Normal)
        }else{
            if ((User!.Size) == nil) {
                 btnSize.setTitle("Size", forState: .Normal)
            }else {
                size = User!.Size!
                btnSize.setTitle(size, forState: .Normal)
                btnSize.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
            }
        }
        
        if(User!.AgeGroup != nil){
            
            self.AgeGroup = (User!.AgeGroup?.integerValue)!
            if (AgeGroup - 1) >=  0 && (AgeGroup - 1) <=  11 {
                btnDate.setTitle(datePicker[AgeGroup - 1] as? String, forState: .Normal)
                // btnDate.setTitle(User!.AgeGroup?.stringValue, forState: .Normal)
                btnDate.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
            }
        }
        
        
        if (User!.BodyShape?.integerValue)! != nil {
            checkRowIndex = (User!.BodyShape?.integerValue)! - 1
        }
        
        if (checkRowIndex) >=  0 && (checkRowIndex) <=  5 {
            
            shapeSelected = true
            let indexpath = NSIndexPath(forRow: checkRowIndex ,inSection: 0)
            
            collectionView.selectItemAtIndexPath( indexpath, animated: false, scrollPosition: .CenteredHorizontally)
        }
        
        
        if(User!.Gender == "M"){
            
            btnMale.setImage(UIImage(named: "Radio-btn-selected"), forState: .Normal)
            
            btnFemale.setImage(UIImage(named: "Radio-btn"), forState: .Normal)
        }else{
            
            btnFemale.setImage(UIImage(named: "Radio-btn-selected"), forState: .Normal)
            
            btnMale.setImage(UIImage(named: "Radio-btn"), forState: .Normal)
            
        }
        
        
        collectionView.reloadData()
        //let DateInFormat:String = dateFormatter.stringFromDate(todaysDate)
        
       // btnDate.setTitle(DateInFormat, forState: .Normal)
        
        
        let bounds = UIScreen.mainScreen().bounds
        
        let screenheight = bounds.size.height
        
        if (screenheight >= 667 && screenheight < 736) // biger than iphone 5
        {
             scrollView.contentSize = CGSizeMake(400, 680)
        }else if screenheight >= 736 {
            scrollView.contentSize = CGSizeMake(400, 780)
        }
        else
        {
            scrollView.contentSize = CGSizeMake(400, 580)
        }
        
//        if(updateProfile == true){
//            
//            
//            btnSkip.hidden = true
//            
//        }else {
//            
//            btnSkip.hidden = false
//        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnMaleAction(sender: AnyObject) {
        gender = 0
        btnMale.setImage(UIImage(named: "Radio-btn-selected"), forState: .Normal)
        
        btnFemale.setImage(UIImage(named: "Radio-btn"), forState: .Normal)
       
    }
    
    
    @IBAction func btnFemaleAction(sender: AnyObject) {
        gender = 1
        btnFemale.setImage(UIImage(named: "Radio-btn-selected"), forState: .Normal)
        
        btnMale.setImage(UIImage(named: "Radio-btn"), forState: .Normal)
        
        
    }
    
    
    
    @IBAction func btnDatePicker(sender: AnyObject) {
      //openDateSelectionViewController()
        // 1
        let optionMenu = UIAlertController(title: "Select Age", message: nil, preferredStyle: .ActionSheet)
        
        // 2
        let x14 = UIAlertAction(title: "0 - 14", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("0 - 14", forState: .Normal)
            self.AgeGroup = 1
            
            
        })
        let x19 = UIAlertAction(title: "15 - 19", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("15 - 19", forState: .Normal)
            self.AgeGroup = 2
        })
        let x24 = UIAlertAction(title: "20 - 24", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("20 - 24", forState: .Normal)
            self.AgeGroup = 3
            
        })
        let x29 = UIAlertAction(title: "25 - 29", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("25 - 29", forState: .Normal)
            self.AgeGroup = 4
        })
        
        let x34 = UIAlertAction(title: "30 - 34", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("30 - 34", forState: .Normal)
            self.AgeGroup = 5
        })
        
        let x39 = UIAlertAction(title: "35 - 39", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("35 - 39", forState: .Normal)
            self.AgeGroup = 6
        })
        
        let x44 = UIAlertAction(title: "40 - 44", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("40 - 44", forState: .Normal)
            self.AgeGroup = 7
        })
        
        let x49 = UIAlertAction(title: "45 - 49", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("45 - 49", forState: .Normal)
            self.AgeGroup = 8
        })
        
        let x54 = UIAlertAction(title: "50 - 54", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("50 - 54", forState: .Normal)
            self.AgeGroup = 9
        })
        
        let x59 = UIAlertAction(title: "55 - 59", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("55 - 59", forState: .Normal)
            self.AgeGroup = 10
        })
        
        let x64 = UIAlertAction(title: "60 - 64", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("60 - 64", forState: .Normal)
            self.AgeGroup = 11
        })
        
        let x65 = UIAlertAction(title: "65 Above", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnDate.setTitle("65 Above", forState: .Normal)
            self.AgeGroup = 12
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(x14)
        optionMenu.addAction(x19)
        optionMenu.addAction(x24)
        optionMenu.addAction(x29)
        optionMenu.addAction(x34)
        optionMenu.addAction(x39)
        optionMenu.addAction(x44)
        optionMenu.addAction(x49)
        optionMenu.addAction(x54)
        optionMenu.addAction(x59)
        optionMenu.addAction(x64)
        optionMenu.addAction(x65)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
        self.btnDate.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)

        
    }
    
 
    
/*
    func openDateSelectionViewController() {
    let style = RMActionControllerStyle.White
   
   // style = RMActionControllerStyle.Black
    UIDatePickerMode.Date
        
    
    let selectAction = RMAction(title: "Select", style: RMActionStyle.Done) { controller in
    if let dateController = controller as? RMDateSelectionViewController {
   print("Successfully selected date: ", dateController.datePicker.date);
        
        self.btnDate.setTitle(self.dateFormatter.stringFromDate(dateController.datePicker.date), forState: .Normal)
         self.btnDate.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
    }
    }
    
    let cancelAction = RMAction(title: "Cancel", style: RMActionStyle.Cancel) { _ in
    print("Date selection was canceled")
    }
    
    let actionController = RMDateSelectionViewController(style: style, title: "", message: "", selectAction: selectAction, andCancelAction: cancelAction)!;
        
    let nowAction = RMAction(title: "Today", style: .Additional) { controller -> Void in
    if let dateController = controller as? RMDateSelectionViewController {
    dateController.datePicker.date = NSDate();
    print("Now button tapped");
    }
    }
    nowAction!.dismissesActionController = false;
    
    actionController.addAction(nowAction!);
    
    
    //You can access the actual UIDatePicker via the datePicker property
    actionController.datePicker.datePickerMode = .Date;
    actionController.datePicker.minuteInterval = 5;
    actionController.datePicker.date = NSDate(timeIntervalSinceReferenceDate: 0);
    
    //On the iPad we want to show the date selection view controller within a popover. Fortunately, we can use iOS 8 API for this! :)
    //(Of course only if we are running on iOS 8 or later)
    if actionController.respondsToSelector(Selector("popoverPresentationController:")) && UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
    //First we set the modal presentation style to the popover style
    actionController.modalPresentationStyle = UIModalPresentationStyle.Popover
    
    //Then we tell the popover presentation controller, where the popover should appear
    if let popoverPresentationController = actionController.popoverPresentationController {
    popoverPresentationController.sourceView = self.view
   // popoverPresentationController.sourceRect = self.tableView.rectForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
    }
    }
    
    //Now just present the date selection controller using the standard iOS presentation method
    presentViewController(actionController, animated: true, completion: nil)
    }
    */
    
 
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shapeImg.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell: CcImageGallery = collectionView.dequeueReusableCellWithReuseIdentifier("CcImageGallery", forIndexPath: indexPath) as! CcImageGallery
        
        
        cell.btnShape.setImage(UIImage(named: shapeImg[indexPath.row] as! String), forState: .Normal)
        cell.btnShapeText.setTitle(shapeTxt[indexPath.row] as? String, forState: .Normal)
        
        cell.btnShapeRadio.addTarget(self, action:  #selector(selectImage(_:)), forControlEvents: .TouchUpInside)
        cell.btnShapeRadio.tag = indexPath.row
        
        cell.btnShape.addTarget(self, action:  #selector(selectImage(_:)), forControlEvents: .TouchUpInside)
        cell.btnShape.tag = indexPath.row
        
        cell.btnShapeText.addTarget(self, action:  #selector(selectImage(_:)), forControlEvents: .TouchUpInside)
        cell.btnShapeText.tag = indexPath.row
        
        
        if indexPath.row == checkRowIndex {
            
            cell.btnShapeRadio.setImage(UIImage(named: "Radio-btn-selected" ), forState: .Normal)
            cell.btnShapeText.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
            cell.backgroundColor =  UIColor(red: 179/255, green: 121/255, blue: 231/255, alpha: 0.1)
            
        }else {
            cell.btnShapeRadio.setImage(UIImage(named: "Radio-btn" ), forState: .Normal)
            cell.btnShapeText.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
            cell.backgroundColor =  UIColor.clearColor()
        }

        
        
        return cell
    }
    
    
    
    
    
    func selectImage(mybutton: UIButton) {

        shapeSelected = true
        checkRowIndex = mybutton.tag
        collectionView.reloadData()
    }

    @IBAction func btnSizeAction(sender: AnyObject) {
        
        
        // 1
        let optionMenu = UIAlertController(title: "Select Size", message: nil, preferredStyle: .ActionSheet)
        
        // 2
        let xs = UIAlertAction(title: "XS", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnSize.setTitle("XS", forState: .Normal)
            self.size = "XS"
           
            
        })
        let s = UIAlertAction(title: "S", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.btnSize.setTitle("S", forState: .Normal)
             self.size = "S"
        })
        let m = UIAlertAction(title: "M", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnSize.setTitle("M", forState: .Normal)
             self.size = "M"
            
        })
        let l = UIAlertAction(title: "L", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
          self.btnSize.setTitle("L", forState: .Normal)
            self.size = "L"
        })
        
        let xl = UIAlertAction(title: "XL", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnSize.setTitle("XL", forState: .Normal)
            self.size = "XL"
        })
        
        let xxl = UIAlertAction(title: "XXL", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnSize.setTitle("XXL", forState: .Normal)
            self.size = "XXL"
        })
        
        let xxxl = UIAlertAction(title: "XXXL", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.btnSize.setTitle("XXXL", forState: .Normal)
            self.size = "XXXL"
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(xs)
        optionMenu.addAction(s)
        optionMenu.addAction(m)
        optionMenu.addAction(l)
        optionMenu.addAction(xl)
        optionMenu.addAction(xxl)
        optionMenu.addAction(xxxl)
        
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
        self.btnSize.setTitleColor(UIColor(hex: "#b379e7"), forState: .Normal)
        

        
    }
    

    
    @IBAction func btnConfirm(sender: AnyObject) {
        
        
        if txtWeight.text == "" {
            self.view.makeToast("Please Enter Weight")
            return
        }else if txtHeight.text == "" {
            self.view.makeToast("Please Enter Height")
            return
        }else if btnSize.titleLabel!.text == "Size" || btnSize.titleLabel!.text == "" {
            self.view.makeToast("Please Select Size")
            return
        }else if shapeSelected == false {
            
            self.view.makeToast("Please Select Body Type")
            return
            
        }
        
        if(updateProfile == true){
            updateProfileTwo()
           // fashionMaster.LoadProfile((User?.Id)!.stringValue)
        }else{
            
            self.view.endEditing(true)
            tvCreatePost.targetview = self.view
            let feedProfilePros : ModFeedProfilePros = ModFeedProfilePros()
            feedProfilePros.AgeGroup = self.AgeGroup
            feedProfilePros.BodyShape = self.checkRowIndex + 1
            if ( gender == 0)
            {
                feedProfilePros.Gender = "M"
            }
            else  if ( gender == 1)
            {
                feedProfilePros.Gender = "F"
            }
            
            if let height = Int(txtHeight.text!)
            {
                feedProfilePros.Height = height
            }
            else
            {
                self.view.makeToast("Please Enter Height")
                feedProfilePros.Height = 0
                return
            }
            feedProfilePros.Size = self.size
            
            if (feedProfilePros.Size == "Size" )
            {
                self.view.makeToast("Please Enter Size")
                return
            }
            
            if let Weight = Int(txtWeight.text!)
            {
                feedProfilePros.Weight = Weight
            }
            else
            {
                self.view.makeToast("Please Enter Weight")
                feedProfilePros.Weight = 0
                return
                
            }
            if (comeFromWelcome)
            {
                let vcPostChoice = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcPostChoice") as! VcPostChoice
                vcPostChoice.tvCreatePost = tvCreatePost
                vcPostChoice.feedProfilePros = feedProfilePros
                self.navigationController?.pushViewController(vcPostChoice, animated: true)
            }
                
            else
            {
                
                
                
                tvCreatePost.CreatePost(self.view, FeedProfilePros: feedProfilePros)
            }
        }

        
    }
 
    
    
    func updateProfileTwo (){
        var kind : String!
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Updating..."
        
        if(gender == 0){
            kind = "Male"
        }else{
            kind = "Female"
        }
        
        let RequestParameters : NSDictionary = [
            
            "AgeGroup":self.AgeGroup,
            "BodyShape":self.checkRowIndex + 1,
            "Gender":kind,
            "Height":txtHeight.text!,
            "Size":self.size,
            "UserId":(User?.Id)!,
            "Weight":txtWeight.text!
        ]
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.updateProfileTwo, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil,callbackDictionary:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if((response.NSError) != nil)
                {
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    let resultResponse = response.JSON["ResultResponse"].stringValue
                    
                    if( resultResponse == "0"){
                        
                        
                        let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                        
                        var foundview = false
                        for selectingPeople : UIViewController in allViewController
                        {
                            if selectingPeople .isKindOfClass(VcMasterNewsFeed)
                            {
                                
                                let cast = selectingPeople as! VcMasterNewsFeed
                                
                                foundview  = true
                                
                                
                                self.navigationController?.popToViewController(selectingPeople, animated: true)
                            }
                        }
                        if (foundview == false)
                        {
                            
                            
                            let followeesList = self.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
                            
                            
                            
                            self.navigationController?.pushViewController(followeesList, animated: true)
                        }
                        
                        
                        
                        
                        
                    }else{
                        
                        self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                        
                    }
                    
                }
        })
        
    }
    
    @IBAction func skipAction(sender: AnyObject) {
   
        if(updateProfile == false){
            
            if (comeFromWelcome)
            {
                let vcPostChoice = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcPostChoice") as! VcPostChoice
                vcPostChoice.tvCreatePost = tvCreatePost
                vcPostChoice.feedProfilePros = nil
                self.navigationController?.pushViewController(vcPostChoice, animated: true)
            }
                
            else
            {
                
                tvCreatePost.CreatePost(self.view, FeedProfilePros: nil)
            }
            
        
        }else {
            
            let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            
            var foundview = false
            for selectingPeople : UIViewController in allViewController
            {
                if selectingPeople .isKindOfClass(VcMasterNewsFeed)
                {
                    
                    let cast = selectingPeople as! VcMasterNewsFeed
                    
                    foundview  = true
                    
                    
                    self.navigationController?.popToViewController(selectingPeople, animated: true)
                }
            }
            if (foundview == false)
            {
                
                
                let followeesList = self.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
                
                
                
                self.navigationController?.pushViewController(followeesList, animated: true)
            }
        }
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
