//
//  VcSlideMenu.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 9/9/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Kingfisher
import MBProgressHUD
import SwiftyJSON
import Firebase
import FirebaseCrash
import FirebaseInstanceID
import FirebaseMessaging

class VcSlideMenu: UIViewController ,UITableViewDelegate, UITableViewDataSource {
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .None
        tableView.frame = CGRectMake((self.view.frame.size.width - 54 * 4) / 2.0, (self.view.frame.size.height - 54 * 3) / 2.0, self.view.frame.size.width, 40 * 8)
        tableView.autoresizingMask = [.FlexibleTopMargin, .FlexibleBottomMargin, .FlexibleWidth]
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.opaque = false
        tableView.backgroundColor = UIColor.clearColor()
        tableView.backgroundView = nil
        tableView.bounces = false
        return tableView
    }()
    
    
    lazy var imageView:UIImageView = {
        
        let imageView: UIImageView = UIImageView(frame: CGRectMake((self.view.frame.size.width - 54 * 3) / 2.0, (self.view.frame.size.height - 54 * 7.5) / 2.0, 80, 80))
        imageView.autoresizingMask = UIViewAutoresizing.FlexibleLeftMargin
        imageView.autoresizingMask = UIViewAutoresizing.FlexibleRightMargin
        
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 40.0
        imageView.layer.borderColor = UIColor.whiteColor().CGColor
        imageView.layer.borderWidth = 0.0
        imageView.layer.rasterizationScale = UIScreen.mainScreen().scale
        imageView.layer.shouldRasterize = true
        imageView.contentMode = .ScaleAspectFill
        imageView.clipsToBounds = true
        
        if ((User) != nil)
        {
            let str : String? = (User?.ProfilePicture)!
            
            let url = NSURL(string:str!)
            //"http://i3.mirror.co.uk/incoming/article7154237.ece/ALTERNATES/s615b/Katy-Perry.jpg")
            // print("helloooo \((User?.ProfilePicture)!)")
            
            imageView.kf_setImageWithURL(url, placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
        }
        
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(VcSlideMenu.imageTapped(_:)))
        imageView.userInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        
        return imageView
    }()
    
    func imageTapped(img: AnyObject)
    {
        // image action to go to user profile
        
        let Feedsmaster = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        Feedsmaster.SetupForTimeLine((User?.Id!.stringValue)!)
        Feedsmaster.slideMenuCustomBtn()
        sideMenuViewController?.contentViewController = UINavigationController(rootViewController: Feedsmaster)
        sideMenuViewController?.hideMenuViewController()
        //        print("img clicked")
        
        //        let FollowContainer=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("TableViewControllerTest")//VcFollowContainer
        //        sideMenuViewController?.contentViewController = UINavigationController(rootViewController: FollowContainer)
        //        sideMenuViewController?.hideMenuViewController()
        
        
    }
    
    
    lazy var imageViewIndicator:UIImageView = {
        
        let imageViewIndicator: UIImageView = UIImageView(frame: CGRectMake((self.view.frame.size.width - 54 * 1) / 2.0, (self.view.frame.size.height - 54 * 5.5) / 2.0, 24, 24))
        imageViewIndicator.autoresizingMask = UIViewAutoresizing.FlexibleLeftMargin
        imageViewIndicator.autoresizingMask = UIViewAutoresizing.FlexibleRightMargin
        
        imageViewIndicator.layer.masksToBounds = true
        imageViewIndicator.layer.cornerRadius = 12.0
        imageViewIndicator.layer.borderColor = UIColor.whiteColor().CGColor
        imageViewIndicator.layer.borderWidth = 0.0
        imageViewIndicator.layer.rasterizationScale = UIScreen.mainScreen().scale
        imageViewIndicator.layer.shouldRasterize = true
        imageViewIndicator.clipsToBounds = true
        
        if User!.AccountType == AccountType.Standard.rawValue
        {
            imageViewIndicator.image = UIImage(named: "Slide-Profile-Indicator")
        }
        else if User!.AccountType == AccountType.Moderator.rawValue
        {
            imageViewIndicator.image = UIImage(named: "Profile-(moderator-symbol)")
        }
        else if User!.AccountType == AccountType.Verified.rawValue
        {
            imageViewIndicator.image = UIImage(named: "right")
        }
        
        return imageViewIndicator
    }()
    
    lazy var label:UILabel = {
        
        let label: UILabel = UILabel(frame: CGRectMake((self.view.frame.size.width - 54 * 5.2) / 2.0, (self.view.frame.size.height - 54 * 4.5) / 2.0, 200, 24))
        if ((User) != nil)
        {
            label.text = User!.FullName!
            
        }
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        label.backgroundColor = UIColor.clearColor()
        label.textColor = UIColor.whiteColor()
        
        // label.sizeToFit()
        //label.adjustsFontSizeToFitWidth = true
        label.textAlignment = NSTextAlignment.Center
        label.autoresizingMask = UIViewAutoresizing.FlexibleLeftMargin
        label.autoresizingMask = UIViewAutoresizing.FlexibleRightMargin
        return label
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clearColor()
        view.addSubview(tableView)
        // Do any additional setup after loading the view.
        
        self.view.addSubview(imageView)
        self.view.addSubview(label)
        
        self.view.addSubview(imageViewIndicator)
        
        
        //  imageView.image = UIImage(named: profileData.ProfilePicture!)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 8
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 40
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        let titles: [String] = ["Wall", "Lists", "Special Offers", "My Tips", "Invite a Friend", "Settings","Logout", "Help" ]
        
        let images: [String] = ["IconWall", "IconGroups", "IconOffers", "IconTips", "IconInvite", "IconSettings","About-Us", "IconHelp"]
        
        cell.backgroundColor = UIColor.clearColor()
        cell.textLabel?.font = UIFont(name: "Montserrat-Regular", size: 16)
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.textLabel?.text  = titles[indexPath.row]
        cell.selectionStyle = .None
        cell.imageView?.image = UIImage(named: images[indexPath.row])
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        switch indexPath.row
        {
        case 0:
            
            let h = VcHome()
            AppDelegate.navControler.viewControllers = [h]
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: h)
            sideMenuViewController?.hideMenuViewController()
            break
            //        case 1:
            //            let savedposts = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
            //            savedposts.SetupForMySavedPosts()
            //            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: savedposts)
            //            sideMenuViewController?.hideMenuViewController()
        //            break
        case 1:
            let groups=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcGetGroups")
            openedScreen = groups
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: groups)
            sideMenuViewController?.hideMenuViewController()
            
            break
            
        case 2:
            
            let CvOffers_=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("CvOffers")
            openedScreen = CvOffers_
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: CvOffers_)
            sideMenuViewController?.hideMenuViewController()
            break
            
        case 3:
            let mytips = VcMyTips(nibName: "VcMyTips", bundle: nil)
            openedScreen = mytips
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: mytips)
            sideMenuViewController?.hideMenuViewController()
            break
        case 5:
            //  sideMenuViewController?.contentViewController =
            let CvSettings_=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("TvSettings")
            openedScreen = CvSettings_
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: CvSettings_)
            sideMenuViewController?.hideMenuViewController()
            break
            
        case 6:
            //  sideMenuViewController?.contentViewController =
            
            /*
             let TvAboutUs_=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("TvAboutUs")
             openedScreen = TvAboutUs_
             sideMenuViewController?.contentViewController = UINavigationController(rootViewController: TvAboutUs_)
             sideMenuViewController?.hideMenuViewController()
             */
            
            let alertController = UIAlertController(title: "ARE YOU SURE?", message: "", preferredStyle: .Alert)
            
            
            let logoutAction = UIAlertAction(title: "Log Out", style: .Destructive , handler: {
                (alert: UIAlertAction!) -> Void in
                
                self.logout()
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { _ in }
            
            alertController.addAction(logoutAction)
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            
            break
        case 7:
            //  sideMenuViewController?.contentViewController =
            let TvHelp_=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcHelpMenu")
            openedScreen = TvHelp_
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: TvHelp_)
            sideMenuViewController?.hideMenuViewController()
            break
            
        case 4:
            
            let TvInvite_=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcTellAFriend")
            openedScreen = TvInvite_
            sideMenuViewController?.contentViewController = UINavigationController(rootViewController: TvInvite_)
            sideMenuViewController?.hideMenuViewController()
            
            break
            
        default:
            break
        }
        
        
    }
    
    func logout(){
        
        
        // self.tableView.makeToast("Logout", duration: 5, position: CGPoint(x: self.view.frame.size.width/2 , y: 450))
        let RequestParameters : NSDictionary = [
            "UserId":(User?.Id)!
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        NetworkHelper.RequestHelper(nil, service: WSMethods.Logout, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: jsonCallBack2)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
    }
    
    func jsonCallBack2(response: (JSON: JSON, NSError: NSError?))
    {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    
                    self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                }
                else
                {
                    self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                    
                    print("error")
                }
                return
            }
        }
        else
        {
            let resultResponse = response.JSON["ResultResponse"].stringValue
            if( resultResponse == "0"){
                
                var regId = FIRInstanceID.instanceID().token()
                if regId == nil {
                    
                    regId = NSUserDefaults.standardUserDefaults().stringForKey("regId")
                }
                
                let appDomain = NSBundle.mainBundle().bundleIdentifier!
                
                NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "launchedBefore")
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                NSUserDefaults.standardUserDefaults().setObject(regId, forKey: "regId")
                AppDelegate.RoutToScreen("VcLogin")
                
            }else{
                
                self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                
            }
            
        }
        
    }
    
    
    //    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    //    {
    //        let cell = tableView.dequeueReusableCellWithIdentifier("MenuCell\(indexPath.row)", forIndexPath: indexPath)
    //        
    //        return cell
    //    }
    
    
    
}

