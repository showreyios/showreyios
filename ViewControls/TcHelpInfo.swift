//
//  TcHelpInfo.swift
//  ShowRey
//
//  Created by User on 1/15/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit


class TcHelpInfo: UITableViewCell {

    @IBOutlet weak var lblTitleQuestion: UILabel!
    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func cellIdentifier() -> String {

      return "Content1"
    }
}
