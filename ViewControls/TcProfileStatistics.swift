//
//  TcProfileStatistics.swift
//  ShowRey
//
//  Created by User on 12/21/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import CorePlot

class TcProfileStatistics: UITableViewCell,  CPTPieChartDataSource, CPTPieChartDelegate {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPostType: UIImageView!
    @IBOutlet weak var  hostView: CPTGraphHostingView!
    var parent:PieChartViewController!
    
    var dataForChart:[Float] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
       containerView.layer.cornerRadius = containerView.frame.size.width / 24
       containerView.clipsToBounds = true
        
       for i in 1...10 {
            
            viewWithTag(i)?.layer.cornerRadius = (viewWithTag(i)?.frame.size.width)! / 2
            viewWithTag(i)?.layer.borderWidth = 2
        
            viewWithTag(i)?.layer.borderColor = UIColor.whiteColor().CGColor
        }
        
        initPlot()
        
    }
    
    func initPlot() {
        configureHostView()
        configureGraph()
        configureChart()
        configureLegend()
    }
    
    func configureHostView() {
        
         hostView.allowPinchScaling = false
    }
    
    func configureGraph() {
      
        // 1 - Create and configure the graph
        let graph = CPTXYGraph(frame:  hostView.bounds)
         hostView.hostedGraph = graph
        graph.axisSet = nil
        
        
        
        
        // 2 - Create text style
        //        let textStyle: CPTMutableTextStyle = CPTMutableTextStyle()
        //        textStyle.color = CPTColor.blackColor()
        //        textStyle.fontName = "HelveticaNeue-Bold"
        //        textStyle.fontSize = 16.0
        //        textStyle.textAlignment = .Center
        //
        //        // 3 - Set graph title and text style
        //        graph.title = "Pie Chart"
        //        graph.titleTextStyle = textStyle
        //        graph.titlePlotAreaFrameAnchor = CPTRectAnchor.Top
    }
    
    func configureChart() {
        
        
        // 1 - Get a reference to the graph
        let graph = hostView.hostedGraph!
        
        // 2 - Create the chart
        let pieChart = CPTPieChart()
        pieChart.delegate = self
        pieChart.dataSource = self
        pieChart.pieRadius = (min(hostView.bounds.size.width, hostView.bounds.size.height) * 0.5) / 2
       // pieChart.identifier = NSString(string: graph.title!)
        pieChart.startAngle = CGFloat(M_PI_2)
        pieChart.sliceDirection = .Clockwise
        pieChart.labelOffset = -0.6 * pieChart.pieRadius
        
        // 3 - Configure border style
        let borderStyle = CPTMutableLineStyle()
        borderStyle.lineColor = CPTColor.whiteColor()
        borderStyle.lineWidth = 0.0
        pieChart.borderLineStyle = borderStyle
        
         
        // 5 - Add chart to graph
        graph.addPlot(pieChart)
        
    }
    
    func configureLegend() {
    }

    // MARK: - plot data source
    
    func numberOfRecordsForPlot(plot: CPTPlot) -> UInt {
        return UInt(self.dataForChart.count)
    }
    
    func numberForPlot(plot: CPTPlot, field fieldEnum: UInt, recordIndex idx: UInt) -> AnyObject? {
        
        
       if Int(idx) > self.dataForChart.count {
            return nil
        }
        else {
            switch CPTPieChartField(rawValue: Int(fieldEnum))! {
            case .SliceWidth:
                return (self.dataForChart)[Int(idx)] as NSNumber
                
            default:
                return idx as NSNumber
            }
        }
        
    }
    
    
    func sliceFillForPieChart(pieChart: CPTPieChart, recordIndex idx: UInt) -> CPTFill? {
        
        switch idx {
        case 0:   return CPTFill(color: CPTColor(componentRed:158/255, green:159/255, blue:163/255, alpha:1.00))
        case 1:   return CPTFill(color: CPTColor(componentRed:229/255, green:131/255, blue:58/255, alpha:1.00))
        case 2:   return CPTFill(color: CPTColor(componentRed:179/255, green:121/255, blue:231/255, alpha:1.00))
            
        default:  return nil
        }
    }
    
    func legendTitleForPieChart(pieChart: CPTPieChart, recordIndex idx: UInt) -> String? {
        return nil
    }
    

    
    
}
