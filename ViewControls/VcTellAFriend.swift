//
//  VcTellAFriend.swift
//  ShowRey
//
//  Created by User on 1/10/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit
import FBSDKShareKit
import MBProgressHUD

class VcTellAFriend: UIViewController  {

   
    @IBOutlet weak var lblPromoCode: UILabel!
    var postTxt:String = ""
    var postId:String = ""
    var linkParam:String = ""
    var postType:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let button = UIButton()
        button.frame = CGRectMake(0, 0, 21, 31)
        button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
        button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
        let barButton = UIBarButtonItem()
        barButton.customView = button
        navigationItem.leftBarButtonItem = barButton
        
        lblPromoCode.text = (User?.InviteCode)!
        
        //regularPost = 0 / fashion = 1
        if postTxt != "" {
            linkParam = "Invite=postID-\(postId)_postType-\(postType)"
        }else{
            linkParam = "Invite=postID-0_postType-0"
            postTxt = "Invite Code: \((User?.InviteCode)!)"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnInvite(sender: AnyObject) {

        getDynamicLink()
    }

    @IBAction func btnCopy(sender: AnyObject) {
        
      UIPasteboard.generalPasteboard().string = (User?.InviteCode)!
      self.view.makeToast("Copied", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))

    }
    
    
    func getDynamicLink(){
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        //"longDynamicLink" :"https://yvf65.app.goo.gl/?link=http://rw-universal-links-final.herokuapp.com?invite=\("")&ibi=com.mobileznation.ShowRey&ius=fb155466188278867&ifl=https://www.google.com.eg/?ios&apn=com.mobileznation.showrey&amv=1&afl=https://www.google.com.eg/?android"
       // "https://yvf65.app.goo.gl/?link=http://www.showrey.com?invite=\("")&ibi=com.mobileznation.ShowRey&ius=fb155466188278867&ifl=http://www.showrey.com&apn=com.mobileznation.showrey&amv=1&afl=http://www.showrey.com"
        
       
        
        let RequestParameters : NSDictionary = [
            "longDynamicLink": "https://yvf65.app.goo.gl/?link=http://cms.mobileznation.com/ShowreyApp/LandingPage?\(linkParam)&ibi=com.mobileznation.ShowRey&ius=fb155466188278867&ifl=\((AppConfiguration?.AboutCompany.Website)!)&apn=com.mobileznation.showrey&amv=1&afl=\((AppConfiguration?.AboutCompany.Website)!)"
        ]
        
        var FinalDictionaryString = ""
        
        let data = try! NSJSONSerialization.dataWithJSONObject(RequestParameters, options: [])
        FinalDictionaryString = (NSString(data: data, encoding: NSUTF8StringEncoding) as? String)!
        
        NetworkHelper.RequestHelper("https://firebasedynamiclinks.googleapis.com", service: "/v1/shortLinks?key=AIzaSyBeRStwuMghujfa8NlUn2ALQErh2pUfBoA", hTTPMethod: Method.post, parameters: nil, httpBody: FinalDictionaryString
            , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: { (JSON, NSError) in
                
                 MBProgressHUD.hideHUDForView(self.view, animated: true)
                if((NSError) != nil)
                {
                    if let networkError = NSError {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    
                    
                    if (!JSON["shortLink"])
                    {
                        let shortLink =   JSON["shortLink"].rawString()
                        
                        
                        // 1
                        let optionMenu = UIAlertController(title: "Invite Via", message: nil, preferredStyle: .ActionSheet)
                        
                        // 2
                        let face = UIAlertAction(title: "Facebook", style: .Default, handler: {
                            (alert: UIAlertAction!) -> Void in
                            
                            
                            let content = FBSDKShareLinkContent()
                            
                            content.contentURL = NSURL(string: shortLink!)
                            content.contentTitle = (AppConfiguration?.ConfigString.TellAFriend)!
                           // content.imageURL = NSURL(string: (AppConfiguration?.AboutCompany.Logo)!)
                            content.contentDescription = self.postTxt
                                
                            
                            let dialog = FBSDKShareDialog()
                            dialog.fromViewController = self
                            dialog.shareContent = content
                            dialog.mode = FBSDKShareDialogMode.Native
                            
                            if (!dialog.canShow()) {
                                // fallback presentation when there is no FB app
                                dialog.mode = FBSDKShareDialogMode.FeedBrowser
                            }
                            dialog.show()
                            
                            
                        })
                        
                        let other = UIAlertAction(title: "Other", style: .Default, handler: {
                            (alert: UIAlertAction!) -> Void in
                            
                            var activityItems: [AnyObject]?
                              
                            activityItems = ["\(AppConfiguration?.ConfigString.TellAFriend as! AnyObject)\n\(self.postTxt)", NSURL(string: shortLink!)!]
                         
                            let activityController = UIActivityViewController(activityItems:
                                activityItems!, applicationActivities: nil)
                            activityController.excludedActivityTypes = [UIActivityTypePostToFacebook]
                            self.presentViewController(activityController, animated: true, completion: nil)
                            
                        })
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
                            (alert: UIAlertAction!) -> Void in
                        })
                        
                        
                        // 4
                        optionMenu.addAction(face)
                        optionMenu.addAction(other)
                        optionMenu.addAction(cancelAction)
                        
                        // 5
                        self.presentViewController(optionMenu, animated: true, completion: nil)
                        
                    }else {
                        
                        print("Error")
                    }
                }
                
                
        })
    }
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
