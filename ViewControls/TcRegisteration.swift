//
//  TcRegisteration.swift
//  ShowRey
//
//  Created by Appsinnovate on 2/7/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit
import EGFloatingTextField
import NVActivityIndicatorView
import Alamofire
import MBProgressHUD
import Toast_Swift
import SwiftyJSON
import PureLayout

class TcRegisteration: UITableViewController ,UITextFieldDelegate,CountryListViewDelegate {
    
    @IBOutlet weak var TxtUserName: EGFloatingTextField!
    @IBOutlet weak var TxtFullName: EGFloatingTextField!
    @IBOutlet weak var TxtEmail: EGFloatingTextField!
    @IBOutlet weak var TxtMobileNumber: EGFloatingTextField!
    @IBOutlet weak var TxtInviteCode: EGFloatingTextField!
    @IBOutlet weak var IndicatorUserName: NVActivityIndicatorView!
    @IBOutlet weak var IndicatorEmail: NVActivityIndicatorView!
    @IBOutlet weak var mobileNumberIndicator: NVActivityIndicatorView!
    
    @IBOutlet weak var LblErrorUser: UILabel!
    @IBOutlet weak var LblErrorFull: UILabel!
    @IBOutlet weak var LblErrorEmail: UILabel!
    @IBOutlet weak var LblErrorInviteCode: UILabel!
    @IBOutlet weak var LblErrorMobileNumber: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    var instagramProfile : SLInstagramUser!
    
    var FacebookName = "";
    var FacebookEmail = "";
    var FacebookID = "";
    var FacebookImageUrl = "";
    
    override func viewDidAppear(animated: Bool) {
        
        TxtUserName.setPlaceHolder(" User Name")
        TxtFullName.setPlaceHolder(" Full Name")
        TxtMobileNumber.setPlaceHolder(" Mobile Number")
        TxtInviteCode.setPlaceHolder(" Invite Code (optional)")
        TxtEmail.setPlaceHolder(" Email Address (optional)")
        
        if (self.TxtUserName.text != ""){
            self.UserNameEndEdit(self.TxtUserName);
        }
        
        if (self.TxtMobileNumber.text != ""){
            
            //AA : To remove the first zero if its zero
            if (TxtMobileNumber.text?.hasPrefix("0"))! {
                TxtMobileNumber.text?.removeAtIndex((TxtMobileNumber.text?.startIndex)!)
            }
            
            if (self.countryCodeLabel.text?.hasPrefix("+"))! {
                self.countryCodeLabel.text?.removeAtIndex((self.countryCodeLabel.text?.startIndex)!)
            }
            
            self.mobileNumberValidator(TxtMobileNumber.text!)
        }
        
        
        /*
         if (self.TxtEmail.text != ""){
         self.EmailEndedit(self.TxtEmail);
         }
         */
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Registration"
        
        TxtUserName.floatingLabel = true
        // TxtUserName.text = "radwa5"
        TxtUserName.validationType = .Empty
        TxtUserName.errorLabel = LblErrorUser
        //  TxtUserName.customerrorMessage = "Enter username"
        
        
        
        
        TxtFullName.floatingLabel = true
        TxtFullName.validationType = .OnlyText
        TxtFullName.errorLabel = LblErrorFull
        
        TxtEmail.floatingLabel = true
        //    TxtEmail.text = "Mina.fared@appsinnovate.com"
        TxtEmail.validationType = .Email
        TxtEmail.errorLabel = LblErrorEmail
        // TxtEmail.customerrorMessage = "Enter a valid email address"
        
        
        TxtMobileNumber.floatingLabel = true
        TxtMobileNumber.validationType = .Phone
        TxtMobileNumber.errorLabel = LblErrorMobileNumber
        // TxtMobileNumber.customerrorMessage = "Phone number should be less than 15 charachter"
        
        TxtInviteCode.floatingLabel = true
        
        // Do any additional setup after loading the view.
        if(instagramProfile != nil)
        {
            TxtUserName.text = instagramProfile.username
            TxtFullName.text = instagramProfile.fullName
        }
        else if (FacebookID != "")
        {
            TxtEmail.text = FacebookEmail
            TxtFullName.text = FacebookName
            
        }
        else
        {
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if (textField.tag == 10 ) //userName
        {
            if (string == " " || string == "." || string == "@" || string == "#") {
                return false
            }
            return true
        }
        else
        {
            return true
        }
    }
    
    @IBAction func UserNameEndEdit(sender: AnyObject) {
        
        if ( sender.text == ""){return}
        
        IndicatorUserName.startAnimating()
        
        
        let RequestParameters : NSDictionary = [
            "MobileNumberorUserName" : TxtUserName.text!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.NickNameValidator, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString:nil , callbackDictionary:
            {response in
                
                self.IndicatorUserName.stopAnimating()
                
                if((response.NSError) != nil)
                {
                    
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                        }
                        return
                    }
                }
                else{
                    if (response.JSON["ResultResponse"].stringValue != "0")
                    {
                        self.LblErrorUser.text = response.JSON["Description"].string
                        self.TxtUserName.hasError = true
                        self.LblErrorUser.hidden = false
                    }
                    else{
                        //       self.TxtUserName.hasError = false
                    }
                    
                    
                }
                
            }
        )
    }
    
    
    @IBAction func mobileDidEndEditing(sender: AnyObject) {
        
        if ( TxtMobileNumber.text == ""){return}
        
        //AA : To remove the first zero if its zero
        if (TxtMobileNumber.text?.hasPrefix("0"))! {
            TxtMobileNumber.text?.removeAtIndex((TxtMobileNumber.text?.startIndex)!)
        }
        
        if (self.countryCodeLabel.text?.hasPrefix("+"))! {
            self.countryCodeLabel.text?.removeAtIndex((self.countryCodeLabel.text?.startIndex)!)
        }
        
        //AA : To check mobile number if valid
        mobileNumberValidator(countryCodeLabel.text! + TxtMobileNumber.text!)
        
    }
    
    @IBAction func EmailEndedit(sender: AnyObject) {
        
        /*
         if ( sender.text == ""){return}
         IndicatorEmail.startAnimating()
         
         
         let RequestParameters : NSDictionary = [
         "MobileNumberorUserName" : TxtEmail.text!
         ]
         
         let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
         
         NetworkHelper.RequestHelper(nil, service: WSMethods.EmailValidator, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
         , responseType: ResponseType.DictionaryJson , callbackString:nil , callbackDictionary:
         {response in
         
         self.IndicatorEmail.stopAnimating()
         
         if((response.NSError) != nil)
         {
         
         if let networkError = response.NSError {
         if (networkError.code == -1009) {
         self.view.makeToast("No Internet connection")
         
         }
         else
         {
         self.view.makeToast("An error has been occurred")
         }
         return
         }
         }
         else{
         if (response.JSON["ResultResponse"].stringValue != "0")
         {
         self.LblErrorEmail.text = response.JSON["Description"].string
         self.TxtEmail.hasError = true
         self.LblErrorEmail.hidden = false
         }
         else{
         //  self.TxtEmail.hasError = false
         }
         
         
         }
         
         }
         )
         */
        
    }
    
    func mobileNumberValidator(mobileNumber:String) {
        
        mobileNumberIndicator.startAnimating()
        
        
        let RequestParameters : NSDictionary = [
            "MobileNumber" : mobileNumber
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.MobileNumberValidator, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString:nil , callbackDictionary:
            {response in
                
                self.mobileNumberIndicator.stopAnimating()
                
                print(response)
                if((response.NSError) != nil)
                {
                    
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                        }
                        return
                    }
                }
                else{
                    if (response.JSON["ResultResponse"].stringValue != "0")
                    {
                        
                        self.LblErrorMobileNumber.text = response.JSON["Description"].string
                        self.TxtMobileNumber.hasError = true
                        self.LblErrorMobileNumber.hidden = false
                        
                    }
                    else{
                        
                        self.TxtMobileNumber.hasError = false
                        self.LblErrorMobileNumber.hidden = true
                        
                    }
                    
                    
                }
                
            }
        )
    }
    
    @IBAction func termsAndCond(sender: AnyObject) {
        
        let view = self.storyboard?.instantiateViewControllerWithIdentifier("VcTerms") as! VcTerms
        self.navigationController?.pushViewController(view, animated: true)
        view.terms = 2
    }
    
    func sendPinCode(target:UIViewController,mobileNubmer:String)
    {
        
        // login
        let RequestParameters : NSDictionary = [
            "MobileNumber" : mobileNubmer
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(target.view, animated: true)
        loadingNotification.label.text = "Logging in"
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.SendPinCode, hTTPMethod: .post, parameters: nil, httpBody: RequestParametersString, responseType: .DictionaryJson, callbackString: nil, callbackDictionary: { (JSON, NSError) in
            MBProgressHUD.hideHUDForView(target.view, animated: true)
            if(NSError != nil)
            {
                if (NSError!.code == -1009)
                {
                    target.view.makeToast("No Internet connection")
                }
                else
                {
                    target.view.makeToast("An error has been occurred")
                    print("error")
                }
            }
            else
            {
                
                let ResultResponse = JSON["ResultResponse"].stringValue
                
                print(ResultResponse)
                
                //  VcLogin.LastLoginType = .Instagram
                if (ResultResponse == "0" || ResultResponse == "1")
                {
                    
                    //AA : Go to pin code screen
                    let pinCodeScreen = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcPinCode") as! PinCodeViewController
                    pinCodeScreen.passedEmail = self.TxtEmail.text ?? ""
                    pinCodeScreen.passedFullName = self.TxtFullName.text ?? ""
                    pinCodeScreen.passedUserName = self.TxtUserName.text ?? ""
                    pinCodeScreen.passedInvitationCode = self.TxtInviteCode.text ?? ""
                    pinCodeScreen.passedPhoneNumber = self.countryCodeLabel.text! + self.TxtMobileNumber.text!
                    
                    target.navigationController?.pushViewController(pinCodeScreen, animated: true)
                    
                }
                else if (ResultResponse == "2")
                {
                    //   invalid user or pass
                    target.view.makeToast("Invalid username or password")
                }
                else
                {
                    target.view.makeToast("Somthing went wrong. try again")
                }
            }
            
        })
    }
    
    func callRegisterApi(pinCode:String) {
        
        var RequestParameters: [String:String] = [:]  //= NSDictionary()
        var wsmethod = WSMethods.Registration
        
        if(instagramProfile == nil && FacebookID == "")
        {
            RequestParameters = [
                "City":"",
                "Country":"",
                "Email":"\(TxtEmail.text!)",
                "FullName":"\(TxtFullName.text ?? "")",
                "MobileNumber":countryCodeLabel.text! + TxtMobileNumber.text!,
                "PinCode":"\(pinCode)",
                "RegId":"",
                "Username":"\(TxtUserName.text ?? "")",
                "InviteCode":"\(TxtInviteCode.text ?? "")"
            ]
        }
        else
        {
            wsmethod = WSMethods.RegistrationSocial
            //RequestParameters = [];
            
            RequestParameters["RegType"] = (instagramProfile != nil ? "3" : "2")
            RequestParameters["SMUserId"] = (instagramProfile != nil ? instagramProfile!.ID!: FacebookID)
            RequestParameters["City"] = ""
            RequestParameters["Country"] = ""
            RequestParameters["Email"] = "\(TxtEmail.text ?? "")"
            RequestParameters["FullName"] = "\(TxtFullName.text ?? "")"
            RequestParameters["MobileNumber"] = "\(TxtMobileNumber.text!)"
            RequestParameters["ProfilePicture"] = (instagramProfile != nil ? (instagramProfile.profilePictureURL ?? "") : FacebookImageUrl)
            RequestParameters["RegId"] = ""
            RequestParameters["Username"] = "\(TxtUserName.text ?? "")"
            RequestParameters["InviteCode"] = "\(TxtInviteCode.text ?? "")"
            
        }
        
        
        
        
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: wsmethod, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString , responseType: ResponseType.DictionaryJson
            , callbackString: nil, callbackDictionary: jsonCallBack)
        
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
    }
    
    @IBAction func BtnRegister(sender: AnyObject) {
        
        /*
         var RequestParameters: [String:String] = [:]  //= NSDictionary()
         var wsmethod = WSMethods.Registration
         
         if(instagramProfile == nil && FacebookID == "")
         {
         RequestParameters = [
         "City":"",
         "Country":"",
         "Email":"\(TxtEmail.text!)",
         "FullName":"\(TxtFullName.text ?? "")",
         "MobileNumber":"\(TxtMobileNumber.text!)",
         "RegId":"",
         "Username":"\(TxtUserName.text ?? "")",
         "InviteCode":"\(TxtInviteCode.text ?? "")"
         ]
         }
         else
         {
         wsmethod = WSMethods.RegistrationSocial
         //RequestParameters = [];
         
         RequestParameters["RegType"] = (instagramProfile != nil ? "3" : "2")
         RequestParameters["SMUserId"] = (instagramProfile != nil ? instagramProfile!.ID!: FacebookID)
         RequestParameters["City"] = ""
         RequestParameters["Country"] = ""
         RequestParameters["Email"] = "\(TxtEmail.text ?? "")"
         RequestParameters["FullName"] = "\(TxtFullName.text ?? "")"
         RequestParameters["MobileNumber"] = "\(TxtMobileNumber.text!)"
         RequestParameters["ProfilePicture"] = (instagramProfile != nil ? (instagramProfile.profilePictureURL ?? "") : FacebookImageUrl)
         RequestParameters["RegId"] = ""
         RequestParameters["Username"] = "\(TxtUserName.text ?? "")"
         RequestParameters["InviteCode"] = "\(TxtInviteCode.text ?? "")"
         
         }
         
         
         
         
         
         let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
         
         NetworkHelper.RequestHelper(nil, service: wsmethod, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString , responseType: ResponseType.DictionaryJson
         , callbackString: nil, callbackDictionary: jsonCallBack)
         
         
         let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
         loadingNotification.label.text = "Loading..."
         */
        
        for subview in self.view.subviews {
            
            if subview.tag == 2 && subview.hidden
            {
                
                continue
                
            }
            if let textField = subview as? EGFloatingTextField {
                
                textField.resignFirstResponder()
                
                if textField.hasError == true
                {
                    return
                }
            }
            
        }
        
        if LblErrorUser.text != "" || TxtUserName.text == "" {
            if TxtUserName.text == "" {
                self.view.makeToast("Please enter User Name")
                
                self.LblErrorUser.text = "Please enter User Name"
                self.TxtUserName.hasError = true
                self.LblErrorUser.hidden = false
                
            }else {
                self.view.makeToast(LblErrorUser.text!)
            }
            return
        }else if LblErrorFull.text != "" || TxtFullName.text == "" {
            if TxtFullName.text == "" {
                
                self.LblErrorFull.text = "Please enter Full Name"
                self.TxtFullName.hasError = true
                self.LblErrorFull.hidden = false
                
            }else {
                self.view.makeToast(LblErrorFull.text!)
            }
            return
        }else if LblErrorMobileNumber.text != "" || TxtMobileNumber.text == "" {
            if TxtMobileNumber.text == "" {
                
                self.LblErrorMobileNumber.text = "Please enter Mobile Number"
                self.TxtMobileNumber.hasError = true
                self.LblErrorMobileNumber.hidden = false
                
            }else {
                self.view.makeToast(LblErrorMobileNumber.text!)
            }
            return
        }
        
        //AA : To remove the first zero if its zero
        if (TxtMobileNumber.text?.hasPrefix("0"))! {
            TxtMobileNumber.text?.removeAtIndex((TxtMobileNumber.text?.startIndex)!)
        }
        
        if (self.countryCodeLabel.text?.hasPrefix("+"))! {
            self.countryCodeLabel.text?.removeAtIndex((self.countryCodeLabel.text?.startIndex)!)
        }
        
        sendPinCode(self, mobileNubmer: countryCodeLabel.text! + TxtMobileNumber.text!)
        
    }
    
    
    func jsonCallBack(response: (JSON: JSON, NSError: NSError?))
    {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    self.view.makeToast("No Internet connection")
                }
                else
                {
                    self.view.makeToast("An error has been occurred")
                    print("error")
                }
                return
            }
        }
        else
        {
            
            let UserId = response.JSON["UserId"].stringValue;
            AppDelegate.RoutToScreen("VcLogin")
            
        }
        
    }
    
    // MARK: - Actions
    
    @IBAction func selectCountryCodeAction(sender: AnyObject) {
        
        let countryListVc = CountryListViewController(nibName: "CountryListViewController", delegate: self)
        self.presentViewController(countryListVc, animated: true, completion: nil)
        
    }
    
    // MARK: - CountryListViewDelegate Methods
    
    func didSelectCountry(country: [NSObject : AnyObject]!) {
        
        //AA : To update country code label
        countryCodeLabel.text = country["dial_code"] as? String ?? ""
        
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
