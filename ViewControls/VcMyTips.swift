//
//  VcMyTips.swift
//  ShowRey
//
//  Created by M-Hashem on 11/20/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD
import FirebaseAnalytics

class VcMyTips: UIViewController ,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var DateTitle: UILabel!
    @IBOutlet weak var TipsTable: UITableView!
    
    @IBOutlet weak var tipTitle: UILabel!
    @IBOutlet weak var tipDetails: UITextView!
   
    var tipsData = [ModTip]()
    var PagesCount = 1
    var selectedCellIndex = -1;
    
    var InitialTip:ModTip?
    
    override func viewDidLoad()
    {
        TipsTable.registerClass(TcTip.self, forCellReuseIdentifier: "TcTip")
        TipsTable.registerNib(UINib(nibName: "TcTip",bundle: nil), forCellReuseIdentifier: "TcTip")
        
        super.viewDidLoad()
        TipsTable.dataSource = self
        TipsTable.delegate = self
        
        
        navigationItem.title = "Tips"
        
        if InitialTip == nil
        {
            let button = UIButton()
            button.frame = CGRectMake(0, 0, 21, 31)
            button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
            button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
            let barButton = UIBarButtonItem()
            barButton.customView = button
            navigationItem.leftBarButtonItem = barButton
        }
        
        TipsTable.addInfiniteScrollWithHandler { (UITableView) in
            self.LoadTips(self.PagesCount + 1, fromRefresher: true);
            UITableView.reloadData()
        }
        
        LoadTips(PagesCount)
        if InitialTip != nil
        {
            FillTip(InitialTip!)
        }
        //        else
        //        {
        //
        //            tipsData.append(InitialTip!)
        //            selectedCellIndex = 0
        //        }
        
        FIRAnalytics.logEventWithName("MyTips", parameters: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tipsData.count
//        return InitialTip == nil ? tipsData.count : 1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TcTip") as! TcTip
        cell.setData(tipsData[indexPath.row])
        
        if selectedCellIndex == indexPath.row
        {
//            cell.setSelected(true, animated: false)
//            cell.selected = true
            
            let indexpath = NSIndexPath(forRow: selectedCellIndex,inSection: 0)
            self.TipsTable.selectRowAtIndexPath(indexpath, animated: false, scrollPosition: .Middle)
            self.tableView(self.TipsTable, didSelectRowAtIndexPath: indexpath)
        }
        else
        {
            if indexPath.row % 2 == 0
            {
                
                cell.backgroundColor = UIColor(hex: "#f7f7f7")
                
            }
            else
            {
                cell.backgroundColor = UIColor.whiteColor()
            }
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       // let cell = tableView.cellForRowAtIndexPath(indexPath) as! TcTip
        let tip = tipsData[indexPath.row]
        FillTip(tip)
        
        selectedCellIndex = indexPath.row
    }
    func FillTip(tip:ModTip)
    {
        tipTitle.text = tip.Title
        tipDetails.text = tip.Text
        DateTitle.text = tip.Date
    }
    
    func LoadTips(index:Int,fromRefresher:Bool = false)
    {
        if !fromRefresher
        {
            MBProgressHUD.showHUDAddedTo(self.view, animated: true).label.text = "Loading..."
        }
        
        let RequestParameters:NSDictionary =
            [
                "PageIndex" : index,
                "PageSize" : 10,
                "ProfileUserId" : User!.Id!,
                "UserId" : User!.Id!,
            ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetMyTips, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                self.TipsTable.finishInfiniteScroll()
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        
                        // print("JSON: \(JSON)")
                        let Response = ModTipsResponse(json:JSON)
                        //test
//                        let t = ModTip(); t.Text = "adsaASsd SAD s fEF we"
//                        t.Title = "tip title"; t.Date = "5/6/2010"
//                        Response.Tips?.append(t)
//                        let t2 = ModTip(); t2.Text = "dmveapfmpkawe sdwf sdnifaisfpwehhf wff wff jkWDjgkwfkj jwfkjwbg kweedfc"
//                        t2.Title = "tip title 2"; t2.Date = "9/6/2010"
//                        Response.Tips?.append(t2)
//                        
//                        self.tipsData.appendContentsOf(Response.Tips!)
//                        self.TipsTable.reloadData()
                        //
                        
                        if (Response.ResultResponse != "0")
                        {
                            if self.PagesCount == 1
                            { self.view.makeToast("No Content")}
                        }
                        else
                        {
                            
                            self.tipsData.appendContentsOf(Response.Tips!)
                            
                            if(Response.ListCount?.intValue > 0)
                            {self.PagesCount += 1}
                            
//                          let ci = self.TipsTable.indexPathForSelectedRow
                            
                            self.TipsTable.reloadData()
                            
                            if self.PagesCount == 2 && self.InitialTip == nil
                            {
                                
                                let indexpath = NSIndexPath(forRow: 0,inSection: 0)
                                self.TipsTable.selectRowAtIndexPath(indexpath, animated: false, scrollPosition: .Middle)
                                self.tableView(self.TipsTable, didSelectRowAtIndexPath: indexpath)
                            }

//                            if self.selectedCellIndex >= 0
//                            {
//                                self.TipsTable.selectRowAtIndexPath(ci, animated: true, scrollPosition: .None)
//                            }
                        }
                    }
                }
            }
            , callbackDictionary: nil)
        
        
    }
}
