//
//  VcTest2.swift
//  ShowRey
//
//  Created by User on 10/31/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import SSImageBrowser
import Social
import MobileCoreServices
import AVKit
import AVFoundation

class VcTest2: UIViewController, SSImageBrowserDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var textPost: UITextView!
    @IBOutlet weak var photoPost: UIImageView!
    var mediaPicker: UIImagePickerController!
   // var images : [UIImage] = []
    var videoURL: NSURL? = nil
    var videoData: NSData!
    var VideoThumb : UIImage!
    var avPlayer: AVPlayerViewController = AVPlayerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addChildViewController(avPlayer)
        self.view.addSubview(avPlayer.view)
        
        
        //# MARK:- Init avPlayer
        
        avPlayer.view.frame =  CGRect(x: 20 , y: 180 , width: self.view.frame.size.width - 40 , height: 54 * 5)
        avPlayer.view.hidden = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func pick(sender: AnyObject) {
        
        avPlayer.player?.pause()
        
        
        // 1
        let optionMenu = UIAlertController(title: "Select Madia", message: nil, preferredStyle: .ActionSheet)
        
        // 2
        let PhotoGallery = UIAlertAction(title: "Photo Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
           // self.images.removeAll()
   
            
            if (self.videoURL != nil){
                self.videoURL = nil
                self.avPlayer.view.hidden = true
                
            }
            
            
            else{
                
                self.mediaPicker =  UIImagePickerController()
                self.mediaPicker.delegate = self
                self.mediaPicker.sourceType = .PhotoLibrary
                
                
                self.presentViewController(self.mediaPicker, animated: true, completion: nil)
            }
            
            
        })
        let PhotoCamera = UIAlertAction(title: "Photo Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
          //  self.images.removeAll()
            if (self.videoURL != nil){
                self.videoURL = nil
                self.avPlayer.view.hidden = true
            }
            self.mediaPicker =  UIImagePickerController()
            self.mediaPicker.delegate = self
            self.mediaPicker.sourceType = .Camera
            
            
            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
            
            
        })
        let VideoGallery = UIAlertAction(title: "Video Gallery", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
           // self.images.removeAll()

            if (self.videoURL != nil){
                self.videoURL = nil
                self.avPlayer.view.hidden = true
                
            }
            self.mediaPicker =  UIImagePickerController()
            self.mediaPicker.sourceType = .PhotoLibrary
            self.mediaPicker.delegate = self
            self.mediaPicker.mediaTypes = ["public.movie"] //"public.image",
            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
            
            
        })
        let VideoCamera = UIAlertAction(title: "Video Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
           // self.images.removeAll()
            if (self.videoURL != nil){
                self.videoURL = nil
                self.avPlayer.view.hidden = true
            }
            self.mediaPicker =  UIImagePickerController()
            self.mediaPicker.sourceType = .Camera
            self.mediaPicker.delegate = self
            self.mediaPicker.mediaTypes = [kUTTypeMovie as String]
            //self.mediaPicker.mediaTypes = ["public.movie"] //"public.image",
            self.presentViewController(self.mediaPicker, animated: true, completion: nil)
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(PhotoGallery)
        optionMenu.addAction(PhotoCamera)
                   optionMenu.addAction(VideoGallery)
            optionMenu.addAction(VideoCamera)
        
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    // in case of camera (I or V)  or video gallery
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        
        mediaPicker.dismissViewControllerAnimated(true, completion: nil)
        
        
        if (info[UIImagePickerControllerOriginalImage] as? UIImage) != nil  //photo
        {
            
            
            /*
             let cropViewController:TOCropViewController = TOCropViewController(image: (info[UIImagePickerControllerOriginalImage] as? UIImage)!)
             cropViewController.delegate = self;
             self.presentViewController(cropViewController, animated: true, completion: nil)
             */
            
           // images.append((info[UIImagePickerControllerOriginalImage] as? UIImage)!)
           
            photoPost.image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            
            
        }
        else // video
        {
            
            //# MARK: -  add video
            self.avPlayer.view.hidden = false
            
            videoURL = NSURL()
            videoURL = info["UIImagePickerControllerReferenceURL"] as? NSURL  // gallery
            videoURL = info["UIImagePickerControllerMediaURL"] as? NSURL // camera
            
            
            videoData = NSData(contentsOfURL: videoURL!)
            
            
            let player = AVPlayer(URL: videoURL! as  NSURL)
            
            
            // generating thumbnail
            do {
                let asset = AVURLAsset(URL: videoURL!, options: nil)
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImageAtTime(CMTimeMake(0, 1), actualTime: nil)
                VideoThumb = UIImage(CGImage: cgImage)
                // lay out this image view, or if it already exists, set its image property to uiImage
            } catch let error as NSError {
                print("Error generating thumbnail: \(error)")
            }
            
            avPlayer.player = player
            
            
            
            
            
        }
    }

    
    
    @IBAction func sharePost(sender: AnyObject) {

        
        var activityItems: [AnyObject]?
        
        if (photoPost.image != nil) {
            activityItems = [textPost.text as AnyObject, photoPost.image!]
        } else if videoURL != nil {
            activityItems = [textPost.text as AnyObject, videoURL! ]
        }else {
            
            activityItems = [textPost.text as AnyObject]
        }
        
      
        let activityController = UIActivityViewController(activityItems:
            activityItems!, applicationActivities: nil)
        self.presentViewController(activityController, animated: true, completion: nil)
        
        
    }
    
    
    
    @IBAction func imageViewer(sender: AnyObject) {
        
        // Create an array to store IDMPhoto objects
        var photos = [SSPhoto]()
        var photo: SSPhoto!
        
        
        photo = SSPhoto(image: UIImage(named: "back")!) //SSPhoto(filePath: NSBundle.mainBundle().pathForResource("back", ofType: "jpeg")!)
        photo.aCaption = "Grotto of the Madonna"
        photos.append(photo)
        
        photo = SSPhoto(image: UIImage(named: "NoImageAvailable")!)//SSPhoto(filePath: NSBundle.mainBundle().pathForResource("NoImageAvailable", ofType: "png")!)
        photo.aCaption = "York Floods"
        photos.append(photo)
        //
        //        photo = SSPhoto(filePath: NSBundle.mainBundle().pathForResource("photo2l", ofType: "jpg")!)
        //        photo.aCaption = "The London Eye is a giant Ferris wheel situated on the banks of the River Thames, in London, England."
        //        photos.append(photo)
        //
        //        photo = SSPhoto(filePath: NSBundle.mainBundle().pathForResource("photo4l", ofType: "jpg")!)
        //        photo.aCaption = "Campervan"
        //        photos.append(photo)
        //
        //
        //            photo = SSPhoto(filePath: NSBundle.mainBundle().pathForResource("photo1l", ofType: "jpg")!)
        //            photo.aCaption = "Grotto of the Madonna"
        //            photos.append(photo)
        //
        
        // Create and setup browser
        let browser = SSImageBrowser(aPhotos: photos, animatedFromView: sender as! UIView) // using initWithPhotos:animatedFromView: method to use the zoom-in animation
        browser.delegate = self
        browser.displayActionButton = true
        browser.displayArrowButton = true
        browser.displayCounterLabel = true
        browser.usePopAnimation = true
        browser.scaleImage = sender.currentImage
        
        //browser.useWhiteBackgroundColor = true
        
        
        // Show
        self.presentViewController(browser, animated: true, completion: nil)
    }
   

        // hellow iam radwa
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
