//
//  NetworkHelper.swift
//  Triage
//
//  Created by Mohamed Helmy on 4/13/16.
//  Copyright © 2016 appsinnovate. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum Method : String{
    case get="GET"
    case post="POST"
    
}

enum ResponseType : Int{
    case StringJson = 0
    case DictionaryJson = 1
    
}
class NetworkHelper {
    
    internal static func RequestHelper(domainusrl:String?,service:String , hTTPMethod: Method,parameters: [String:AnyObject]?, httpBody:String?, httpBodyData:NSData? = nil
        ,responseType: ResponseType = .StringJson, callbackString: (Response<String, NSError> -> Void)? , callbackDictionary: ((JSON: JSON, NSError: NSError?) -> Void)?)
    {
        let UrlString = (domainusrl == nil ? AppDelegate.domainurl : domainusrl!)+service;
        let url = NSURL(string: UrlString)!
        var request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = hTTPMethod.rawValue
        
        var formdata = ""
        print("request url >>>> "+UrlString)
        print("Request Body >>>>>>"+(httpBody == nil ? (parameters == nil ? "" : String(parameters!)) : httpBody!))
        
        var HttpBodyString = httpBody == nil ? "" : httpBody!
        
        
        if (httpBodyData == nil)
        {
            if(hTTPMethod == .post){
                var contenttype = "application/json"
                
                if let params = parameters
                {
                    for param in params
                    {
                        formdata += "\(param.0)=\(param.1)&"
                    }
                    formdata = String(formdata.characters.dropLast())
                    
                    contenttype = "application/x-www-form-urlencoded"
                    HttpBodyString = formdata
                }
                
                request.setValue(contenttype, forHTTPHeaderField: "Content-Type")
            }
            else if (parameters != nil) // get
            {
                request = ParameterEncoding.URL.encode(request, parameters: parameters).0
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            
            let data = HttpBodyString.dataUsingEncoding(NSUTF8StringEncoding)
            
            request.HTTPBody = data
        }
        else{
            request.HTTPBody = httpBodyData
        }
        
        
        var webservices : Request?
        
        // if (responseType == .StringJson)
        //   {
        webservices = Alamofire.request(request).responseString
            { response in
                
                print("Network code for StringJson  ..... \(response.response?.statusCode)")
                //   print("network code ..... \(webservices)")
                if (responseType == .StringJson)
                {
                    callbackString!(response)
                }
                else //Dictionary
                {
                    if(response.result.isSuccess)
                    {
                        callbackDictionary!(JSON:JSON.parse(response.result.value!), NSError:  response.result.error)
                    }
                    else
                    {
                        callbackDictionary!(JSON: nil, NSError:  response.result.error)
                    }
                }
        }
        
        //        }
        //        else
        //        {
        //
        //            webservices =  Alamofire.request(request)
        //
        //                .responseData { response in
        //                    //                    let json = JSON(data: response.data!)
        //                    //                    callbackDictionary(json)
        //                    switch response.result {
        //                    case .Success:
        //                        if let value = response.result.value {
        //                            let json = JSON(value)
        //                            print("Network code for DictionaryJson  ..... \(json)")
        //                            callbackDictionary!(json,response.result.error)
        //                        }
        //                    case .Failure(let error):
        //                        print(error)
        //                    }
        //            }
        //        }
        // print(webservices);
        
    }
    
}//classs
