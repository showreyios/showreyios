//
//  BaseModel.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 9/24/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import UIKit

class OauthInjector  {
    
    static var baseParameters :NSMutableDictionary!
    static var DeviceInfo :NSMutableDictionary!
    
    internal static func InitBaseDictionary()
    {
        
        DeviceInfo = [
            "Platform" : "iOS",
            "Resolution" : "\(UIScreen.mainScreen().bounds.width)X\(UIScreen.mainScreen().bounds.height)"  ,
            "Version" : NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"]!
        ]
        baseParameters = [
            "WSUsername":"ShowreyApp",
            "WSPassword":"$h0Reu@Pb16",
            "device" : DeviceInfo
        ]
        
    }
    
    internal static func UpdateDictionary(InjectedDictionary : NSDictionary) ->  String
    {
        let FinalDictionary : NSMutableDictionary = NSMutableDictionary(dictionary: baseParameters);//=
        FinalDictionary.addEntriesFromDictionary(InjectedDictionary as [NSObject : AnyObject])
        
        print (FinalDictionary);
        
        var FinalDictionaryString = ""
        
        // let data = try! NSJSONSerialization.dataWithJSONObject(FinalDictionary, options: NSJSONWritingOptions())
        
        let data = try! NSJSONSerialization.dataWithJSONObject(FinalDictionary, options: [])
        FinalDictionaryString = (NSString(data: data, encoding: NSUTF8StringEncoding) as? String)!
//        FinalDictionaryString = FinalDictionaryString.stringByReplacingOccurrencesOfString("\\n", withString: "")
//        FinalDictionaryString = FinalDictionaryString.stringByReplacingOccurrencesOfString("\\", withString: "")
        print("--converted jason result >>> "+FinalDictionaryString)//.stringByReplacingOccurrencesOfString("\\", withString: "")
        
        return FinalDictionaryString
    }
    
    
    
}

