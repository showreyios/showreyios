//
//  VedioThumbnail.swift
//  ShowRey
//
//  Created by M-Hashem on 11/1/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import  AVFoundation

class VedioThumbnail {
    
    static func videoViewWithAttachedFile(file:ModFeedAttachedFiles?,frame:CGRect,refreshBlock:()->()) -> UIView {
       let containerView = UIView(frame: frame)
        if let file = file{
        
        if((file.thumbImage) != nil)
        {
            VedioThumbnail.addVideoViewWithImage(file.thumbImage!, view:containerView);
        }
        else if(file.Thumb != nil) //&& file.Thumb != "")
        {
           VedioThumbnail.addVideoViewWithImage(nil, view:containerView,imgURL: file.Thumb!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!);
        }
        else
        {
           
            let URL=NSURL(string: file.URL!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
            let asset = AVURLAsset(URL: URL!)
            let generator = AVAssetImageGenerator(asset: asset)
            generator.appliesPreferredTrackTransform=true;
            let thumbTime:CMTime = CMTimeMakeWithSeconds(0,5);
            var tr = [NSValue]()
            tr.append(NSValue.init(CMTime: thumbTime))
            
            let maxSize = CGSizeMake(frame.size.width, frame.size.height);
            generator.maximumSize = maxSize;
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                
                generator.generateCGImagesAsynchronouslyForTimes(tr, completionHandler: { (CMTime, im, actualTime, result, error) in
                    
                    if (result != AVAssetImageGeneratorResult.Succeeded)
                    {
                        print("couldn't generate thumbnail, error:%@", error);
                        //                        failure(error.localizedDescription);
                    }
                    let thumbImg:UIImage = UIImage(CGImage: im!)
                    file.thumbImage = thumbImg;
                    dispatch_async( dispatch_get_main_queue(), {
                        
                        VedioThumbnail.addVideoViewWithImage(file.thumbImage!, view:containerView);
                        
                    });
                })
            })
            
        }
        }
        else{
            addVideoViewWithImage(nil, view: containerView, imgURL: "")
        }
        return containerView
    }
        
    
    
    static func addVideoViewWithImage(image:UIImage?,view:UIView,imgURL:String? = nil) {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        imageView.contentMode = .ScaleAspectFill//.Center;
        imageView.clipsToBounds = true
        if(imgURL == nil)
        {imageView.image = image;}
        else
        {imageView.kf_setImageWithURL(NSURL(string: imgURL!), placeholderImage: UIImage(named:"placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            //kf_setImageWithURL(NSURL(string: imgURL!))}
        view.addSubview(imageView)
        
        let playImageSize:CGFloat = 80;
        let playImageX = (view.frame.size.width - playImageSize)/2;
        let playImageY = (view.frame.size.height - playImageSize)/2;
        let playImageView = UIImageView(frame: CGRect(x: playImageX, y: playImageY, width: playImageSize, height: playImageSize))
        playImageView.image = UIImage(named: "video-play")
    
        view.addSubview(playImageView)
    }
   

}
}
