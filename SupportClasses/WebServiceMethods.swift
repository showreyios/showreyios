//
//  WebServiceMethods.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 9/21/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation


class WSMethods {
    
    internal static let GetApplicationConfiguration  = "/GetApplicationConfiguration"
    internal static let GetProfileInformation  = "/GetProfileInformation"
    internal static let Login  = "/Login"
    internal static let EmailValidator  = "/EmailValidator"
    internal static let NickNameValidator  = "/NickNameValidator"
    internal static let MobileNumberValidator  = "/MobileNumberValidator"
    internal static let SendPinCode  = "/SendPinCode"
    internal static let LoginSocial  = "/LoginSocial"
    internal static let Registration  = "/Registration"
    internal static let GetSpecialOffers  = "/GetSpecialOffers"
    internal static let RegistrationSocial  = "/RegistrationSocial"
    internal static let ForgetPasswordStp1  = "/ForgetPasswordStp1"
    internal static let ForgetPasswordStp2  = "/ForgetPasswordStp2"
    internal static let ForgetPasswordStp3  = "/ForgetPasswordStp3"
    internal static let GetNewsFeed  = "/GetNewsFeed"
    internal static let GetUserTimeLine  = "/GetUserTimeLine"
    internal static let GetGroupPosts  = "/GetGroupPosts"
    internal static let GetMySavedPosts  = "/GetMySavedPosts"
    internal static let PostsSearch  = "/PostsSearch"
    internal static let GetNewsByNewsId  = "/GetNewsByNewsId"
    internal static let GetPostByPostId  = "/GetPostByPostId"
    internal static let GetGetSpecialOfferByGetSpecialOfferId  = "/GetGetSpecialOfferByGetSpecialOfferId"
    internal static let GetMyTips = "/GetMyTips"
    internal static let GetTipOfTheDay = "/GetTipOfTheDay"
    internal static let LikeDislikePost  = "/LikeDislikePost"
    internal static let LikeDislikeSpecialOffers  = "/LikeDislikeSpecialOffers"
    internal static let AddToMySavedPost  = "/AddToMySavedPost"//
    internal static let RemoveFromMySavedPosts  = "/RemoveFromMySavedPosts"
    internal static let HidePostorComment = "/HidePostorComment"
    internal static let DeletePostandCommment = "/DeletePostandCommment"
    internal static let DeleteSpecialOffersPostandCommment = "/DeleteSpecialOffersPostandCommment"
    internal static let PostShowrey = "/PostShowrey"
    internal static let PostSpecialOffers = "/PostSpecialOffers"
    internal static let EditPost = "/EditPost"
    internal static let EditSpecialOffersPost = "/EditSpecialOffersPost"
    internal static let GetMyGroups  = "/GetMyGroups"
    internal static let PostImages = "/PostImages"
    internal static let PostSpecialOffersImages = "/PostSpecialOffersImages"
    internal static let PostVideosThumbnails = "/PostVideosThumbnails"
    internal static let PostSpecialOffersVideosThumbnails = "/PostSpecialOffersVideosThumbnails"
    internal static let PostVideos = "/PostVideos"
    internal static let PostSpecialOffersVideos = "/PostSpecialOffersVideos"
    internal static let DeleteImage = "/DeleteImage"
    internal static let DeleteSpecialOffersImage = "/DeleteSpecialOffersImage"
    internal static let DeleteVideo = "/DeleteVideo"
    internal static let DeleteSpecialOffersVideo = "/DeleteSpecialOffersVideo"
    internal static let AutoCompleteTaggedUser = "/AutoCompleteTaggedUser"
    internal static let AutoCompleteFollowers = "/AutoCompleteFollowers"
    internal static let Report = "/Report"
    internal static let Logout = "/Logout"
    internal static let GetFollowees = "/GetFollowees"
    internal static let CreateGroup = "/CreateGroup"
    internal static let GetFollowers = "/GetFollowers"
    internal static let FollowUser = "/FollowUser"
    internal static let GetParticipantsByGroupId = "/GetParticipantsByGroupId"
    internal static let DeleteGroup = "/DeleteGroup"
    internal static let RemoveParticipantsFromGroup = "/RemoveParticipantsFromGroup"
    internal static let GetLikers = "/GetLikers"
    internal static let GetPostItemsLikers = "/GetPostItemsLikers"
    internal static let GetSpecialOffersLikers = "/GetSpecialOffersLikers"
    internal static let PeopleSearch = "/PeopleSearch"
    internal static let updateProfileOne = "/UpdateProfilePart1"
    internal static let updateProfileTwo = "/UpdateProfilePart2"
    internal static let updateProfileImage = "/UpdateProfilePicture"
    internal static let updateCoverPhoto = "/UpdateCoverPage"
    internal static let SetParticipantAsAdmin = "/SetParticipantAsAdmin"
    internal static let WriteAComment = "/WriteAComment"
    internal static let WriteSpecialOffersComment = "/WriteSpecialOffersComment"
    internal static let EditComment = "/EditComment"
    internal static let EditSpecialOffersComment = "/EditSpecialOffersComment"
    internal static let GetComments = "/GetComments"
    internal static let GetSpecialOffersComments = "/GetSpecialOffersComments"
    internal static let GetSpecialOffersWhispers = "/GetSpecialOffersWhispers"
    internal static let GetWhispers = "/GetWhispers"
    internal static let UpdateGroupPicture = "/UpdateGroupPicture"
    internal static let AddParticipantToGroup = "/AddParticipantToGroup"
    internal static let  EditGroup = "/EditGroup"
    internal static let  LikeDislikePostItems = "/LikeDislikePostItems"
    internal static let  UpdateGroupPrivacy = "/UpdateGroupPrivacy"
    internal static let  LikeDislikeComment = "/LikeDislikeComment"
    internal static let  LikeDislikeSpecialOffersComment = "/LikeDislikeSpecialOffersComment"
    internal static let  GetNotification = "/GetNotification"
    internal static let  GetNotificationNumber = "/GetNotificationNumber"
    internal static let  DeactivateAccount = "/DeactivateAccount"
    internal static let  GetPostStatistics = "/GetPostStatistics"
    internal static let  GetPostStatisticbyPostId = "/GetPostStatisticbyPostId"
    internal static let  GetHelpCenter = "/GetHelpCenter"//GetAdvertisement
    internal static let  GetAdvertisement = "/GetAdvertisement"
    internal static let  GetGroupByGroupId = "/GetGroupByGroupId"

    
    
    
    
    
    
    
    
    
}



