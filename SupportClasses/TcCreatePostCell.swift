//
//  TcCreatePostCell.swift
//  ShowRey
//
//  Created by M-Hashem on 11/3/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class TcCreatePostCell: UITableViewCell {

   // @IBOutlet weak var RegulerPost: UIButton!
   // @IBOutlet weak var FasionPost: UIButton!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var ShadeView: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    
    var parent:VcMasterNewsFeed!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        // Initialization code
        //        RegulerPost.imageView?.contentMode = .ScaleAspectFit
        //        RegulerPost.setImage(UIImage(named: "Main-Wall-(earth-icon)-selected"), forState: .Highlighted)
        //        FasionPost.imageView?.contentMode = .ScaleAspectFit
        //        FasionPost.setImage(UIImage(named: "Main-Wall-(hanger-icon)-Selected"), forState: .Highlighted)
        
        if ((User) != nil)
        {
            let str : String? = (User?.ProfilePicture)!
            let url = NSURL(string:str!)
            imgUser.kf_setImageWithURL(url, placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(viewTapped(_:)))
        container.userInteractionEnabled = true
        container.addGestureRecognizer(tapGestureRecognizer)

        
    }

    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        // radus
        container.layer.cornerRadius = 3;
        container.clipsToBounds = true;
        // shadow
        
        Styles.Shadow(self, target: container, Radus: 1,shadowview: ShadeView)
    }
    
    func viewTapped(view: AnyObject)
    {
        
        let createPost = parent.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost
        if parent.Mode == .GroupPosts
        {
            createPost.isItPrivate =  parent.ParentGroup.privacy
            createPost.PostToType = 2
            createPost.PostToId = parent.ParentGroup.groupId
        }
        else if parent.Mode == .TimeLine
        {
            createPost.PostToId = parent.profileCell.Profile.Id!
        }
        
        createPost.FromScreen = .CreateFashion
        createPost.Createblock = {()in
            self.parent.PagesCount = 1
            self.parent.GetNewsFeeds(self.parent.PagesCount, FromPagerOrRefresher: false)
        }
        parent.parent?.navigationController?.pushViewController(createPost, animated: true)
       
    }
    
    @IBAction func CreateRegular(sender: AnyObject)
    {
        let createPost = parent.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost
        if parent.Mode == .GroupPosts
        {
            createPost.isItPrivate =  parent.ParentGroup.privacy
            createPost.PostToType = 2
            createPost.PostToId = parent.ParentGroup.groupId
        }
        else if parent.Mode == .TimeLine
        {
            createPost.PostToId = parent.profileCell.Profile.Id!
        }
        
        createPost.FromScreen = .CreatePost
        createPost.Createblock = {()in
            self.parent.PagesCount = 1
            self.parent.GetNewsFeeds(self.parent.PagesCount, FromPagerOrRefresher: false)
        }
        
        parent.parent?.navigationController?.pushViewController(createPost, animated: true)
    }
    
    @IBAction func CreateFashion(sender: AnyObject)
    {
       
    }
}
