//
//  SLInstagramEngine.swift
//  instagram-swift
//
//  Created by Dimas Gabriel on 10/21/14.
//  Copyright (c) 2014 Swift Libs. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

let kSLInstagramEngineErrorDomain = "kSLInstagramEngineErrorDomain"
let kSLInstagramEngineErrorCodeAccessNotGranted = -1

private let kInstagramAuthURL : String = "https://instagram.com/oauth/authorize/"
private let kInstagramBaseAPIURL : String = "https://api.instagram.com/v1/"

typealias SLInstagramUserClosure = (SLInstagramUser,SLInstagramMeta) -> ()
typealias SLInstagramErrorClosure = (error: NSError) -> ()

class InstagramEngine : SLLoginViewControllerDelegate {
    
    private var clientId : String? = "2b87d4030f47425b9230fe7d538c9f49"//"074399c189b94f1184f9b88715d3b6c5"
    private var accessToken: String? //= "1998181730.074399c.944fef50f8cd40f19c2049f6712a8126"
    private var redirectURI : String? = "http://cms.mobileznation.com/"
    private var httpManager : Alamofire.Manager
    private var loginCallback : ((success : Bool, error : NSError?)->())!
    
    class var sharedEngine : InstagramEngine {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : InstagramEngine? = nil
        }
        
        dispatch_once(&Static.onceToken, {
            Static.instance = InstagramEngine()
        })
        
        return Static.instance!;
    }
    
    // Default init method
    init () {
//        if let confPlistPath = NSBundle.mainBundle().pathForResource("SLInstagramConf", ofType: "plist") {
//            let confDic = NSDictionary(contentsOfFile: confPlistPath)
//            
//            clientId = (confDic?.objectForKey("InstagramClientId") as! String)
//            redirectURI = (confDic?.objectForKey("InstagramRedirectURL") as! String)
//        }
        
        httpManager = Alamofire.Manager.sharedInstance
    }
    
    // MARK: - Login
    
    func loginFromViewController(viewController: UIViewController, completionClosure: (success : Bool, error : NSError?) -> ()){
        let urlString = "\(kInstagramAuthURL)?client_id=\(clientId!)&redirect_uri=\(redirectURI!)&response_type=token"
        if let url = NSURL(string: urlString) {
            
            let loginVC = InstgramAuth(nibName: "InstgramAuth", bundle: nil)
            loginVC.delegate = self
            viewController.presentViewController(loginVC, animated: true, completion: nil)
            
            loginVC.redirectURI = redirectURI!
            let req =  NSURLRequest(URL: url)
            for item in NSHTTPCookieStorage.sharedHTTPCookieStorage().cookies! {
                NSHTTPCookieStorage.sharedHTTPCookieStorage().deleteCookie(item)
            }
            loginCallback = completionClosure;
            loginVC.mainWebView.loadRequest(req)
        }
    }
    
    // MARK: - SLLoginViewController Delegate Methods
    func loginViewControllerDidCancel(vc: InstgramAuth) {
        
    }
    
    func loginViewControllerDidError(error: NSError) {
        loginCallback!(success: false,error: error)
    }
    
    func loginViewControllerDidLogin(accesToken: String) {
        self.accessToken = accesToken;//        print("access token "+accesToken)
        saveToken()
        loginCallback!(success: true, error: nil)
    }
    
    // userdefaults 
    func saveToken() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue(self.accessToken, forKey: "InstagramAccessToken")
        userDefaults.synchronize();
    }
    func LoadToken() -> Bool {
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        self.accessToken = userDefaults.stringForKey("InstagramAccessToken")
        if(self.accessToken == nil) {return false}
        return true;
    }
    
    // MARK: - Self Logged User Methods
    
    func selfWithSuccessClosure(success: SLInstagramUserClosure, error: SLInstagramErrorClosure) {
        
        var params = [String : String]()
        params["access_token"] = accessToken
        
        NetworkHelper.RequestHelper(self.fullURLForPath("users/self"), service: ""
            , hTTPMethod: .get, parameters: params, httpBody: nil,responseType: .DictionaryJson
        , callbackString: nil) { (JSON, NSError) -> Void in
            if NSError != nil {
                error(error: NSError!);
            }else{
                let meta = SLInstagramMeta(json: JSON["meta"])
                
                let profile = SLInstagramUser(json: JSON["data"])
                // let pagination = JSON["pagination"].dictionary!
                success(profile, meta)
            }
        }
        
    }
    
    
    // MARK: - HTTP Calls
    
    private func getPath(path: String, parameters: [String :  String]?, responseModel : AnyClass,
                         authNeeded: Bool, success: (objects : JSON/*, pagination : [String : JSON]*/) -> (), error: SLInstagramErrorClosure) {
        
        assert(authNeeded == true && accessToken != nil, "SLInstagram Error: You need an Instagram access token to access the '\(path)' path")
        
        var params = [String : String]()
        
        if parameters != nil {
            params = parameters!
        }
        
        if authNeeded == true {
            params["access_token"] = accessToken
        }
        
        NetworkHelper.RequestHelper(self.fullURLForPath(path), service: ""
            , hTTPMethod: .get, parameters: params, httpBody: nil,responseType: .DictionaryJson
            , callbackString: nil) { (JSON, NSError) -> Void in
                if NSError != nil {
                    error(error: NSError!);
                }else{
                    let feed = JSON["data"]
                   // let pagination = JSON["pagination"].dictionary!
                    success(objects: feed/*, pagination: pagination*/)
                }
        }
        
//        httpManager.request(.GET, self.fullURLForPath(path), parameters: params).res { (request, response, json, err) -> Void in
//            if err != nil {
//                error(error: err!);
//            }else{
//                let feed = json["data"]
//                let pagination = json["pagination"].dictionary!
//                success(objects: feed, pagination: pagination)
//            }
//        }
    }
    
    private func fullURLForPath(path: String) -> String {
        return "\(kInstagramBaseAPIURL)\(path)"
    }
    
}
