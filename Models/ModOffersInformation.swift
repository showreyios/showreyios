////
////  ModOffersInformation.swift
////  ShowRey
////
////  Created by Radwa Khaled on 1nil/9/16.
////  Copyright © 2nil16 Appsinnovate. All rights reserved.
////
//
//
//import Foundation
//import EVReflection
//
//class ModAttachedFiles : EVObject {
//      var Id : NSNumber? = nil
//      var LevelId : NSNumber? = nil
//      var ObjId : NSNumber? = nil
//      var Thumb : String? = nil
//      var URL : String? = nil
//      var UserId : NSNumber? = nil
//}
//
//class ModCItems : EVObject {
//      var Disliked : Bool = false
//      var Id : NSNumber? = nil
//      var It_Id : NSNumber? = nil
//      var It_Name : String? = nil
//      var Liked : Bool = false
//      var NoOfDislikes : NSNumber? = nil
//      var NoOfLikes : NSNumber? = nil
//}
//
//class ModProfilePros : EVObject {
//      var AgeGroup : NSNumber? = nil
//      var BodyShape : NSNumber? = nil
//      var Gender : String? = nil
//      var Height : NSNumber? = nil
//      var Size : String? = nil
//      var Weight : NSNumber? = nil
//}
//
//class ModUserProfile : EVObject {
//      var AccountType : NSNumber? = nil
//      var FollowStatus : NSNumber? = nil
//      var FullName : String? = nil
//      var Id : NSNumber? = nil
//      var ProfilePicture : String? = nil
//      var UserName : String? = nil
//}
//
//class ModSharedPost : EVObject {
//      let AttachedFiles : [ModAttachedFiles]? = nil
//      let CItems : [ModCItems]? = nil
//      var Disliked : Bool = false
//      var Id : NSNumber? = nil
//      var IsEdited : Bool = false
//      var Liked : Bool = false
//      var NoOfComments : NSNumber? = nil
//      var NoOfDislikes : NSNumber? = nil
//      var NoOfLikes : NSNumber? = nil
//      var PostCType : NSNumber? = nil
//      var PostToId : NSNumber? = nil
//      var PostToType : NSNumber? = nil
//      var PostType : NSNumber? = nil
//      var ProfilePros : ModProfilePros? = nil
//      var SharedPost : ModSharedPost? = nil
//      var SharedPostId : NSNumber? = nil
//      let TaggedUsers : [ModUserProfile]? = nil
//      var Text : String? = nil
//      var Time : String? = nil
//      var ToUserProfile : ModUserProfile? = nil
//      var UserId : NSNumber? = nil
//      var UserProfile : ModUserProfile? = nil
//      var isAnonymous : Bool = false
//}
//
//class ModFeedobj : EVObject {
//      let AttachedFiles : [ModAttachedFiles]? = nil
//      var Disliked : Bool = false
//      var Id : NSNumber? = nil
//      var IsAdmin : Bool = false
//      var IsEdited : Bool = false
//      var Liked : Bool = false
//      var NoOfComments : NSNumber? = nil
//      var NoOfDislikes : NSNumber? = nil
//      var NoOfLikes : NSNumber? = nil
//      var OriginalPrice : NSNumber? = nil
//      var PostType : NSNumber? = nil
//      var Price : NSNumber? = nil
//      var SharedPost : ModSharedPost? = nil
//      var SharedPostId : NSNumber? = nil
//      var Text : String? = nil
//      var Time : String? = nil
//      var Title : String? = nil
//      var ToUserProfile : ModUserProfile? = nil
//      var UserId : NSNumber? = nil
//      var UserProfile : ModUserProfile? = nil
//      var ValidTo : String? = nil
//      var isAnonymous : Bool = false
//}
//
//class ModOffersInformationResponse : ModResponse {
//    
//      let Feedobj : [ModFeedobj]? = nil
//      var ListCount : NSNumber? = nil
//      var PageCount : NSNumber? = nil
//}
// 
