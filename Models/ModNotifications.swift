//
//  ModNotifications.swift
//  ShowRey
//
//  Created by M-Hashem on 12/17/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import EVReflection

enum NotificationType:Int
{
    case From_Admin = 0
    case Following_you = 1
    case posted_on_your_Wall = 2
    case tagged_you_in_apost = 3
    case liked_your_post = 4
    case commented_on_your_post = 5
    case liked_your_comment = 6
    case replied_to_your_comment = 7
    case mentioned_you_in_acomment = 8
    case added_you_in_aGroup = 9
    case fashion_Posted_on_your_Wall = 10
    case tagged_you_in_afashion_Post = 11
    case like_your_Fashon_Post = 12
    case commented_on_your_Fashon_Post = 13
    case like_your_comment_Fashon_Post = 14
    case replied_to_your_fashion_Post = 15
    case mentioned_you_in_aFashion_Post = 16
    case new_special_offer_posted = 17
    case like_your_special_offer = 18
    case commented_on_your_special_offer = 19
    case like_your_special_offer_comment = 20
    case replied_to_your_special = 21
    case mentioned_you_in_aspecial_offer_comments = 22
   
}

class ModNotification : EVObject
{
    var title = "Showery"
    var message = "Push Notificaiton"
     var Date : String? = nil
     var Id : String? = nil
     var ObjId : NSNumber? = nil
     var Text : String? = nil
     var Type : NSNumber? = nil
     var UserProfile : ModNotiUserProfile? = nil
     var isread : Bool = false
}
class ModNotiUserProfile : EVObject {
     var AccountType : NSNumber? = nil
     var FollowStatus : NSNumber? = nil
     var FullName : String? = nil
     var Id : NSNumber? = nil
     var ProfilePicture : String? = nil
     var UserName : String? = nil
}
class ModNotificationsResponse: ModResponse {
    var ListCount : NSNumber? = nil
    var Notifications : [ModNotification]?
    var PageCount : NSNumber? = nil
}

class ModNotiNum: ModResponse {
    var NotiNo:NSNumber? = nil
    
}
