//
//  ModUser.swift
//  ShowRey
//
//  Created by M-Hashem on 9/24/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import EVReflection
enum AccountType:Int
    {
        case Standard=1
        case Verified=2
        case Moderator=3
        
    }
class ModProfileInformation : EVObject
{
    
     var AccountType : NSNumber? = nil
     var BodyShape : NSNumber? = nil
     var City : String? = nil
     var Country : String? = nil
     var DOB : String? = nil
     var AgeGroup : NSNumber? = nil
     var Email : String? = nil
     var FollowStatus : NSNumber? = nil
     var Followers : NSNumber? = nil
     var Following : NSNumber? = nil
     var FullName : String? = nil
     var Gender : String? = nil
     var Height : NSNumber? = nil
     var Id : NSNumber? = nil
     var MobileNumber : String? = nil
     var Platform : String? = nil
     var ProfilePicture : String? = nil
     var Rating : NSNumber? = nil
     var RegId : String? = nil
     var Size : String? = nil
     var UserName : String? = nil
     var Verified : Bool = false
     var Weight : NSNumber? = nil
    var Ranking : NSNumber? = nil
    var CoverPage : String? = nil
    var Invited : NSNumber? = nil
    var L2Invited : NSNumber? = nil
    var Points : NSNumber? = nil
    var QFriends : NSNumber? = nil
    var Questioned : NSNumber? = nil
    var RFriends : NSNumber? = nil
    var Replied : NSNumber? = nil
    var InviteCode : String? = nil


}
class ModProfileInformationResponse : ModResponse
{
    var Profileobj : ModProfileInformation? = nil
}
