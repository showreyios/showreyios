//
//  AppDelegate.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 9/5/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import FacebookLogin
import FacebookCore
import FBSDKShareKit
import Hue
import Firebase
import FirebaseCrash
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import FBSDKCoreKit
import EAIntroView
import Fabric
import Crashlytics


var User:ModProfileInformation?
var AppConfiguration:ModAppConfiguration?
var leftSideMenu:VcSlideMenu!
var HomeScreen:VcHome!
var redirectionId:String!
var redirectionScreen:String!
var PushNotificationsRedirData: [NSObject : AnyObject]!
var openedScreen:UIViewController!


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SSASideMenuDelegate,FIRMessagingDelegate,UNUserNotificationCenterDelegate, EAIntroDelegate{
    
    func applicationReceivedRemoteMessage(remoteMessage: FIRMessagingRemoteMessage){
        
        
    }
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var comFromPushNotifcationFlag = false
    var splash : UIViewController?
    
    internal static var storyboard: UIStoryboard!
    internal static var navControler : UINavigationController!
    //internal static var domainurl =  "http://cms.mobileznation.com/ShowReYApp/Showrey.svc"//http:cms.mobileznation.com/ShowReYAppNew/Showrey.svc
    internal static var domainurl =  "http://cms.mobileznation.com/ShowReYAppV2/Showrey.svc"
    
    static var ResorceDomain = ""//"http://ftp.mccarabia.com"
    let customURLScheme = "fb155466188278867"
    
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        
        if  ((launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary) != nil) {
            
            comFromPushNotifcationFlag = true
            
            
        }
        else
        {
            comFromPushNotifcationFlag = false
        }
        
        //        if remoteNotif != nil {
        //            let itemName = remoteNotif?["aps"] as! String
        //            print("Custom: \(itemName)")
        //        }
        //        else {
        //            print("//////////////////////////")
        //        }
        //
        
        //  [[UILabel appearance] setFont:[UIFont fontWithName:@"YourFontName" size:17.0]];
        
        //UILabel.appearance().font = UIFont(name: "yourFont", size: 12)
        Fabric.with([Crashlytics.self])
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions);
        OauthInjector.InitBaseDictionary();
        // Override point for customization after application launch.
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        
        UINavigationBar.appearance().barTintColor = UIColor(hex: "#b379e7")  //bar color
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()  //back button color
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()] //bar title color
        self.window!.tintColor = UIColor(hex: "#b379e7") //tint color for all application
        
        //        VcMasterNewsFeed.loadAds()
        AppDelegate.storyboard = self.grabStoryboard()
        splash = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("splash")
        window!.rootViewController = splash;
        
        //   AppDelegate.RoutToScreen("VcStackBar")//VcTest
        
        let launchedBefore = NSUserDefaults.standardUserDefaults().boolForKey("launchedBefore")
        if launchedBefore  {
            print("Not first launch.")
            VcLogin.autologin(splash!)
            
        }
        else {
            //  print("First launch, setting NSUserDefault.")
            
            application.applicationIconBadgeNumber = 0
            
            let v = window?.rootViewController
            
            let page1 = EAIntroPage()
            page1.bgImage = UIImage(named: "page1")
            
            let page2 = EAIntroPage()
            page2.bgImage = UIImage(named: "page2")
            
            let page3 = EAIntroPage()
            page3.bgImage = UIImage(named: "page3")
            
            let page4 = EAIntroPage()
            page4.bgImage = UIImage(named: "page4")
            
            let page5 = EAIntroPage()
            page5.bgImage = UIImage(named: "page5")
            
            
            let intro = EAIntroView()
            intro.frame = (v?.view.bounds)!//(window?.bounds)!//self.view.bounds
            intro.pages = [page1,page2,page3,page4,page5]
            
            intro.pageControlY = (v?.view.bounds.size.height)! - ((v?.view.bounds.size.height)! - 65)
            
            intro.pageControl.currentPageIndicatorTintColor = UIColor(hex: "#b379e7")
            intro.pageControl.pageIndicatorTintColor = UIColor(hex: "#7a7a7b")
            // intro.skipButton.setTitle("SKIP", forState: .Normal)
            
            let btn = UIButton() //type: .roundedRect
            btn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(230), height: CGFloat(40))
            btn.setTitle("SKIP", forState: .Normal)
            btn.setTitleColor(UIColor.whiteColor() , forState: .Normal)
            //  btn.backgroundColor = UIColor(hex: "#b379e7")
            // btn.layer.borderWidth = 2.0
            btn.layer.cornerRadius = 10
            //  btn.layer.borderColor = UIColor(hex: "#b379e7").CGColor //UIColor.whiteColor().CGColor
            intro.skipButton = btn
            intro.skipButtonY = 70.0
            intro.skipButtonAlignment = EAViewAlignment.Right //Center //EAViewAlignmentCenter
            
            intro.delegate = self
            intro.tapToNext = true
            
            intro.showInView(v?.view, animateDuration: 0.3)
            
            
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "launchedBefore")
            
        }
        
        
        
        
        FIROptions.defaultOptions().deepLinkURLScheme = self.customURLScheme
        FIRApp.configure()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.tokenRefreshNotification), name: "firInstanceIDTokenRefresh" , object: nil)
        
        
        ///////////PushNotification/////////////////////////////
        
        if #available(iOS 10.0, *) {
            //            let authOptions: UNAuthorizationOptions = [.Alert , .Badge, .Sound]
            //            UNUserNotificationCenter.currentNotificationCenter().requestAuthorizationWithOptions(authOptions, completionHandler: {_, _ in })
            //
            //
            //            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.currentNotificationCenter().delegate = self
            //            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            //
        } else {
            //            let settings: UIUserNotificationSettings =
            //                UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            //            application.registerUserNotificationSettings(settings)
        }
        //
        //     application.registerForRemoteNotifications()
        
        
        //  fatalError()
        return true
    }
    
    func introDidFinish(introView: EAIntroView!, wasSkipped: Bool) {
        
        VcLogin.autologin(splash!)
        
    }
    
    //////tokenRefresh///////////
    func tokenRefreshNotification(notification: NSNotification)
    {
        if let refreshedToken = FIRInstanceID.instanceID().token()
        {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData)
    {
        //        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.Sandbox)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.Prod)
        
    }
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        let av = application.applicationState
        print("  🙌 \(application.applicationState)")
        if let messageID = userInfo[gcmMessageIDKey]
        {
            print("Message ID: \(messageID)")
        }
        
        PushNotificationsRedirData = userInfo
        
        if AppDelegate.navControler == nil{
            
            VcPostingOptions.openLink = true
            // AppDelegate.HandlePushNotifications()
        }
        else
        {
            /*switch UIApplication.sharedApplication().applicationState {
             case .Active:
             AppDelegate.HandlePushNotifications()
             break
             default:
             break
             }*/
            AppDelegate.HandlePushNotifications(comFromPushNotifcationFlag)
            
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.NewData)
    }
    static func HandlePushNotifications(comFromPushNotifcationFlag:Bool)
        
    {
        //app was crashing trying to map elements with wrong call , i logged to consol and found diffrent keys
        
        //previous version
        //let type = PushNotificationsRedirData["Type"]
        // let ActionObjId = PushNotificationsRedirData["ActionObjId"]
        // let ActionUserId = PushNotificationsRedirData["ActionUserId"]
        
        //new verison
        let type = PushNotificationsRedirData["gcm.notification.Type"]
        let ActionObjId = PushNotificationsRedirData["gcm.notification.ActionObjId"]
        let ActionUserId = PushNotificationsRedirData["gcm.notification.ActionUserId"]
        let aps = PushNotificationsRedirData["aps"]
        let alert = aps?.valueForKey("alert")
        
        let noti = ModNotification()
        print("ttyp=\(PushNotificationsRedirData) ttyup = \(type)")
        noti.Type = (type as? NSString)!.doubleValue
        noti.ObjId = (ActionObjId as? NSString)!.doubleValue//ActionObjId as! NSNumber?
        noti.title =  alert?.valueForKey("title") as! String
        noti.message =  alert?.valueForKey("body") as! String
        /*
         switch UIApplication.sharedApplication().applicationState {
         case .Active:
         let alert = UIAlertController(title: "Push notification", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
         let ok = UIAlertAction(title: "Show", style: UIAlertActionStyle.Default , handler:
         {(action: UIAlertAction) -> Void in
         VcNotificationsLocal.HaandleNotifications(noti, parent: openedScreen,userID: ActionUserId as! String?)
         })
         let cancel = UIAlertAction(title: "Cancel", style: .Default, handler: {(action: UIAlertAction) -> Void in
         alert.dismissViewControllerAnimated(true, completion: { _ in })
         })
         alert.addAction(ok)
         alert.addAction(cancel)
         if AppDelegate.navControler != nil
         {
         print("root= \(AppDelegate.navControler.topViewController)")
         AppDelegate.navControler.topViewController!.presentViewController(alert, animated: true, completion: { _ in })
         }//
         default:
         VcNotificationsLocal.HaandleNotifications(noti, parent: openedScreen,userID: ActionUserId as! String?)
         }
         */
        
        VcNotificationsLocal.HaandleNotifications(noti,comFromPushNotifcationFlag: comFromPushNotifcationFlag, parent: openedScreen,userID: ActionUserId as! String?)
        
        PushNotificationsRedirData = nil
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return;
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connectWithCompletion({(error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
                
                
            }
        })
        
        
    }
    
    //    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
    //        return application(app, openURL: url, sourceApplication: nil, annotation: [:])
    //    }
    
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        
        let dynamicLink = FIRDynamicLinks.dynamicLinks()?.dynamicLinkFromCustomSchemeURL(url)
        if let dynamicLink = dynamicLink
        {
            
            dump(dynamicLink.url)
            let deepLink = NSURLComponents(URL: dynamicLink.url!, resolvingAgainstBaseURL: false)!
            let deepLinkQueryString = deepLink.queryItems
            
            if let offerID = deepLinkQueryString!.filter({$0.name == "offerID"}).first?.value {
                
                print("hellooooo\(offerID)")
                // Set up the transition
                
                if AppDelegate.navControler == nil
                {
                    redirectionId = offerID
                    redirectionScreen = "VcOffersDetails"
                    
                    //                    let splash = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("splash")
                    //                    window!.rootViewController = splash;
                    //                    VcLogin.autologin(splash)
                    
                }
                else
                {
                    let offersDetails = VcOffersDetails(nibName: "VcOffersDetails", bundle: nil)
                    //                offersDetails.FeedData.Id = offerID
                    offersDetails.loadFromID(offerID)
                    AppDelegate.navControler.pushViewController(offersDetails, animated: true)
                }
                
                //offersDetails.setData(self, data: Offers[indexPath.row])
                //UINavigationController.pushViewController(offersDetails)
                
            }
            
            // [END_EXCLUDE]
            return true
        }
        
        return false
        
    }
    
    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool {
        guard let dynamicLinks = FIRDynamicLinks.dynamicLinks() else {
            return false
        }
        let handled = dynamicLinks.handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            // [START_EXCLUDE]
            //            let message = self.generateDynamicLinkMessage(dynamiclink!)
            //            self.showDeepLinkAlertView(withMessage: message)
            // [END_EXCLUDE]
            
            dump(dynamiclink?.url)
            let deepLink = NSURLComponents(URL: (dynamiclink?.url!)!, resolvingAgainstBaseURL: false)!
            let deepLinkQueryString = deepLink.queryItems
            
            if let offerID = deepLinkQueryString!.filter({$0.name == "Offer"}).first?.value {
                
                
                let offerId = (offerID.componentsSeparatedByString("-"))[1]
                
                print("hellooooo\(offerId)")
                if AppDelegate.navControler == nil{
                    
                    redirectionId = offerId
                    redirectionScreen = "VcOffersDetails"
                    
                    
                }
                else
                {
                    let offersDetails = VcOffersDetails(nibName: "VcOffersDetails", bundle: nil)
                    //                offersDetails.FeedData.Id = offerID
                    offersDetails.loadFromID(offerId)
                    //                    AppDelegate.navControler.pushViewController(offersDetails, animated: true)
                    openedScreen.navigationController!.pushViewController(offersDetails, animated: true)
                }
                
            }else if let invite = deepLinkQueryString!.filter({$0.name == "Invite"}).first?.value {
                
                print("inviteeee= \(invite)")
                
                // if invite != "0" {
                var paramArr = invite.componentsSeparatedByString("_")
                var postType: String = paramArr [1]
                var postId: String = paramArr [0]
                postType = (postType.componentsSeparatedByString("-"))[1]
                postId = (postId.componentsSeparatedByString("-"))[1]
                
                if postId != "0" {
                    if AppDelegate.navControler == nil{
                        
                        redirectionId = postId
                        VcPostingOptions.openLink = true
                        
                        if postType == "0"{
                            redirectionScreen = "inviteWithRegular"
                        }else if postType == "1"{
                            redirectionScreen = "inviteWithFashion"
                        }
                        
                    }
                    else
                    {
                        
                        if postType == "0"{
                            VcNotificationsLocal.ToRegularPost(Int(postId)!, parent: openedScreen)
                        }else if postType == "1"{
                            VcNotificationsLocal.ToFasionPost(Int(postId)!, parent: openedScreen)
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        // [START_EXCLUDE silent]
        if !handled {
            // Show the deep link URL from userActivity.
            //            let message = "continueUserActivity webPageURL:\n\(userActivity.webpageURL)"
            //
            //            showDeepLinkAlertView(withMessage: message)
            print("errorrrrrrrrrrrrrrrr")
        }
        // [END_EXCLUDE]
        
        return handled
        
        
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
        
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp();
        connectToFcm()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.Appsinnovate.ShowRey" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("ShowRey", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    //-------------------------------------------------------------------------------------------
    
    
    func grabStoryboard() -> UIStoryboard {
        
        // determine screen size
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        /*
         let height = UIScreen.mainScreen().bounds.size.height
         switch (height)
         {
         // iPhone 4s
         case 480:
         storyboard = UIStoryboard(name: "Main", bundle: nil)
         break;
         
         default:
         // it's an iPad
         storyboard = UIStoryboard(name: "Main", bundle: nil)
         break;
         }
         */
        return storyboard;
    }
    
    internal static func RoutToScreen(screen: String) -> UIViewController
    {
        let VcHomeViewController = storyboard!.instantiateViewControllerWithIdentifier(screen)
        
        if (navControler == nil )
        {
            navControler = storyboard!.instantiateInitialViewController()as! UINavigationController
        }
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        
        if (screen == "VcHome")
        {
            
            // (VcHomeViewController as! VcHome).invitAlertMessage = "55"
            
            
            //MARK : Setup SSASideMenu
            leftSideMenu = VcSlideMenu()
            //            UINavigationController(rootViewController: VcHomeViewController)
            //  navControler.viewControllers = [VcHomeViewController]
            let sideMenu = SSASideMenu(contentViewController:  UINavigationController(rootViewController: VcHomeViewController), leftMenuViewController: leftSideMenu)
            sideMenu.backgroundImage = UIImage(named: "Slide-Menu-BG")
            sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
            sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
            sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.blackColor(), opacity: 0.6, radius: 6.0))
            
            sideMenu.delegate = appDelegate
            appDelegate.window?.rootViewController = sideMenu
            
            appDelegate.window?.makeKeyAndVisible()
            
        }
        else
        {
            
            navControler.viewControllers = [VcHomeViewController]
            appDelegate.window!.rootViewController = navControler
            
        }
        
        return VcHomeViewController
    }
    
    static func attemptOpenURL(urlstring: String) {
        print(" 🤓str \(urlstring) with lowers = \(urlstring.lowercaseString) url of me = \( NSURL(string: urlstring.lowercaseString))")
        let unrappedURL = NSURL(string: urlstring.lowercaseString)
        if let rappedURL = unrappedURL
        {
            let url = NSURL(string: urlstring.lowercaseString)!
            let safariCompatible = (url.scheme == "http") || (url.scheme == "https")
            if safariCompatible && UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
            }
            else {
                let strurl = "http://\(url)"
                UIApplication.sharedApplication().openURL(NSURL(string: strurl)!)
            }
        }
        else
        {
            return
        }
    }
    
    
}

